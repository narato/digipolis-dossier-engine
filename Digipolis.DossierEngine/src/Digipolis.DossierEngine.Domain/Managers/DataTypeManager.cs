﻿using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Logging;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Paging;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.Extensions.Options;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class DataTypeManager : IDataTypeManager
    {
        private readonly IDataTypeDataProvider _dataTypeDataProvider;
        private readonly IDataTypeValidator _dataTypeValidator;
        private readonly IEventPublisher _eventHandler;
        private readonly DossierConfiguration _dossierConfiguration;
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;
        private readonly IChangeCalculator<DataType> _changeCalculator;

        public DataTypeManager(IDataTypeDataProvider dataTypeDataProvider, IDataTypeValidator dataTypeValidator, IEventPublisher eventHandler, 
            IOptions<DossierConfiguration> adminConfiguration, ICurrentTenantProvider<Config> currentTenantProvider, IChangeCalculator<DataType> changeCalculator)
        {
            _dataTypeDataProvider = dataTypeDataProvider;
            _dataTypeValidator = dataTypeValidator;
            _eventHandler = eventHandler;
            _dossierConfiguration = adminConfiguration.Value;
            _currentTenantProvider = currentTenantProvider;
            _changeCalculator = changeCalculator;
        }

        public async Task<DataType> GetDataTypeByIdAsync(Guid id)
        {
            var dtTree = await GetDataTypeTreeByIdAsync(id).LogOnException();
            var dt = await _dataTypeDataProvider.GetDataTypeByIdAsync(id);

            dt.Template = AttachTemplate(dtTree);

            return dt;
        }

        public async Task<DataType> GetDataTypeTreeByIdAsync(Guid id)
        {
            var dataType = await _dataTypeDataProvider.GetDataTypeByIdAsync(id).LogOnException();
            return await HydrateDataTypeTreeAsync(dataType);
        }

        public async Task<DataType> GetDataTypeByNameAsync(string name)
        {
            var dt = _dataTypeDataProvider.GetDataTypeByNameAsync(name);

            var dtTree = await GetDataTypeTreeByNameAsync(name);

            var dtResult = dt.Result;
            dtResult.Template = AttachTemplate(dtTree);

            return dtResult;
        }

        public async Task<DataType> GetDataTypeTreeByNameAsync(string name)
        {
            var dataType = await _dataTypeDataProvider.GetDataTypeByNameAsync(name);
            return await HydrateDataTypeTreeAsync(dataType);
        }

        private async Task<DataType> HydrateDataTypeTreeAsync(DataType dataType)
        {
            var guidDictionary = new Dictionary<Guid, List<LinkDataTypeAttributeType>>();

            var dataTypeAttributes = dataType.Attributes.Where(att => att.Type == LinkDataTypeAttributeType.TypeName);

            foreach (var attribute in dataTypeAttributes)
            {
                var dataTypeId = ((LinkDataTypeAttributeType)attribute).DataTypeId;
                if (!guidDictionary.ContainsKey(dataTypeId))
                {
                    guidDictionary.Add(dataTypeId,
                        new List<LinkDataTypeAttributeType> {(LinkDataTypeAttributeType) attribute});
                }
                else
                {
                    guidDictionary[dataTypeId].Add((LinkDataTypeAttributeType)attribute);
                }                   
            }

            if (guidDictionary.Count == 0)
            {
                return dataType;
            }

            var linkedDataTypes = await _dataTypeDataProvider.GetDataTypeByIdListAsync(guidDictionary.Keys.ToList()).LogOnException();

            linkedDataTypes.ForEach(dt => guidDictionary[dt.Id].ForEach(at => at.DataType = dt));

            // TODO : this is bad! does a query per dataType. rewrite
            foreach (var linkedDataType in linkedDataTypes)
            {
                // hydrate this as well :-)
                await HydrateDataTypeTreeAsync(linkedDataType).LogOnException();
            }

            dataType.Template = AttachTemplate(dataType);
            return dataType;
        }


        // TODO: this should be in a TemplateGenerator Service. So it can be tested :-)
        private Dictionary<string, object> AttachTemplate(DataType dt)
        {
            var dict = new Dictionary<string, object>();

            foreach (IAttributeType att in dt.Attributes)
            {
                // either move to a strategy like pattern so you can do dict.Add(att.Name, templateHandler.Get(att.GetType()).Handle(att))
                // or move to a switch, and cause a crash in the default. This way we make sure we don't forget about the template when we add new attributeTypes
                if (att is StringAttributeType)
                {
                    dict.Add(att.Name, "MyString");
                }
                else if (att is BooleanAttributeType)
                {
                    dict.Add(att.Name, true);
                }
                else if (att is IntAttributeType)
                {
                    dict.Add(att.Name, 10);
                }
                else if (att is DoubleAttributeType)
                {
                    dict.Add(att.Name, 10.5);
                }
                else if (att is LinkAttributeType)
                {
                    dict.Add(att.Name, "http://www.mylink.be");
                }
                else if (att is DateTimeAttributeType)
                {
                    dict.Add(att.Name, "2014-04-20T10:30:50Z");
                }
                else if (att is LinkDataTypeAttributeType)
                {
                    var linkData = (LinkDataTypeAttributeType)att;
                    dict.Add(linkData.Name, AttachTemplate(linkData.DataType));
                }
            }

            return dict;
        }

        public async Task<PagedCollectionResponse<IEnumerable<DataType>>> GetAllDataTypesAsync(bool syncedOnly, int page, int pageSize)
        {
            var dataTypes = await _dataTypeDataProvider.GetAllDataTypesAsync(syncedOnly).LogOnException();

            var pagedList = dataTypes.OrderBy(dt => dt.Id).ToPagedList(page, pageSize);

            return pagedList.ToPagedCollectionResponse();
        }

        public async Task<DataType> InsertDataTypeAsync(DataType dataType)
        {
            if (await _dataTypeDataProvider.VerifyIfDataTypeExistsAsync(dataType.Name))
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("DataType with name '{0}' already exists", dataType.Name) });

            var feedbackItems = await _dataTypeValidator.ValidateDataTypeAttributesAsync(dataType).LogOnException();
            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);

            dataType.Id = Guid.NewGuid();
            var returnValue = await _dataTypeDataProvider.InsertDataTypeAsync(dataType).LogOnException();

            var topic = Constants.DataTypeCreatedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = returnValue.Id, Name = returnValue.Name, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));
            return returnValue;
        }

        public async Task<DataType> UpdateDataTypeAsync(DataType dataType)
        {
            var originalDataType = await _dataTypeDataProvider.GetDataTypeByIdAsync(dataType.Id).LogOnException();

            var changes = _changeCalculator.CalculateChangeSet(originalDataType, dataType);

            //Validate only the remaining attributes, execute after Exists check
            var feedbackItems = await _dataTypeValidator.ValidateDataTypeAttributesAsync(dataType).LogOnException();

            //Check for differences in Attributes that will be ignored and provide feedback
            feedbackItems.AddRange(_dataTypeValidator.DifferentiateDataTypeAttributes(originalDataType, dataType));

            //Check for DefaultValues when IsRequired is true
            feedbackItems.AddRange(_dataTypeValidator.ValidateDataTypeDefaultValue(dataType));

            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);
            
            var returnValue = await _dataTypeDataProvider.UpdateDataTypeAsync(dataType, changes).LogOnException();
            await _dataTypeDataProvider.SetDefaultValueAsync(dataType, changes);

            var topic = Constants.DataTypeUpdatedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = returnValue.Id, Name = returnValue.Name, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));
            return returnValue;
        }

        public async Task<DataType> DeleteDataTypeByIdAsync(Guid id)
        {
            var dataTypeToDelete = await _dataTypeDataProvider.GetDataTypeByIdAsync(id).LogOnException();

            if (await _dataTypeDataProvider.VerifyIfDataTypeIsNestedAsync(id))
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "DataType cannot be deleted because it is currently nested" });

            if (! await _dataTypeDataProvider.DeleteDataTypeByIdAsync(id))
            {
                throw new Exception("Delete was not successful");
            }

            var topic = Constants.DataTypeDeletedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = id, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));

            return dataTypeToDelete;
        }
    }
}