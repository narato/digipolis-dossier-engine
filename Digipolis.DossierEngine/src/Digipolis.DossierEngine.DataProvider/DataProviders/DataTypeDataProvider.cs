﻿using System;
using System.Collections.Generic;
using System.Linq;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Neo4jClient;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.Common.Status.Models;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.Model.Interfaces;
using Neo4jClient.Cypher;
using Narato.Common.Exceptions;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class DataTypeDataProvider : IDataTypeDataProvider
    {
        private readonly IPooledGraphClientProvider _pooledGraphClientProvider;
        private readonly IDataTypeMapper _dataTypeMapper;

        public DataTypeDataProvider(IPooledGraphClientProvider pooledGraphClientProvider, IDataTypeMapper dataTypeMapper)
        {
            _pooledGraphClientProvider = pooledGraphClientProvider;
            _dataTypeMapper = dataTypeMapper;
        }

        public async  Task<DbConnectionCheckResult> CheckDBConnectionAsync()
        {
            try
            {
                using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
                {
                    var client = pooledClient.Value;
                    if (!client.IsConnected)
                        client.Connect();

                    client.Connect();
                    var query = client
                            .Cypher
                            .Match("(n:NonExistingLabel)")
                            .Return((n) => n.Count());
                    var output = await query.ResultsAsync;
                }
            }
            catch (Exception ex)
            {
                return new DbConnectionCheckResult(false, ex);
            }
            return new DbConnectionCheckResult(true);
        }

        public async Task<DataType> InsertDataTypeAsync(DataType dataType)
        {
            //neo datatype due to attributes that may not be persisted in Neo4j on datatype node
            var neoDataType = new NeoDataType { Id = dataType.Id, Name = dataType.Name, Synced = dataType.Synced};

            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client
                .Cypher
                .Create("(dt:DataType {dataType})")
                .WithParam("dataType", neoDataType);

                if (dataType.Attributes?.Any() ?? false)
                {
                    for (var i = 0; i < dataType.Attributes.Count; i++)
                    {
                        query = query
                            .Create("(:AttributeType {attributeType" + i + "})<-[:HAS_ATTRIBUTE]-(dt)")
                            .WithParam("attributeType" + i, dataType.Attributes[i]);
                    }
                }

                query = query
                    .With("dt")
                    .Match("(at:AttributeType)<-[:HAS_ATTRIBUTE]-(dt)")
                    .Where("HAS (at.dataTypeId)")
                    .Match("(refdt:DataType{id:at.dataTypeId})")
                    .Create("(at)-[:OF_NESTED_TYPE]->(refdt)");

                query.ExecuteWithoutResults();

                return dataType;
            }
        }      

        public async Task<DataType> UpdateDataTypeAsync(DataType dataType, IEnumerable<Change> changes)
        {
            Change currentChange;
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client
                .Cypher
                .Match("(dt:DataType)")
                .Where("dt.id = {id}")
                .WithParam("id", dataType.Id)
                .AndWhere("not (has (dt.synced))")
                .AndWhere("not (has (dt.isDeleted))");

                if (changes?.Any() ?? false)
                {
                    for (var i = 0; i < changes.Count(); i++)
                    {
                        currentChange = changes.ToList()[i];
                        var currentAtt = dataType.Attributes.FirstOrDefault(a => a.Name == currentChange.Attribute);

                        switch (currentChange.Type)
                        {
                            case ChangeType.NEW:
                                query = query
                                    .Create("(:AttributeType {attributeType" + i + "})<-[:HAS_ATTRIBUTE]-(dt)")
                                    .WithParam("attributeType" + i, currentAtt)
                                    .With("dt");
                                break;
                            case ChangeType.DELETED:
                                query = query
                                    .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                                    .Where("at.name = {name" + i + "}")
                                    .WithParam("name" + i, currentChange.Attribute)
                                    .Set("at.isDeleted = true")
                                    .With("dt");
                                break;
                            case ChangeType.UPDATED:
                                query = query
                                    .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                                    .Where("at.name = {name" + i + "}")
                                    .WithParam("name" + i, currentAtt.Name)
                                    .Set("at.isRequired = {updateR" + i + "}, at.defaultValue = {updateD" + i + "}")
                                    .WithParam("updateR" + i, currentAtt.IsRequired)
                                    .WithParam("updateD" + i, currentAtt.DefaultValue)
                                    .With("dt");
                                break;
                            //case ChangeType.SKIP:
                            //    continue;
                        }

                    }
                }

                var result = await query.Return<NeoDataType>("dt").ResultsAsync;

                return await GetDataTypeByIdAsync(dataType.Id);
            }
        }

        public async Task<DataType> SetDefaultValueAsync(DataType dataType, IEnumerable<Change> changes)
        {
            Change change;
            IAttributeType att;

            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();


                var query = client
                .Cypher
                .Match("(do:DataObject)-[:OF_DATATYPE]->(dt:DataType)")
                .Where("dt.id = {id}")
                .WithParam("id", dataType.Id);

                if (dataType.Attributes?.Any() ?? false)
                {
                    for (var i = 0; i < dataType.Attributes.Count; i++)
                    {
                        att = dataType.Attributes[i];
                        change = changes.FirstOrDefault(c => c.Attribute == att.Name);
                        if (change == null)
                            continue;

                        if (change.Type == ChangeType.UPDATED && att.IsRequired == true)
                        {
                            query = query
                                .Match("(do)-[:HAS_ATTRIBUTE]->(av:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                                .With("collect(at.name) as atName, do, dt")
                                .Where("not({name} in atName)")
                                .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType {name : {name" + i + "}})")
                                .WithParam("name" + i, att.Name);
                            query = SetDefaultValueQueryBuilder(query, i, att);
                        }
                        else if (change.Type == ChangeType.NEW && att.IsRequired == true)
                        {
                            query = query
                                .Match("(do)-[:OF_DATATYPE]->(dt:DataType)-[:HAS_ATTRIBUTE]->(at:AttributeType {name: {name" + i + "}})")
                                .WithParam("name" + i, att.Name);
                            query = SetDefaultValueQueryBuilder(query, i, att);
                        }
                    }
                }

                var result = await query.Return<NeoDataType>("dt").ResultsAsync;
                
                return dataType;
            }
        }

        private ICypherFluentQuery SetDefaultValueQueryBuilder(ICypherFluentQuery query, int i, IAttributeType att)
        {
            var datetime = DateTime.Now;

            query = query
                .Match("(do)-[oldHCs:HAS_CHANGESET]->(oldCs:ChangeSet)")
                .Match("(day:Day {value: " + datetime.Day + "})<-[:CONTAINS]-(:Month {value: " + datetime.Month + "})<-[:CONTAINS]-(:Year {value: " + datetime.Year + "})")
                .Create("(do)-[:HAS_CHANGESET]->(newCs:ChangeSet { date: '" + datetime.ToString("u") + "' })-[:CONTAINS]->(oldCs)")
                .Create("(newCs)-[:CHANGED_AT]->(day)")
                .Delete("oldHCs")
                .Create("(newCs)-[:CHANGED]->(av:AttributeValue {value : {defaultvalue" + i + "}})<-[:HAS_ATTRIBUTE]-(do)")
                .WithParam("defaultvalue" + i, att.DefaultValue)
                .Create("(av)-[:OF_ATTRIBUTETYPE]->(at)")
                .With("do, dt");

            return query;
        }

        public async Task<List<DataType>> GetAllDataTypesAsync(bool syncedOnly)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                  .Match("(dt:DataType)")
                  .Where("not (has (dt.isDeleted))");

                if (syncedOnly)
                    query = query.AndWhere("has (dt.synced)");

                var resultquery = query
                  .Return((dt) => new DataType
                  {
                      Id = dt.As<NeoDataType>().Id,
                      Name = dt.As<NeoDataType>().Name,
                      Synced = dt.As<NeoDataType>().Synced
                  });

                return (await resultquery.ResultsAsync).ToList();
            }
        }

        //Throws when multiple or no entities found
        public async Task<DataType> GetDataTypeByIdAsync(Guid id)
        {
            var returnValue = await FindDataTypeByIdAsync(id);
            if (returnValue == null)
            {
                throw new EntityNotFoundException("DataType with id " + id + " could not be found.");
            }
            return returnValue;
        }

        //Returns null when no entity found
        public async Task<DataType> FindDataTypeByIdAsync(Guid id)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var result = (await GetDataTypeByIdQueryBuilder(client, id).ResultsAsync).FirstOrDefault();

                if (result == null)
                    return null;

                return _dataTypeMapper.Map(result);
            }
        }

        private ICypherFluentQuery<RawDataType> GetDataTypeByIdQueryBuilder(IGraphClient client, Guid id)
        {
            return client.Cypher
                .Planner("rule")
                .Match("(dt:DataType)")
                .UsingIndex("dt:DataType(id)")
                .Where((NeoDataType dt) => dt.Id == id)
                .AndWhere("not (has (dt.isDeleted))")
                .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                .Where("not (has (at.isDeleted))")
                .Return((dt, at) => new RawDataType
                {
                    DataType = dt.As<NeoDataType>(),
                    Attributes = at.CollectAs<Node<string>>()
                });
        }

        //Throws when multiple or no entities found
        public async Task<DataType> GetDataTypeByNameAsync(string name)
        {
            var returnValue = await FindDataTypeByNameAsync(name);
            if (returnValue == null)
            {
                throw new EntityNotFoundException("DataType with name " + name + " could not be found.");
            }
            return returnValue;
        }

        //Returns null when no entity found
        public async Task<DataType> FindDataTypeByNameAsync(string name)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                client.Connect();
                var result = GetDataTypeByNameQueryBuilder(client, name).Results.FirstOrDefault();

                if (result == null)
                    return null;

                return _dataTypeMapper.Map(result);
            }
        }

        private ICypherFluentQuery<RawDataType> GetDataTypeByNameQueryBuilder(IGraphClient client, string name)
        {
            return client.Cypher
                .Planner("rule")
                .Match("(dt:DataType)")
                .Where((NeoDataType dt) => dt.Name == name)
                .AndWhere("not (has (dt.isDeleted))")
                .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                .Where("not (has (at.isDeleted))")
                .Return((dt, at) => new RawDataType
                {
                    DataType = dt.As<NeoDataType>(),
                    Attributes = at.CollectAs<Node<string>>()
                });
        }        

        public async Task<List<DataType>> GetDataTypeByIdListAsync(List<Guid> ids)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                  .Match("(dt:DataType)")
                  .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                  .Where("dt.id IN {ids}")
                  .WithParam("ids", ids)
                  .AndWhere("not (has (dt.isDeleted))")
                  .AndWhere("not (has (at.isDeleted))")
                  .Return((dt, at) => new RawDataType
                  {
                      DataType = dt.As<NeoDataType>(),
                      Attributes = at.CollectAs<Node<string>>()
                  });

                return _dataTypeMapper.Map(await query.ResultsAsync);
            }
        } 

        public async Task<bool> DeleteDataTypeByIdAsync(Guid id)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client
                    .Cypher
                    .Match("(dt:DataType)")
                    .UsingIndex("dt:DataType(id)")
                    .Where((NeoDataType dt) => dt.Id == id)
                    .Set("dt.isDeleted = true")
                    .Return<NeoDataType>("dt");

                return (await query.ResultsAsync).Count() > 0;
            }
        }

        public async Task<bool> VerifyIfDataTypeIsNestedAsync(Guid id)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client
                    .Cypher
                    .Match("(dt:DataType)")
                    .UsingIndex("dt:DataType(id)")
                    .Where((NeoDataType dt) => dt.Id == id)
                    .Match("(dt)<-[:OF_NESTED_TYPE]-(at:AttributeType)<-[:HAS_ATTRIBUTE]-(parentDt:DataType)")
                    .Where("not(has(at.isDeleted)) and not(has(parentDt.isDeleted))")
                    .Return<NeoDataType>("dt");

                return (await query.ResultsAsync).Count() > 0;
            }
        }

        // Validates if datatype with given name doesn't exist yet, including deleted datatypes
        public async Task<bool> VerifyIfDataTypeExistsAsync(string name)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                client.Connect();
                var query = client.Cypher
                    .Match("(dt:DataType)")
                    .Where((NeoDataType dt) => dt.Name == name)
                    .Return<NeoDataType>("dt");

                return query.Results.Count() > 0;
            }
        }
    }
}
