﻿using Digipolis.Common.Events;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Paging;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.Extensions.Options;
using Narato.Common.Exceptions;
using System;
using System.Linq;
using System.Collections.Generic;
using Digipolis.DossierEngine.Domain.Changes;
using Narato.Common.Models;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.Domain.Dto;
using System.Threading.Tasks;
using Digipolis.DossierEngine.Common.Logging;
using NLog;
using Narato.Common.Paging;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class DataObjectManager : IDataObjectManager
    {
        private readonly IDataObjectDataProvider _dataObjectDataProvider;
        private readonly IDataTypeClient _dataTypeClient;
        private readonly IPluginClient _pluginClient;
        private readonly IDataObjectValidator _dataObjectValidator;
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;
        private readonly IEventPublisher _eventHandler;
        private readonly DossierConfiguration _dossierConfiguration;
        private readonly IChangeCalculator<DataObject> _changeCalculator;

        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public DataObjectManager(IDataObjectDataProvider dataObjectDataProvider, IDataTypeClient dataTypeClient, IPluginClient pluginClient, IDataObjectValidator dataObjectValidator, ICurrentTenantProvider<Config> currentTenantProvider, IEventPublisher eventHandler, IChangeCalculator<DataObject> changeCalculator, IOptions<DossierConfiguration> adminConfiguration)
        {
            _dataObjectDataProvider = dataObjectDataProvider;
            _dataTypeClient = dataTypeClient;
            _pluginClient = pluginClient;
            _dataObjectValidator = dataObjectValidator;
            _currentTenantProvider = currentTenantProvider;
            _eventHandler = eventHandler;
            _changeCalculator = changeCalculator;
            _dossierConfiguration = adminConfiguration.Value;
        }

        public async Task<DataObject> GetDataObjectByIdAsync(Guid id)
        {
            return await _dataObjectDataProvider.GetDataObjectByIdAsync(id);
        }

        public async Task<List<ChangeSet>> GetHistoryGetHistoryOfDataObjectByIdAsync(Guid id)
        {
            return await _dataObjectDataProvider.GetHistoryOfDataObjectByIdAsync(id);
        }

        public async Task<DataObject> DeleteDataObjectByIdAsync(Guid id)
        {
            var returnValue = await GetDataObjectByIdAsync(id).LogOnException(); // this will throw an exception if no DO was found.
            if (! await _dataObjectDataProvider.DeleteDataObjectByIdAsync(id))
            {
                throw new Exception("Delete was not successful");
            }

            var topic = Constants.DataObjectDeletedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = id, ExternalId = returnValue.ExternalId, DataTypeId = returnValue.DataTypeId, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey,
                     _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));

            return returnValue;
        }

        public async Task<List<DataObject>> GetDataObjectsByIdListAsync(List<Guid> ids)
        {
            return await _dataObjectDataProvider.FindDataObjectsByIdListAsync(ids);
        }

        public async Task<PagedCollectionResponse<IEnumerable<DataObject>>> GetDataObjectsForDataTypeIdAsync(Guid dataTypeId, int page, int pageSize, string orderbyField = null, bool descending = false)
        {
            if (orderbyField != null)
            {
                var dataType = await _dataTypeClient.GetDataTypeById(dataTypeId, _currentTenantProvider.GetCurrentTenantKey());
                if (dataType.Attributes.Where(at => at.Name == orderbyField && (at.IsCollection != true && at.Type != "LinkDataType")).Count() == 0)
                {
                    throw new ValidationException(FeedbackItem.CreateValidationErrorFeedbackItem("field " + orderbyField + " doesn't exist or isn't sortable (collection)"));
                }
            }

            var count = await _dataObjectDataProvider.CountDataObjectsForDataTypeIdAsync(dataTypeId);
            var dataObjects = await _dataObjectDataProvider.FindDataObjectsForDataTypeIdAsync(dataTypeId, page, pageSize, orderbyField, descending);

            if (orderbyField == null)
            { // avoid random ordering
                dataObjects = dataObjects.OrderBy(dobj => dobj.Name).ToList();
            }

            return dataObjects.ToPagedCollectionResponse(page, pageSize, (int)count);
        }

        public async Task<List<Guid>> GetDataObjectIdsForDataTypeIdAsync(Guid dataTypeId)
        {
            return await _dataObjectDataProvider.FindDataObjectIdsForDataTypeIdAsync(dataTypeId);
        }

        public async Task<DataObject> InsertDataObjectAsync(DataObject dataObject)
        {
            if (dataObject.DataTypeId == Guid.Empty)
            {
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "DataType id isn't filled in." });
            }
            var dataType = await _dataTypeClient.GetDataTypeTreeById(dataObject.DataTypeId, _currentTenantProvider.GetCurrentTenantKey()).LogOnException();

            var feedbackItems = _dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);
            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);

            dataObject.Id = Guid.NewGuid();
            var createdDO = await _dataObjectDataProvider.InsertDataObjectForDataTypeAsync(dataObject, dataType).LogOnException();

            var topic = Constants.DataObjectCreatedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = createdDO.Id, Name = createdDO.Name, ExternalId = createdDO.ExternalId, DataTypeId = createdDO.DataTypeId, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey,
                _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));

            await SyncUnsyncedSyncDataObjectsAsync(createdDO);

            return await _dataObjectDataProvider.GetDataObjectByIdAsync(createdDO.Id);
        }

        // we don't allow changing name or dataTypeId. Just the values.
        public async Task<DataObject> UpdateDataObjectForIdAsync(Guid id, DataObject dataObject, bool isSyncedDataObject = false)
        {
            if (id != dataObject.Id)
            {
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "DataObject id from body doesn't equal the id passed in the path." });
            }
            var dataType = await _dataTypeClient.GetDataTypeTreeById(dataObject.DataTypeId, _currentTenantProvider.GetCurrentTenantKey()).LogOnException();

            var feedbackItems = _dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);
            if (feedbackItems.Count > 0)
                throw new ValidationException(feedbackItems);

            var originalDataObject = await _dataObjectDataProvider.GetDataObjectByIdAsync(id, isSyncedDataObject).LogOnException();

            if (originalDataObject.IsDeleted == true)
            {
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "Trying to update a deleted DataObject!" });
            }

            var changeSet = _changeCalculator.CalculateChangeSet(originalDataObject, dataObject);

            // if nothing changed, we don't have to do anything :-)
            if (changeSet.Count() == 0)
            {
                return originalDataObject;
            }

            // note we pass the ORIGINAL dataObject. This is because we need to be sure the name is correct
            var updatedDO = await _dataObjectDataProvider.UpdateDataObjectAsync(originalDataObject, changeSet, isSyncedDataObject).LogOnException();

            await SyncUnsyncedSyncDataObjectsAsync(updatedDO).LogOnException();

            var topic = Constants.DataObjectUpdatedTopic(_currentTenantProvider.GetCurrentTenantKey()); // not in task, because we need the httpcontext to be alive at this point
            var eventPayload = new { Id = updatedDO.Id, Name = updatedDO.Name, ExternalId = updatedDO.ExternalId, DataTypeId = updatedDO.DataTypeId, TenantKey = _currentTenantProvider.GetCurrentTenantKey() };
            Task.Factory.StartNew(() => _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey,
                 _dossierConfiguration.EventHandlerNamespace, topic, eventPayload));

            return await _dataObjectDataProvider.GetDataObjectByIdAsync(updatedDO.Id, isSyncedDataObject);
        }

        // think of a better name? :p
        private async Task SyncUnsyncedSyncDataObjectsAsync(DataObject dataObject)
        {
            var unsyncedSyncDataObjects = await _dataObjectDataProvider.GetUnsyncedSyncDataObjectsForDataObjectAsync(dataObject).LogOnException();

            foreach (var unsyncedSyncDataObject in unsyncedSyncDataObjects)
            {
                try
                {
                    var pluginObjectLookup = new PluginObjectLookupDto
                    {
                        TenantKey = _currentTenantProvider.GetCurrentTenantKey(),
                        ExternalId = unsyncedSyncDataObject.DataObject.ExternalId,
                        DataTypeId = unsyncedSyncDataObject.DataType.Id
                    };
                    _pluginClient.SyncUnsyncedSyncDataObject(pluginObjectLookup);
                }
                catch (Exception e)
                {
                    Logger.Warn(e, "SyncUnsyncedDataObjec failed.");
                    // TODO: log shit + add warning feedback to response
                }
            }
        }
    }
}