﻿using Digipolis.DossierEngine.DataType.API.Controllers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using Xunit;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class DataTypeControllerTest
    {
        [Fact]
        public void GetReturnsAnActionResult()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<Model.Models.DataType>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetWithMissingTenantIdReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void GetByNameReturnsAnActionResult()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<Model.Models.DataType>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetByNameAsync("myDatatype", "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetByNameWithMissingTenantIdReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetByNameAsync("myDatatype", string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void GetAllReturnsAnActionResult()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseForCollectionAsync(It.IsAny<Func<Task<PagedCollectionResponse<IEnumerable<Model.Models.DataType>>>>>(), It.IsAny<string>()))
                .ReturnsAsync(new ObjectResult(null));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAllAsync("tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetAllWithMissingTenantIdReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAllAsync(string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataTypeWithoutTenantIdReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataTypeAsync(new Model.Models.DataType(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataTypeWithoutDataTypeReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataTypeAsync(null, "tentanShortName").Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataTypeReturnsAnActionResult()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreatePostResponseAsync(
                It.IsAny<Func<Task<Model.Models.DataType>>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Func<Model.Models.DataType, object>>()))
                .ReturnsAsync(new ObjectResult(null));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataTypeAsync(new Model.Models.DataType(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void UpdateDataTypeReturnsAnActionResult()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreatePostResponseAsync(It.IsAny<Func<Task<Model.Models.DataType>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.UpdateDataTypeAsync(new Model.Models.DataType(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void UpdateDataTypeWithoutDataTypeReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.UpdateDataTypeAsync(null, "tentanShortName").Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void UpdateDataTypeWithoutTenantIdReturnsBadRequest()
        {
            var dataTypeManagerMock = new Mock<IDataTypeManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataTypeController(dataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.UpdateDataTypeAsync(new Model.Models.DataType(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }
    }
}
