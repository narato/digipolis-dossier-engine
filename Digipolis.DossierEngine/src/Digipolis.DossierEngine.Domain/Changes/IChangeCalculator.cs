﻿using Digipolis.DossierEngine.DataProvider.Changes;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Changes
{
    public interface IChangeCalculator<T>
    {
        IEnumerable<Change> CalculateChangeSet(T oldObject, T newObject);
    }
}
