﻿namespace Digipolis.DossierEngine.Common.Configurations
{
    public class DossierConfiguration
    {
        public string IneoHome { get; set; }
        public string SshUsername { get; set; }
        public string SshPassword { get; set; }
        public string ActiveNeo4JServer { get; set; }
        public string Neo4jUsername { get; set; }
        public string Neo4jPassword { get; set; }
        public int SshPort { get; set; }
        public int DefaultHttpPort { get; set; }
        public int DefaultHttpsPort { get; set; }
        public int DefaultBoltPort { get; set; }
        public int MinYear { get; set; }
        public int MaxYear { get; set; }
        public string EventHandlerNamespace { get; set; }
        public string EventHandlerOwnerKey { get; set; }
        public string PluginEndpoint { get; set; }

        public string AdminApiEndpoint { get; set; }
        public string DataTypeApiEndpoint { get; set; }
    }
}
