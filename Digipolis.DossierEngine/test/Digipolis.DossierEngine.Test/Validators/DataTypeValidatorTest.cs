﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Validators;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Moq;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.DossierEngine.Test.Validators
{
    public class DataTypeValidatorTest
    {
        [Fact]
        public void ValidateDataType_TypeCheck_ReturnsFeedbackList()
        {
            var dt = InitDataTypeModel();
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.IsType<List<FeedbackItem>>(result);
        }

        [Fact]
        public void ValidateDataType_TypeWithPeriodInName_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new StringAttributeType { Name = "test.period", RegexValidation = "a" });
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDataType_LinkDataTypeAttributeWithNonExistingDataType_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new LinkDataTypeAttributeType { Name = "nonexistingDataType", DataTypeId = Guid.NewGuid() });
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();
            simpleDataTypeCacherMock.Setup(sdtcm => sdtcm.GetAllDataTypesAsync()).ReturnsAsync(new List<Model.Models.DataType>());
            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDataType_TypeWithNameNull_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new StringAttributeType { RegexValidation = "a" });
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDataType_NoAttributes_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateStringType_InvalidRegEx_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new StringAttributeType { Name = "test", RegexValidation = "(a" });
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateStringType_InvalidDefaultValue_ReturnsFeedback()
        {
            var strType = new StringAttributeType { Name = "test", RegexValidation = "a", DefaultValue = "b" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(strType);
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateStringObject(strType, strType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateIntType_MinHigherThanMax_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new IntAttributeType { Name = "test", MinRangeValidation = 1, MaxRangeValidation = 0 });
            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateIntType_InvalidDefaultValue_ReturnsFeedback()
        {
            var intType = new IntAttributeType { Name = "test", DefaultValue = "invalidInt" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(intType);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateIntegerObject(intType, intType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_MinHigherThanMax_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new DoubleAttributeType { Name = "test", MinRangeValidation = 2, MaxRangeValidation = 1 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_InvalidDefaultValue_ReturnsFeedback()
        {
            var doubleType = new DoubleAttributeType { Name = "test", DefaultValue = "invalidDouble" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(doubleType);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateDoubleObject(doubleType, doubleType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateBoolType_InvalidDefaultValue_ReturnsFeedback()
        {
            var boolType = new BooleanAttributeType { Name = "test", DefaultValue = "invalidBool" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(boolType);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateBooleanObject(boolType, boolType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateLinkType_InvalidDefaultValue_ReturnsFeedback()
        {
            var linkType = new LinkAttributeType { Name = "test", DefaultValue = "invalidLink" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(linkType);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateLinkObject(linkType, linkType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDateTimeType_InvalidDefaultValue_ReturnsFeedback()
        {
            var dateTimeType = new DateTimeAttributeType { Name = "test", DefaultValue = "invalidDateTime" };
            var dt = InitDataTypeModel();
            dt.Attributes.Add(dateTimeType);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(dov => dov.ValidateDateTimeObject(dateTimeType, dateTimeType.DefaultValue)).Returns(new List<FeedbackItem>() { new FeedbackItem() });
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeAttributesAsync(dt).Result;

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void CheckExistingStringType_DifferentRegEx_ReturnsFeedback()
        {
            var dtNew = InitDataTypeModel();
            var dtOld = InitDataTypeModel();
            dtNew.Attributes.Add(new StringAttributeType { Name = "test", RegexValidation = "a" });
            dtOld.Attributes.Add(new StringAttributeType { Name = "test", RegexValidation = "b" });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.DifferentiateDataTypeAttributes(dtOld, dtNew);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void CheckExistingIntType_DifferentMinValidation_ReturnsFeedback()
        {
            var dtNew = InitDataTypeModel();
            var dtOld = InitDataTypeModel();
            dtNew.Attributes.Add(new IntAttributeType { Name = "test", MinRangeValidation = 1 });
            dtOld.Attributes.Add(new IntAttributeType { Name = "test", MinRangeValidation = 2 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.DifferentiateDataTypeAttributes(dtOld, dtNew);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void CheckExistingIntType_DifferentMaxValidation_ReturnsFeedback()
        {
            var dtNew = InitDataTypeModel();
            var dtOld = InitDataTypeModel();
            dtNew.Attributes.Add(new IntAttributeType { Name = "test", MaxRangeValidation = 1 });
            dtOld.Attributes.Add(new IntAttributeType { Name = "test", MaxRangeValidation = 2 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.DifferentiateDataTypeAttributes(dtOld, dtNew);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void CheckExistingDoubleType_DifferentMinValidation_ReturnsFeedback()
        {
            var dtNew = InitDataTypeModel();
            var dtOld = InitDataTypeModel();
            dtNew.Attributes.Add(new DoubleAttributeType { Name = "test", MinRangeValidation = 1 });
            dtOld.Attributes.Add(new DoubleAttributeType { Name = "test", MinRangeValidation = 2 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.DifferentiateDataTypeAttributes(dtOld, dtNew);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void CheckExistingDoubleType_DifferentMaxValidation_ReturnsFeedback()
        {
            var dtNew = InitDataTypeModel();
            var dtOld = InitDataTypeModel();
            dtNew.Attributes.Add(new DoubleAttributeType { Name = "test", MaxRangeValidation = 1 });
            dtOld.Attributes.Add(new DoubleAttributeType { Name = "test", MaxRangeValidation = 2 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.DifferentiateDataTypeAttributes(dtOld, dtNew);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDefaultValueWhenIsRequired_DefaultValueNull_ReturnsFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new DoubleAttributeType { Name = "test", IsRequired = true });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeDefaultValue(dt);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDefaultValueWhenIsRequired_DefaultValueNotNull_ReturnsNoFeedback()
        {
            var dt = InitDataTypeModel();
            dt.Attributes.Add(new DoubleAttributeType { Name = "test", IsRequired = true, DefaultValue = 1 });

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var simpleDataTypeCacherMock = new Mock<ISimpleDataTypeCacher>();

            var dataTypeValidator = new DataTypeValidator(dataObjectValidatorMock.Object, simpleDataTypeCacherMock.Object);
            var result = dataTypeValidator.ValidateDataTypeDefaultValue(dt);

            Assert.Equal(0, result.Count);
        }

        private Model.Models.DataType InitDataTypeModel()
        {
            return new Model.Models.DataType
            {
                Id = Guid.NewGuid(),
                Name = "DataType1",
                Attributes = new List<IAttributeType>()
            };
        }
    }
}