﻿using Digipolis.Common.DataStore.ResponseExtensions;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Converters;
using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.Extensions.Options;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Clients
{
    // TODO: this is as generic as it gets. move to Digipolis.Common
    public class DataTypeClient : IDataTypeClient
    {
        private readonly IGroupedLimitedPool<string, HttpClient> _clientPool;
        private readonly DossierConfiguration _dossierConfiguration;
        private readonly string _tenantKeyName;
        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public DataTypeClient(IGroupedLimitedPool<string, HttpClient> clientPool, IOptions<DossierConfiguration> dossierConfiguration)
        {
            _clientPool = clientPool;
            _dossierConfiguration = dossierConfiguration.Value;
            _tenantKeyName = "Tenant-Key";
        }

        public async Task<DataType> GetDataTypeById(Guid id, string tenantKey)
        {
            return await GetDataTypeByUrl($"datatypes/{id}", tenantKey);
        }

        public async Task<DataType> GetDataTypeTreeById(Guid id, string tenantKey)
        {
            return await GetDataTypeByUrl($"datatypes/{id}?flatstructure=false", tenantKey);
        }

        public async Task<DataType> GetDataTypeByName(string name, string tenantKey)
        {
            return await GetDataTypeByUrl($"datatypes/name/{name}", tenantKey);
        }

        public async Task<DataType> InsertSyncedDataTypeAsync(string dataTypeJson, string tenantKey)
        {
            dataTypeJson = AddSyncedPropertyToJson(dataTypeJson);
            return await PostDataType("synceddatatypes", dataTypeJson, tenantKey);
        }

        public async Task<DataType> InsertDataTypeAsync(string dataTypeJson, string tenantKey)
        {
            return await PostDataType("datatypes", dataTypeJson, tenantKey);
        }

        private string AddSyncedPropertyToJson(string dataTypeJson)
        {
            //TODO: Find another way to do this, since it's quite hacky...
            // TODO: move this "hacky" part to where you insert a plugin in the postgres
            var json = JObject.Parse(dataTypeJson);
            var attributeToMerge = JObject.Parse("{\"synced\": true}");

            json.Merge(attributeToMerge);
            return json.ToString();
        }

        private async Task<DataType> PostDataType(string url, string dataTypeJson, string tenantKey)
        {
            using (var poolItem = _clientPool.Get(_dossierConfiguration.DataTypeApiEndpoint))
            {
                var client = poolItem.Value;
                SetupClient(client, tenantKey);

                HttpContent content = new StringContent(dataTypeJson);

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress + url);

                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                request.Content = content;

                using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseContentRead))
                {
                    // TODO: use digipolis.common HandleResponseAsync method

                    var responseString = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                    {
                        var dataTypeResponse = JsonConvert.DeserializeObject<Response<DataType>>(responseString, new AttributeTypeConverter());
                        return dataTypeResponse.Data;
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                        var feedbackList = dataTypeResponse.Feedback;
                        feedbackList.Add(new FeedbackItem() { Description = "Could not insert datatype." });
                        throw new ExceptionWithFeedback(feedbackList);
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                        if (dataTypeResponse.Feedback.Count > 0)
                        {
                            throw new EntityNotFoundException(dataTypeResponse.Feedback[0].Description);
                        }
                        throw new EntityNotFoundException();
                    }
                    else if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        var dataTypeResponse = JsonConvert.DeserializeObject<Response>(responseString);
                        if (dataTypeResponse.Feedback.Count > 0)
                        {
                            throw new ExceptionWithFeedback(dataTypeResponse.Feedback);
                        }
                        throw new ExceptionWithFeedback(new FeedbackItem() { Type = FeedbackType.Error, Description = "An error occured while talking to the DataStore Engine" });
                    }

                    throw new ExceptionWithFeedback(FeedbackItem.CreateErrorFeedbackItem("Could not insert datatype. Response statuscode: " + response.StatusCode));
                }
            }
        }

        private async Task<DataType> GetDataTypeByUrl(string url, string tenantKey)
        {
            Logger.Info("idle count for dataType: " + _clientPool.IdleCount(_dossierConfiguration.DataTypeApiEndpoint));
            using (var poolItem = _clientPool.Get(_dossierConfiguration.DataTypeApiEndpoint))
            {
                var client = poolItem.Value;
                SetupClient(client, tenantKey);

                using (var response = await client.GetAsync(url))
                {
                    var responseString = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var dataTypeResponse = JsonConvert.DeserializeObject<Response<DataType>>(responseString, new AttributeTypeConverter());
                        return dataTypeResponse.Data;
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        if (string.IsNullOrEmpty(responseString))
                            throw new EntityNotFoundException();
                        var ResponseObject = JsonConvert.DeserializeObject<Response>(responseString);
                        throw new EntityNotFoundException(ResponseObject.Feedback[0].Description);
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        throw new UnauthorizedAccessException();
                    } else
                    {
                        if (string.IsNullOrEmpty(responseString))
                            throw new ExceptionWithFeedback(FeedbackItem.CreateErrorFeedbackItem($"The datatype could not be retrieved. Status code returned: {response.StatusCode}"));
                        var ResponseObject = JsonConvert.DeserializeObject<Response>(responseString);
                        throw new ExceptionWithFeedback(ResponseObject.Feedback);
                    }
                }
            }
        }

        private void SetupClient(HttpClient client, string tenantKey)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (client.DefaultRequestHeaders.Contains(_tenantKeyName))
                client.DefaultRequestHeaders.Remove(_tenantKeyName);

            client.DefaultRequestHeaders.Add(_tenantKeyName, tenantKey);
        }
    }
}