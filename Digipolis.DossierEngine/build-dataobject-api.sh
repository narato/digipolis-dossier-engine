#!/bin/bash

dotnet build src/Digipolis.DossierEngine.DataObject.API/
dotnet publish src/Digipolis.DossierEngine.DataObject.API/ -o build/data-object
mkdir build/data-object/Plugins
