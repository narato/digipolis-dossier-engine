﻿using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Model.Models
{
    public class ChangeSet
    {
        public DateTime Date { get; set; }
        public IEnumerable<ChangeSetEntry> Changes { get; set; }
    }
}
