﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Common.Pooling.Interfaces
{
    public interface IGroupedLimitedPool<K, T> : IDisposable where T : class
    {
        int IdleCount(K group);

        ILimitedPoolItem<T> Get(K group);
        Task<ILimitedPoolItem<T>> GetAsync(K group);
    }
}
