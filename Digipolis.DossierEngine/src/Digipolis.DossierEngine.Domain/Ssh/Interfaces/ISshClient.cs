﻿using Renci.SshNet;
using System;

namespace Digipolis.DossierEngine.Domain.Ssh.Interfaces
{
    public interface ISshClient : IDisposable
    {
        void Connect();
        void Disconnect();

        SshCommand RunCommand(string commandText);
    }
}
