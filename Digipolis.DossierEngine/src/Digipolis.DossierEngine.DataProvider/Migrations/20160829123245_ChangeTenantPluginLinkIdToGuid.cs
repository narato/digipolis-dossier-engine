﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class ChangeTenantPluginLinkIdToGuid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("TenantPluginLink");

            migrationBuilder.CreateTable(
               name: "TenantPluginLink",
               columns: table => new
               {
                   Id = table.Column<Guid>(nullable: false),
                   PluginId = table.Column<Guid>(nullable: true),
                   TenantId = table.Column<Guid>(nullable: true),
                   DataTypeId = table.Column<Guid>(nullable: false, defaultValue: new Guid("00000000-0000-0000-0000-000000000000")),
                   Prepopulate = table.Column<bool>(nullable: false, defaultValue: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_TenantPluginLink", x => x.Id);
                   table.ForeignKey(
                       name: "FK_TenantPluginLink_Plugin_PluginId",
                       column: x => x.PluginId,
                       principalTable: "Plugin",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.Restrict);
                   table.ForeignKey(
                       name: "FK_TenantPluginLink_Tenant_TenantId",
                       column: x => x.TenantId,
                       principalTable: "Tenant",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.Restrict);
               });

            migrationBuilder.CreateIndex(
                name: "IX_TenantPluginLink_PluginId",
                table: "TenantPluginLink",
                column: "PluginId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantPluginLink_TenantId",
                table: "TenantPluginLink",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPostgresExtension("uuid-ossp");
        }
    }
}
