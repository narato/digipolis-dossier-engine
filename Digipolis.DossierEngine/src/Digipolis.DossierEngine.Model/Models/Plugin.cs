﻿using System;
using System.Linq;
using System.Collections.Generic;
using TenantModel = Digipolis.Common.Tenancy.Models;

namespace Digipolis.DossierEngine.Model.Models
{
    public class Plugin
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Version { get; set; }
        public string DataType { get; set; }
        public List<TenantPluginLink> TenantPluginLinks { get; set; }

        public List<TenantModel.Tenant> GetTenants()
        {
            return TenantPluginLinks.Select(tp => tp.Tenant).ToList();
        }
    }
}