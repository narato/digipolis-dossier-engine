﻿using Digipolis.DossierEngine.Model.Dto;
using Newtonsoft.Json;
using System;

namespace Digipolis.DossierEngine.Model.Models
{
    public class DataObject : DataObjectDTO
    {
        public Guid Id { get; set; }

        [JsonProperty("isNested", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsNested { get; set; } // internal property, so not exposed

        [JsonProperty("isDeleted", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsDeleted { get; set; } // internal property, so not exposed

        [JsonProperty("externalId", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ExternalId { get; set; } // internal property, so not exposed (can be int/guid/string)

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}