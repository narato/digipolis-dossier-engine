﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Ssh.Interfaces;

namespace Digipolis.DossierEngine.Domain.Ssh
{
    public class SshClientProvider : ISshClientProvider
    {
        public ISshClient GetSshClient(string server, int port, string username, string password)
        {
            return new IneoSshClient(server, port, username, password);
        }
    }
}
