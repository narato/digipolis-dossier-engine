#!/bin/bash

# Restore all packages in all projects, including local packages
dotnet restore -f local_packages

# Build output projects
umask 000 && ./build-admin-api.sh
umask 000 && ./build-datatype-api.sh
umask 000 && ./build-dataobject-api.sh

# Run test suites
umask 000 && ./run-test-suite.sh
