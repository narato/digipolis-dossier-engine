﻿using Digipolis.DossierEngine.Common.Models;
using Digipolis.Tenant.Controllers;
using Digipolis.Tenant.Library.Managers;
using Digipolis.Tenant.Library.Tenancy;
using Narato.Common.Factory;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    public class TenantController : TenantController<Config>
    {
        public TenantController(ITenantManager<Config> tenantManager, ITenantCreator<Config> tenantCreator, IResponseFactory responseFactory) : base(tenantManager, tenantCreator, responseFactory)
        {
        }
    }
}
