﻿using Microsoft.AspNetCore.Mvc;
using Narato.Common.Factory;
using Digipolis.DossierEngine.Domain.Interfaces;
using Narato.Common;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    [Route("api/synchdatatypes")]
    public class SynchDataTypeController : Controller
    {
        private IResponseFactory _responseFactory;
        private ISyncedDataTypeManager _synchDataTypeManager;
        public SynchDataTypeController(ISyncedDataTypeManager synchDataTypeManager, IResponseFactory responseFactory)
        {
            _synchDataTypeManager = synchDataTypeManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Gets all synchDataTypes
        /// </summary>
        /// <returns>All all synchDataTypes</returns>
        // GET api/synchdatatypes
        [ProducesResponseType(typeof(Response<List<DataType>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<List<DataType>>), (int)System.Net.HttpStatusCode.BadRequest)]
        [HttpGet]
        public IActionResult GetAll()
        {
            return _responseFactory.CreateGetResponse(() => _synchDataTypeManager.GetAllSynchDataTypes(),
                      this.GetRequestUri());
        }
    }
}
