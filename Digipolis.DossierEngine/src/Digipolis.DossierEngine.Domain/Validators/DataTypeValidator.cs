﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Validators
{
    public class DataTypeValidator : IDataTypeValidator
    {
        private readonly IDataObjectValidator _dataObjectValidator;
        private readonly ISimpleDataTypeCacher _simpleDataTypeCacher;

        public DataTypeValidator(IDataObjectValidator dataObjectValidator, ISimpleDataTypeCacher simpleDataTypeCacher)
        {
            _dataObjectValidator = dataObjectValidator;
            _simpleDataTypeCacher = simpleDataTypeCacher;
        }

        public async Task<List<FeedbackItem>> ValidateDataTypeAttributesAsync(DataType dataType)
        {
            var feedbackItems = new List<FeedbackItem>();

            foreach (IAttributeType att in dataType.Attributes)
            {
                if (att.Name == null)
                    feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "The Name attribute cannot be NULL" });

                if (att.Name != null && att.Name.Contains("."))
                    feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The Name of an attribute isn't allowed to contain periods ({0}) ", att.Name) });

                if (att.Name == "externalId")
                    feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The Name of an attribute isn't allowed to 'externalId', this is a reserved attribute ({0}) ", att.Name) });

                if (att is StringAttributeType)
                {
                    var stringType = (StringAttributeType)att;
                    try
                    {
                        Regex rgx = new Regex(stringType.RegexValidation);
                    }
                    catch
                    {
                        feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for RegexValidation is not a valid regular expression : {0}", stringType.RegexValidation) });
                    }

                    //Validate given DefaultValue
                    if (att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateStringObject(stringType, att.DefaultValue));
                }
                else if (att is IntAttributeType)
                {
                    var intType = (IntAttributeType)att;

                    if (intType.MinRangeValidation != null && intType.MaxRangeValidation != null && intType.MinRangeValidation > intType.MaxRangeValidation)
                    {
                        feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The value of MinRangeValidation ({0}) cannot exceed the value of MaxRangeValidation ({1})", intType.MinRangeValidation, intType.MaxRangeValidation) });
                    }

                    //Validate given DefaultValue
                    if (att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateIntegerObject(intType, att.DefaultValue));
                }
                else if (att is DoubleAttributeType)
                {
                    var doubleType = (DoubleAttributeType)att;

                    if (doubleType.MinRangeValidation != null && doubleType.MaxRangeValidation != null && doubleType.MinRangeValidation > doubleType.MaxRangeValidation)
                    {
                        feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The value of MinRangeValidation ({0}) cannot exceed the value of MaxRangeValidation ({1})", doubleType.MinRangeValidation, doubleType.MaxRangeValidation) });
                    }

                    //Validate given DefaultValue
                    if (att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateDoubleObject(doubleType, att.DefaultValue));
                }
                else if (att is BooleanAttributeType)
                {
                    //Validate given DefaultValue
                    if (att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateBooleanObject((BooleanAttributeType)att, att.DefaultValue));
                }
                else if (att is LinkAttributeType)
                {
                    //Validate given DefaultValue
                    if (att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateLinkObject((LinkAttributeType)att, att.DefaultValue));
                }
                else if (att is DateTimeAttributeType)
                {
                    //Validate given DefaultValue
                    if(att.DefaultValue != null)
                        feedbackItems.AddRange(_dataObjectValidator.ValidateDateTimeObject((DateTimeAttributeType)att, att.DefaultValue));
                }
                else if (att is LinkDataTypeAttributeType)
                {
                    var linkAtt = (LinkDataTypeAttributeType)att;
                    var dataTypeList = await _simpleDataTypeCacher.GetAllDataTypesAsync();
                    if (dataTypeList.Where(dt => dt.Id == linkAtt.DataTypeId).Count() == 0)
                    {
                        feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The DataType with Id ({0}) doesn't exist", linkAtt.DataTypeId) });
                    }
                }
            }

            if (dataType.Attributes.Count == 0)
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "No attributes were found in the collection" });

            return feedbackItems;
        }

        /// <summary>
        /// Checks for changes to existing attributes that will be ignored and provide feedback
        /// </summary>
        /// <param name="originalDataType">The original DataType before requested changes</param>
        /// <param name="dataType">The given DataType with requested changes</param>
        /// <returns>Gives a feedback collection</returns>
        public List<FeedbackItem> DifferentiateDataTypeAttributes(DataType originalDataType, DataType dataType)
        {
            var feedbackItems = new List<FeedbackItem>();
            var attributesToRemove = new List<IAttributeType>();

            foreach (BaseAttributeType att in dataType.Attributes)
            {
                if (att is StringAttributeType)
                {
                    //Only check for changes on already existing attributes
                    var originalAtt = (StringAttributeType)originalDataType.Attributes.FirstOrDefault(a => a.Name == att.Name);
                    if (originalAtt != null)
                    {
                        //No feedback on potential changes when IsAtive is False
                        if (att.IsDeleted != true)
                        {
                            var stringType = (StringAttributeType)att;
                            if (originalAtt.RegexValidation != stringType.RegexValidation)
                                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The RegexValidation for attribute ({0}) cannot be changed", att.Name) });
                        }
                    }
                }

                else if (att is IntAttributeType)
                {
                    //Only check for changes on already existing attributes
                    var originalAtt = (IntAttributeType)originalDataType.Attributes.FirstOrDefault(a => a.Name == att.Name);
                    if (originalAtt != null)
                    {
                        //No feedback on potential changes when IsAtive is False
                        if (att.IsDeleted != true)
                        {
                            var intType = (IntAttributeType)att;
                            if (originalAtt.MinRangeValidation != intType.MinRangeValidation)
                                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The MinRangeValidation for attribute ({0}) cannot be changed", att.Name) });
                            if (originalAtt.MaxRangeValidation != intType.MaxRangeValidation)
                                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The MaxRangeValidation for attribute ({0}) cannot be changed", att.Name) });
                        }
                    }
                }
                else if (att is DoubleAttributeType)
                {
                    //Only check for changes on already existing attributes
                    var originalAtt = (DoubleAttributeType)originalDataType.Attributes.FirstOrDefault(a => a.Name == att.Name);
                    if (originalAtt != null)
                    {
                        //No feedback on potential changes when IsAtive is False
                        if (att.IsDeleted != true)
                        {
                            var doubleType = (DoubleAttributeType)att;
                            if (originalAtt.MinRangeValidation != doubleType.MinRangeValidation)
                                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The MinRangeValidation for attribute ({0}) cannot be changed", att.Name) });
                            if (originalAtt.MaxRangeValidation != doubleType.MaxRangeValidation)
                                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The MaxRangeValidation for attribute ({0}) cannot be changed", att.Name) });
                        }
                    }
                }
            }

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateDataTypeDefaultValue(DataType dataType)
        {
            var feedbackItems = new List<FeedbackItem>();

            foreach (IAttributeType att in dataType.Attributes)
            {
                //Provide feedback when attribute is required but no DefaultValue was given
                //TODO : Find better solution for LinkDataType type comparison
                if (att.IsRequired == true && att.DefaultValue == null && att.Type != "LinkDataType")
                    feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("DefaultValue cannot be NULL when IsRequired is true for attribute : {0}", att.Name) });
            }

            return feedbackItems;
        }
    }
}