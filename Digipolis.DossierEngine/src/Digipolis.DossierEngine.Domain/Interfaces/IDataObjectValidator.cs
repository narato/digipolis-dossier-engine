﻿using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataObjectValidator
    {
        List<FeedbackItem> ValidateDataObjectAttributes(DataType dataType, DataObject dataObject);
        List<FeedbackItem> ValidateStringObject(StringAttributeType att, object value);
        List<FeedbackItem> ValidateIntegerObject(IntAttributeType att, object value);
        List<FeedbackItem> ValidateDoubleObject(DoubleAttributeType att, object value);
        List<FeedbackItem> ValidateBooleanObject(BooleanAttributeType att, object value);
        List<FeedbackItem> ValidateLinkObject(LinkAttributeType att, object value);
        List<FeedbackItem> ValidateDateTimeObject(DateTimeAttributeType att, object value);
    }
}