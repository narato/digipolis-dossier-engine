﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class IntAttributeType : BaseAttributeType
    {
        public const string TypeName = "Int";

        public IntAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
        public int? MinRangeValidation { get; set; }
        public int? MaxRangeValidation { get; set; }
    }
}