﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Digipolis.DossierEngine.Domain.Interfaces;
using Models = Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataObject.API.Controllers
{
    [Route("api/datatypes/{datatypeId}/dataobjects/search/")]
    public class SearchController : Controller
    {
        private ISearchManager _searchManager;
        private IResponseFactory _responseFactory;

        public SearchController(ISearchManager searchManager, IResponseFactory responseFactory)
        {
            _searchManager = searchManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Search for dataobjects
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="query">The search criteria. see https://acpaas.wiki.antwerpen.be/Dossier+Engine+v2+-+DataObject+API#toc4 for examples</param>
        /// <param name="datatypeId">The id of the datatype</param>
        /// <param name="page">The page number to be returned</param>
        /// <param name="pagesize">The number of records to be returned per page</param>
        /// <returns>A list of dataobjects</returns>
        [ProducesResponseType(typeof(Response<List<Models.DataObject>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPost]
        public async Task<IActionResult> SearchAsync([FromBody] [Required] dynamic query, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey, [Required] Guid datatypeId, [FromQuery] int page = 1, [FromQuery] int pagesize = 10)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (datatypeId == null || datatypeId == Guid.Empty)
                missingParams.Add(new MissingParam("datatypeId", MissingParamType.QuerystringParam));
            if (query == null)
                missingParams.Add(new MissingParam("query", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            // TODO : validate the query?

            return await _responseFactory.CreateGetResponseForCollectionAsync<Models.DataObject>(async () => {
                return await _searchManager.FindDataObjectsForDataTypeIdBySearchQueryObjectAsync(datatypeId, query, page, pagesize); 
            }, this.GetRequestUri());
        }
    }
}
