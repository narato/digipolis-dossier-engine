﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class TenantPluginLinkAddDataTypeId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DataTypeId",
                table: "TenantPluginLink",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataTypeId",
                table: "TenantPluginLink");
        }
    }
}
