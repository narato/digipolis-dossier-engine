﻿using Digipolis.DossierEngine.DataObject.API.Controllers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using Xunit;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class SearchControllerTest
    {
        [Fact]
        public void SearchReturnsAnActionResult()
        {
            var searchManagerMock = new Mock<ISearchManager>();
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponseForCollectionAsync(It.IsAny<Func<Task<PagedCollectionResponse<IEnumerable<Model.Models.DataObject>>>>>(), It.IsAny<string>()))
                .ReturnsAsync(new ObjectResult(null));

            var controller = new SearchController(searchManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.SearchAsync(new ExpandoObject(), "tenant", Guid.NewGuid()).Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void SearchWithoutTenantKeyReturnsABadRequest()
        {
            var searchManagerMock = new Mock<ISearchManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new SearchController(searchManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.SearchAsync(new ExpandoObject(), "", Guid.NewGuid()).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void SearchWithoutQueryObjectReturnsABadRequest()
        {
            var searchManagerMock = new Mock<ISearchManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new SearchController(searchManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.SearchAsync(null, "tenant", Guid.NewGuid()).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void SearchWithoutDataTypeIdReturnsABadRequest()
        {
            var searchManagerMock = new Mock<ISearchManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new SearchController(searchManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.SearchAsync(new ExpandoObject(), "tenant", Guid.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }
    }
}
