﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class NeoMigrationManager : INeoMigrationManager
    {
        private INeoMigrationDataProvider _neoMigrationDataProvider;

        public NeoMigrationManager(INeoMigrationDataProvider neoMigrationDataProvider)
        {
            _neoMigrationDataProvider = neoMigrationDataProvider;
        }

        public void SetupDatabase(Tenant<Config> tenant)
        {
            _neoMigrationDataProvider.SetupDatabase(tenant.Config);
        }
    }
}