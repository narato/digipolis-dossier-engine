﻿using System;

namespace Digipolis.DossierEngine.Domain.Dto
{
    public class PluginObjectLookupDto
    {
        public Guid DataTypeId { get; set; } // only way to identify the tenantpluginlink (in the DO/DT context, we have no knowledge of a plugin name)
        public string TenantKey { get; set; }
        public string ExternalId { get; set; }
    }
}
