﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Visitor
{
    public interface IConditionVisitor // make generic, normally the returnValue would be a bool...
    {
        string WalkComparison(IComparison condition, bool partOfComposite);
        string WalkCompositeCondition(CompositeCondition condition);
        string Visit(ICondition condition);
    }
}
