﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Narato.Common.ActionFilters;
using System;
using Narato.Common.Exceptions;
using Narato.Common.Interfaces;
using Narato.Common.Factory;
using Autofac.Extensions.DependencyInjection;
using Autofac;
using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Tenancy.Mappers;
using Digipolis.DossierEngine.Domain.Clients;
using Digipolis.DossierEngine.Domain.Managers;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Mappers;
using Digipolis.DossierEngine.Domain.Providers;
using Digipolis.DossierEngine.Domain.Factories;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Digipolis.DossierEngine.Domain.Validators;
using Digipolis.DossierEngine.DataProvider.Mappers;
using Digipolis.DossierEngine.Common.Util;
using Digipolis.DossierEngine.DataProvider.Helpers;
using Newtonsoft.Json;
using Digipolis.Common.Events;
using System.Net.Http;
using Digipolis.Common.Events.Config;
using Digipolis.DossierEngine.Domain.Search.Cypher;
using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Digipolis.DossierEngine.Common.OperationFilters;
using Digipolis.Common.Tenancy.Providers;
using System.Collections.Generic;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Digipolis.DossierEngine.Common.Dictionaries;
using Swashbuckle.Swagger.Model;
using Digipolis.DossierEngine.Common.Events;
using Digipolis.Common.Status.Models;
using NLog.Extensions.Logging;
using Digipolis.DossierEngine.Common.Pooling;
using Narato.Common.Mappers;
using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Neo4jClient;

namespace Digipolis.DossierEngine.DataObject.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("config.json")
                .AddJsonFile("config.json.local", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.Configure<SystemConfiguration>(Configuration.GetSection("SystemConfiguration"));
            services.Configure<EventHandlerConfiguration>(Configuration.GetSection("EventHandlerConfiguration"));
            services.Configure<DossierConfiguration>(Configuration.GetSection("DossierConfiguration"));

            // Add framework services.
            services.AddMvc(
            //Add this filter globally so every request runs this filter to recored execution time
            config =>
            {
                config.Filters.Add(new ExecutionTimingFilter());
                config.Filters.Add(new ModelValidationFilter());
            })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    x.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                });

            services.AddMemoryCache();

            //Swagger configuation + swagger UI
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "DataObject API",
                    Version = "v1",
                    Title = "Dossier Engine"
                });
                options.OperationFilter<ProducesConsumesFilter>();

                var xmlPaths = GetXmlCommentsPaths();
                foreach (var entry in xmlPaths)
                {
                    try
                    {
                        options.IncludeXmlComments(entry);
                    }
                    catch (Exception e)
                    {
                        //TODO: Automate the generation and copying of the tenant library xmldocs for swagger
                    }
                }
            });

            var engineConfigSection = Configuration.GetSection("DossierConfiguration");
            var adminApiEndpoint = engineConfigSection.GetValue<string>("AdminApiEndpoint");
            var dataTypeApiEndpoint = engineConfigSection.GetValue<string>("DataTypeApiEndpoint");

            var eventHandlerConfigSection = Configuration.GetSection("EventHandlerConfiguration");
            var eventHandlerEndpoint = eventHandlerConfigSection.GetValue<string>("EventHandlerEndpoint");

            //Confgure Autofac
            var builder = new ContainerBuilder();

            builder.RegisterType<DataObjectDataProvider>().As<IDataObjectDataProvider>();
            builder.RegisterType<DataObjectMapper>().As<IDataObjectMapper>();
            builder.RegisterType<DataObjectManager>().As<IDataObjectManager>();
            builder.RegisterType<DataObjectQueryProvider>().As<IDataObjectQueryProvider>();
            builder.RegisterType<SyncedDataObjectManager>().As<ISyncedDataObjectManager>();
            builder.RegisterType<SyncedDataObjectDataProvider>().As<ISyncedDataObjectDataProvider>();
            builder.RegisterType<DataObjectChangeCalculator>().As<IChangeCalculator<Model.Models.DataObject>>();
            builder.RegisterType<SearchManager>().As<ISearchManager>();

            services.AddSingleton<IGroupedLimitedPool<string, HttpClient>, GroupedLimitedPool<string, HttpClient>>(
                c =>
                    new GroupedLimitedPool<string, HttpClient>(
                        (string group) => new HttpClient() { BaseAddress = new Uri(group) },
                        (client) => client.Dispose()
                    )
            );

            services.AddSingleton<IGroupedLimitedPool<string, IGraphClient>, GroupedLimitedPool<string, IGraphClient>>(
                c =>
                {
                    var graphClientFactory = c.GetService<ITenantGraphClientFactory>();
                    return new GroupedLimitedPool<string, IGraphClient>(
                        async (string group) => await graphClientFactory.CreateAsync(),
                        (graphClient) => graphClient.Dispose()
                    );
                }
            );

            builder.RegisterType<TenantClient>().As<ITenantClient>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) });
            builder.RegisterType<PluginClient>().As<IPluginClient>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(adminApiEndpoint) });
            builder.RegisterType<DataTypeClient>().As<IDataTypeClient>();
            builder.RegisterType<CurrentTenantProvider>().As<ICurrentTenantProvider<Config>>();
            builder.RegisterType<TenantGraphClientFactory>().As<ITenantGraphClientFactory>();
            builder.RegisterType<PooledTenantGraphClientProvider>().As<IPooledGraphClientProvider>();
            builder.RegisterType<DataObjectValidator>().As<IDataObjectValidator>();

            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>();
            builder.RegisterType<ExceptionToActionResultMapper>().As<IExceptionToActionResultMapper>();
            builder.RegisterType<ConfigMapper<Config>>().As<IConfigMapper<Config>>();
            builder.RegisterType<ResponseFactory>().As<IResponseFactory>();
            builder.RegisterType<SystemStatusDataObjectManager>().As<ISystemStatusManager>().WithParameter("tenantClient", new TenantClient(new System.Net.Http.HttpClient() { BaseAddress = new Uri(adminApiEndpoint) }));

            if (string.IsNullOrEmpty(eventHandlerEndpoint))
            {
                builder.RegisterType<NoOpEventHandler>().As<IEventPublisher>();
            }
            else
            {
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventPublisher>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(eventHandlerEndpoint) });
            }


            // conditions
            builder.RegisterType<CypherQueryBuilder>().As<ICypherQueryBuilder>();
            builder.RegisterType<ConditionBuilder>().As<IConditionBuilder>();

            // utils
            builder.RegisterType<StringHelper>().AsSelf();
            builder.RegisterType<NestedDataObjectHelper>().AsSelf();
            builder.RegisterType<DictionaryComparer<string, object>>().As<IEqualityComparer<IDictionary<string, object>>>();
            builder.RegisterType<ComparisonHelper>().AsSelf();
            builder.RegisterType<DictionaryComparisonHelper<string, object>>().AsSelf();

            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            app.UseMvc();
        }

        private List<string> GetXmlCommentsPaths()
        {
            var app = PlatformServices.Default.Application;
            var files = new List<string>()
                        {
                            "Digipolis.DossierEngine.DataObject.API.xml"
                        };

            List<string> paths = new List<string>();
            foreach (var file in files)
            {
                paths.Add(Path.Combine(app.ApplicationBasePath, file));
            }

            return paths;
        }
    }
}
