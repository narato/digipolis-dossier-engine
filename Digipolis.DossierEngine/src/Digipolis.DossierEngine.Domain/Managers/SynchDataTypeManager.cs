﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class SynchDataTypeManager : ISyncedDataTypeManager
    {
        private IPluginDataProvider _pluginDataProvider;
        public SynchDataTypeManager(IPluginDataProvider pluginDataProvider)
        {
            _pluginDataProvider = pluginDataProvider;
        }

        public List<List<DataType>> GetAllSynchDataTypes()
        {
            return _pluginDataProvider.GetAllSynchDataTypes();
        }
    }
}
