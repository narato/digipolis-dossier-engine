﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Ssh.Interfaces;
using Renci.SshNet;

namespace Digipolis.DossierEngine.Domain.Ssh
{
    public class IneoCommandRunner : IIneoCommandRunner
    {
        public SshCommand CreateNeo4jInstance(ISshClient client, string ineoHome, int instanceHttpPort, string instanceName)
        {
            return client.RunCommand($"INEO_HOME={ineoHome} {ineoHome}/bin/ineo create -a -p {instanceHttpPort} {instanceName}");
        }

        public SshCommand StartNeo4jInstance(ISshClient client, string ineoHome, string instanceName)
        {
            return client.RunCommand($"INEO_HOME={ineoHome} {ineoHome}/bin/ineo start {instanceName}");
        }

        public SshCommand StopNeo4jInstance(ISshClient client, string ineoHome, string instanceName)
        {
            return client.RunCommand($"INEO_HOME={ineoHome} {ineoHome}/bin/ineo stop {instanceName}");
        }

        public SshCommand SetPasswordForNeo4jInstance(ISshClient client, int instanceHttpPort, string password)
        {
            return client.RunCommand($"curl -s -X POST http://neo4j:neo4j@127.0.0.1:{instanceHttpPort}/user/neo4j/password -d 'password={password}'");
        }
    }
}
