﻿using System.Threading.Tasks;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.Tenant.Library.Managers;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class TimeSeriesManager : ITimeSeriesManager
    {
        private readonly ITimeSeriesDataProvider _timeSeriesDataProvider;
        private readonly ITenantManager<Config> _tenantManager;

        public TimeSeriesManager(ITimeSeriesDataProvider timeSeriesDataProvider, ITenantManager<Config> tenantManager)
        {
            _timeSeriesDataProvider = timeSeriesDataProvider;
            _tenantManager = tenantManager;
        }

        public async Task<string> InsertTimeSeriesForTenantForYearAsync(string tenantKey, int year)
        {
            var tenant = await _tenantManager.GetTenantByKeyAsync(tenantKey);
            _timeSeriesDataProvider.InsertTimeSeriesForYear(tenant.Config, year);
            return "OK";
        }
    }
}
