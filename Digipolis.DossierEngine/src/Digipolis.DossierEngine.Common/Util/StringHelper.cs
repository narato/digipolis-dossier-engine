﻿using System.Text.RegularExpressions;

namespace Digipolis.DossierEngine.Common.Util
{
    public class StringHelper
    {
        public virtual string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9]+", "", RegexOptions.Compiled);
        }

        public virtual string ConcatenateWithDelimiter(string original, string toConcatenate, char delimiter = ',')
        {
            if (toConcatenate.Length == 0)
            {
                return original;
            }
            return $"{original}{delimiter} {toConcatenate}";
        }
    }
}
