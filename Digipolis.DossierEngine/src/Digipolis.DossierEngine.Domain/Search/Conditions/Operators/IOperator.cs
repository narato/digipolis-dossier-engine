﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Operators
{
    public interface IOperator
    {
        string Value { get; }
    }
}
