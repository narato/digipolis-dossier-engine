﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Digipolis.DossierEngine.Domain.Interfaces;
using Models = Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Linq;
using Digipolis.DossierEngine.Model.Dto;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataObject.API.Controllers
{
    [Route("api/")]
    public class DataObjectController : Controller
    {
        private IDataObjectManager _dataObjectManager;
        private IResponseFactory _responseFactory;

        public DataObjectController(IDataObjectManager dataObjectManager, IResponseFactory responseFactory)
        {
            _dataObjectManager = dataObjectManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Returns a dataobject by id
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="id">The dataObject to create</param>
        /// <returns>A response containing the dataobject</returns>
        // GET api/dataobjects/{id}
        [ProducesResponseType(typeof(Response<Models.DataObject>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet("dataobjects/{id}", Name = "GetDataObjectById")]
        public async Task<IActionResult> GetAsync([Required]Guid id, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.QuerystringParam));

            return await _responseFactory.CreateGetResponseAsync(async () => await _dataObjectManager.GetDataObjectByIdAsync(id),
                this.GetRequestUri());
        }

        /// <summary>
        /// Deletes a dataobject by id
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="id">The dataObject to delete</param>
        /// <returns>A statuscode indicating the result</returns>
        /// <response code="204">Deleted sucessfully</response>
        // DELETE api/dataobjects/{id}
        [ProducesResponseType(typeof(void), (int)System.Net.HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(void), (int)System.Net.HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.NotFound)]
        [HttpDelete("dataobjects/{id}")]
        public async Task<IActionResult> DeleteAsync([Required]Guid id, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.QuerystringParam));

            return await _responseFactory.CreateDeleteResponseAsync(async () => await _dataObjectManager.DeleteDataObjectByIdAsync(id),
                this.GetRequestUri());
        }

        /// <summary>
        /// Creates a dataobject
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="dataObjectDTO">The dataobject to create</param>
        /// <returns>A statuscode indicating the result</returns>
        [ProducesResponseType(typeof(Response<Models.DataObject>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPost("dataobjects")]
        public async Task<IActionResult> InsertDataObjectAsync([FromBody] [Required]  DataObjectDTO dataObjectDTO, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataObjectDTO == null)
                missingParams.Add(new MissingParam("dataobject", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            Models.DataObject dataObject = new Models.DataObject() { Name = dataObjectDTO.Name, DataTypeId = dataObjectDTO.DataTypeId, Values = dataObjectDTO.Values };

            Func<Models.DataObject, object> pathMapper = (data) => { 
                return new { controller = "DataObject", id = data.Id, tenantKey = tenantKey }; 
            };

            return await _responseFactory.CreatePostResponseAsync(async () => await _dataObjectManager.InsertDataObjectAsync(dataObject), this.GetRequestUri(),
                        "GetDataObjectById", pathMapper);
        }

        /// <summary>
        /// Updates a dataobject
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="dataObject">The dataobject to update</param>
        /// <param name="id">The id of the dataobject to update</param>
        /// <returns>A statuscode indicating the result</returns>
        [ProducesResponseType(typeof(Response<Models.DataObject>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPut("dataobjects/{id}")]
        public async Task<IActionResult> UpdateDataObjectAsync([Required]Guid id, [FromBody] [Required] Models.DataObject dataObject, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataObject == null)
                missingParams.Add(new MissingParam("dataobject", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePostResponseAsync(async () => await _dataObjectManager.UpdateDataObjectForIdAsync(id, dataObject), this.GetRequestUri());
        }

        /// <summary>
        /// Returns dataobjects for a given dataTypeId
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="dataTypeId">The id of the DataType to get the DataObjects for</param>
        /// <param name="page">The page number to be returned. Optional. Default 1</param>
        /// <param name="pagesize">The number of records to be returned per page. Optional. Default 10</param>
        /// <param name="orderbyField">the field on which to order. This is optional</param>
        /// <param name="descending">whether the field has to be ordered descending (default is ascending)</param>
        /// <returns>A response containing the dataobjects for the given datatype</returns>
        // GET api/datatypes/{id}/dataobjects
        [ProducesResponseType(typeof(Response<Models.DataObject>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet("datatypes/{dataTypeId}/dataobjects", Name = "GetDataObjectForDataType")]
        public async Task<IActionResult> GetDataObjectsForDataTypeAsync([Required]Guid dataTypeId, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey, [FromQuery] int page = 1, [FromQuery] int pagesize = 10, [FromQuery] string orderbyField = null, [FromQuery] bool descending = false)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));
            
            return await _responseFactory.CreateGetResponseForCollectionAsync(async () => await _dataObjectManager.GetDataObjectsForDataTypeIdAsync(dataTypeId, page, pagesize, orderbyField, descending),
                this.GetRequestUri());
        }

        /// <summary>
        /// Gets the history of a DataObject
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="id">The id of the dataobject</param>
        /// <returns>A List of changesets</returns>
        [ProducesResponseType(typeof(Response<List<Models.ChangeSet>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet("dataobjects/{id}/changesets")]
        public async Task<IActionResult> GetHistoryOfDataObjectById([Required]Guid id, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _dataObjectManager.GetHistoryGetHistoryOfDataObjectByIdAsync(id), this.GetRequestUri());
        }

        [ApiExplorerSettings(IgnoreApi = true)] // TODO: this is a temp solution for the lack of real linked dataObjects
        [HttpPost("dataobjects/idList")]
        public async Task<IActionResult> GetDataObjectsByIdList([Required][FromBody]List<Guid> ids, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _dataObjectManager.GetDataObjectsByIdListAsync(ids), this.GetRequestUri());
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("datatypes/{dataTypeId}/dataobjects/ids")]
        public async Task<IActionResult> GetDataObjectsIdsForDataTypeAsync([Required]Guid dataTypeId, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => await _dataObjectManager.GetDataObjectIdsForDataTypeIdAsync(dataTypeId),
                this.GetRequestUri());
        }
    }
}
