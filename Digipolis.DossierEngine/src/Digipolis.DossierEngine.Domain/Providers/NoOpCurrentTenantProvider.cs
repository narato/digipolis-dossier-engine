﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.Common.Tenancy.Providers;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Providers
{
    public class NoOpCurrentTenantProvider : ICurrentTenantProvider<Config>
    {
        public Tenant<Config> GetCurrentTenant(bool force = false)
        {
            return null;
        }

        public Task<Tenant<Config>> GetCurrentTenantAsync(bool force = false)
        {
            return null;
        }

        public string GetCurrentTenantKey()
        {
            return null;
        }
    }
}
