﻿using Narato.Common.Models;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.Domain.Paging
{
    public static class PagedCollectionResponseExtension
    {
        public static PagedCollectionResponse<IEnumerable<T>> ToPagedCollectionResponse<T>(this PagedList<T> pagedData)
        {
            var response = new PagedCollectionResponse<IEnumerable<T>>();
            response.Data = pagedData;
            response.Total = pagedData.TotalCount;
            response.Skip = pagedData.Skip;
            response.Take = pagedData.Count;
            return response;
        }

        public static PagedCollectionResponse<IEnumerable<T>> ToPagedCollectionResponse<T>(this IEnumerable<T> source, int page, int pageSize)
        {
            var response = new PagedCollectionResponse<IEnumerable<T>>();
            response.Data = source;
            response.Skip = (page - 1) * pageSize;
            response.Take = source.Count();
            // you have to set the total yourself
            return response;
        }
    }
}
