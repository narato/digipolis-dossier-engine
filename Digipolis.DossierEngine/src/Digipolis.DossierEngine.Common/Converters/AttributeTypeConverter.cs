﻿using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;

namespace Digipolis.DossierEngine.Common.Converters
{
    public class AttributeTypeConverter:JsonConverter
    {
        private readonly bool _tryPopulate;

        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public AttributeTypeConverter(bool tryPopulate = false)
        {
            _tryPopulate = tryPopulate;
        }

        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var attributeType = default(BaseAttributeType);
            string type = jsonObject["type"].Value<string>();

            switch (type)
            {
                case StringAttributeType.TypeName:
                    attributeType = new StringAttributeType();
                    break;
                case BooleanAttributeType.TypeName:
                    attributeType = new BooleanAttributeType();
                    break;
                case IntAttributeType.TypeName:
                    attributeType = new IntAttributeType();
                    break;
                case LinkAttributeType.TypeName:
                    attributeType = new LinkAttributeType();
                    break;
                case DateTimeAttributeType.TypeName:
                    attributeType = new DateTimeAttributeType();
                    break;
                case LinkDataTypeAttributeType.TypeName:
                    attributeType = new LinkDataTypeAttributeType();
                    break;
                case DoubleAttributeType.TypeName:
                    attributeType = new DoubleAttributeType();
                    break;
            }
            if (_tryPopulate)
            {
                try
                {
                    serializer.Populate(jsonObject.CreateReader(), attributeType);
                } catch (Exception)
                {
                    Logger.Warn("couldn't populate following attribute: " + jsonObject.ToString());
                }
            } else
            {
                serializer.Populate(jsonObject.CreateReader(), attributeType);
            }

            attributeType.Name = ConvertFirstCharToLower(attributeType.Name);

            return attributeType;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IAttributeType);
        }

        private string ConvertFirstCharToLower(string input)
        {
            return char.ToLowerInvariant(input[0]) + input.Substring(1);
        }
    }
}
