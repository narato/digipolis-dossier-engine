﻿using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class TimeSeriesDataProvider : ITimeSeriesDataProvider
    {
        private readonly ITenantGraphClientFactory _graphClientFactory;

        public TimeSeriesDataProvider(ITenantGraphClientFactory graphClientFactory)
        {
            _graphClientFactory = graphClientFactory;
        }

        public void InsertTimeSeriesForYear(Config tenantConfig, int year)
        {
            using (var client = _graphClientFactory.CreateForConfig(tenantConfig))
            {
                client.Connect();

                var query = new CypherQuery("MATCH (y:Year) RETURN COUNT(y) as count", null, CypherResultMode.Set);
                var yearsInSeries = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).First();

                //Time series
                var queryParams = new Dictionary<string, object>();
                queryParams.Add("year", year);

                var queryTimeseries = new CypherQuery(TimeSeriesQuery(), queryParams, CypherResultMode.Set);
                var result = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryTimeseries);
                var yearCount = result.First();
                if (yearCount != (yearsInSeries + 1))
                    throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "The year you tried to insert aleady exists." });
            }
        }

        private string TimeSeriesQuery()
        {
            return @"WITH {year} as insertedYear, range(1, 12) as months
                    MERGE (y:Year {value: insertedYear})
                    FOREACH (month IN months |
	                    CREATE UNIQUE (y)-[:CONTAINS]->(m:Month {value: month})
	                    FOREACH(day IN (
		                    CASE
			                    WHEN month IN [1,3,5,7,8,10,12] THEN range(1,31)
			                    WHEN month = 2 THEN
			                    CASE
				                    WHEN insertedYear % 4 <> 0 THEN range(1,28)
				                    WHEN insertedYear % 100 = 0 AND insertedYear % 400 = 0 THEN range(1,29)
				                    ELSE range(1,28)
			                    END
			                    ELSE range(1,30)
		                    END) |
		                    CREATE UNIQUE (m)-[:CONTAINS]->(d:Day {value: day})
	                    )
                    )

                    WITH *
                    // connect years, but only if the difference is 1 year
                    MATCH (year:Year)
                    WITH year
                    ORDER BY year.value
                    WITH collect(year) AS years
                    FOREACH(i in RANGE(0, size(years)-2) |
                        FOREACH(year1 in [years[i]] |
                            FOREACH(year2 in [years[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN year2.value - year1.value = 1 THEN [1] ELSE [] END | CREATE UNIQUE (year1)-[:NEXT]->(year2))
                            )
                        )
                    )
                    WITH *
                    // connect months, but ONLY when the following month is within the same year or in the following year. Not if there's a gap in years
                    MATCH (year:Year)-[:CONTAINS]->(month)
                    WITH year, month
                    ORDER BY year.value, month.value
                    WITH collect(month) AS months, collect(year) as years
                    FOREACH(i in RANGE(0, size(months)-2) |
                        FOREACH(month1 in [months[i]] |
                            FOREACH(month2 in [months[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN (head([years[i + 1]]).value = head([years[i]]).value) OR (head([years[i + 1]]).value - head([years[i]]).value = 1) THEN [1] ELSE [] END | CREATE UNIQUE (month1)-[:NEXT]->(month2))
                            )
                        )
                    )
                    WITH *
                    // connect days, but ONLY when the following day is within the same year or in the following year. Not if there's a gap in years
                    MATCH (year:Year)-[:CONTAINS]->(month)-[:CONTAINS]->(day)
                    WITH year, month, day
                    ORDER BY year.value, month.value, day.value
                    WITH collect(day) AS days, collect(year) as years
                    FOREACH(i in RANGE(0, size(days)-2) |
                        FOREACH(day1 in [days[i]] |
                            FOREACH(day2 in [days[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN (head([years[i + 1]]).value = head([years[i]]).value) OR (head([years[i + 1]]).value - head([years[i]]).value = 1) THEN [1] ELSE [] END | CREATE UNIQUE (day1)-[:NEXT]->(day2))
            
                            )
                        )
                    )

                    WITH *
                    MATCH (y:Year)
                    RETURN COUNT(y) as count";
        }
    }
}
