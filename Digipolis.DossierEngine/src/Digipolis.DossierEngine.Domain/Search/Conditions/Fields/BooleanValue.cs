﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public class BooleanValue : IValue<bool>
    {
        public string Type { get; } = "bool";
        public bool Value { get; }

        public BooleanValue(bool value)
        {
            Value = value;
        }
    }
}
