﻿using Digipolis.DossierEngine.DataProvider.Managers;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Digipolis.Tenant.Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class MigrationManagerTest
    {
        [Fact]
        public void CreatesDbWhenItDoesNotExist()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AdminDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            AdminDbContext tenantDbContext = new AdminDbContext(optionsBuilder.Options);

            Mock<IOptions<TenantConfiguration>> mockConfig = new Mock<IOptions<TenantConfiguration>>();
            mockConfig.SetupGet(config => config.Value).Returns(new TenantConfiguration() { ConnectionString = "Server = 192.168.99.100; Port = 5432; Database = Admin; User Id = postgres; Password = mypass; " } );
          
            Mock<IQueryExecutor> mockQueryExecutor = new Mock<IQueryExecutor>();
            mockQueryExecutor.Setup(x => x.FindDatabase(It.IsAny<string>(), It.IsAny<string>())).Returns(null);
            mockQueryExecutor.Setup(x => x.CreateDatabase(It.IsAny<string>(), It.IsAny<string>()));
            mockQueryExecutor.Setup(x => x.MigrateTenantDb(It.IsAny<TenantConfiguration>())).Returns("Migrations successfully executed");

            var migMan = new MigrationManager(mockConfig.Object, mockQueryExecutor.Object);

            migMan.ExecutePendingMigrations();
            mockQueryExecutor.Verify(x => x.FindDatabase(It.IsAny<string>(),"Admin"), Times.Once);
            mockQueryExecutor.Verify(x => x.CreateDatabase(It.IsAny<string>(), "Admin"), Times.Once);
        }
        
        [Fact]
        public void DoesNotCreateDbWhenItAlreadyExists()
        {
            Mock<IOptions<TenantConfiguration>> mockConfig = new Mock<IOptions<TenantConfiguration>>();
            mockConfig.SetupGet(config => config.Value).Returns(new TenantConfiguration() { ConnectionString = "Server = 192.168.99.100; Port = 5432; Database = Admin; User Id = postgres; Password = narato; " });

            Mock<IQueryExecutor> mockQueryExecutor = new Mock<IQueryExecutor>();
            mockQueryExecutor.Setup(x => x.FindDatabase(It.IsAny<string>(), It.IsAny<string>())).Returns("Anything");
            mockQueryExecutor.Setup(x => x.CreateDatabase(It.IsAny<string>(), It.IsAny<string>()));
            mockQueryExecutor.Setup(x => x.MigrateTenantDb(It.IsAny<TenantConfiguration>())).Returns("Migrations successfully executed");

            var migMan = new MigrationManager(mockConfig.Object, mockQueryExecutor.Object);

            migMan.ExecutePendingMigrations();
            mockQueryExecutor.Verify(x => x.FindDatabase(It.IsAny<string>(), "Admin"), Times.Once);
            mockQueryExecutor.Verify(x => x.CreateDatabase(It.IsAny<string>(), "Admin"), Times.Never);
        }
    }
}
