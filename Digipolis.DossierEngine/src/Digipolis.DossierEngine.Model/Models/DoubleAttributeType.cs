﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class DoubleAttributeType : BaseAttributeType
    {
        public const string TypeName = "Double";

        public DoubleAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
        public double? MinRangeValidation { get; set; }
        public double? MaxRangeValidation { get; set; }
    }
}