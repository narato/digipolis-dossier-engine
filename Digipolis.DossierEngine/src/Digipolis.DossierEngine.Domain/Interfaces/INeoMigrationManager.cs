﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface INeoMigrationManager
    {
        void SetupDatabase(Tenant<Config> tenant);
    }
}