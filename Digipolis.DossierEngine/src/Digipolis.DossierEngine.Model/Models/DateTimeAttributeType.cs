﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class DateTimeAttributeType : BaseAttributeType 
    {
        public const string TypeName = "DateTime";

        public DateTimeAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
    }
}