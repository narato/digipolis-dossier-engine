﻿namespace Digipolis.DossierEngine.Common.Models
{
    public class Config
    {
        public int HttpPort { get; set; }
        public int HttpsPort { get; set; }
        public int BoltPort { get; set; }
        public int SshPort { get; set; }
        public string SshUsername { get; set; }
        public string SshPassword { get; set; }
        public string Neo4JServer { get; set; }
    }
}