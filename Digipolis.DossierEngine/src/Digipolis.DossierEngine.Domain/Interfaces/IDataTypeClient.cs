﻿using Digipolis.DossierEngine.Model.Models;
using System;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataTypeClient
    {
        Task<DataType> GetDataTypeById(Guid id, string tenantKey);
        Task<DataType> GetDataTypeByName(string name, string tenantKey);
        Task<DataType> GetDataTypeTreeById(Guid id, string tenantKey);

        Task<DataType> InsertSyncedDataTypeAsync(string dataTypeJson, string tenantKey);
        Task<DataType> InsertDataTypeAsync(string dataTypeJson, string tenantKey);

    }
}