﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public class Field : Comparable
    {
        public string Name { get; }
        public string Type { get; }

        public Field(string name, string type = "string")
        {
            Name = name;
            Type = type;
        }
    }
}
