﻿using Digipolis.DossierEngine.Domain.Search.Conditions.Visitor;
using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using Digipolis.DossierEngine.Domain.Search.Conditions.Operators;

namespace Digipolis.DossierEngine.Domain.Search.Conditions
{
    public class Comparison : IComparison
    {
        public virtual Comparable LeftHandSide { get; }
        public virtual IOperator Operator { get; }
        public virtual Comparable RightHandSide { get; }

        public Comparison(Comparable leftHandSide, IOperator op, Comparable rightHandSide)
        {
            LeftHandSide = leftHandSide;
            Operator = op;
            RightHandSide = rightHandSide;
        }

        public string visit(IConditionVisitor visitor)
        {
            return visitor.WalkComparison(this, false); // we assume it's a stand-alone comparison
        }

        public string visit(IConditionVisitor visitor, bool partOfComposite)
        {
            return visitor.WalkComparison(this, partOfComposite);
        }
    }
}
