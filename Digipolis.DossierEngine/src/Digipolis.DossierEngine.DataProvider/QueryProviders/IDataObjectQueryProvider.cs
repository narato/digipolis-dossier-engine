﻿using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.Model.Models;
using Neo4jClient.Cypher;

namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public interface IDataObjectQueryProvider
    {
        ICypherFluentQuery UpdateDataObjectWithChange(DataObject dataObject, ICypherFluentQuery query, Change change);
    }
}
