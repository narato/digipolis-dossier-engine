﻿using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IPluginManager
    {
        string LoadPlugins();
        List<Plugin> GetPlugins();
        Task<TenantPluginLink> InsertTenantPluginLinkAsync(TenantPluginLinkDTO tenantPluginLink);
        string ProcessEvent(string pluginName, object payload);
        string UpdateExternalDataObjectByExternalId(PluginObjectLookupDto pluginObjectLookup);
        bool DeleteTenantPluginLink(Guid linkId);
        Task<List<TenantPluginLink>> GetPluginLinksForTenantAsync(string tenantKey);
    }
}
