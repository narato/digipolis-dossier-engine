#!/bin/bash

dotnet build src/Digipolis.DossierEngine.Admin.API/
dotnet publish src/Digipolis.DossierEngine.Admin.API/ -o build/admin
mkdir build/admin/Plugins
