﻿using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.Model.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using Digipolis.DossierEngine.Common.Dictionaries;

namespace Digipolis.DossierEngine.Domain.Changes
{
    // TODO: rename to more appropriate name? couldn't think of anything better
    public class DataObjectChangeCalculator : IChangeCalculator<DataObject>
    {
        private readonly DictionaryComparisonHelper<string, object> _dictionaryComparisonHelper;

        public DataObjectChangeCalculator(DictionaryComparisonHelper<string, object> dictionaryComparisonHelper)
        {
            _dictionaryComparisonHelper = dictionaryComparisonHelper;
        }

        // TODO : support nested DataObjects!
        public IEnumerable<Change> CalculateChangeSet(DataObject oldObject, DataObject newObject)
        {
            var oldValues = DictionarizeNested(oldObject.Values);
            var newValues = DictionarizeNested(newObject.Values);

            return CalculateChangeSet(oldValues, newValues, "");
        }

        private IEnumerable<Change> CalculateChangeSet(IDictionary<string, object> oldValues, IDictionary<string, object> newValues, string attributePrefix)
        {
            var returnValue = new List<Change>();

            foreach (var pair in oldValues)
            {
                if (pair.Key != "externalId" && (!newValues.ContainsKey(pair.Key) || newValues[pair.Key] == null))
                {
                    returnValue.Add(new Change
                    {
                        Type = ChangeType.DELETED,
                        Attribute = attributePrefix + pair.Key
                    });
                }
                else if (newValues.ContainsKey(pair.Key))
                {
                    // nested object
                    if (newValues[pair.Key] is IDictionary<string, object>)
                    {
                        var nestedChanges = CalculateChangeSet((IDictionary<string, object>)pair.Value, (IDictionary<string, object>)newValues[pair.Key], attributePrefix + pair.Key + ".");
                        returnValue.AddRange(nestedChanges); // don't add the actual value now, because this AttributeValue doesn't actually have a value.
                    }
                    // nested list of objects
                    else if (newValues[pair.Key] is List<IDictionary<string, object>>)
                    {
                        if (!_dictionaryComparisonHelper.Equals((IEnumerable<IDictionary<string, object>>)pair.Value, (IEnumerable<IDictionary<string, object>>)newValues[pair.Key]))
                        {
                            // if it's nested array of object, add all subvalues
                            returnValue.Add(new Change
                            {
                                Type = ChangeType.NEW, // THIS IS NOT A BUG!!! because of the fact that we remake every DO in the array as soon as something changes, this can be handled by the Create code
                                Attribute = attributePrefix + pair.Key,
                                NewValue = AddNewNestedObjectList((List<IDictionary<string, object>>)newValues[pair.Key], attributePrefix + pair.Key)
                            });
                        }
                    }
                    // nested list of scalars
                    else if (newValues[pair.Key] is List<object>)
                    {
                        // fancy way for doing list1 <> list 2 (list1 except list2 together with list2 except list1 should have more than 0 elements)
                        if (((List<object>)pair.Value).Except((List<object>)newValues[pair.Key]).Concat(((List<object>)newValues[pair.Key]).Except((List<object>)pair.Value)).Count() > 0)
                        {
                            returnValue.Add(new Change
                            {
                                Type = ChangeType.UPDATED,
                                Attribute = attributePrefix + pair.Key,
                                NewValue = newValues[pair.Key]
                            });
                        }
                    }
                    else if ( (pair.Value != null && !pair.Value.Equals(newValues[pair.Key])) || (pair.Value == null && newValues[pair.Key] != null) )
                    {
                        // if pair.Value is a dictionary here, it means the new value isn't a dictionary.
                        // if that's the case (and assuming validation already passed), this means we're dealing
                        // with a synced DO. This can't be updated manually, only its externalId can be updated
                        if (pair.Value is IDictionary<string, object>)
                        {
                            var syncedDO = pair.Value as IDictionary<string, object>;
                            if (! syncedDO.ContainsKey("externalId"))
                            {
                                throw new Exception("no external Id present in following 'sync' DO: " + pair.Key);
                            }
                            // different externalId!!!
                            if (! syncedDO["externalId"].ToString().Equals(newValues[pair.Key].ToString()))
                            {
                                returnValue.Add(new Change
                                {
                                    Type = ChangeType.UPDATED,
                                    Attribute = attributePrefix + pair.Key,
                                    NewValue = new SyncChange(newValues[pair.Key].ToString())
                                });
                            }
                        }
                        else
                        {
                            returnValue.Add(new Change
                            {
                                Type = ChangeType.UPDATED,
                                Attribute = attributePrefix + pair.Key,
                                NewValue = newValues[pair.Key]
                            });
                        }
                    }
                }
            }

            foreach (var pair in newValues)
            {
                if (!oldValues.ContainsKey(pair.Key))
                {
                    if (pair.Value is IDictionary<string, object>)
                    {
                        // if it's nested object, add all subvalues
                        returnValue.Add(new Change
                        {
                            Type = ChangeType.NEW,
                            Attribute = attributePrefix + pair.Key,
                            NewValue = AddNewNestedObject((IDictionary<string, object>)pair.Value)
                        });
                    }
                    else if (pair.Value is List<IDictionary<string, object>>)
                    {
                        // if it's nested array of object, add all subvalues
                        returnValue.Add(new Change
                        {
                            Type = ChangeType.NEW,
                            Attribute = attributePrefix + pair.Key,
                            NewValue = AddNewNestedObjectList((List<IDictionary<string, object>>)pair.Value, attributePrefix + pair.Key)
                        });
                    }
                    else if (! (pair.Value is List<object> && ((List<object>)pair.Value).Count == 0) && pair.Value != null) // if a list is EMPTY, it shouldnt be inserted
                    {
                        // TODO: maybe allow null values after all? and introduce an isNull property on the node. We cannot search for null values now...
                        // TODO: bugfix: in case of a syncedDataObject, it'll go wrong here, only the externalId will be inserted as a normal value
                        returnValue.Add(new Change
                        {
                            Type = ChangeType.NEW,
                            Attribute = attributePrefix + pair.Key,
                            NewValue = pair.Value
                        });
                    }
                }
            }
            return returnValue;
        }

        private IEnumerable<Change> AddNewNestedObject(IDictionary<string, object> values)
        {
            var returnValue = new List<Change>();
            foreach (var pair in values)
            {
                if (pair.Value is IDictionary<string, object>)
                {
                    returnValue.Add(new Change
                    {
                        Type = ChangeType.NEW,
                        Attribute = pair.Key,
                        NewValue = AddNewNestedObject((IDictionary<string, object>)pair.Value) // rather than add add all the changes in the root level, for a create, we'll actually nest them
                    });
                } else if (pair.Value is IList<IDictionary<string, object>>) {
                    returnValue.Add(new Change
                    {
                        Type = ChangeType.NEW,
                        Attribute = pair.Key,
                        NewValue = AddNewNestedObjectList((List<IDictionary<string, object>>)pair.Value, pair.Key)
                    });
                }
                else
                {
                    returnValue.Add(new Change
                    {
                        Type = ChangeType.NEW,
                        Attribute = pair.Key,
                        NewValue = pair.Value
                    });
                }
            }
            return returnValue;
        }

        private NestedChangeList AddNewNestedObjectList(List<IDictionary<string, object>> values, string attributePrefix)
        {
            var nestedObjects = new List<Change>();

            var counter = 0;
            foreach (var value in values)
            {
                nestedObjects.Add(new Change
                {
                    Type = ChangeType.NEW,
                    Attribute = attributePrefix + "[" + counter++ + "]",
                    NewValue = AddNewNestedObject(value)
                });
            }

            return new NestedChangeList(nestedObjects);
        }

        private IDictionary<string, object> DictionarizeNested(IDictionary<string, object> dict)
        {
            var returnValue = new Dictionary<string, object>();
            foreach (var pair in dict)
            {
                // nested dataObject (note that a list of nested DataObjects is NOT a JObject, but a JArray
                if (pair.Value is JObject)
                {
                    var value = ((JObject)pair.Value).ToObject<IDictionary<string, object>>();
                    returnValue.Add(pair.Key, DictionarizeNested(value));
                } else if (pair.Value is JArray) // list of dataObjects or normal properties
                {
                    // here we have to differentiate between normal properties and dataObjects, because 
                    // normal properties actually get injected in one property of one node, whereas
                    // dataObjects each get their own DataObject in the db
                    if (((JArray)pair.Value).First is JObject)
                    {
                        var values = new List<IDictionary<string, object>>();
                        foreach (var value in (JArray)pair.Value)
                        {
                            if (!(value is JObject))
                            {
                                throw new Exception("All elements in this list should be objects: " + pair.Key);
                            }
                            var rawDictionary = value.ToObject<IDictionary<string, object>>();
                            values.Add(DictionarizeNested(rawDictionary));
                        }
                        returnValue.Add(pair.Key, values);
                    } else
                    {
                        var values = new List<object>();
                        foreach (var value in (JArray)pair.Value)
                        {
                            values.Add(value.ToObject<object>());
                        }
                        returnValue.Add(pair.Key, values);
                    }
                    
                } else // all normal properties
                {
                    returnValue.Add(pair.Key, pair.Value);
                }
            }
            return returnValue;
        }
    }
}
