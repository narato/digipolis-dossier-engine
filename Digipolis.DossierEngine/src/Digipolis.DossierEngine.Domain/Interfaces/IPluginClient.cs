﻿using Digipolis.DossierEngine.Domain.Dto;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IPluginClient
    {
        void SyncUnsyncedSyncDataObject(PluginObjectLookupDto pluginObjectLookup);
    }
}
