﻿using Digipolis.Tenant.Library.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;

namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public class QueryExecutor : IQueryExecutor
    {
        private IMigrationQueryProvider _queryProvider;

        public QueryExecutor(IMigrationQueryProvider queryProvider)
        {
            _queryProvider = queryProvider;
        }

        public int ExecuteNonQuery(NpgsqlCommand command)
        {
            return command.ExecuteNonQuery();
        }

        public int CreateDatabase(string connectionString, string databaseName)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    var createCommand = connection.CreateCommand();
                    createCommand.CommandText = _queryProvider.GetCreateDbQuery(databaseName);
                    return createCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public object FindDatabase(string connectionString, string databaseName)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    string cmdText = _queryProvider.GetDbExistsQuery(databaseName);
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = cmdText;
                    var db = cmd.ExecuteScalar();
                    return db;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public string MigrateTenantDb(TenantConfiguration tenantConfig)
        {
            using (Digipolis.DossierEngine.DataProvider.Models.BaseAdminDbContext context = new Digipolis.DossierEngine.DataProvider.Models.AdminDbContext(tenantConfig))
            {
                context.Database.Migrate();
                return "Migrations successfully executed";
            }
        }
    }
}
