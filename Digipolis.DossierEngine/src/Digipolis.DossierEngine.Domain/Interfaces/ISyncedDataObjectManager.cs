﻿using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ISyncedDataObjectManager
    {
        Task<List<DataObject>> GetSyncedDataObjectsByExternalIdAsync(string externalId);
        Task<DataObject> UpdateSyncedDataObjectAsync(DataObject dataObject);
        Task<DataObject> CreateSyncedDataObjectAsync(DataObject dataObject);
    }
}
