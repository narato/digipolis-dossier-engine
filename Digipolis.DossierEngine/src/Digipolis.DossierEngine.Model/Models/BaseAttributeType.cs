﻿using Digipolis.DossierEngine.Model.Interfaces;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.DossierEngine.Model.Models
{
    public abstract class BaseAttributeType : IAttributeType
    {
        [Required]
        public abstract string Type { get; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool? IsRequired { get; set; }
        public object DefaultValue { get; set; }

        [JsonProperty("isDeleted", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsDeleted { get; set; } // internal property, so not exposed

        public bool? IsCollection { get; set; }
    }
}