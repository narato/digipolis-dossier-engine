﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Models;
using Newtonsoft.Json;
using Digipolis.DossierEngine.DataProvider.Models;
using System;
using System.Text.RegularExpressions;

namespace Digipolis.DossierEngine.DataProvider.Mappers
{
    public class DataObjectMapper : IDataObjectMapper
    {
        // TODO : rewrite to something less of an eyesore? :(
        // parameter is a collection of RawDataObjects because nested DataObjects come in as a seperate object. We have to link them.
        public List<DataObject> Map(IEnumerable<RawDataObject> toMap)
        {
            var dataObjects = new Dictionary<Guid, DataObject>();
            var dataObjectGuidsToLink = new Dictionary<Guid, Guid>(); // <childGuid, parentGuid> this order because 1 parentGuid could have multiple nested objects
            foreach (var rawDataObject in toMap)
            {
                
                var dataObject = new DataObject
                {
                    Id = rawDataObject.DataObject.Id,
                    Name = rawDataObject.DataObject.Name,
                    ExternalId = rawDataObject.DataObject.ExternalId,
                    DataTypeId = rawDataObject.DataType.Id,
                    Values = new Dictionary<string, object>(),
                    IsDeleted = rawDataObject.DataObject.IsDeleted,
                    CreatedAt = rawDataObject.CreatedAt,
                    UpdatedAt = rawDataObject.UpdatedAt
                };
                if (! string.IsNullOrEmpty(dataObject.ExternalId))
                {
                    dataObject.Values.Add("externalId", dataObject.ExternalId); // expose the externalId
                }

                for (int i = 0; i < rawDataObject.AttributeValues.Count(); i++)
                {
                    var attValue = JsonConvert.DeserializeObject(rawDataObject.AttributeValues.ElementAt(i).Data, typeof(NeoAttributeValue)) as NeoAttributeValue;
                    if (attValue.IsDeleted != null)
                    {
                        continue; // this property is deleted, so we don't need to actually map it.
                    }
                    var attName = rawDataObject.AttributeNames.ElementAt(i);
                    if (attValue.DataObjectId != null)
                    {
                        dataObjectGuidsToLink.Add((Guid)attValue.DataObjectId, dataObject.Id);
                    }
                    else if (attValue.DataObjectIds != null)
                    {
                        foreach (var guid in attValue.DataObjectIds)
                        {
                            dataObjectGuidsToLink.Add(guid, dataObject.Id);
                        }
                    }
                    else
                    {
                        dataObject.Values.Add(attName, attValue.Value);
                    }
                }

                dataObjects.Add(dataObject.Id, dataObject);
            }

            // parent object should not have a single (key)record in the link dictionary
            var parentDataObjects = dataObjects.Values.Where((dataObject) => !dataObjectGuidsToLink.ContainsKey(dataObject.Id));

            foreach (var dataObjectGuidSetToLink in dataObjectGuidsToLink)
            {
                var parent = dataObjects[dataObjectGuidSetToLink.Value];
                var child = dataObjects[dataObjectGuidSetToLink.Key];
                // check if the child is part of an array
                if (isNestedDataObjectPartOfList(child))
                {
                    var nestedAttributeName = GetAttributeNameOfNestedDataObject(child);
                    if (! parent.Values.ContainsKey(nestedAttributeName))
                    {
                        parent.Values.Add(nestedAttributeName, new List<IDictionary<string, object>>());
                    }
                    ((List<IDictionary<string, object>>)parent.Values[nestedAttributeName]).Add(child.Values);
                    continue;
                }
                parent.Values.Add(GetAttributeNameOfNestedDataObject(child), child.Values);
            }

            return parentDataObjects.ToList();
        }

        private string GetAttributeNameOfNestedDataObject(DataObject dataObject)
        {
            int idx = dataObject.Name.LastIndexOf('.');

            if (isNestedDataObjectPartOfList(dataObject))
            {
                int idy = dataObject.Name.LastIndexOf('[');
                return dataObject.Name.Substring(idx + 1, idy - idx - 1);
            }
            return dataObject.Name.Substring(idx + 1);
        }

        private bool isNestedDataObjectPartOfList(DataObject dataObject)
        {
            var regex = new Regex(@"\[\d+\]$", RegexOptions.Compiled);
            var match = regex.Match(dataObject.Name);
            return match.Success;
        }
    }
}
