#!/bin/bash

dotnet test test/Digipolis.DossierEngine.Test -xml xunit_result.xml

echo "converting xunit result to nunit result"
xsltproc Xunit2Nunit.xslt xunit_result.xml > nunit_result.xml