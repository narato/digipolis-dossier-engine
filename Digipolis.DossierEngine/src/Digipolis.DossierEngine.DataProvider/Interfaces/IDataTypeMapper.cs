﻿using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IDataTypeMapper
    {
        DataType Map(RawDataType toMap);
        List<DataType> Map(IEnumerable<RawDataType> toMap);
    }
}
