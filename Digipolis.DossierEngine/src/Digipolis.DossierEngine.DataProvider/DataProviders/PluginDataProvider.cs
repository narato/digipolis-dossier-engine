﻿using Digipolis.DossierEngine.Common.Converters;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.EntityFrameworkCore;
using Narato.Common.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class PluginDataProvider : IPluginDataProvider
    {
        private BaseAdminDbContext _adminDbContext;

        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public PluginDataProvider(BaseAdminDbContext adminDbContext)
        {
            _adminDbContext = adminDbContext;
        }

        public List<Plugin> GetPlugins()
        {
            return _adminDbContext.Plugin.ToList();
        }

        public Plugin GetPluginById(Guid id)
        {
            return _adminDbContext.Plugin.Single(p => p.Id == id);
        }

        public void InsertPlugins(List<Plugin> plugins)
        {
            _adminDbContext.Plugin.AddRange(plugins);
            _adminDbContext.SaveChanges();
        }

        public TenantPluginLink InsertTenantPluginLink(TenantPluginLink tenantPluginLink)
        {
            _adminDbContext.TenantPluginLink.Add(tenantPluginLink);
            _adminDbContext.SaveChanges();

            return tenantPluginLink;
        }

        public List<List<DataType>> GetAllSynchDataTypes()
        {
            var stringDataTypeList = _adminDbContext.Plugin.Select(plugin => plugin.DataType).ToList();

            var dataTypeList = new List<List<DataType>>();

            foreach (var stringDataTypes in stringDataTypeList)
            {
                try
                {
                    var dataTypeJsonsForPlugin = JsonConvert.DeserializeObject<List<string>>(stringDataTypes);
                    var innerList = new List<DataType>();
                    foreach (var dataTypeJson in dataTypeJsonsForPlugin)
                    {
                        var jObject = JObject.Parse(dataTypeJson);
                        var name = jObject["name"].ToString();
                        var attributeList = jObject["attributes"].ToList();

                        var dataType = new DataType() { Name = name, Attributes = new List<IAttributeType>() };

                        foreach (var attribute in attributeList)
                        {
                            var att = JsonConvert.DeserializeObject<IAttributeType>(attribute.ToString(), new AttributeTypeConverter(true));
                            dataType.Attributes.Add(att);
                        }
                        innerList.Add(dataType);
                    }
                    dataTypeList.Add(innerList);
                } catch (Exception ex)
                {
                    Logger.Error(ex, "following list of datatypes couldnt be parsed correctly: " + stringDataTypes);
                }
                
            }

            return dataTypeList;
        }

        public bool DeleteTenantPluginLink(Guid linkId)
        {
            var entity = _adminDbContext.TenantPluginLink.Where(link => link.Id == linkId).FirstOrDefault();
            if (entity != null)
            {
                _adminDbContext.TenantPluginLink.Remove(entity);
                _adminDbContext.SaveChanges();
                return true;
            }
            else
            {
                throw new EntityNotFoundException($"Link with id {linkId.ToString()} could not be found");
            }
        }

        public List<TenantPluginLink> GetPluginLinksForTenant(string tenantKey)
        {
            return _adminDbContext.TenantPluginLink
                .Where(tpl => tpl.Tenant.Key == tenantKey)
                .ToList();
        }

        public List<TenantPluginLink> GetPluginLinksForPluginForActiveTenants(string pluginName)
        {
            return _adminDbContext.TenantPluginLink
                .Include(tpl => tpl.Tenant)
                .Where(tpl => tpl.Tenant.Status != "Deleted" && tpl.Plugin.Name == pluginName)
                .ToList();
        }

        public TenantPluginLink GetPluginLinksForTenantAndDataTypeId(string tenantKey, Guid dataTypeId)
        {
            return _adminDbContext.TenantPluginLink
                .Where(tpl => tpl.Tenant.Status != "Deleted" && tpl.Tenant.Key == tenantKey && tpl.DataTypeId == dataTypeId)
                .Include(tpl => tpl.Plugin)
                .Single();
        }
    }
}