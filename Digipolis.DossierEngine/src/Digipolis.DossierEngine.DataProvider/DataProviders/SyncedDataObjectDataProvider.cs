﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using System.Collections.Generic;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Neo4jClient;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class SyncedDataObjectDataProvider : ISyncedDataObjectDataProvider
    {
        private readonly ITenantGraphClientFactory _graphClientFactory;
        private readonly IDataObjectMapper _dataObjectMapper;

        public SyncedDataObjectDataProvider(ITenantGraphClientFactory graphClientFactory, IDataObjectMapper dataObjectMapper)
        {
            _graphClientFactory = graphClientFactory;
            _dataObjectMapper = dataObjectMapper;
        }

        public async Task<List<DataObject>> FindSyncedDataObjectsByExternalIdAsync(string externalId)
        {
            using (var client = await _graphClientFactory.CreateAsync())
            {
                /*
                 * THIS IS MOST OPTIMIZED I CAN GET!
                 * 
optional match (dobj:DataObject)
where dobj.externalId = "49164"
AND ((not(has(dobj.isDeleted))) OR (dobj.isDeleted = false))
with collect(distinct dobj) as d1
optional match (dobj:NestedDataObject)
where dobj.externalId = "49164"
AND ((not(has(dobj.isDeleted))) OR (dobj.isDeleted = false))
with collect(distinct dobj) + d1 as d2
unwind d2 as dobj
with dobj

This is MUCH more optimized than match (dobj) where dobj:DataObject or dobj:NestedDataObject
The long one makes use of indexes, while the naive implementation does a full node scan.
*/
                client.Connect();
                var query = client.Cypher
                    .OptionalMatch("(dobj:DataObject)")
                    .Where((NeoDataObject dobj) => dobj.ExternalId == externalId)
                    .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .With("collect(distinct dobj) as d1")
                    .OptionalMatch("(dobj:NestedDataObject)")
                    .Where((NeoDataObject dobj) => dobj.ExternalId == externalId)
                    .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .With("collect(distinct dobj) +d1 as d2")
                    .Unwind("d2", "dobj")
                    .With("dobj")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where("not (has (dt.isDeleted))")
                    .OptionalMatch("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)")
                    .OptionalMatch("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                    .Where("not (has (at.isDeleted))")

                    .With("dt, dobj, av, at.name as atName")
                    .Return((dt, dobj, av, atName) => new RawDataObject
                    {
                        DataType = dt.As<NeoDataType>(),
                        DataObject = dobj.As<NeoDataObject>(),
                        AttributeNames = atName.CollectAs<string>(),
                        AttributeValues = av.CollectAs<Node<string>>()
                    })
                    .Union()
                    .OptionalMatch("(dobj:DataObject)")
                    .Where((NeoDataObject dobj) => dobj.ExternalId == externalId)
                    .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .With("collect(distinct dobj) as d1")
                    .OptionalMatch("(dobj:NestedDataObject)")
                    .Where((NeoDataObject dobj) => dobj.ExternalId == externalId)
                    .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .With("collect(distinct dobj) +d1 as d2")
                    .Unwind("d2", "dobj")
                    .With("dobj")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where("not (has (dt.isDeleted))")
                    .Match("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)")
                    .Match("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                    .Match("(av)-[r:OF_VALUE|:HAS_ATTRIBUTE*]->(dobjnested:NestedDataObject)-[:OF_DATATYPE]->(dtnested:DataType)")
                    .Where("not (has (dtnested.isDeleted))")
                    .OptionalMatch("(dobjnested)-[:HAS_ATTRIBUTE]->(avnested:AttributeValue)")
                    .OptionalMatch("(avnested)-[:OF_ATTRIBUTETYPE]->(atnested:AttributeType)")
                    .Where("not (has (atnested.isDeleted))")
                    // no match on changesets for the create/update times here because 1) nestedDataObjects could have multiple first-in-line Changesets
                    // and 2) they're not needed... Only the root DO has to know it
                    .With("dtnested, dobjnested, avnested, atnested.name as atnestedName, null as updatedAt, null as createdAt")
                    .Return((dtnested, dobjnested, avnested, atnestedName) => new RawDataObject
                    {
                        DataType = dtnested.As<NeoDataType>(),
                        DataObject = dobjnested.As<NeoDataObject>(),
                        AttributeNames = atnestedName.CollectAs<string>(),
                        AttributeValues = avnested.CollectAs<Node<string>>()
                    });

                return _dataObjectMapper.Map(query.Results);
            }
        }
    }
}
