﻿using System.Collections.Generic;
using Digipolis.DossierEngine.Synchronization;

namespace Digipolis.DossierEngine.PluginCreation
{
    public class TestPlugin2 : AbstractPlugin
    {
        public TestPlugin2()
        {
            Name = "MyTestPlugin2";
            Version = 1;
            EventNamespace = "LocalDevDossierEngine";
            EventHandlerTopic = "Tenant.Created";
            DataTypes = new List<string>() {
                @"{""name"": ""VerySimpleType"",""attributes"": [{""type"": ""String"",""name"": ""locatie"",""regexValidation"": ""^[a-zA-Z]*$"",""isRequired"": ""true""}]}"
            };
        }

        public override string GetObjectById(object id)
        {
            if (id is string)
            {
                return $@"{{
                    ""externalId"": ""{id}"",
                    ""name"": ""test"",
                    ""values"": {{
                        ""locatie"": ""Kontich""
                    }}
                }}";
            } else
            {
                return id.ToString();
            }
            
        }

        protected override IEnumerable<object> GetAllObjects()
        {
            var returnValue = new List<string>();
            returnValue.Add(@"{
                ""externalId"": ""512"",
                ""name"": ""prepopulateTest"",
                ""values"": {
                    ""locatie"": ""Antwerpen""
                }
            }");
            return returnValue;
        }
    }
}