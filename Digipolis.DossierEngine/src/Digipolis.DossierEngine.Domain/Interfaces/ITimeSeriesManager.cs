﻿using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ITimeSeriesManager
    {
        Task<string> InsertTimeSeriesForTenantForYearAsync(string tenantKey, int year);
    }
}
