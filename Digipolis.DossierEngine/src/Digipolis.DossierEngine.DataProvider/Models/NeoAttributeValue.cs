﻿using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public class NeoAttributeValue
    {
        public object Value { get; set; }
        
        public Guid? DataObjectId { get; set; } // used for nested dataObjects
        public List<Guid> DataObjectIds { get; set; } // used for an array of nested dataObjects.

        public bool? IsDeleted { get; set; } // used for softDelete
    }
}