﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System;
using Digipolis.Common.Tenancy.Providers;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace Digipolis.DossierEngine.Domain.Providers
{
    public class CurrentTenantProvider : ICurrentTenantProvider<Config>
    {
        private const string TENANT_KEY_NAME = "Tenant-Key";
        private readonly MemoryCacheEntryOptions _cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromHours(24)); // will stay in the cache for 24 hours, resets the timer when accessed

        private readonly IActionContextAccessor _actionContextAccessor;
        private readonly ITenantClient _tenantClient;
        private readonly IMemoryCache _cache;

        // cached tenant so that we don't need to fetch the tenant
        //private Tenant<Config> cachedTenant;

        public CurrentTenantProvider(IMemoryCache memoryCache, IActionContextAccessor actionContextAccessor, ITenantClient tenantClient)
        {
            _actionContextAccessor = actionContextAccessor;
            _tenantClient = tenantClient;
            _cache = memoryCache;
        }

        private void AssertTenantKeyExists()
        {
            if (!_actionContextAccessor.ActionContext.HttpContext.Request.Headers.ContainsKey(TENANT_KEY_NAME))
            {
                throw new Exception("No tenantKey found in header.");
            }
        }

        // if force = true, the tenant will be fetched from the client even while it's already in the cache
        public async Task<Tenant<Config>> GetCurrentTenantAsync(bool force = false)
        {
            var currentTenantKey = GetCurrentTenantKey();

            Tenant<Config> tenant;
            if (force || ! _cache.TryGetValue(currentTenantKey, out tenant))
            {
                tenant = await _tenantClient.GetTenantAsync(currentTenantKey);
                _cache.Set(currentTenantKey, tenant, _cacheEntryOptions);
            }
            return tenant;
        }

        public Tenant<Config> GetCurrentTenant(bool force = false)
        {
            return GetCurrentTenantAsync(force).Result;
        }        

        public string GetCurrentTenantKey()
        {
            AssertTenantKeyExists();
            return _actionContextAccessor.ActionContext.HttpContext.Request.Headers[TENANT_KEY_NAME];
        }

    }
}
