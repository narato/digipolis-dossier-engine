﻿using System;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using Digipolis.Common.Status.Models;
using Digipolis.DossierEngine.DataProvider.Changes;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IDataTypeDataProvider
    {
        Task<DataType> GetDataTypeByIdAsync(Guid id);
        Task<DataType> GetDataTypeByNameAsync(string name);
        Task<List<DataType>> GetDataTypeByIdListAsync(List<Guid> ids);
        Task<List<DataType>> GetAllDataTypesAsync(bool syncedOnly);
        Task<DataType> InsertDataTypeAsync(DataType dataType);
        Task<DataType> UpdateDataTypeAsync(DataType dataType, IEnumerable<Change> changes);
        Task<DbConnectionCheckResult> CheckDBConnectionAsync();
        Task<DataType> SetDefaultValueAsync(DataType dataType, IEnumerable<Change> changes);
        Task<bool> DeleteDataTypeByIdAsync(Guid id);
        Task<DataType> FindDataTypeByIdAsync(Guid id);
        Task<bool> VerifyIfDataTypeIsNestedAsync(Guid id);
        Task<bool> VerifyIfDataTypeExistsAsync(string name);
    }
}