﻿using Digipolis.DossierEngine.Domain.Ssh;
using Digipolis.DossierEngine.Domain.Ssh.Interfaces;
using Moq;
using Renci.SshNet;
using Xunit;

namespace Digipolis.DossierEngine.Test.Ssh
{
    public class IneoCommandRunnerTest
    {
        [Fact]
        public void CreateNeo4jInstanceBehaviourTest()
        {
            var argument = "INEO_HOME=home home/bin/ineo create -a -p 1 test";

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(scm => scm.RunCommand(argument)).Returns((SshCommand)null);

            var ineoCommandRunner = new IneoCommandRunner();

            ineoCommandRunner.CreateNeo4jInstance(sshClientMock.Object, "home", 1, "test");

            sshClientMock.Verify(x => x.RunCommand(argument), Times.Once);
        }

        [Fact]
        public void StartNeo4jInstanceBehaviourTest()
        {
            var argument = "INEO_HOME=home home/bin/ineo start test";

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(scm => scm.RunCommand(argument)).Returns((SshCommand)null);

            var ineoCommandRunner = new IneoCommandRunner();

            ineoCommandRunner.StartNeo4jInstance(sshClientMock.Object, "home", "test");

            sshClientMock.Verify(x => x.RunCommand(argument), Times.Once);
        }

        [Fact]
        public void SetPasswordForNeo4jInstanceBehaviourTest()
        {
            var argument = "curl -s -X POST http://neo4j:neo4j@127.0.0.1:1/user/neo4j/password -d 'password=test'";

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(scm => scm.RunCommand(argument)).Returns((SshCommand)null);

            var ineoCommandRunner = new IneoCommandRunner();

            ineoCommandRunner.SetPasswordForNeo4jInstance(sshClientMock.Object, 1, "test");

            sshClientMock.Verify(x => x.RunCommand(argument), Times.Once);
        }

        [Fact]
        public void StopNeo4jInstanceBehaviourTest()
        {
            var argument = "INEO_HOME=home home/bin/ineo stop test";

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(scm => scm.RunCommand(argument)).Returns((SshCommand)null);

            var ineoCommandRunner = new IneoCommandRunner();

            ineoCommandRunner.StopNeo4jInstance(sshClientMock.Object, "home", "test");

            sshClientMock.Verify(x => x.RunCommand(argument), Times.Once);
        }
    }
}
