﻿using System.Threading.Tasks;
using Digipolis.DossierEngine.Common.Models;
using Neo4jClient;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface ITenantGraphClientFactory : IGraphClientFactory
    {
        Task<IGraphClient> CreateAsync();
        IGraphClient CreateForConfig(Config tenantConfig);
    }
}
