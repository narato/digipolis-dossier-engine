﻿using Digipolis.DossierEngine.Domain.Ssh.Interfaces;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ISshClientProvider
    {
        ISshClient GetSshClient(string server, int port, string username, string password);
    }
}