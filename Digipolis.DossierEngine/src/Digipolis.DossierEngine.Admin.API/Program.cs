﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace Digipolis.DossierEngine.Admin.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var url = string.Empty;
            var argsOk = true;
            if (args.Length > 0)
            {
                var urlArgs = args.FirstOrDefault(x => x.StartsWith("-url:"));
                if (!string.IsNullOrEmpty(urlArgs))
                {
                    url = urlArgs.Replace("-url:", "").Trim();

                    try
                    {
                        var uri = new Uri(url);
                    }
                    catch (Exception)
                    {
                        argsOk = false;
                        Console.WriteLine($"Given value for -url is not a well formatted url ({url})");
                    }
                }
            }

            if (argsOk)
            {
                var host = new WebHostBuilder()
                    .UseKestrel()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseIISIntegration()
                    .UseStartup<Startup>()
                    .UseUrls(string.IsNullOrEmpty(url) ? Environment.GetEnvironmentVariable("ASPNETCORE_URLS") : url)
                    .Build();

                host.Run();
            }
        }
    }
}