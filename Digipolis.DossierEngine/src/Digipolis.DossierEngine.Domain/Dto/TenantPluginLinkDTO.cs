﻿using System;

namespace Digipolis.DossierEngine.Domain.Dto
{
    public class TenantPluginLinkDTO
    {
        public Guid TenantId { get; set; }
        public Guid PluginId { get; set; }
        public bool Prepopulate { get; set; }
    }
}
