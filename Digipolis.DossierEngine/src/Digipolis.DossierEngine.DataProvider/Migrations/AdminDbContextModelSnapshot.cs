﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Digipolis.DossierEngine.DataProvider.Models;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    [DbContext(typeof(AdminDbContext))]
    partial class AdminDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Digipolis.Common.Tenancy.Models.Tenant", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Config");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<string>("Name");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.ToTable("Tenant");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.Plugin", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DataType");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Version");

                    b.HasKey("Id");

                    b.HasAlternateKey("Name");

                    b.ToTable("Plugin");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.TenantPluginLink", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("DataTypeId");

                    b.Property<Guid>("PluginId");

                    b.Property<bool>("Prepopulate");

                    b.Property<Guid>("TenantId");

                    b.HasKey("Id");

                    b.HasAlternateKey("TenantId", "PluginId");

                    b.HasIndex("PluginId");

                    b.HasIndex("TenantId");

                    b.ToTable("TenantPluginLink");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.TenantPluginLink", b =>
                {
                    b.HasOne("Digipolis.DossierEngine.Model.Models.Plugin", "Plugin")
                        .WithMany("TenantPluginLinks")
                        .HasForeignKey("PluginId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Digipolis.Common.Tenancy.Models.Tenant", "Tenant")
                        .WithMany()
                        .HasForeignKey("TenantId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
