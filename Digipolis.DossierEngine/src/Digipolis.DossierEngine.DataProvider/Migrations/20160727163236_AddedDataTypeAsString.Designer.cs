﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digipolis.DossierEngine.DataProvider.Models;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    [DbContext(typeof(AdminDbContext))]
    [Migration("20160727163236_AddedDataTypeAsString")]
    partial class AddedDataTypeAsString
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rc2-20896");

            modelBuilder.Entity("Digipolis.Common.Tenancy.Models.Tenant", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Config");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<string>("Name");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.ToTable("Tenant");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.Plugin", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DataType");

                    b.Property<string>("Name");

                    b.Property<int>("Version");

                    b.HasKey("Id");

                    b.ToTable("Plugin");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.TenantPluginLink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("PluginId");

                    b.Property<Guid>("TenantId");

                    b.HasKey("Id");

                    b.HasIndex("PluginId");

                    b.HasIndex("TenantId");

                    b.ToTable("TenantPluginLink");
                });

            modelBuilder.Entity("Digipolis.DossierEngine.Model.Models.TenantPluginLink", b =>
                {
                    b.HasOne("Digipolis.DossierEngine.Model.Models.Plugin")
                        .WithMany()
                        .HasForeignKey("PluginId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Digipolis.Common.Tenancy.Models.Tenant")
                        .WithMany()
                        .HasForeignKey("TenantId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
