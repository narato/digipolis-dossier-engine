﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Digipolis.DossierEngine.Domain.Interfaces;
using Models = Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Linq;
using Digipolis.DossierEngine.Model.Dto;
using NLog;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataType.API.Controllers
{
    [Route("api/datatypes/")]
    public class DataTypeController : Controller
    {
        private IDataTypeManager _dataTypeManager;
        private IResponseFactory _responseFactory;
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public DataTypeController(IDataTypeManager dataTypeManager, IResponseFactory responseFactory)
        {
            _dataTypeManager = dataTypeManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Gets a datatype by id
        /// </summary>
        /// <param name="id">The id of the datatype</param>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="flatstructure">Indicates whether the datastructure is nested or not</param>
        /// <returns>A response containing the datatype</returns>
        // GET api/datatypes/{id}

        [ProducesResponseType(typeof(Response<Models.DataType>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response),(int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet("{id}", Name = "GetDataTypeById")]
        public async Task<IActionResult> GetAsync([Required]Guid id, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey, [FromQuery]bool flatstructure = true)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => { 
                return flatstructure ? await _dataTypeManager.GetDataTypeByIdAsync(id) : await _dataTypeManager.GetDataTypeTreeByIdAsync(id);
            }, this.GetRequestUri());
        }

        /// <summary>
        /// Gets a datatype by name
        /// </summary>
        /// <param name="name">The name of the datatype</param>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="flatstructure">Indicates whether the datastructure is nested or not</param>
        /// <returns>A response containing the datatype</returns>
        // GET api/datatypes/name/{name}
        [ProducesResponseType(typeof(Response<Models.DataType>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response),(int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet("name/{name}", Name = "GetDataTypeByName")]
        public async Task<IActionResult> GetByNameAsync([Required]string name, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey, [FromQuery]bool flatstructure = true)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.Header));

            return await _responseFactory.CreateGetResponseAsync(async () => flatstructure ? await _dataTypeManager.GetDataTypeByNameAsync(name) : await _dataTypeManager.GetDataTypeTreeByNameAsync(name),
                this.GetRequestUri());
        }

        /// <summary>
        /// Gets all datatypes
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="syncedonly">Indicates whether the query should return all or only synced DataTypes</param>
        /// <param name="page">The page number to be returned</param>
        /// <param name="pagesize">The number of records to be returned per page</param>
        /// <returns>A response containing all datatypes</returns>
        // GET api/datatypes
        [ProducesResponseType(typeof(Response<List<Models.DataType>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync([Required][FromHeader(Name = "Tenant-Key")]string tenantKey, [FromQuery]bool syncedonly = false, [FromQuery] int page = 1, [FromQuery] int pagesize = 10)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.QuerystringParam));

            return await _responseFactory.CreateGetResponseForCollectionAsync(async () => await _dataTypeManager.GetAllDataTypesAsync(syncedonly, page, pagesize),
                this.GetRequestUri());
        }

        /// <summary>
        /// This method creates a new datatype
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="dataTypeDTO">The datatype to create</param>
        /// <returns>A statuscode indicating the result</returns>
        // POST api/datatypes
        [ProducesResponseType(typeof(Response<Models.DataType>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPost]
        public async Task<IActionResult> InsertDataTypeAsync([FromBody] [Required] DataTypeDTO dataTypeDTO, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataTypeDTO == null)
                missingParams.Add(new MissingParam("datatype", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            Models.DataType dataType = new Models.DataType() { Attributes = dataTypeDTO.Attributes, Name = dataTypeDTO.Name };

            Func<Model.Models.DataType, object> pathMapper = (data) => { 
                return new { controller = "DataType", id = data.Id, tenantKey = tenantKey }; 
            };

            return await _responseFactory.CreatePostResponseAsync(async () => await _dataTypeManager.InsertDataTypeAsync(dataType), this.GetRequestUri(),
                        "GetDataTypeById", pathMapper);
        }

        /// <summary>
        /// This method updates a new datatype
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="dataType">The datatype to update</param>
        /// <returns>A statuscode indicating the result</returns>
        // GET api/datatypes
        [ProducesResponseType(typeof(void), (int)System.Net.HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(Response<Models.DataType>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPut]
        public async Task<IActionResult> UpdateDataTypeAsync([FromBody] [Required] Models.DataType dataType, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataType == null)
                missingParams.Add(new MissingParam("datatype", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePostResponseAsync(async () => await _dataTypeManager.UpdateDataTypeAsync(dataType), this.GetRequestUri());
        }

        /// <summary>
        /// Deletes a datatype by id
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="id">The dataType to delete</param>
        /// <returns>A statuscode indicating the result</returns>
        /// <response code="204">Deleted sucessfully</response>
        // DELETE api/datatypes/{id}
        [ProducesResponseType(typeof(void), (int)System.Net.HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([Required]Guid id, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.QuerystringParam));

            return await _responseFactory.CreateDeleteResponseAsync(async () => await _dataTypeManager.DeleteDataTypeByIdAsync(id), this.GetRequestUri());
        }
    }
}