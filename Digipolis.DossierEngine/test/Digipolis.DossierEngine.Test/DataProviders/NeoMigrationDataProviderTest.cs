﻿using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Microsoft.Extensions.Options;
using Moq;
using Neo4jClient;
using Neo4jClient.Cypher;
using Xunit;

namespace Digipolis.DossierEngine.Test.DataProviders
{
    public class NeoMigrationDataProviderTest
    {
        [Fact]
        public void SetupDatabaseTest()
        {
            var config = new Config();

            var optionsMock = new Mock<IOptions<DossierConfiguration>>();
            optionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { MinYear = 2010, MaxYear = 2015 });

            var graphClientMock = new Mock<IRawGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.Setup(gcm => gcm.ExecuteGetCypherResults<int>(It.IsAny<CypherQuery>())).Returns(new [] {6});
            graphClientMock.Setup(gcm => gcm.ExecuteCypher(It.IsAny<CypherQuery>()));

            var tenantGraphClientFactoryMock = new Mock<ITenantGraphClientFactory>();
            tenantGraphClientFactoryMock.Setup(tgcfm => tgcfm.CreateForConfig(config)).Returns(graphClientMock.Object);

            var dataProvider = new NeoMigrationDataProvider(optionsMock.Object, tenantGraphClientFactoryMock.Object);

            dataProvider.SetupDatabase(config);

            tenantGraphClientFactoryMock.Verify(x => x.CreateForConfig(config), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");

            graphClientMock.Verify(gcm => gcm.ExecuteCypher(It.IsAny<CypherQuery>()), Times.AtLeastOnce);
            graphClientMock.Verify(gcm => gcm.ExecuteGetCypherResults<int>(It.IsAny<CypherQuery>()), Times.Once);
        }
    }
}
