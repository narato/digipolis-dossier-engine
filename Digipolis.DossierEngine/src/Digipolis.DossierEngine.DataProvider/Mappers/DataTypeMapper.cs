﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using System.Collections.Generic;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.Model.Interfaces;
using Newtonsoft.Json;
using Digipolis.DossierEngine.Common.Converters;

namespace Digipolis.DossierEngine.DataProvider.Mappers
{
    public class DataTypeMapper : IDataTypeMapper
    {
        public DataType Map(RawDataType toMap)
        {
            var dataType = new DataType
            {
                Id = toMap.DataType.Id,
                Name = toMap.DataType.Name,
                Synced = toMap.DataType.Synced,
                Attributes = new List<IAttributeType>()
            };

            foreach (var attribute in toMap.Attributes)
            {
                var att = JsonConvert.DeserializeObject<IAttributeType>(attribute.Data, new AttributeTypeConverter());
                dataType.Attributes.Add(att);
            }

            return dataType;
        }

        public List<DataType> Map(IEnumerable<RawDataType> toMap)
        {
            var returnValue = new List<DataType>();
            foreach (var rawDataType in toMap)
            {
                returnValue.Add(Map(rawDataType));
            }
            return returnValue;
        }
    }
}
