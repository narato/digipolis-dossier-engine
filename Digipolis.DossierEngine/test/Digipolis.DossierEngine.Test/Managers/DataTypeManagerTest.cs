﻿using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Microsoft.Extensions.Options;
using Moq;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class DataTypeManagerTest
    {
        [Fact]
        public void GetAllDataTypesReturnsListOfDataTypes()
        {
            var dataTypeList = new List<Model.Models.DataType>();
            dataTypeList.Add(new Model.Models.DataType());
            dataTypeList.Add(new Model.Models.DataType());

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(tdp => tdp.GetAllDataTypesAsync(false)).ReturnsAsync(dataTypeList);

            var dataTypeValidatorMock = new Mock<IDataTypeValidator>();

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctp => ctp.GetCurrentTenantKey()).Returns("CurrentTennantey");

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var manager = new DataTypeManager(dataTypeDataProviderMock.Object, dataTypeValidatorMock.Object, eventPublisherMock.Object, configOptionsMock.Object, currentTenantProviderMock.Object, changeCalculatorMock.Object);

            var dataTypes = manager.GetAllDataTypesAsync(false, 1, 10).Result;

            Assert.NotNull(dataTypes);
            Assert.Equal(dataTypes.Data.ToList().Count, dataTypeList.Count);
        }

        [Fact]
        public void GetDataTypeByIdReturnsDataType()
        {
            var guid = new Guid();
            var guidList = new List<Guid>();
            var dataType = new Model.Models.DataType();
            var dataTypeList = new List<Model.Models.DataType>();

            dataType.Attributes = new List<Model.Interfaces.IAttributeType>();

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(tdp => tdp.GetDataTypeByIdAsync(guid)).ReturnsAsync(dataType);
            dataTypeDataProviderMock.Setup(tdp => tdp.FindDataTypeByIdAsync(guid)).ReturnsAsync(dataType);
            dataTypeDataProviderMock.Setup(tdp => tdp.GetDataTypeByIdListAsync(guidList)).ReturnsAsync(dataTypeList);

            var dataTypeValidatorMock = new Mock<IDataTypeValidator>();

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctp => ctp.GetCurrentTenantKey()).Returns("CurrentTennantey");

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var manager = new DataTypeManager(dataTypeDataProviderMock.Object, dataTypeValidatorMock.Object, eventPublisherMock.Object, configOptionsMock.Object, currentTenantProviderMock.Object, changeCalculatorMock.Object);

            var returnedDataType = manager.GetDataTypeByIdAsync(guid).Result;

            Assert.NotNull(returnedDataType);
            Assert.Equal(dataType, returnedDataType);
        }

        [Fact]
        public void GetDataTypeByNameReturnsDataType()
        {
            var name = "";
            var dataType = new Model.Models.DataType();

            dataType.Attributes = new List<Model.Interfaces.IAttributeType>();

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(tdp => tdp.GetDataTypeByNameAsync(name)).ReturnsAsync(dataType);

            var dataTypeValidatorMock = new Mock<IDataTypeValidator>();

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctp => ctp.GetCurrentTenantKey()).Returns("CurrentTennantey");

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var manager = new DataTypeManager(dataTypeDataProviderMock.Object, dataTypeValidatorMock.Object, eventPublisherMock.Object, configOptionsMock.Object, currentTenantProviderMock.Object, changeCalculatorMock.Object);

            var returnedDataType = manager.GetDataTypeByNameAsync(name).Result;

            Assert.NotNull(returnedDataType);
            Assert.Equal(dataType, returnedDataType);
        }

        [Fact]
        public void InsertDataTypeReturnsDataTypeWithGuid()
        {
            var dataType = new Model.Models.DataType();
            Assert.Equal(Guid.Empty, dataType.Id);

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(tdp => tdp.InsertDataTypeAsync(dataType)).ReturnsAsync(dataType);

            var dataTypeValidatorMock = new Mock<IDataTypeValidator>();
            dataTypeValidatorMock.Setup(dtv => dtv.ValidateDataTypeAttributesAsync(dataType)).ReturnsAsync(new List<FeedbackItem>());

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctp => ctp.GetCurrentTenantKey()).Returns("CurrentTennantey");

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var manager = new DataTypeManager(dataTypeDataProviderMock.Object, dataTypeValidatorMock.Object, eventPublisherMock.Object, configOptionsMock.Object, currentTenantProviderMock.Object, changeCalculatorMock.Object);

            var returnedDataType = manager.InsertDataTypeAsync(dataType).Result;

            Assert.NotEqual(Guid.Empty, dataType.Id);
            Assert.Equal(dataType, returnedDataType);
        }
    }
}
