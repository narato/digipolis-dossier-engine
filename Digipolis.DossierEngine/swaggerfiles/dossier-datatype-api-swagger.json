{
  "swagger": "2.0",
  "info": {
    "version": "v1",
    "title": "Dossier Engine",
    "description": "DataType API",
    "contact": {
      "name": "Narato NV"
    }
  },
  "basePath": "/",
  "paths": {
    "/api/datatypes/{id}": {
      "get": {
        "tags": ["DataType"],
        "summary": "Gets a datatype by id",
        "operationId": "ApiDatatypesByIdGet",
        "consumes": ["application/json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "The id of the datatype",
          "required": true,
          "type": "string",
          "format": "uuid"
        }, {
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }, {
          "name": "flatstructure",
          "in": "query",
          "description": "Indicates whether the datastructure is nested or not",
          "required": false,
          "type": "boolean"
        }],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "404": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          }
        },
        "deprecated": false
      },
      "delete": {
        "tags": ["DataType"],
        "summary": "Deletes a datatype by id",
        "operationId": "ApiDatatypesByIdDelete",
        "consumes": ["application/json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "id",
          "in": "path",
          "description": "The dataType to delete",
          "required": true,
          "type": "string",
          "format": "uuid"
        }, {
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }],
        "responses": {
          "204": {
            "description": "Deleted sucessfully"
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          },
          "404": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response"
            }
          }
        },
        "deprecated": false
      }
    },
    "/api/datatypes/names/{name}": {
      "get": {
        "tags": ["DataType"],
        "summary": "Gets a datatype by name",
        "operationId": "ApiDatatypesNamesByNameGet",
        "consumes": ["application/json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "name",
          "in": "path",
          "description": "The name of the datatype",
          "required": true,
          "type": "string"
        }, {
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }, {
          "name": "flatstructure",
          "in": "query",
          "description": "Indicates whether the datastructure is nested or not",
          "required": false,
          "type": "boolean"
        }],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "404": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          }
        },
        "deprecated": false
      }
    },
    "/api/datatypes": {
      "get": {
        "tags": ["DataType"],
        "summary": "Gets all datatypes",
        "operationId": "ApiDatatypesGet",
        "consumes": ["application/json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }, {
          "name": "syncedonly",
          "in": "query",
          "description": "Indicates whether the query should return all or only synced DataTypes",
          "required": false,
          "type": "boolean"
        }, {
          "name": "page",
          "in": "query",
          "description": "The page number to be returned",
          "required": false,
          "type": "integer",
          "format": "int32"
        }, {
          "name": "pagesize",
          "in": "query",
          "description": "The number of records to be returned per page",
          "required": false,
          "type": "integer",
          "format": "int32"
        }],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[List[DataType]]"
            }
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[List[DataType]]"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[List[DataType]]"
            }
          }
        },
        "deprecated": false
      },
      "put": {
        "tags": ["DataType"],
        "summary": "This method updates a new datatype",
        "operationId": "ApiDatatypesPut",
        "consumes": ["application/json", "text/json", "application/json-patch+json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "dataType",
          "in": "body",
          "description": "The datatype to update",
          "required": false,
          "schema": {
            "$ref": "#/definitions/DataType"
          }
        }, {
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }],
        "responses": {
          "204": {
            "description": "Success"
          },
          "201": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          }
        },
        "deprecated": false
      },
      "post": {
        "tags": ["DataType"],
        "summary": "This method creates a new datatype",
        "operationId": "ApiDatatypesPost",
        "consumes": ["application/json", "text/json", "application/json-patch+json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "dataTypeDTO",
          "in": "body",
          "description": "The datatype to create",
          "required": false,
          "schema": {
            "$ref": "#/definitions/DataTypeDTO"
          }
        }, {
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }],
        "responses": {
          "201": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "400": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          },
          "401": {
            "description": "Client Error",
            "schema": {
              "$ref": "#/definitions/Response[DataType]"
            }
          }
        },
        "deprecated": false
      }
    },
    "/api/system/status": {
      "get": {
        "tags": ["System"],
        "summary": "This method returns the status of the system for the given tenantkey",
        "operationId": "ApiSystemStatusGet",
        "consumes": ["application/json"],
        "produces": ["text/plain", "application/json", "text/json"],
        "parameters": [{
          "name": "Tenant-Key",
          "in": "header",
          "description": "The key of the tenant",
          "required": false,
          "type": "string"
        }],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Response[SystemStatus]"
            }
          }
        },
        "deprecated": false
      }
    }
  },
  "definitions": {
    "Response[DataType]": {
      "type": "object",
      "properties": {
        "data": {
          "$ref": "#/definitions/DataType"
        },
        "self": {
          "type": "string"
        },
        "skip": {
          "format": "int32",
          "type": "integer"
        },
        "take": {
          "format": "int32",
          "type": "integer"
        },
        "total": {
          "format": "int32",
          "type": "integer"
        },
        "generation": {
          "$ref": "#/definitions/Generation"
        },
        "feedback": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FeedbackItem"
          }
        },
        "status": {
          "format": "int32",
          "type": "integer"
        },
        "identifier": {
          "format": "uuid",
          "type": "string"
        }
      }
    },
    "DataType": {
      "required": ["id", "name"],
      "type": "object",
      "properties": {
        "id": {
          "format": "uuid",
          "type": "string"
        },
        "synced": {
          "type": "boolean"
        },
        "template": {
          "type": "object",
          "additionalProperties": {
            "type": "object"
          }
        },
        "name": {
          "type": "string"
        },
        "attributes": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/IAttributeType"
          }
        }
      }
    },
    "Generation": {
      "type": "object",
      "properties": {
        "timeStamp": {
          "format": "date-time",
          "type": "string"
        },
        "duration": {
          "format": "int64",
          "type": "integer"
        }
      }
    },
    "FeedbackItem": {
      "type": "object",
      "properties": {
        "type": {
          "format": "int32",
          "enum": [1, 2, 3, 4],
          "type": "integer"
        },
        "description": {
          "type": "string"
        }
      }
    },
    "IAttributeType": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "readOnly": true
        },
        "name": {
          "type": "string"
        },
        "isRequired": {
          "type": "boolean"
        },
        "defaultValue": {
          "type": "object"
        },
        "isDeleted": {
          "type": "boolean"
        },
        "isCollection": {
          "type": "boolean"
        }
      }
    },
    "Response": {
      "type": "object",
      "properties": {
        "self": {
          "type": "string"
        },
        "skip": {
          "format": "int32",
          "type": "integer"
        },
        "take": {
          "format": "int32",
          "type": "integer"
        },
        "total": {
          "format": "int32",
          "type": "integer"
        },
        "generation": {
          "$ref": "#/definitions/Generation"
        },
        "feedback": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FeedbackItem"
          }
        },
        "status": {
          "format": "int32",
          "type": "integer"
        },
        "identifier": {
          "format": "uuid",
          "type": "string"
        }
      }
    },
    "Response[List[DataType]]": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DataType"
          }
        },
        "self": {
          "type": "string"
        },
        "skip": {
          "format": "int32",
          "type": "integer"
        },
        "take": {
          "format": "int32",
          "type": "integer"
        },
        "total": {
          "format": "int32",
          "type": "integer"
        },
        "generation": {
          "$ref": "#/definitions/Generation"
        },
        "feedback": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FeedbackItem"
          }
        },
        "status": {
          "format": "int32",
          "type": "integer"
        },
        "identifier": {
          "format": "uuid",
          "type": "string"
        }
      }
    },
    "DataTypeDTO": {
      "required": ["name"],
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "attributes": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/IAttributeType"
          }
        }
      }
    },
    "Response[SystemStatus]": {
      "type": "object",
      "properties": {
        "data": {
          "$ref": "#/definitions/SystemStatus"
        },
        "self": {
          "type": "string"
        },
        "skip": {
          "format": "int32",
          "type": "integer"
        },
        "take": {
          "format": "int32",
          "type": "integer"
        },
        "total": {
          "format": "int32",
          "type": "integer"
        },
        "generation": {
          "$ref": "#/definitions/Generation"
        },
        "feedback": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/FeedbackItem"
          }
        },
        "status": {
          "format": "int32",
          "type": "integer"
        },
        "identifier": {
          "format": "uuid",
          "type": "string"
        }
      }
    },
    "SystemStatus": {
      "type": "object",
      "properties": {
        "id": {
          "format": "uuid",
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "moreInfo": {
          "type": "string"
        },
        "version": {
          "type": "string"
        },
        "environment": {
          "type": "string"
        },
        "builtOn": {
          "format": "date-time",
          "type": "string"
        },
        "up": {
          "type": "boolean"
        },
        "components": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Component"
          }
        }
      }
    },
    "Component": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "up": {
          "type": "boolean"
        },
        "moreInfo": {
          "type": "string"
        }
      }
    }
  },
  "securityDefinitions": {}
}
