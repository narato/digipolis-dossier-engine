﻿using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Narato.Common.Models;
using Narato.Common.Exceptions;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Clients
{
    // TODO: this is as generic as it gets. move to Digipolis.Common
    public class TenantClient : ITenantClient
    {
        private readonly HttpClient _client;

        public TenantClient(HttpClient httpClient)
        {
            _client = httpClient;
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var bla = new HttpClient();
        }

        public async Task<Tenant<Config>> GetTenantAsync(string key)
        {
            var response = await _client.GetAsync($"tenants/{key}");

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                var tenantResponse = JsonConvert.DeserializeObject<Response<Tenant<Config>>>(responseString);
                return tenantResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The tenant could not be retrieved. Received a bad request from Admin api." });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                if (response.Content == null)
                    throw new EntityNotFoundException();
                var responseString = await response.Content.ReadAsStringAsync();
                var tenantResponse = JsonConvert.DeserializeObject<Response<Tenant<Config>>>(responseString);
                throw new EntityNotFoundException(tenantResponse.Feedback[0].Description);
            }

            return null;
        }
    }
}
