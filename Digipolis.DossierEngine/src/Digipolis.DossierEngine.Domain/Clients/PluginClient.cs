﻿using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Digipolis.DossierEngine.Domain.Clients
{
    public class PluginClient : IPluginClient
    {
        private readonly HttpClient _client;

        public PluginClient(HttpClient httpClient)
        {
            _client = httpClient;
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void SyncUnsyncedSyncDataObject(PluginObjectLookupDto pluginObjectLookup)
        {
            var url = $"pluginobjectlookup";
            HttpContent content = new StringContent(JsonConvert.SerializeObject(pluginObjectLookup));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + url);


            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            HttpResponseMessage response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var feedbackResponse = JsonConvert.DeserializeObject<Response>(responseString);
                throw new ExceptionWithFeedback(feedbackResponse.Feedback);
            }
        }
    }
}
