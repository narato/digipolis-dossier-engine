﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class AddedForeignKeyIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenantPluginLink_Plugin_PluginId",
                table: "TenantPluginLink");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantPluginLink_Tenant_TenantId",
                table: "TenantPluginLink");

            migrationBuilder.AlterColumn<Guid>(
                name: "TenantId",
                table: "TenantPluginLink",
                nullable: false);

            migrationBuilder.AlterColumn<Guid>(
                name: "PluginId",
                table: "TenantPluginLink",
                nullable: false);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantPluginLink_Plugin_PluginId",
                table: "TenantPluginLink",
                column: "PluginId",
                principalTable: "Plugin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantPluginLink_Tenant_TenantId",
                table: "TenantPluginLink",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenantPluginLink_Plugin_PluginId",
                table: "TenantPluginLink");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantPluginLink_Tenant_TenantId",
                table: "TenantPluginLink");

            migrationBuilder.AlterColumn<Guid>(
                name: "TenantId",
                table: "TenantPluginLink",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "PluginId",
                table: "TenantPluginLink",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantPluginLink_Plugin_PluginId",
                table: "TenantPluginLink",
                column: "PluginId",
                principalTable: "Plugin",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantPluginLink_Tenant_TenantId",
                table: "TenantPluginLink",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
