﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using System.Threading.Tasks;
using Neo4jClient;
using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Models;
using NLog;

namespace Digipolis.DossierEngine.Domain.Providers
{
    public class PooledTenantGraphClientProvider : IPooledGraphClientProvider
    {
        private readonly IGroupedLimitedPool<string, IGraphClient> _clientPool;
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;
        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public PooledTenantGraphClientProvider(IGroupedLimitedPool<string, IGraphClient> clientPool, ICurrentTenantProvider<Config> currentTenantProvider)
        {
            _clientPool = clientPool;
            _currentTenantProvider = currentTenantProvider;
        }

        public async Task<ILimitedPoolItem<IGraphClient>> GetAsync()
        {
            var tenantKey = _currentTenantProvider.GetCurrentTenantKey();
            Logger.Info("idle count for graphclientPool for tenant " + tenantKey + ": " + _clientPool.IdleCount(tenantKey));
            return await _clientPool.GetAsync(tenantKey);
        }
    }
}
