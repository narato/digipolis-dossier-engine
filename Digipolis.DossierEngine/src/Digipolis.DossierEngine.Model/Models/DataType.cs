﻿using Digipolis.DossierEngine.Model.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.DossierEngine.Model.Models
{
    public class DataType : DataTypeDTO
    {
        [Required]
        public Guid Id { get; set; }

        public bool? Synced { get; set; }
        public Dictionary<string, object> Template { get; set; }
    }
}