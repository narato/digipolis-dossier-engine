﻿namespace Digipolis.DossierEngine.DataProvider.Changes
{
    public enum ChangeType
    {
        NEW,
        UPDATED,
        DELETED
    //    SKIP
    }
}
