﻿using Digipolis.Common.Events;
using Digipolis.Common.Events.Models;

namespace Digipolis.DossierEngine.Common.Events
{
    public class NoOpEventHandler : IEventPublisher, IEventHandlerAdministrator, IEventSubscriber, IEventhandlerPing
    {
        public void EnsureNamespaceExists(NamespaceConfiguration newNamespace)
        {
            return;
        }

        public void EnsureTopicExists(string ownerKey, string namespaceOfTopic, string topic)
        {
            return;
        }

        public bool Ping()
        {
            return true;
        }

        public void Publish(string ownerKey, string eventNamespace, string topic, object payload)
        {
            return;
        }

        public void Subscribe(string eventNamespace, string nameSubscription, Subscription subscription, string subscriptionOwnerKey)
        {
            return;
        }
    }
}
