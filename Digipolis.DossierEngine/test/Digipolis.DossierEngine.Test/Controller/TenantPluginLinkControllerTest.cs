﻿using Digipolis.DossierEngine.Admin.API.Controllers;
using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class TenantPluginLinkControllerTest
    {
        [Fact]
        public void InsertTenantPluginLinkReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreatePostResponseAsync(
                It.IsAny<Func<Task<TenantPluginLink>>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Func<TenantPluginLink, object>>()))
                .ReturnsAsync(new ObjectResult(null));

            var controller = new TenantPluginLinkController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertTenantPluginLinkAsync(new TenantPluginLinkDTO()).Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void InsertTenantPluginLinkWithoutObjectReturnsBadRequest()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new TenantPluginLinkController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertTenantPluginLinkAsync(null).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void DeleteTenantPluginLinkReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateDeleteResponse(It.IsAny<Action>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new TenantPluginLinkController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.Delete(It.IsAny<Guid>());

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetPluginsForTenantReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<List<TenantPluginLink>>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new TenantPluginLinkController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetPluginsForTenantAsync(It.IsAny<string>()).Result;

            Assert.NotNull(actionResult);
        }
    }
}
