﻿using Digipolis.DossierEngine.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.DossierEngine.Model.Dto
{
    public class DataTypeDTO
    {
        [Required]
        public string Name { get; set; }
        public List<IAttributeType> Attributes { get; set; }
    }
}
