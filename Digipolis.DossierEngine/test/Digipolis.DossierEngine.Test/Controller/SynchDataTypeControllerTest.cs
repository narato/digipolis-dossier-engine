﻿using Digipolis.DossierEngine.Admin.API.Controllers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using System;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class SynchDataTypeControllerTest
    {
        [Fact]
        public void GetAllReturnsAnActionResult()
        {
            var synchDataTypeManagerMock = new Mock<ISyncedDataTypeManager>(MockBehavior.Strict);
            var factoryMock = new Mock<IResponseFactory>(MockBehavior.Strict);

            factoryMock.Setup(fm => fm.CreateGetResponse(It.IsAny<Func<List<List<Model.Models.DataType>>>>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new SynchDataTypeController(synchDataTypeManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAll();

            Assert.NotNull(actionResult);
        }

    }
}
