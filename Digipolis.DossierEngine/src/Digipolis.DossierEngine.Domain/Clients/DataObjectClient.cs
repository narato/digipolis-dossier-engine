﻿using Digipolis.DossierEngine.Common.Converters;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Digipolis.DossierEngine.Domain.Clients
{
    public class DataObjectClient : IDataObjectClient
    {
        private readonly HttpClient _client;
        private readonly string _tenantKeyName;

        public DataObjectClient(HttpClient httpClient)
        {
            _client = httpClient;
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _tenantKeyName = "Tenant-Key";
        }

        public DataObject InsertDataObject(string dataObjectJson, string tenantKey)
        {
            var url = $"dataobjects";
            HttpContent content = new StringContent(dataObjectJson);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + url);

            AddTenantKeyHeader(tenantKey);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;

            HttpResponseMessage response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            var dataTypeResponse = JsonConvert.DeserializeObject<Response<DataObject>>(responseString, new AttributeTypeConverter());

            return dataTypeResponse.Data;
        }

        public DataObject UpdateSyncedDataObject(string dataObjectJson, string externalId, string tenantKey)
        {
            var url = $"synceddataobjects/{externalId}";
            HttpContent content = new StringContent(dataObjectJson);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, _client.BaseAddress + url);

            AddTenantKeyHeader(tenantKey);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;


            HttpResponseMessage response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var dataObjectResponse = JsonConvert.DeserializeObject<Response<DataObject>>(responseString);
                return dataObjectResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new ExceptionWithFeedback(new FeedbackItem() { Description = $"The SyncedDataObject could not be updated." });
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new EntityNotFoundException();
            }

            return null;
        }

        public DataObject CreateSyncedDataObject(string dataObjectJson, string tenantKey)
        {
            var url = $"synceddataobjects";
            HttpContent content = new StringContent(dataObjectJson);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _client.BaseAddress + url);

            AddTenantKeyHeader(tenantKey);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            request.Content = content;


            HttpResponseMessage response = _client.SendAsync(request, HttpCompletionOption.ResponseContentRead).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var dataObjectResponse = JsonConvert.DeserializeObject<Response<DataObject>>(responseString);
                return dataObjectResponse.Data;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                var parsedResponse = JsonConvert.DeserializeObject<Response>(responseString);
                throw new ExceptionWithFeedback(parsedResponse.Feedback);
            }

            return null;
        }

        private void AddTenantKeyHeader(string tenantKey)
        {
            if (_client.DefaultRequestHeaders.Contains(_tenantKeyName))
                _client.DefaultRequestHeaders.Remove(_tenantKeyName);

            _client.DefaultRequestHeaders.Add(_tenantKeyName, tenantKey);
        }
    }
}
