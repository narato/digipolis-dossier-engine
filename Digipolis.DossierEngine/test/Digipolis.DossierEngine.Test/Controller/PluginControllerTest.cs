﻿using Digipolis.DossierEngine.Admin.API.Controllers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using System;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class PluginControllerTest
    {
        [Fact]
        public void GetPluginsReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponse(It.IsAny<Func<List<Plugin>>>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new PluginController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetPlugins();

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void LoadPluginsReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponse(It.IsAny<Func<string>>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new PluginController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.LoadPlugins();

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void EventReceiverReturnsAnActionResult()
        {
            var pluginManagerMock = new Mock<IPluginManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponse(It.IsAny<Func<string>>(), It.IsAny<string>())).Returns(new ObjectResult(null));

            var controller = new PluginController(pluginManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.EventReceiver(It.IsAny<string>(), It.IsAny<object>());

            Assert.NotNull(actionResult);
        }
    }
}
