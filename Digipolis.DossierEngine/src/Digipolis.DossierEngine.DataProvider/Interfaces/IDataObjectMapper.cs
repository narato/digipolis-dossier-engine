﻿using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IDataObjectMapper
    {
        List<DataObject> Map(IEnumerable<RawDataObject> toMap);
    }
}
