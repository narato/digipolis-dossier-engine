﻿using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.Domain.Changes
{
    // TODO: rename to more appropriate name? couldn't think of anything better
    public class DataTypeChangeCalculator : IChangeCalculator<DataType>
    {
        public IEnumerable<Change> CalculateChangeSet(DataType originalDataType, DataType dataType)
        {
            var changeList = new List<Change>();

            foreach (BaseAttributeType att in dataType.Attributes)
            {
                if (originalDataType.Attributes.Exists(a => a.Name == att.Name))
                {
                    var originalAtt = originalDataType.Attributes.First(a => a.Name == att.Name);

                    if ((att.IsRequired != originalAtt.IsRequired) || (att.DefaultValue == null && originalAtt.DefaultValue != null) || (att.DefaultValue != null && !att.DefaultValue.Equals(originalAtt.DefaultValue)))
                    {
                        changeList.Add(new Change { Type = ChangeType.UPDATED, Attribute = att.Name });
                    }

                    // TODO: find out WHY BELOW LINES WERE NEEDED?!!!!!!!!!
                    //if (!changeList.Exists(c => c.Attribute == att.Name))
                    //{
                    //    changeList.Add(new Change { Type = ChangeType.SKIP, Attribute = att.Name });
                    //}
                }
                else
                {
                    changeList.Add(new Change { Type = ChangeType.NEW, Attribute = att.Name });
                }
            }

            var diff = originalDataType.Attributes.Select(a => a.Name).Except(dataType.Attributes.Select(b => b.Name));
            diff.ToList().ForEach(n => changeList.Add(new Change { Type = ChangeType.DELETED, Attribute = n }));

            return changeList;
        }
    }
}
