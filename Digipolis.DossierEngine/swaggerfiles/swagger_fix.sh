#!/bin/bash

# Remove the isDeleted Attribute from the list to be shown
sed -i 's/,"isDeleted":{"type":"boolean"}//g' dossier-datatype-api-swagger.json

# Add description for header params and make them required
sed -i 's/{"name":"Tenant-Key","in":"header","description":"The key of the tenant","required":false,"type":"string"}/{"name":"Tenant-Key","in":"header","description":"The key of the tenant","required":true,"type":"string"}/g' dossier-datatype-api-swagger.json
sed -i 's/{"name":"Tenant-Key","in":"header","description":"The key of the tenant","required":false,"type":"string"}/{"name":"Tenant-Key","in":"header","description":"The key of the tenant","required":true,"type":"string"}/g' dossier-dataobject-api-swagger.json

# FromBody parameters need to be required
sed -i 's/{"name":"tenantDTO","in":"body","description":"The tenant to be saved","required":false,/{"name":"tenantDTO","in":"body","description":"The tenant to be saved","required":true,/g' dossier-admin-api-swagger.json
sed -i 's/{"name":"dataObjectDTO","in":"body","description":"The dataobject to create","required":false/{"name":"dataObjectDTO","in":"body","description":"The dataobject to create","required":true/g' dossier-dataobject-api-swagger.json
sed -i 's/{"name":"dataTypeDTO","in":"body","description":"The datatype to create","required":false/{"name":"dataTypeDTO","in":"body","description":"The datatype to create","required":true/g' dossier-datatype-api-swagger.json
sed -i 's/{"name":"payload","in":"body","description":"The payload coming from the eventhandler","required":false,"schema":{"type":"object"}}/{"name":"payload","in":"body","description":"The payload coming from the eventhandler","required":true,"schema":{"type":"object"}}/g' dossier-admin-api-swagger.json
sed -i 's/{"name":"tenant","in":"body","required":false/{"name":"tenant","in":"body","required":true/g' dossier-admin-api-swagger.json


# Posting a tenant needs a summary
sed -i 's/{"tags":["Tenant"],"operationId":"ApiTenantsPost"/{"tags":["Tenant"],"summary":"This method saves 1 tenant given in the body of the request","operationId":"ApiTenantsPost"/g' dossier-admin-api-swagger.json
