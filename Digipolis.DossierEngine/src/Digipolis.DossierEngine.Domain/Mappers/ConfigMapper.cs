﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Mappers;
using Newtonsoft.Json;

namespace Digipolis.DossierEngine.Domain.Mappers
{
    // TODO: this is as generic as it gets. move to Digipolis.Common
    public class ConfigMapper<ConfigT> : IConfigMapper<ConfigT>
    {
        public Digipolis.Common.Tenancy.Models.Tenant Map(Tenant<ConfigT> tenant)
        {
            return new Digipolis.Common.Tenancy.Models.Tenant 
            {
                Id = tenant.Id,
                Description = tenant.Description,
                Key = tenant.Key,
                Name = tenant.Name,
                Status = tenant.Status,
                Config = JsonConvert.SerializeObject(tenant.Config),
                CreatedAt = tenant.CreatedAt
            };
        }

        public Tenant<ConfigT> Map(Digipolis.Common.Tenancy.Models.Tenant tenant)
        {
            return new Tenant<ConfigT>()
            {
                Id = tenant.Id,
                Description = tenant.Description,
                Key = tenant.Key,
                Name = tenant.Name,
                Status = tenant.Status,
                Config = JsonConvert.DeserializeObject<ConfigT>(tenant.Config),
                CreatedAt = tenant.CreatedAt
            };
        }
    }
}
