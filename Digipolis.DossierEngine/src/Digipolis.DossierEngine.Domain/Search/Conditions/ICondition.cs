﻿
using Digipolis.DossierEngine.Domain.Search.Conditions.Visitor;

namespace Digipolis.DossierEngine.Domain.Search.Conditions
{
    public interface ICondition
    {
        string visit(IConditionVisitor visitor);
    }
}
