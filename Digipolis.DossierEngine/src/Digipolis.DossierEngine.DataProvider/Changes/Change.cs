﻿namespace Digipolis.DossierEngine.DataProvider.Changes
{
    public class Change
    {
        public ChangeType Type { get; set; }
        public string Attribute { get; set; }
        public object NewValue { get; set; }
    }
}
