﻿using Digipolis.DossierEngine.DataProvider.Helpers;
using Digipolis.DossierEngine.Domain.Validators;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.DossierEngine.Test.Validators
{
    public class DataObjectValidatorTest
    {
        [Fact]
        public void ValidateDataObject_TypeCheck_ReturnsFeedbackList()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Clear();

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.IsType<List<FeedbackItem>>(result);
        }

        [Fact]
        public void ValidateDataObject_MissingIsRequiredType_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new StringAttributeType { IsRequired = true, Name = "StringType" });

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDataObject_MissingIsRequiredTypeWithDefaultValue_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new StringAttributeType { IsRequired = true, Name = "String1", DefaultValue = "myDefault", RegexValidation = "^[a-zA-Z]*$" });

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateDataObject_UnknownType_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataObject.Values.Add("Unknown", "");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateStringType_RegExInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new StringAttributeType { IsRequired = true, RegexValidation = "^[a-zA-Z]*$", Name = "String1" });
            dataObject.Values.Add("String1", "123");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateStringType_RegExValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new StringAttributeType { IsRequired = true, RegexValidation = "^[a-zA-Z]*$", Name = "String1" });
            dataObject.Values.Add("String1", "abc");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateIntType_MinRangeInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new IntAttributeType { IsRequired = true, MinRangeValidation = 0, Name = "Int1" });
            dataObject.Values.Add("Int1", -1);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateIntType_MaxRangeInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new IntAttributeType { IsRequired = true, MaxRangeValidation = 100, Name = "Int1" });
            dataObject.Values.Add("Int1", 101);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateIntType_IntParseInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new IntAttributeType { IsRequired = true, Name = "Int1" });
            dataObject.Values.Add("Int1", "a");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateIntType_IntValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new IntAttributeType { IsRequired = true, Name = "Int1" });
            dataObject.Values.Add("Int1", 1);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_MinRangeInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DoubleAttributeType { IsRequired = true, MinRangeValidation = 0.5, Name = "Double1" });
            dataObject.Values.Add("Double1", -1.5);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_MaxRangeInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DoubleAttributeType { IsRequired = true, MaxRangeValidation = 100.5, Name = "Double1" });
            dataObject.Values.Add("Double1", 101.5);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_IntParseInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DoubleAttributeType { IsRequired = true, Name = "Double1" });
            dataObject.Values.Add("Double1", "a");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDoubleType_IntValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DoubleAttributeType { IsRequired = true, Name = "Double1" });
            dataObject.Values.Add("Double1", 1);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateBoolType_BoolValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new BooleanAttributeType { IsRequired = true, Name = "Bool1" });
            dataObject.Values.Add("Bool1", true);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateBoolType_BoolParseInValid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new BooleanAttributeType { IsRequired = true, Name = "Bool1" });
            dataObject.Values.Add("Bool1", "a");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateLinkType_UriInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new LinkAttributeType { IsRequired = true, Name = "Link1" });
            dataObject.Values.Add("Link1", "htp://www.mylink.be");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateLinkType_UriValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new LinkAttributeType { IsRequired = true, Name = "Link1" });
            dataObject.Values.Add("Link1", "http://www.mylink.be");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        [Fact]
        public void ValidateDateTimeType_DateInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DateTimeAttributeType { IsRequired = true, Name = "DateTime1" });
            dataObject.Values.Add("DateTime1", "a");

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDateTimeType_DateUtcInvalid_ReturnsFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DateTimeAttributeType { IsRequired = true, Name = "DateTime1" });
            dataObject.Values.Add("DateTime1", DateTime.Now);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void ValidateDateTimeType_DateUtcValid_ReturnsNoFeedback()
        {
            var nestedDataObjectHelper = new NestedDataObjectHelper();
            var dataType = InitDataTypeModel();
            var dataObject = InitDataObjectModel();

            dataType.Attributes.Add(new DateTimeAttributeType { IsRequired = true, Name = "DateTime1" });
            dataObject.Values.Add("DateTime1", DateTime.UtcNow);

            var dataObjectValidator = new DataObjectValidator(nestedDataObjectHelper);
            var result = dataObjectValidator.ValidateDataObjectAttributes(dataType, dataObject);

            Assert.Equal(0, result.Count);
        }

        private Model.Models.DataType InitDataTypeModel()
        {
            return new Model.Models.DataType
            {
                Id = Guid.NewGuid(),
                Name = "DataType1",
                Attributes = new List<IAttributeType>()
            };
        }

        private Model.Models.DataObject InitDataObjectModel()
        {
            return new Model.Models.DataObject
            {
                Id = Guid.NewGuid(),
                Name = "DataObject1",
                Values = new Dictionary<string, object>()
            };
        }
    }
}