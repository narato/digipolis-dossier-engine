﻿using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/pluginobjectlookup")]
    public class PluginObjectLookupController : Controller
    {
        private IPluginManager _pluginManager;
        private IResponseFactory _responseFactory;

        public PluginObjectLookupController(IPluginManager pluginManager, IResponseFactory responseFactory)
        {
            _responseFactory = responseFactory;
            _pluginManager = pluginManager;
        }

        [HttpPost]
        public IActionResult Lookup([FromBody] PluginObjectLookupDto pluginObjectLookup)
        {
            return _responseFactory.CreateGetResponse(() => _pluginManager.UpdateExternalDataObjectByExternalId(pluginObjectLookup), this.GetRequestUri());
        }
    }
}
