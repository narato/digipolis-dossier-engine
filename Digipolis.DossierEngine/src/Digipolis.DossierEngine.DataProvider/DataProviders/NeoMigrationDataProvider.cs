﻿using System;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Microsoft.Extensions.Options;
using Neo4jClient;
using Neo4jClient.Cypher;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class NeoMigrationDataProvider : INeoMigrationDataProvider
    {
        private readonly DossierConfiguration _dossierConfiguration;
        private readonly ITenantGraphClientFactory _graphClientFactory;

        public NeoMigrationDataProvider(IOptions<DossierConfiguration> dossierConfiguration, ITenantGraphClientFactory graphClientFactory)
        {
            _dossierConfiguration = dossierConfiguration.Value;
            _graphClientFactory = graphClientFactory;
        }

        public void SetupDatabase(Config tenantConfig)
        {
            using (var client = _graphClientFactory.CreateForConfig(tenantConfig))
            {
                client.Connect();

                //Indexes
                var indexQueries = GetIndexes();
                foreach (var indexQuery in indexQueries)
                {
                    var query = new CypherQuery(indexQuery, null, CypherResultMode.Set);
                    ((IRawGraphClient)client).ExecuteCypher(query);
                }

                //Time series
                var queryParams = new Dictionary<string, object>();
                queryParams.Add("minyear", _dossierConfiguration.MinYear);
                queryParams.Add("maxyear", _dossierConfiguration.MaxYear);

                var queryTimeseries = new CypherQuery(TimeSeriesQuery(), queryParams, CypherResultMode.Set);
                var result = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryTimeseries);
                var yearCount = result.First();
                if (yearCount != (_dossierConfiguration.MaxYear - _dossierConfiguration.MinYear + 1))
                    throw new Exception("Create of Time Series failed");

            }
        }

        private string[] GetIndexes()
        {
            return new[] {
                "CREATE CONSTRAINT ON (y:Year) ASSERT y.value IS UNIQUE",
                "CREATE INDEX ON :Month(value)",
                "CREATE INDEX ON :Day(value)",
                "CREATE CONSTRAINT ON (dt:DataType) ASSERT dt.id IS UNIQUE",
                "CREATE CONSTRAINT ON (do:DataObject) ASSERT do.id IS UNIQUE",
                "CREATE CONSTRAINT ON (do:NestedDataObject) ASSERT do.id IS UNIQUE",
                "CREATE INDEX ON :NestedDataObject(externalId)",
                "CREATE INDEX ON :DataObject(externalId)"
            };
        }

        private string TimeSeriesQuery()
        {
            return @"WITH range({minyear}, {maxyear}) AS years, range(1,12) AS months
                    FOREACH(year IN years |
                        CREATE (y:Year {value: year})
                        FOREACH(month IN months |
                        CREATE (m:Month {value: month})
                        MERGE (y)-[:CONTAINS]->(m)
                        FOREACH(day IN (CASE
                                            WHEN month IN [1,3,5,7,8,10,12] THEN range(1,31)
                                            WHEN month = 2 THEN
                                            CASE
                                                WHEN year % 4 <> 0 THEN range(1,28)
                                                WHEN year % 100 = 0 AND year % 400 = 0 THEN range(1,29)
                                                ELSE range(1,28)
                                            END
                                            ELSE range(1,30)
                                        END) |
                            CREATE (d:Day {value: day})
                            MERGE (m)-[:CONTAINS]->(d))))
                    WITH *
                    // connect years, but only if the difference is 1 year
                    MATCH (year:Year)
                    WITH year
                    ORDER BY year.value
                    WITH collect(year) AS years
                    FOREACH(i in RANGE(0, size(years)-2) |
                        FOREACH(year1 in [years[i]] |
                            FOREACH(year2 in [years[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN year2.value - year1.value = 1 THEN [1] ELSE [] END | CREATE UNIQUE (year1)-[:NEXT]->(year2))
                            )
                        )
                    )
                    WITH *
                    // connect months, but ONLY when the following month is within the same year or in the following year. Not if there's a gap in years
                    MATCH (year:Year)-[:CONTAINS]->(month)
                    WITH year, month
                    ORDER BY year.value, month.value
                    WITH collect(month) AS months, collect(year) as years
                    FOREACH(i in RANGE(0, size(months)-2) |
                        FOREACH(month1 in [months[i]] |
                            FOREACH(month2 in [months[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN (head([years[i + 1]]).value = head([years[i]]).value) OR (head([years[i + 1]]).value - head([years[i]]).value = 1) THEN [1] ELSE [] END | CREATE UNIQUE (month1)-[:NEXT]->(month2))
                            )
                        )
                    )
                    WITH *
                    // connect days, but ONLY when the following day is within the same year or in the following year. Not if there's a gap in years
                    MATCH (year:Year)-[:CONTAINS]->(month)-[:CONTAINS]->(day)
                    WITH year, month, day
                    ORDER BY year.value, month.value, day.value
                    WITH collect(day) AS days, collect(year) as years
                    FOREACH(i in RANGE(0, size(days)-2) |
                        FOREACH(day1 in [days[i]] |
                            FOREACH(day2 in [days[i+1]] |
        	                    FOREACH(ignoreMe IN CASE WHEN (head([years[i + 1]]).value = head([years[i]]).value) OR (head([years[i + 1]]).value - head([years[i]]).value = 1) THEN [1] ELSE [] END | CREATE UNIQUE (day1)-[:NEXT]->(day2))
            
                            )
                        )
                    )
                    WITH *
                    MATCH (y:Year)
                    RETURN COUNT(y) as count";
        }
    }
}