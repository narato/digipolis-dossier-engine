﻿using Digipolis.DossierEngine.Common.Models;
using Digipolis.Tenant.Library.Tenancy;
using System.Collections.Generic;
using Digipolis.Common.Tenancy.MappedModels;
using Narato.Common.Models;
using Narato.Common.Exceptions;

namespace Digipolis.DossierEngine.Domain.Validators
{
    public class TenantValidator : ITenantValidator<Config>
    {
        public void ValidateForInsert(Tenant<Config> tenant)
        {
            List<FeedbackItem> feedbackItems = new List<FeedbackItem>();

            if (string.IsNullOrEmpty(tenant.Name))
            {
                feedbackItems.Add(new FeedbackItem() { Description = "The tenant has no value for the name property.", Type = FeedbackType.ValidationError });
            }

            // TODO: more validation for tenant?

            if (feedbackItems.Count > 0)
            {
                throw new ValidationException(feedbackItems);
            }
        }
    }
}
