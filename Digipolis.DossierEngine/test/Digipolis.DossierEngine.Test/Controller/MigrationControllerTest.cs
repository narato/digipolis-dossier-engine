﻿using Digipolis.DossierEngine.Admin.API.Controllers;
using Digipolis.DossierEngine.DataProvider.Managers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Exceptions;
using Narato.Common.Factory;
using Narato.Common.Mappers;
using Xunit;

namespace Digipolis.Tenant.Library.Test.Controllers
{
    public class MigrationControllerTest
    {
        [Fact]
        public void PostExecutesMigrations()
        {

            Mock<IMigrationManager> migManagerMock = new Mock<IMigrationManager>();

            var exceptionHandler = new ExceptionHandler();
            var exceptionMapper = new ExceptionToActionResultMapper();
            var responseFactory = new ResponseFactory(exceptionHandler, exceptionMapper);
            migManagerMock.Setup(x => x.ExecutePendingMigrations());
            Mock<IResponseFactory> responseFactoryMock = new Mock<IResponseFactory>();

            var controller = new MigrationController(migManagerMock.Object, responseFactory);

            IActionResult actionResult = controller.Post();

            migManagerMock.Verify(x => x.ExecutePendingMigrations(), Times.Once);
            Assert.NotNull(actionResult);
        }
    }
}
