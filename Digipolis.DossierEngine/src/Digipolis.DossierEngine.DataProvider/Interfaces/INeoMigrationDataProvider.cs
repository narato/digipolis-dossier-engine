﻿using Digipolis.DossierEngine.Common.Models;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface INeoMigrationDataProvider
    {
        void SetupDatabase(Config tenantConfig);
    }
}