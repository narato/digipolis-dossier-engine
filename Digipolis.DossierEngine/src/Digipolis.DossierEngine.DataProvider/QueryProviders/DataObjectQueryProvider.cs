﻿using Digipolis.DossierEngine.Common.Util;
using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.Model.Models;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public class DataObjectQueryProvider : IDataObjectQueryProvider
    {
        private readonly StringHelper _stringHelper;

        public DataObjectQueryProvider(StringHelper stringHelper)
        {
            _stringHelper = stringHelper;
        }

        public ICypherFluentQuery UpdateDataObjectWithChange(DataObject dataObject, ICypherFluentQuery query, Change change)
        {
            switch (change.Type)
            {
                case ChangeType.UPDATED:
                    return UpdatePropertyOfDataObject(dataObject, query, change);
                case ChangeType.NEW:
                    return CreatePropertyOfDataObject(dataObject, query, change, "dobj");
                case ChangeType.DELETED:
                    return DeletePropertyOfDataObject(dataObject, query, change);
                default:
                    throw new Exception("changetype " + change.Type + " doesnt exist?");
            }
        }

        private ICypherFluentQuery MatchNestedDataObject(DataObject dataObject, ICypherFluentQuery query, ref string dataObjectNodeName, ref string attributeName)
        {
            var activeName = dataObject.Name; // and this is why we don't allow the name of dataObjects to be updated
            var segments = attributeName.Split('.');
            attributeName = segments.Last();
            foreach (var property in segments)
            {
                if (property == attributeName)
                {
                    break;
                }
                activeName = activeName + "." + property;
                query = query.Match($"({dataObjectNodeName})-[:HAS_ATTRIBUTE]->(:AttributeValue)-[:OF_VALUE]->(do{property}:NestedDataObject {{name:'{activeName}'}})");
                dataObjectNodeName = $"do{property}";
            }
            return query;
        }

        private ICypherFluentQuery CreatePropertyOfDataObject(DataObject dataObject, ICypherFluentQuery query, Change change, string activeDO)
        {
            var paramName = _stringHelper.RemoveSpecialCharacters($"attributeValue{change.Attribute}" + Guid.NewGuid().ToString());
            var attributeName = change.Attribute;
            if (change.Attribute.Contains("."))
            {
                query = MatchNestedDataObject(dataObject, query, ref activeDO, ref attributeName);
            }

            query = query.Match($"({activeDO})-[:OF_DATATYPE]->(:DataType)-[:HAS_ATTRIBUTE]->(at:AttributeType {{name:'{attributeName}'}})"); // match the attribute type

            // if the value is a DataObject
            if (change.NewValue is IEnumerable<Change>)
            {
                var nestedNeoDataObject = new NeoDataObject { Id = Guid.NewGuid(), Name = dataObject.Name + "." + change.Attribute };
                var sanitizedName = _stringHelper.RemoveSpecialCharacters(nestedNeoDataObject.Name).ToLower();
                // create new nested object!!!
                query = query.Match("(at)-[:OF_NESTED_TYPE]->(dt:DataType)") // first we match the datatype of the nested object we're about to insert
                    .OptionalMatch($"({activeDO})-[oldHA:HAS_ATTRIBUTE]->(oldAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)") // maybe an old value already existed but was deleted?
                    .Create($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue {{{paramName}}})-[:OF_ATTRIBUTETYPE]->(at)") // create new value, link DO to it and link the value to at. Also set the dataObjectId of the value
                    .Create($"(nesteddo{attributeName}:NestedDataObject {{{sanitizedName}}})-[:OF_DATATYPE]->(dt)") // create the new nested DataObject
                    .Create($"(newAv)-[:OF_VALUE]->(nesteddo{attributeName})") // link the new av to the newly created NestedDataObject
                    .Create("(newCs)-[:CHANGED]->(newAv)") // link the new ChangeSet to the new value.
                    .ForEach("(ignoreMe IN CASE WHEN oldAv is not null THEN [1] ELSE [] END | CREATE (newAv)-[:UPDATED_FROM]->(oldAv) DELETE oldHA)") // foreach here is a trick to mimic if-else. Create Updated_from link in case previous value was deleted, and delete the old link with the DO
                    .WithParam(paramName, new NeoAttributeValue { DataObjectId = nestedNeoDataObject.Id })
                    .WithParam(sanitizedName, nestedNeoDataObject)
                    .With($"nesteddo{attributeName}, dobj, newCs");

                var nestedChange = change.NewValue as IEnumerable<Change>;

                // order the list. Subnested DataObjects should come last (so we don't get in trouble with the with cypher clause
                nestedChange = nestedChange.OrderBy(c => (c.NewValue is IEnumerable<Change>) ? 1 : 0);

                // now loop over the changes and insert them one by one (1 change is 1 property)
                foreach (var insert in nestedChange)
                {
                    query = CreatePropertyOfDataObject(dataObject, query, insert, $"nesteddo{attributeName}");
                }
                return query;
            // else if value is an ARRAY of DataObjects
            } else if (change.NewValue is NestedChangeList)
            {
                var nestedDataObjectChangeList = change.NewValue as NestedChangeList;

                query = query.Match("(at)-[:OF_NESTED_TYPE]->(dt:DataType)") // first we match the datatype of the nested object we're about to insert
                   .OptionalMatch($"({activeDO})-[oldHA:HAS_ATTRIBUTE]->(oldAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)") // maybe an old value already existed but was deleted?
                   .Create($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)") // create new value, link DO to it and link the value to at. Also set the dataObjectIdList of the value
                   .Create("(newCs)-[:CHANGED]->(newAv)") // link the new ChangeSet to the new value.
                   .ForEach("(ignoreMe IN CASE WHEN oldAv is not null THEN [1] ELSE [] END | CREATE (newAv)-[:UPDATED_FROM]->(oldAv) DELETE oldHA)"); // foreach here is a trick to mimic if-else. Create Updated_from link in case previous value was deleted, and delete the old link with the DO

                // now we create all the nested NeoDataObjects and merge them with the query
                foreach (var nestedChange in nestedDataObjectChangeList.NestedObjects) // we loop over the changesets that represent the nested DataObjects (so their newValues are also a list of changes)
                {
                    query = CreateNestedDataObjectForList(dataObject, query, activeDO, nestedChange)
                        .Match($"({activeDO})-[:OF_DATATYPE]->(:DataType)-[:HAS_ATTRIBUTE]->(at:AttributeType {{name:'{attributeName}'}})") // rematch the at
                        .Match("(at)-[:OF_NESTED_TYPE]->(dt:DataType)") // rematch the dt
                        .Match($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)"); // rematch the newAv
                }

                query = query.Match("(doForIdList:NestedDataObject)<-[:OF_VALUE]-(newAv)")
                    .With($"dobj, newCs, newAv, collect(distinct doForIdList.id) as idList")
                    .Set("newAv.dataObjectIds = idList");

                return query.With("dobj, newCs");
            }

            // pass nested object and all of its parents to the WITH clause
            var recursiveName = activeDO;
            var withNames = new List<string>() { recursiveName };
            while (recursiveName.Contains("nesteddo"))
            {
                recursiveName = recursiveName.Substring(0, recursiveName.LastIndexOf("nesteddo"));
                withNames.Add(recursiveName);
            }            

            var withClause = activeDO == "dobj" ? "dobj, newCs" : $"newCs, {String.Join(", ", withNames)}";

            query = query
                .OptionalMatch($"({activeDO})-[oldHA:HAS_ATTRIBUTE]->(oldAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)") // maybe an old value already existed but was deleted?
                .Create($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue {{{paramName}}})-[:OF_ATTRIBUTETYPE]->(at)") // create new value, link DO to it and link the value to at
                .Create("(newCs)-[:CHANGED]->(newAv)") // link the new ChangeSet to the new value
                .ForEach("(ignoreMe IN CASE WHEN oldAv is not null THEN [1] ELSE [] END | CREATE (newAv)-[:UPDATED_FROM]->(oldAv) DELETE oldHA)") // foreach here is a trick to mimic if-else. Create Updated_from link in case previous value was deleted, and delete the old link with the DO
                .WithParam(paramName, new NeoAttributeValue { Value = change.NewValue });
                

            if (change.NewValue is DateTime)
            {
                var date = (DateTime)change.NewValue;
                query = query.With(withClause + ", newAv")
                    .Match("(day:Day {value: " + date.Day + "})<-[:CONTAINS]-(:Month {value: " + date.Month + "})<-[:CONTAINS]-(:Year {value: " + date.Year + "})")
                    .Create("(newAv)-[:LINKED_VALUE]->(day)");
            }
            return query.With(withClause); ;
        }

        private ICypherFluentQuery CreateNestedDataObjectForList(DataObject dataObject, ICypherFluentQuery query, String activeDO, Change change)
        {
            if (! (change.NewValue is IEnumerable<Change>))
            {
                throw new Exception("the Change should be a nested DataObject...");
            }

            var nestedNeoDataObject = new NeoDataObject { Id = Guid.NewGuid(), Name = activeDO + "." + change.Attribute }; // e.g. dobj.contracthistoriek[0].normwerktijd[0]
            var sanitizedName = _stringHelper.RemoveSpecialCharacters(nestedNeoDataObject.Name).ToLower(); // dobjcontracthistoriek0normwerktijd0
            var nestedDoSanitizedName = _stringHelper.RemoveSpecialCharacters(nestedNeoDataObject.Name.Replace(".", "nesteddo")); // e.g. dobjnesteddocontracthistoriek0nesteddonormwerktijd

            // pass nested object and all of its parents to the WITH clause
            var recursiveName = nestedDoSanitizedName;
            var withNames = new List<string>() { recursiveName };
            while (recursiveName.Contains("nesteddo"))
            {
                recursiveName = recursiveName.Substring(0, recursiveName.LastIndexOf("nesteddo"));
                withNames.Add(recursiveName);
            }

            query = query.Create($"({nestedDoSanitizedName}:NestedDataObject {{{sanitizedName}}})-[:OF_DATATYPE]->(dt)") // create the new nested DataObject
                .Create($"(newAv)-[:OF_VALUE]->({nestedDoSanitizedName})") // link the new av to the newly created NestedDataObject
                .WithParam(sanitizedName, nestedNeoDataObject)
                .With($"newCs, {String.Join(", ", withNames)}");

            foreach (var property in (IEnumerable<Change>)change.NewValue)
            {
                query = CreatePropertyOfDataObject(dataObject, query, property, nestedDoSanitizedName);
            }

            return query;
        }

        private ICypherFluentQuery DeletePropertyOfDataObject(DataObject dataObject, ICypherFluentQuery query, Change change)
        {
            var paramName = _stringHelper.RemoveSpecialCharacters($"attributeValue{change.Attribute}");
            var activeDO = "dobj";
            var attributeName = change.Attribute;
            if (change.Attribute.Contains("."))
            {
                query = MatchNestedDataObject(dataObject, query, ref activeDO, ref attributeName);
            }

            return query.Match($"({activeDO})-[oldHA:HAS_ATTRIBUTE]->(oldAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType {{name:'{attributeName}'}})")
                .Delete("oldHA") // destroy link between DO and value as to make place for the new value
                .Create($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue {{{paramName}}})-[:OF_ATTRIBUTETYPE]->(at)")  // create new value, link DO to it and link the value to at
                .Create("(newCs)-[:CHANGED]->(newAv)-[:UPDATED_FROM]->(oldAv)") // link the new ChangeSet to the new value and make the value point to the old value
                .WithParam(paramName, new NeoAttributeValue { Value = change.NewValue, IsDeleted = true })
                .With("dobj, newCs");
        }

        private ICypherFluentQuery UpdatePropertyOfDataObject(DataObject dataObject, ICypherFluentQuery query, Change change)
        {
            var paramName = _stringHelper.RemoveSpecialCharacters($"attributeValue{change.Attribute}");
            var activeDO = "dobj";
            var attributeName = change.Attribute;
            if (change.Attribute.Contains("."))
            {
                query = MatchNestedDataObject(dataObject, query, ref activeDO, ref attributeName);
            }

            query = query.Match($"({activeDO})-[oldHA:HAS_ATTRIBUTE]->(oldAv:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType {{name:'{attributeName}'}})")
                .Delete("oldHA") // destroy link between DO and value as to make place for the new value
                .Create($"({activeDO})-[:HAS_ATTRIBUTE]->(newAv:AttributeValue {{{paramName}}})-[:OF_ATTRIBUTETYPE]->(at)")  // create new value, link DO to it and link the value to at
                .Create("(newCs)-[:CHANGED]->(newAv)-[:UPDATED_FROM]->(oldAv)"); // link the new ChangeSet to the new value and make the value point to the old value
            
            if (change.NewValue is SyncChange)
            {
                var nestedNeoDataObject = new NeoDataObject { Id = Guid.NewGuid(), Name = dataObject.Name + "." + change.Attribute, ExternalId = ((SyncChange)change.NewValue).ExternalId };
                var sanitizedName = _stringHelper.RemoveSpecialCharacters(nestedNeoDataObject.Name).ToLower();

                var nestedDoSanitizedName = _stringHelper.RemoveSpecialCharacters(nestedNeoDataObject.Name.Replace(".", "nesteddo"));

                // pass nested object and all of its parents to the WITH clause
                var recursiveName = nestedDoSanitizedName;
                var withNames = new List<string>() { recursiveName };
                while (recursiveName.Contains("nesteddo"))
                {
                    recursiveName = recursiveName.Substring(0, recursiveName.LastIndexOf("nesteddo"));
                    withNames.Add(recursiveName);
                }

                query = query.With("*") // TODO: quickfix, look into this...
                    .Match("(at)-[:OF_NESTED_TYPE]->(dt:DataType)") // first we match the datatype of the nested object we're about to insert
                    .Create($"({nestedDoSanitizedName}:NestedDataObject {{{sanitizedName}}})-[:OF_DATATYPE]->(dt)") // create the new nested DataObject
                    .Create($"(newAv)-[:OF_VALUE]->({nestedDoSanitizedName})") // link the new av to the newly created NestedDataObject
                    .WithParam(sanitizedName, nestedNeoDataObject)
                    .With($"newCs, {String.Join(", ", withNames)}")
                    .WithParam(paramName, new NeoAttributeValue { DataObjectId = nestedNeoDataObject.Id });
            } else
            {
                query = query.WithParam(paramName, new NeoAttributeValue { Value = change.NewValue });
            } 

            if (change.NewValue is DateTime)
            {
                var date = (DateTime)change.NewValue;
                query = query.With("dobj, newCs, newAv")
                    .Match("(day:Day {value: " + date.Day + "})<-[:CONTAINS]-(:Month {value: " + date.Month + "})<-[:CONTAINS]-(:Year {value: " + date.Year + "})")
                    .Create("(newAv)-[:LINKED_VALUE]->(day)");
            }
            return query.With("dobj, newCs");
        }
    }
}
