﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class StringAttributeType : BaseAttributeType
    {
        public const string TypeName = "String";

        public StringAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
        public string RegexValidation { get; set; }
    }
}