﻿using Digipolis.Common.Events;
using Digipolis.Common.Events.Models;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Converters;
using Digipolis.DossierEngine.Common.Logging;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.Synchronization;
using Digipolis.Tenant.Library.DataProviders;
using Digipolis.Tenant.Library.Managers;
using Microsoft.Extensions.Options;
using Narato.Common.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class PluginManager : IPluginManager
    {
        private readonly IChangeCalculator<DataType> _dataTypeChangeCalculator;
        private readonly IPluginDataProvider _pluginDataProvider;
        private readonly IEnumerable<IPlugin> _plugins;
        private readonly ITenantDataProvider _tenantDataProvider;
        private readonly IEventSubscriber _eventSubscriber;
        private readonly IDataTypeClient _dataTypeClient;
        private readonly IDataObjectClient _dataObjectClient;
        private readonly ITagReplacer _tagReplacer;
        private readonly ITenantManager<Config> _tenantManager;
        private readonly DossierConfiguration _dossierConfiguration;

        public PluginManager(IChangeCalculator<DataType> dataTypeChangeCalculator, ITenantManager<Config> tenantManager, IPluginDataProvider pluginDataProvider, IEnumerable<IPlugin> plugins, ITenantDataProvider tenantDataProvider, IEventSubscriber eventSubscriber, IDataTypeClient dataTypeClient, IDataObjectClient dataObjectClient, ITagReplacer tagReplacer, IOptions<DossierConfiguration> dossierConfiguration)
        {
            _dataTypeChangeCalculator = dataTypeChangeCalculator;
            _tenantManager = tenantManager;
            _pluginDataProvider = pluginDataProvider;
            _plugins = plugins;
            _tenantDataProvider = tenantDataProvider;
            _eventSubscriber = eventSubscriber;
            _dataTypeClient = dataTypeClient;
            _dataObjectClient = dataObjectClient;
            _tagReplacer = tagReplacer;
            _dossierConfiguration = dossierConfiguration.Value;
        }

        public string LoadPlugins()
        {
            var existingPlugins = _pluginDataProvider.GetPlugins();
            var plugingsToAdd = new List<Plugin>();

            foreach (var plugin in _plugins)
            {
                // TODO: Add version verification
                if (existingPlugins.Where(p => p.Name == plugin.Name).ToList().Count == 0)
                {
                    var serialisedDataTypes = JsonConvert.SerializeObject(plugin.DataTypes);

                    plugingsToAdd.Add(new Plugin { Name = plugin.Name, Version = plugin.Version, DataType = serialisedDataTypes });

                    var pushConfig = new PushConfig { Url = _dossierConfiguration.PluginEndpoint + plugin.Name };
                    var subscriptionConfig = new SubscriptionConfig { Push = pushConfig };
                    var subscription = new Subscription { Config = subscriptionConfig, Topic = plugin.EventHandlerTopic };

                    var uniqueSubscriptionName = plugin.Name + Guid.NewGuid().ToString();
                    _eventSubscriber.Subscribe(plugin.EventNamespace, uniqueSubscriptionName, subscription, null);// null because owner key is name of the subscription by default
                }
            }

            if (plugingsToAdd.Count > 0)
            {
                _pluginDataProvider.InsertPlugins(plugingsToAdd);
                return "Plugins registered succesfully";
            }
            else
            {
                return "No plugins found to register";
            }
        }

        public List<Plugin> GetPlugins()
        {
            return _pluginDataProvider.GetPlugins();
        }

        public async Task<TenantPluginLink> InsertTenantPluginLinkAsync(TenantPluginLinkDTO tenantPluginLinkDto)
        {
            TenantPluginLink createdTenantPluginLink = null;
            try
            {
                if (tenantPluginLinkDto == null)
                    throw new ArgumentNullException("tenantPluginLinkDto");

                var plugin = _pluginDataProvider.GetPluginById(tenantPluginLinkDto.PluginId);
                Digipolis.Common.Tenancy.Models.Tenant tenant;
                try
                {
                    tenant = await _tenantDataProvider.GetTenantByIdAsync(tenantPluginLinkDto.TenantId).LogOnException();
                } catch (InvalidOperationException e)
                {
                    // TODO: THIS TRY CATCH IS TEMPORARY. GetTenantById should throw an entitynotfoundException if none is found,
                    // but there is some stuff going on with tenant library (about async stuff)
                    throw new EntityNotFoundException("Tenant with id " + tenantPluginLinkDto.TenantId + " couldn't be found.");
                }

                var rawDataTypeJsons = JsonConvert.DeserializeObject<List<string>>(plugin.DataType);
                if (rawDataTypeJsons.Count == 0)
                {
                    throw new Exception("Plugin " + plugin.Name + " doesn't have a valid list of datatypes.");
                }
                var datatypeDictionary = new Dictionary<string, DataType>();

                var last = rawDataTypeJsons.Last();
                DataType syncDataType = null;
                foreach (var rawDataTypeJson in rawDataTypeJsons)
                {
                    DataType dataType;
                    var dataTypeJson = _tagReplacer.replace(rawDataTypeJson, datatypeDictionary.ToDictionary(kvp => kvp.Key, kvp => (object)kvp.Value));
                    try
                    {
                        if (rawDataTypeJson.Equals(last))
                        {
                            syncDataType = await _dataTypeClient.InsertSyncedDataTypeAsync(dataTypeJson, tenant.Key).LogOnException(); // this one gets inserted as a synced DT
                            break; // only the LAST datatype (root datatype) has to hold the "synced" flag

                        }

                        dataType = await _dataTypeClient.InsertDataTypeAsync(dataTypeJson, tenant.Key).LogOnException();
                    } catch (ExceptionWithFeedback ex) // maybe the dataType already exists?
                    {
                        if (!ex.Feedback.First().Description.Contains("already exists")) // admittedly not the most robust check...
                        {
                            throw ex;
                        }
                        var newDataType = JsonConvert.DeserializeObject<DataType>(dataTypeJson, new AttributeTypeConverter());
                        dataType = await _dataTypeClient.GetDataTypeByName(newDataType.Name, tenant.Key);
                        if (_dataTypeChangeCalculator.CalculateChangeSet(dataType, newDataType).Count() > 0)
                        {
                            throw ex;
                        }

                        if (rawDataTypeJson.Equals(last))
                        {
                            syncDataType = dataType;
                            break;
                        }
                    }
                    
                    datatypeDictionary.Add(dataType.Name, dataType);
                }

                var tenantPluginLink = new TenantPluginLink
                {
                    Id = Guid.NewGuid(),
                    PluginId = plugin.Id,
                    TenantId = tenant.Id,
                    Prepopulate = tenantPluginLinkDto.Prepopulate,
                    DataTypeId = syncDataType.Id // this is the last inserted datatype; logically this is the "master" datatype
                };

                createdTenantPluginLink = _pluginDataProvider.InsertTenantPluginLink(tenantPluginLink);

                if (tenantPluginLink.Prepopulate)
                {
                    _plugins.Where(p => p.Name == plugin.Name)
                        .Single()
                        .Prepopulate(syncDataType.Id, (string DataObjectJson) => _dataObjectClient.CreateSyncedDataObject(DataObjectJson, tenant.Key));
                }
            }
            catch (Exception e)
            {
                if (e is EntityNotFoundException || e is ExceptionWithFeedback)
                {
                    throw e; // TODO: ugly.... wtf is this shit anyway
                }
                throw new Exception(e.Message + ' ' + e.InnerException?.Message);
            }

            return createdTenantPluginLink;
        }

        public string ProcessEvent(string pluginName, object payload)
        {
            var plugin = _plugins.Single(p => p.Name == pluginName);

            List<TenantPluginLink> tenantPluginLinks = _pluginDataProvider.GetPluginLinksForPluginForActiveTenants(pluginName); // tenants are eager loaded

            foreach(var tenantPluginLink in tenantPluginLinks)
            {
                // not a problem if any of those fail. event handler retry mechanism will trigger
                // if one gets updated twice, it's not a problem.... update won't trigger anything because the data is actually the same
                var dataObjectJson = plugin.ProcessMessage(tenantPluginLink.DataTypeId, payload);
                var parsedDataObject = JObject.Parse(dataObjectJson);

                var externalId = parsedDataObject.Value<string>("externalId");
                if (string.IsNullOrEmpty(externalId))
                {
                    throw new Exception("Plugin delivered a dataObjectJson without an externalId");
                }

                try
                {
                    _dataObjectClient.UpdateSyncedDataObject(dataObjectJson, externalId, tenantPluginLink.Tenant.Key);
                } catch (EntityNotFoundException e) // not found, so now we try to create it IF prepopulate = on
                {
                    if (tenantPluginLink.Prepopulate)
                    {
                        _dataObjectClient.CreateSyncedDataObject(dataObjectJson, tenantPluginLink.Tenant.Key);
                    }
                } catch (Exception)
                {
                    // TODO handle other exceptions than the EntityNotFoundException
                }
            }
            return "Event has been processed";
        }

        public string UpdateExternalDataObjectByExternalId(PluginObjectLookupDto pluginObjectLookup)
        {
            var tenantPluginLink = _pluginDataProvider.GetPluginLinksForTenantAndDataTypeId(pluginObjectLookup.TenantKey, pluginObjectLookup.DataTypeId);

            var plugin = _plugins.Single(p => p.Name == tenantPluginLink.Plugin.Name);

            var dataObjectJson = MergeDataTypeIdWithJson(plugin.GetObjectById(pluginObjectLookup.ExternalId), pluginObjectLookup.DataTypeId);
            var parsedDataObject = JObject.Parse(dataObjectJson);

            var externalId = parsedDataObject.Value<string>("externalId");
            if (string.IsNullOrEmpty(externalId))
            {
                throw new Exception("Plugin delivered a dataObjectJson without an externalId");
            }
            _dataObjectClient.UpdateSyncedDataObject(dataObjectJson, externalId, pluginObjectLookup.TenantKey);
            return "OK";
        }

        public bool DeleteTenantPluginLink(Guid linkId)
        {
            return _pluginDataProvider.DeleteTenantPluginLink(linkId);
        }

        public async Task<List<TenantPluginLink>> GetPluginLinksForTenantAsync(string tenantKey)
        {
            await _tenantManager.GetTenantByKeyAsync(tenantKey); // this is basicly an assert. It'll throw an entitynotfoundexception if tenant is deleted
            return _pluginDataProvider.GetPluginLinksForTenant(tenantKey);
        }

        private string MergeDataTypeIdWithJson(string dataObjectJson, Guid dataTypeId)
        {
            var json = JObject.Parse(dataObjectJson);
            var attributeToMerge = JObject.Parse("{\"dataTypeId\": \"" + dataTypeId.ToString() + "\"}");

            json.Merge(attributeToMerge);
            return json.ToString();
        }
    }
}