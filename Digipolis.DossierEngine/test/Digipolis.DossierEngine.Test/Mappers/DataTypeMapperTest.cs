﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xunit;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.DataProvider.Models;
using Neo4jClient;
using Moq;
using Digipolis.DossierEngine.DataProvider.Mappers;
using Digipolis.DossierEngine.Model.Models;
using Newtonsoft.Json.Serialization;

namespace Digipolis.DossierEngine.Test.Mappers
{
    public class DataTypeMapperTest
    {
        [Fact]
        public void MapperReturnsADataTypeFromRawDataType()
        {
            var rawDataType = new RawDataType();

            var graphClientMock = new Mock<IGraphClient>();
            rawDataType.DataType = new NeoDataType { Id = new Guid(), Name = "NeoDataType1" };
            rawDataType.Attributes = new List<Node<string>>
            {
                new Node<string>(JsonConvert.SerializeObject(new StringAttributeType { Name = "TestName" }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() } ), 1, graphClientMock.Object)
            };

            var typeMapper = new DataTypeMapper();
            var outDataType = typeMapper.Map(rawDataType);

            Assert.Equal(rawDataType.DataType.Id, outDataType.Id);
            Assert.Equal(rawDataType.DataType.Name, outDataType.Name);
            Assert.Equal(1, outDataType.Attributes.Count);
        }
    }
}
