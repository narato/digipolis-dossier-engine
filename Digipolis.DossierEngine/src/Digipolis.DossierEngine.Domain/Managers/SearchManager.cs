﻿using Digipolis.DossierEngine.Domain.Interfaces;
using System.Collections.Generic;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Search.Cypher;
using System;
using System.Linq;
using Narato.Common.Models;
using System.Threading.Tasks;
using Narato.Common.Paging;
using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class SearchManager : ISearchManager
    {

        private readonly IDataObjectDataProvider _dataObjectDataProvider;
        private readonly IConditionBuilder _conditionBuilder;
        private readonly ICypherQueryBuilder _cypherQueryBuilder;

        public SearchManager(IDataObjectDataProvider dataObjectDataProvider, IConditionBuilder conditionBuilder, ICypherQueryBuilder cypherQueryBuilder)
        {
            _dataObjectDataProvider = dataObjectDataProvider;
            _conditionBuilder = conditionBuilder;
            _cypherQueryBuilder = cypherQueryBuilder;
        }

        public async Task<PagedCollectionResponse<IEnumerable<DataObject>>> FindDataObjectsForDataTypeIdBySearchQueryObjectAsync(Guid dataTypeId, dynamic queryObject, int page, int pageSize)
        {
            ICondition condition = _conditionBuilder.BuildConditionFromJsonObject(queryObject);

            var whereClause = _cypherQueryBuilder.Visit(condition);

            if (condition is Comparison)
            {
                var comparison = condition as Comparison;
                var fieldName = ((Field)comparison.LeftHandSide).Name;
                var totalCount = await _dataObjectDataProvider.CountDataObjectsForDataTypeIdBySimpleComparisonAsync(dataTypeId, fieldName, whereClause);
                var dataObjects = await _dataObjectDataProvider.FindDataObjectsForDataTypeIdBySimpleComparisonAsync(dataTypeId, fieldName, whereClause, page, pageSize);
                return dataObjects.ToPagedCollectionResponse(page, pageSize, (int)totalCount);
            } else
            {
                // both lines below take a ridiculous amount of time... this has to be fixed :-)
                var totalCount = await _dataObjectDataProvider.CountDataObjectsForDataTypeIdByWhereClauseAsync(dataTypeId, whereClause);
                var dataObjects = await _dataObjectDataProvider.FindDataObjectsForDataTypeIdByWhereClauseAsync(dataTypeId, whereClause, page, pageSize);
                return dataObjects.ToPagedCollectionResponse(page, pageSize, (int)totalCount);
            }
        }
    }
}
