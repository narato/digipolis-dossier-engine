﻿using System.Collections.Generic;
using Digipolis.DossierEngine.Synchronization;

namespace Digipolis.DossierEngine.PluginCreation
{
    public class TestPlugin : AbstractPlugin
    {
        public TestPlugin()
        {
            Name = "MyTestPlugin";
            Version = 1;
            EventNamespace = "LocalDevDossierEngine";
            EventHandlerTopic = "Tenant.Created";
            DataTypes = new List<string>
            {
                @"{""name"": ""SlightlyMoreComplex"",""attributes"": [{""type"": ""String"",""name"": ""locatie"",""regexValidation"": ""^[a-zA-Z]*$"",""isRequired"": ""true""}, {""type"": ""Int"",""name"": ""aantal"",""minRangeValidation"": 0,""maxRangeValidation"": 1000,""isRequired"": true}]}"
            };
        }

        public override string GetObjectById(object id)
        {
            if (id is string)
            {
                return $@"{{
                    ""externalId"": ""{id}"",
                    ""name"": ""test"",
                    ""values"": {{
                        ""locatie"": ""Kontich"",
                        ""aantal"": 50
                    }}
                }}";
            }
            else
            {
                return id.ToString();
            }
        }

        protected override IEnumerable<object> GetAllObjects()
        {
            return new List<string>() { "test" };
        }
    }
}