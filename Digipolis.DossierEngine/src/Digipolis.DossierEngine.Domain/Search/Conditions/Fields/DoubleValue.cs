﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public class DoubleValue : IValue<double>
    {
        public string Type { get; } = "double";
        public double Value { get; }

        public DoubleValue(double value)
        {
            Value = value;
        }
    }
}
