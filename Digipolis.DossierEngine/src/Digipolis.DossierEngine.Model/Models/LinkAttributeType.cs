﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class LinkAttributeType : BaseAttributeType
    {
        public const string TypeName = "Link";

        public LinkAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
        public string RegexValidation { get; } = @"^(http(?:s)?\:\/\/[a-zA-Z0-9]+(?:(?:\.|\-)[a-zA-Z0-9]+)+(?:\:\d+)?(?:\/[\w\-]+)*(?:\/?|\/\w+\.[a-zA-Z]{2,4}(?:\?[\w]+\=[\w\-]+)?)?(?:\&[\w]+\=[\w\-]+)*)$";
    }
}