﻿using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    public class TenantPluginLinkController : Controller
    {
        private IPluginManager _pluginManager;
        private IResponseFactory _responseFactory;

        public TenantPluginLinkController(IPluginManager pluginManager, IResponseFactory responseFactory)
        {
            _responseFactory = responseFactory;
            _pluginManager = pluginManager;
        }

        /// <summary>
        /// This action is used to register a plugin for a given tenant
        /// </summary>
        /// <param name="tenantPluginLink">A JSON object representing the subscription</param>
        /// <returns>Statuscode of the request</returns>
        // POST api/tenantpluginlinks
        [ProducesResponseType(typeof(Response<TenantPluginLinkDTO>), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response<TenantPluginLinkDTO>),(int)System.Net.HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)System.Net.HttpStatusCode.Unauthorized)]
        [HttpPost("api/tenantpluginlinks", Name = "InsertTenantPluginLink")]
        public async Task<IActionResult> InsertTenantPluginLinkAsync([FromBody] [Required] TenantPluginLinkDTO tenantPluginLink)
        {
            var missingParams = new List<MissingParam>();
            if (tenantPluginLink == null)
                missingParams.Add(new MissingParam("tenantPluginLink", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePostResponseAsync(async () => await _pluginManager.InsertTenantPluginLinkAsync(tenantPluginLink), this.GetRequestUri(),
                        "InsertTenantPluginLink", (data) => { return new { controller = "Plugin" }; });
        }

        /// <summary>
        /// This method deletes the subscription of a tenant on a synchdatatype
        /// </summary>
        /// <param name="linkId">The id of the TenantPluginLink to be deleted</param>
        /// <returns>Statuscode of the request</returns>
        /// <response code="204">Deleted sucessfully</response>
        // DELETE api/tenantpluginlinks
        [ProducesResponseType(typeof(void),(int)System.Net.HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.NotFound)]
        [HttpDelete("api/tenantpluginlinks/{linkId}", Name = "DeleteTenantPluginLink")]
        public IActionResult Delete([Required] Guid linkId)
        {
            return _responseFactory.CreateDeleteResponse(() => _pluginManager.DeleteTenantPluginLink(linkId), this.GetRequestUri());
        }

        /// <summary>
        /// This method returns the pluginlinks for a given tenantKey
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <returns>The tenant identified by the id for the given tenant key</returns>
        // GET api/tenants/{tenantKey}/tenantpluginlinks
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.NotFound)]
        [HttpGet("api/tenants/{tenantKey}/tenantpluginlinks", Name = "GetPluginsForTenant")]
        public async Task<IActionResult> GetPluginsForTenantAsync([Required]string tenantKey)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _pluginManager.GetPluginLinksForTenantAsync(tenantKey), this.GetRequestUri());
        }
    }
}
