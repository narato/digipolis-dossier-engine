﻿using System;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using Narato.Common.Models;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataTypeManager
    {
        Task<DataType> GetDataTypeByIdAsync(Guid id);
        Task<DataType> GetDataTypeByNameAsync(string name);
        Task<DataType> GetDataTypeTreeByIdAsync(Guid id);
        Task<DataType> GetDataTypeTreeByNameAsync(string name);
        Task<DataType> InsertDataTypeAsync(DataType dataType);
        Task<DataType> UpdateDataTypeAsync(DataType dataType);
        Task<PagedCollectionResponse<IEnumerable<DataType>>> GetAllDataTypesAsync(bool syncedOnly, int page, int pageSize);
        Task<DataType> DeleteDataTypeByIdAsync(Guid id);
    }
}