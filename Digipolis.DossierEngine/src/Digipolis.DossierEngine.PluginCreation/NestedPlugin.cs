﻿using Digipolis.DossierEngine.Synchronization;
using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.PluginCreation
{
    public class NestedPlugin : AbstractPlugin
    {
        public NestedPlugin()
        {
            Name = "MyNestedPlugin";
            Version = 1;
            EventNamespace = "LocalDevDossierEngine";
            EventHandlerTopic = "Tenant.Created";
            DataTypes = new List<string>() {
                @"{""name"":""NestedPluginNested"",""attributes"":[{""type"":""String"",""name"":""nestedValue"",""regexValidation"":""^[a-zA-Z]*$"",""isRequired"":""true""}]}",
                @"{""name"":""NestedPluginRoot"",""attributes"":[{""type"":""String"",""name"":""rootValue"",""regexValidation"":""^[a-zA-Z]*$"",""isRequired"":""true""},{""type"":""LinkDataType"",""name"":""nested"",""dataTypeId"":""{{NestedPluginNested.Id}}"",""isRequired"":true}]}"
            };
        }

        public override string GetObjectById(object id)
        {
            if (id is string)
            {
                return $@"{{
                    ""externalId"": ""{id}"",
                    ""name"": ""test"",
                    ""values"": {{
                        ""rootValue"": ""root"",
                        ""nested"": {{
                            ""nestedValue"": ""nested""
                        }}
                    }}
                }}";
            }
            else
            {
                return id.ToString();
            }
        }

        protected override IEnumerable<object> GetAllObjects()
        {
            throw new NotImplementedException();
        }
    }
}
