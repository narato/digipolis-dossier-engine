﻿using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Synchronization
{
    public interface IPlugin
    {
        int Version { get; set; }
        string Name { get; set; }
        IEnumerable<string> DataTypes { get; set; }
        string EventHandlerTopic { get; set; }
        string EventNamespace { get; set; }
        string ProcessMessage(Guid dataTypeId, object payload);
        string GetObjectById(object id);
        void Prepopulate(Guid dataTypeId, Action<string> callback);

    }
}