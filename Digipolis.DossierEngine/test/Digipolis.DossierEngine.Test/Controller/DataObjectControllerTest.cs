﻿using Digipolis.DossierEngine.DataObject.API.Controllers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using Xunit;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class DataObjectControllerTest
    {
        [Fact]
        public void GetReturnsAnActionResult()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<Model.Models.DataObject>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetWithMissingTenantIdReturnsBadRequest()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetAsync(Guid.NewGuid(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataObjectWithoutTenantIdReturnsBadRequest()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataObjectAsync(new Model.Models.DataObject(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataObjectWithoutDataObjectReturnsBadRequest()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<List<MissingParam>>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataObjectAsync(null, "tentanShortName").Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }

        [Fact]
        public void InsertDataObjectReturnsAnActionResult()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreatePostResponseAsync(
                It.IsAny<Func<Task<Model.Models.DataObject>>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Func<Model.Models.DataObject, object>>()))
                .ReturnsAsync(new ObjectResult(null));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.InsertDataObjectAsync(new Model.Models.DataObject(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetByDataTypeIdReturnsAnActionResult()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseForCollectionAsync(It.IsAny<Func<Task<PagedCollectionResponse<IEnumerable<Model.Models.DataObject>>>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetDataObjectsForDataTypeAsync(Guid.NewGuid(), "tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetByDataTypeIdWithMissingTenantIdReturnsBadRequest()
        {
            var dataObjectManagerMock = new Mock<IDataObjectManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new DataObjectController(dataObjectManagerMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.GetDataObjectsForDataTypeAsync(Guid.NewGuid(), string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }
    }
}
