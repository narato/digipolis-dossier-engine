﻿using Digipolis.DossierEngine.DataProvider.Managers;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/admin/migrations")]
    public class MigrationController : Controller
    {
        private IMigrationManager _migrationManager;
        private IResponseFactory _responseFactory;

        public MigrationController(IMigrationManager migrationManager, IResponseFactory responseFactory)
        {
            _migrationManager = migrationManager;
            _responseFactory = responseFactory;
        }

        /// <summary>
        /// Executes all pending migration
        /// </summary>
        /// <returns>Returns OK or error</returns>
        // POST api/migrations
        [HttpPost]
        public IActionResult Post()
        {
            return _responseFactory.CreateGetResponse(() => _migrationManager.ExecutePendingMigrations(), this.GetRequestUri());
        }
    }
}
