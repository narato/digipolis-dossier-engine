﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Digipolis.DossierEngine.Common.Dictionaries
{
    public class DictionaryComparer<TKey, TValue> : IEqualityComparer<IDictionary<TKey, TValue>>
    {

        private bool NestedEquals(IEnumerable<IDictionary<TKey, TValue>> list1, IEnumerable<IDictionary<TKey, TValue>> list2)
        {
            var cnt = new Dictionary<IDictionary<TKey, TValue>, int>(this);
            foreach (IDictionary<TKey, TValue> s in list1)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }
            foreach (IDictionary<TKey, TValue> s in list2)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }
            return cnt.Values.All(c => c == 0);
        }

        public bool Equals(IDictionary<TKey, TValue> dict1, IDictionary<TKey, TValue> dict2)
        {
            if (dict1 == null)
            {
                throw new ArgumentNullException("x");
            }
            if (dict2 == null)
            {
                throw new ArgumentNullException("y");
            }
            if (dict1.Count != dict2.Count)
            {
                return false;
            }
            foreach (var kvp in dict1)
            {
                TValue value; // TValue might be a dangerous bet here... if the value is actually a list itself, this might fail
                // if dict2 doesnt contain this key, they're not equal
                if (!dict2.TryGetValue(kvp.Key, out value))
                {
                    return false;
                }
                // if the value (in both dictionaries) is an instance of a dictionary, don't compare the object as a whole, but compare them using this comparer
                if (value is IDictionary<TKey, TValue> && kvp.Value is IDictionary<TKey, TValue>)
                {
                    if (!Equals((IDictionary<TKey, TValue>)value, (IDictionary<TKey, TValue>)kvp.Value))
                        return false;
                }
                else if (kvp.Value is IList<IDictionary<TKey, TValue>>)
                {
                    if (! NestedEquals((IEnumerable<IDictionary<TKey, TValue>>)kvp.Value, (IEnumerable<IDictionary<TKey, TValue>>)value))
                    {
                        return false;
                    }
                }
                else if (kvp.Value == null && value != null)
                {
                    return false;
                }
                else if (kvp.Value != null && !kvp.Value.Equals(value))// else, just compare the values
                {
                    return false;
                }
            }
            return true;
        }

        public int GetHashCode(IDictionary<TKey, TValue> obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            int hash = 0;
            foreach (var kvp in obj)
            {
                if (kvp.Value is IDictionary<TKey, TValue>)
                {
                    hash = hash ^ kvp.Key.GetHashCode() ^ GetHashCode((IDictionary<TKey, TValue>)kvp.Value);
                } else if (kvp.Value is IList<IDictionary<TKey, TValue>>)
                {
                    foreach (var dict in (IList<IDictionary<TKey, TValue>>)kvp.Value)
                    {
                        hash = hash ^ GetHashCode(dict);
                    }
                } else if (kvp.Value == null)
                {
                    hash = hash ^ kvp.Key.GetHashCode();
                } else
                {
                    hash = hash ^ kvp.Key.GetHashCode() ^ kvp.Value.GetHashCode();
                }
            }
            return hash;
        }
    }
}
