﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class AddCompositeKeyToTenantPluginLinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddUniqueConstraint(
                name: "AK_TenantPluginLink_TenantId_PluginId",
                table: "TenantPluginLink",
                columns: new[] { "TenantId", "PluginId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_TenantPluginLink_TenantId_PluginId",
                table: "TenantPluginLink");
        }
    }
}
