﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.Domain.Mappers;
using Models = Digipolis.Common.Tenancy.Models;
using Newtonsoft.Json;
using System;
using Xunit;

namespace Digipolis.DossierEngine.Test.Mappers
{
    // TODO: once ConfigMapper is moved to Digipolis.Common, this can be deleted
    public class ConfigMapperTest
    {
        [Fact]
        public void MapperReturnsATentantWithCorrectConfigObject()
        {
            var tenantIn = new Tenant<Config>();
            var id = Guid.NewGuid();
            tenantIn.Id = id;
            tenantIn.Key = "TenantKey";
            tenantIn.Name = "TenantName";
            tenantIn.Status = "Status";
            tenantIn.Description = "Description";

            var config = new Config();
            config.SshPort = 500;
            config.SshUsername = "meepmoop";

            tenantIn.Config = config;

            var mapper = new ConfigMapper<Config>();
            var tenantOut = mapper.Map(tenantIn);

            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Id, id);
            Assert.Equal(tenantOut.Key, "TenantKey");
            Assert.Equal(tenantOut.Name, "TenantName");
            Assert.Equal(tenantOut.Status, "Status");
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Config, JsonConvert.SerializeObject(config));
        }

        [Fact]
        public void MapperReturnsATentantWithConfigJson()
        {
            var tenantIn = new Models.Tenant();
            var id = Guid.NewGuid();
            tenantIn.Id = id;
            tenantIn.Key = "TenantKey";
            tenantIn.Name = "TenantName";
            tenantIn.Status = "Status";
            tenantIn.Description = "Description";

            var config = new Config();
            config.SshPort = 500;
            config.SshUsername = "meepmoop";

            tenantIn.Config = JsonConvert.SerializeObject(config);

            var mapper = new ConfigMapper<Config>();
            var tenantOut = mapper.Map(tenantIn);

            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Id, id);
            Assert.Equal(tenantOut.Key, "TenantKey");
            Assert.Equal(tenantOut.Name, "TenantName");
            Assert.Equal(tenantOut.Status, "Status");
            Assert.Equal(tenantOut.Description, "Description");
            Assert.Equal(tenantOut.Config.SshPort, 500);
            Assert.Equal(tenantOut.Config.SshUsername, "meepmoop");
        }
    }
}
