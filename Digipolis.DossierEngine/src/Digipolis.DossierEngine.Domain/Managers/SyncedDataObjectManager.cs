﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class SyncedDataObjectManager : ISyncedDataObjectManager
    {
        private readonly ISyncedDataObjectDataProvider _syncedDataObjectDataProvider;
        private readonly IDataObjectManager _dataObjectManager;

        public SyncedDataObjectManager(ISyncedDataObjectDataProvider dataObjectDataProvider, IDataObjectManager dataObjectManager)
        {
            _syncedDataObjectDataProvider = dataObjectDataProvider;
            _dataObjectManager = dataObjectManager;
        }

        public async Task<List<DataObject>> GetSyncedDataObjectsByExternalIdAsync(string externalId)
        {
            return await _syncedDataObjectDataProvider.FindSyncedDataObjectsByExternalIdAsync(externalId);
        }

        public async Task<DataObject> UpdateSyncedDataObjectAsync(DataObject dataObject)
        {
            AssertDataObjectHasExternalId(dataObject);
            var updateList = await _syncedDataObjectDataProvider.FindSyncedDataObjectsByExternalIdAsync(dataObject.ExternalId);
            if (updateList.Count == 0)
            {
                throw new EntityNotFoundException("No synced DataObject found for external id: " + dataObject.ExternalId);
            }

            foreach(var dataObjectToUpdate in updateList)
            {
                if (dataObjectToUpdate.DataTypeId != dataObject.DataTypeId)
                {
                    throw new Exception($"dataTypeId ({dataObjectToUpdate.DataTypeId}) of existing SyncedDataObject doesnt match the plugin datatypeId ({dataObject.DataTypeId})");
                }
                dataObject.Id = dataObjectToUpdate.Id; // this has to be done, because the dataObjectManager checks both Id's
                await _dataObjectManager.UpdateDataObjectForIdAsync(dataObjectToUpdate.Id, dataObject, true);
            }
            return dataObject;
        }

        public async Task<DataObject> CreateSyncedDataObjectAsync(DataObject dataObject)
        {
            AssertDataObjectHasExternalId(dataObject);

            return await _dataObjectManager.InsertDataObjectAsync(dataObject);
        }

        private void AssertDataObjectHasExternalId(DataObject dataObject)
        {
            if (string.IsNullOrEmpty(dataObject.ExternalId))
            {
                throw new ValidationException(new FeedbackItem { Type = FeedbackType.ValidationError, Description = "Plugin is trying to insert/update a non-synced DataObject! This means no externalId is present on the dataObject. This might be a bug in the plugin!" });
            }
        }
    }
}
