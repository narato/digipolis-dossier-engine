﻿using Digipolis.DossierEngine.Domain.Ssh;
using Xunit;

namespace Digipolis.DossierEngine.Test.Ssh
{
    public class SshClientProviderTest
    {
        [Fact]
        public void CreateNeo4jInstanceBehaviourTest()
        {
            var sshClientProvider = new SshClientProvider();

            var client = sshClientProvider.GetSshClient("testserver", 5, "nope", "dope");

            Assert.IsType(typeof(IneoSshClient), client);
        }
    }
}
