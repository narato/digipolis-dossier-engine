﻿using System;
using System.Linq;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.Model.Models;
using Neo4jClient;
using Digipolis.Common.Status.Models;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Neo4jClient.Cypher;
using Digipolis.DossierEngine.Common.Util;
using Digipolis.DossierEngine.DataProvider.Helpers;
using System.Collections.Generic;
using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Narato.Common.Exceptions;
using Newtonsoft.Json.Linq;
using NLog;
using System.Threading.Tasks;
using Narato.Common;
using Narato.Common.Models;

namespace Digipolis.DossierEngine.DataProvider.DataProviders
{
    public class DataObjectDataProvider : IDataObjectDataProvider
    {
        private readonly IPooledGraphClientProvider _pooledGraphClientProvider;
        private readonly IDataObjectMapper _dataObjectMapper;
        private readonly StringHelper _stringHelper;
        private readonly NestedDataObjectHelper _nestedDataObjectHelper;
        private readonly IDataObjectQueryProvider _dataObjectQueryProvider;

        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public DataObjectDataProvider(IPooledGraphClientProvider pooledGraphClientProvider, IDataObjectMapper dataObjectMapper, StringHelper stringHelper, NestedDataObjectHelper nestedDataObjectHelper, IDataObjectQueryProvider dataObjectQueryProvider)
        {
            _pooledGraphClientProvider = pooledGraphClientProvider;
            _dataObjectMapper = dataObjectMapper;
            _stringHelper = stringHelper;
            _nestedDataObjectHelper = nestedDataObjectHelper;
            _dataObjectQueryProvider = dataObjectQueryProvider;
        }

        // TODO: move to a DatabaseConnectionChecker or something. This has NOTHING to do with a DataObjectDataProvider
        public async Task<DbConnectionCheckResult> CheckDBConnectionAsync()
        {
            try
            {
                using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
                {
                    var client = pooledClient.Value;
                    if (!client.IsConnected)
                        client.Connect();

                    var query = client
                            .Cypher
                            .Match("(n:NonExistingLabel)")
                            .Return((n) => n.Count());
                    var output = await query.ResultsAsync;
                }
            }
            catch (Exception ex)
            {
                return new DbConnectionCheckResult(false, ex);
            }
            return new DbConnectionCheckResult(true);
        }

        public async Task<DataObject> GetDataObjectByIdAsync(Guid id, bool includeNested = false)
        {
            var idList = new List<Guid>();
            idList.Add(id);

            var returnValue = await FindDataObjectsByIdListAsync(idList, includeNested);
            if (returnValue.Count() > 1)
            {
                throw new Exception("Only a single dataObject should be be returned for guid" + id.ToString());
            }
            if (returnValue.Count() == 0)
            {
                throw new EntityNotFoundException("No dataObject could be found for guid " + id.ToString());
            }
            return returnValue.Single();
        }

        public async Task<bool> DeleteDataObjectByIdAsync(Guid id)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (! client.IsConnected)
                    client.Connect();

                var query = client
                    .Cypher
                    .Match("(dobj:DataObject)")
                    .UsingIndex("dobj:DataObject(id)")
                    .Where((NeoDataObject dobj) => dobj.Id == id)
                    .Set("dobj.isDeleted = true")
                    .Return<NeoDataObject>("dobj");

                return (await query.ResultsAsync).Count() > 0;
            }
        }

        public async Task<List<DataObject>> FindDataObjectsByIdListAsync(IEnumerable<Guid> ids, bool includeNested = false)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher;

                if (includeNested) // match DataObject as well as NestedDataObject
                {
                    query = query.OptionalMatch("(dobj:DataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) as d1")
                        .OptionalMatch("(dobj:NestedDataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) +d1 as d2")
                        .Unwind("d2", "dobj")
                        .With("dobj");
                } else
                {
                    query = query.Match("(dobj:DataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false); // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                }

                query = query
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where("not (has (dt.isDeleted))")
                    .OptionalMatch("(dobj)-[:HAS_CHANGESET|CONTAINS*]->(cs)") // optional because of fetching history. DO's in a collection don't have a changeset.
                    .Where("(cs:ChangeSet or cs:SyncedChangeSet)")
                    .OptionalMatch("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)") // optional because we can have synced dataObjects that don't have any AV's filled in yet
                    .OptionalMatch("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)") // optional because read above
                    .Where("not (has (at.isDeleted))")
                    .With("dt, dobj, av, at.name as atName, collect(cs) as csList")
                    .With("dt, dobj, av, atName, head(csList).date as updatedAt, last(csList).date as createdAt")
                    .Return((dt, dobj, av, atName, updatedAt, createdAt) => new RawDataObject
                    {
                        DataType = dt.As<NeoDataType>(),
                        DataObject = dobj.As<NeoDataObject>(),
                        AttributeNames = atName.CollectAs<string>(),
                        AttributeValues = av.CollectAs<Node<string>>(),
                        CreatedAt = createdAt.As<DateTime>(),
                        UpdatedAt = updatedAt.As<DateTime>()
                    })
                    .Union();

                if (includeNested) // match DataObject as well as NestedDataObject
                {
                    query = query.OptionalMatch("(dobj:DataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) as d1")
                        .OptionalMatch("(dobj:NestedDataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) +d1 as d2")
                        .Unwind("d2", "dobj")
                        .With("dobj");
                }
                else
                {
                    query = query.Match("(dobj:DataObject)")
                        .Where("dobj.id IN [\"" + string.Join("\",\"", ids.Select(id => id.ToString())) + "\"]")
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false); // unary operators are not supported because ambigiousness between c# and neo4j on handling null values;
                }

                var result = query
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where("not (has (dt.isDeleted))")
                    .Match("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)")
                    .Match("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                    .Match("(av)-[r:OF_VALUE|:HAS_ATTRIBUTE*]->(dobjnested:NestedDataObject)-[:OF_DATATYPE]->(dtnested:DataType)")
                    .Where("not (has (dtnested.isDeleted))")
                    .OptionalMatch("(dobjnested)-[:HAS_ATTRIBUTE]->(avnested:AttributeValue)")
                    .OptionalMatch("(avnested)-[:OF_ATTRIBUTETYPE]->(atnested:AttributeType)")
                    .Where("not (has (atnested.isDeleted))")
                    // no match on changesets for the create/update times here because 1) nestedDataObjects could have multiple first-in-line Changesets
                    // and 2) they're not needed... Only the root DO has to know it
                    .With("dtnested, dobjnested, avnested, atnested.name as atnestedName, null as updatedAt, null as createdAt")
                    .Return((dtnested, dobjnested, avnested, atnestedName, updatedAt, createdAt) => new RawDataObject
                    {
                        DataType = dtnested.As<NeoDataType>(),
                        DataObject = dobjnested.As<NeoDataObject>(),
                        AttributeNames = atnestedName.CollectAs<string>(),
                        AttributeValues = avnested.CollectAs<Node<string>>(),
                        CreatedAt = createdAt.As<DateTime>(),
                        UpdatedAt = updatedAt.As<DateTime>()
                    });
                return _dataObjectMapper.Map(await result.ResultsAsync);
            }
        }

        public async Task<List<RawDataObject>> GetUnsyncedSyncDataObjectsForDataObjectAsync(DataObject dataObject)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                    .Match("(dobj:DataObject)")
                    .Where((NeoDataObject dobj) => dobj.Id == dataObject.Id)
                    .Match("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)")
                    .Match("(av)-[:OF_VALUE|:HAS_ATTRIBUTE*]->(dobjnested:NestedDataObject)")
                    .Where((NeoDataObject dobjnested) => dobjnested.ExternalId != null)
                    .Match("(dobjnested)-[:OF_DATATYPE]->(dtnested:DataType)")
                    .OptionalMatch("(dobjnested)-[:HAS_ATTRIBUTE]->(avnested:AttributeValue)")
                    .With("dtnested, dobjnested, collect(avnested) as avnestedcol")
                    .Where("SIZE(avnestedcol) = 0")
                    .Return((dtnested, dobjnested) => new RawDataObject
                    {
                        DataType = dtnested.As<NeoDataType>(),
                        DataObject = dobjnested.As<NeoDataObject>()
                    });

                return (await query.ResultsAsync).ToList();
            }
        }

        public async Task<DataObject> InsertDataObjectForDataTypeAsync(DataObject dataObject, DataType dataType)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client
                    .Cypher
                    .Planner("cost");

                

                // getting current date for the changesets
                var currentDate = DateTime.Now;
                query = query.Match("(today:Day {value: " + currentDate.Day + "})<-[:CONTAINS]-(:Month {value: " + currentDate.Month + "})<-[:CONTAINS]-(:Year {value: " + currentDate.Year + "})");

                if (dataType.Synced == true)
                { // syncedChangeSet to differentiate between a normal DataObject (changeable by the user) and a Synced DataObject (only changeable by the master system)
                    query = query.Create($"(changeset:SyncedChangeSet {{date: \"{DateTime.Now.ToString("u")}\" }})-[:CHANGED_AT]->(today)"); // TODO : date parameter should be inserted as a WithParameter and then NeoChangeSet
                }
                else
                {
                    query = query.Create($"(changeset:ChangeSet {{date: \"{DateTime.Now.ToString("u")}\" }})-[:CHANGED_AT]->(today)"); // TODO : date parameter should be inserted as a WithParameter and then NeoChangeSet
                    
                }
                query = query.With("changeset");

                query = CreateInsertQuery(query, dataType, dataObject, "changeset");

                var dataObjectName = _stringHelper.RemoveSpecialCharacters(dataObject.Name);
                var result = await query.Return<NeoDataObject>($"do{dataObjectName}").ResultsAsync;

                if (result.Count() == 0)
                {
                    // TODO : make into custom exception
                    Logger.Error(query.Query.DebugQueryText);
                    throw new ExceptionWithFeedback(FeedbackItem.CreateValidationErrorFeedbackItem("Insertion of the DataObject failed. Probably the timeseries is not complete enough. Contact support."));
                }

                return dataObject;
            }
        }

        // BIG TODO: rename dobj-HAS_ATTRIBUTE->value to HAS_VALUE
        // TODO: when bored, move this to a DataObjectQueryProvider. (so we have more control over behaviour testing in the unit tests)
        // TODO: turns out names don't have to be unique after a with... Only the parameters do
        // TODO: also here, maybe strategy pattern is good to reduce clutter and increase readability (by alot)
        private ICypherFluentQuery CreateInsertQuery(ICypherFluentQuery query, DataType dataType, DataObject dataObject, string withClause)
        {
            var neoDataObject = new NeoDataObject { Id = dataObject.Id, Name = dataObject.Name, ExternalId = dataObject.ExternalId };

            var dataObjectName = _stringHelper.RemoveSpecialCharacters(dataObject.Name);
            var dataTypeName = _stringHelper.RemoveSpecialCharacters(dataType.Name) + dataObjectName;

            query = query
                .Match($"(dt{dataTypeName}:DataType)")
                .Where($"dt{dataTypeName}.id = \"{dataType.Id}\"");

            // between a create and a match, a with has to be provided
            var with = _stringHelper.ConcatenateWithDelimiter($"do{dataObjectName}, dt{dataTypeName}", withClause);

            //Create the DataObject and relate to the DataType
            var dataObjectType = dataObject.IsNested ?? false ? "NestedDataObject" : "DataObject";
            query = query.Create($"(do{dataObjectName}:{dataObjectType} {{dataObject{dataObjectName}}})-[:OF_DATATYPE]->(dt{dataTypeName})")
                .WithParam($"dataObject{dataObjectName}", neoDataObject)
                .Create($"(do{dataObjectName})-[:HAS_CHANGESET]->(changeset)")
                .With(with);

            foreach (var attribute in dataType.Attributes)
            {
                // if the value doesn't exist, we don't need to create it. We assume that validation already has taken place
                if (!dataObject.Values.ContainsKey(attribute.Name))
                {
                    continue;
                }

                var attributeName = _stringHelper.RemoveSpecialCharacters(attribute.Name);
                var attributeTypeNodeName = $"{dataTypeName}{attributeName}Type";
                var attributeValueNodeName = $"{dataTypeName}{attributeName}Value";

                with = $"{with}, {attributeValueNodeName}";

                // match the attribute
                query = query.Match($"({attributeTypeNodeName}:AttributeType {{name:'{attributeName}'}})<-[:HAS_ATTRIBUTE]-(dt{dataTypeName})");

                if (attribute.Type == LinkDataTypeAttributeType.TypeName)
                {
                    if (((LinkDataTypeAttributeType)attribute).DataType == null)
                    {
                        throw new ArgumentException("DataType has to be fetched with the tree of nested datatypes.");
                    }

                    // create the value node and link it to the attributeType
                    query = query.Create($"({attributeTypeNodeName})<-[:OF_ATTRIBUTETYPE]-({attributeValueNodeName}:AttributeValue {{attributeValue{attributeTypeNodeName}}})<-[:HAS_ATTRIBUTE]-(do{dataObjectName})");

                    // check whether array or single item
                    if (attribute.IsCollection == true)
                    {
                        var nestedDataObjects = _nestedDataObjectHelper.CreateNestedDataObjectListForLinkDataAttributeType((LinkDataTypeAttributeType)attribute, dataObject);

                        query = query.WithParam($"attributeValue{attributeTypeNodeName}", new NeoAttributeValue { DataObjectIds = nestedDataObjects.Select(o => o.Id).ToList() })
                            .With(with);

                        foreach (var nestedDataObject in nestedDataObjects)
                        {
                            var nestedDataObjectName = _stringHelper.RemoveSpecialCharacters(nestedDataObject.Name);

                            // create the nested DTO
                            query = CreateInsertQuery(query, ((LinkDataTypeAttributeType)attribute).DataType, nestedDataObject, with)
                                // link the value node to the nested DTO
                                .Create($"({attributeValueNodeName})-[:OF_VALUE]->(do{nestedDataObjectName})")
                                .With(with);
                        }
                    } else
                    {
                        // create a nested dataObject so we can just call this method recursively
                        var nestedDataObject = _nestedDataObjectHelper.CreateNestedDataObjectForLinkDataAttributeType((LinkDataTypeAttributeType)attribute, dataObject);

                        query = query.WithParam($"attributeValue{attributeTypeNodeName}", new NeoAttributeValue { DataObjectId = nestedDataObject.Id })
                            .With(with);

                        var nestedDataObjectName = _stringHelper.RemoveSpecialCharacters(nestedDataObject.Name);
                        
                        // create the nested DTO
                        query = CreateInsertQuery(query, ((LinkDataTypeAttributeType)attribute).DataType, nestedDataObject, with)
                            // link the value node to the nested DTO
                            .Create($"({attributeValueNodeName})-[:OF_VALUE]->(do{nestedDataObjectName})");
                    }
                }
                else if (attribute.Type == DateTimeAttributeType.TypeName)
                {
                    // check whether array or single item
                    if (attribute.IsCollection == true)
                    {
                        // TODO: please, someone smarter than me, fix this shit. It's fugly
                        var counter = 0;
                        foreach(var rawDate in (JArray)dataObject.Values[attribute.Name])
                        {
                            var date = Convert.ToDateTime(((JValue)rawDate).Value);
                            var dayNodeName = $"{dataTypeName}{attributeName}Day{++counter}";

                            query = query.Match("(" + dayNodeName + ":Day {value: " + date.Day + "})<-[:CONTAINS]-(:Month {value: " + date.Month + "})<-[:CONTAINS]-(:Year {value: " + date.Year + "})");
                        }

                        query = query.Create($"({attributeTypeNodeName})<-[:OF_ATTRIBUTETYPE]-({attributeValueNodeName}:AttributeValue {{attributeValue{attributeTypeNodeName}}})<-[:HAS_ATTRIBUTE]-(do{dataObjectName})")
                            .WithParam($"attributeValue{attributeTypeNodeName}", new NeoAttributeValue { Value = dataObject.Values[attribute.Name] });
                        
                        for (;counter > 0; counter--)
                        {
                            var dayNodeName = $"{dataTypeName}{attributeName}Day{counter}";
                            query = query.Create($"({attributeValueNodeName})-[:LINKED_VALUE]->({dayNodeName})");
                        }
                        // END TODO
                    } else
                    {
                        var date = Convert.ToDateTime(dataObject.Values[attribute.Name]);
                        var dayNodeName = $"{dataTypeName}{attributeName}Day";

                        query = query
                            .Match("(" + dayNodeName + ":Day {value: " + date.Day + "})<-[:CONTAINS]-(:Month {value: " + date.Month + "})<-[:CONTAINS]-(:Year {value: " + date.Year + "})")
                            .Create($"({attributeTypeNodeName})<-[:OF_ATTRIBUTETYPE]-({attributeValueNodeName}:AttributeValue {{attributeValue{attributeTypeNodeName}}})<-[:HAS_ATTRIBUTE]-(do{dataObjectName})")
                            .Create($"({attributeValueNodeName})-[:LINKED_VALUE]->({dayNodeName})")
                            .WithParam($"attributeValue{attributeTypeNodeName}", new NeoAttributeValue { Value = dataObject.Values[attribute.Name] });
                    }
                }
                else // any other AttributeType
                {
                    // create the value
                    query = query
                    .Create($"({attributeTypeNodeName})<-[:OF_ATTRIBUTETYPE]-({attributeValueNodeName}:AttributeValue {{attributeValue{attributeTypeNodeName}}})<-[:HAS_ATTRIBUTE]-(do{dataObjectName})")
                    .WithParam($"attributeValue{attributeTypeNodeName}", new NeoAttributeValue { Value = dataObject.Values[attribute.Name] });
                }

                query = query
                    .Create($"(changeset)-[:CHANGED]->({attributeValueNodeName})")
                    .With(with);
            }
            return query;
        }

        public async Task<long> CountDataObjectsForDataTypeIdAsync(Guid dataTypeId)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                    .Match("(dobj:DataObject)")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where((NeoDataType dt, NeoDataObject dobj) => dt.Id == dataTypeId && (dobj.IsDeleted == null || dobj.IsDeleted == false)) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .AndWhere("not (has (dt.isDeleted))")
                    .Return((dobj) => dobj.Count());

                return (await query.ResultsAsync).First();
            }
        }

        public async Task<List<DataObject>> FindDataObjectsForDataTypeIdAsync(Guid dataTypeId, int page, int pagesize, string orderbyField = null, bool descending = false)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                    .Match("(dobj:DataObject)")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where((NeoDataType dt, NeoDataObject dobj) => dt.Id == dataTypeId && (dobj.IsDeleted == null || dobj.IsDeleted == false)) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .AndWhere("not (has (dt.isDeleted))");

                if (orderbyField != null)
                {
                    query = query.Match("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType {name: \"" + orderbyField + "\"})")
                        .Match("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                        .Where("not (has (at.isDeleted))")
                        .With("*");
                    if (descending)
                    {
                        query = query.OrderByDescending("av.value");
                    } else
                    {
                        query = query.OrderBy("av.value");
                    }
                } else
                { // to avoid random ordering, we order by dobj.name
                    query = query.With("*")
                        .OrderBy("dobj.name");
                }

                var guidList = await query.With("dobj.id as dobjId")
                    .Skip((page - 1) * pagesize)
                    .Limit(pagesize)
                    .Return<Guid>("dobjId")
                    .ResultsAsync;

                return await FindDataObjectsByIdListAsync(guidList);
            }
        }

        public async Task<List<Guid>> FindDataObjectIdsForDataTypeIdAsync(Guid dataTypeId)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = client.Cypher
                    .Match("(dobj:DataObject)")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where((NeoDataType dt, NeoDataObject dobj) => dt.Id == dataTypeId && (dobj.IsDeleted == null || dobj.IsDeleted == false)) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .AndWhere("not (has (dt.isDeleted))")
                    .With("dobj.id as dobjId")
                    .Return<Guid>("dobjId");

                return (await query.ResultsAsync).ToList();
            }
        }

        public async Task<List<ChangeSet>> GetHistoryOfDataObjectByIdAsync(Guid id)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();
                /*
                 * 
match (do:DataObject {id: "265a8962-1cb1-45c6-979c-85b3aa5c797f"})
match (do)-[:HAS_CHANGESET]->(cs:SyncedChangeSet)
match (cs)-[:CHANGED]->(av:AttributeValue)
optional match (av)-[:OF_VALUE]->(nestedDo:NestedDataObject)
return *



match (do:DataObject {id: "265a8962-1cb1-45c6-979c-85b3aa5c797f"})
match (do)-[:HAS_CHANGESET|CONTAINS*]->(cs)
where (cs:ChangeSet or cs:SyncedChangeSet)
match (cs)-[:CHANGED]->(changedValue:AttributeValue)
match (changedValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)
match (changedValue)-[:UPDATED_FROM]->(previousValue:AttributeValue)
optional match (changedValue)-[:OF_VALUE]->(nestedDo:NestedDataObject)
optional match (previousValue)-[:OF_VALUE]->(previousNestedDobj:NestedDataObject)
//with *, split(str(changedValue.dataObjectIds), ',') as col1, split(str(previousValue.dataObjectIds), ',') as col2
with cs, at, previousValue, changedValue, collect(distinct nestedDo.id) as col1, collect(distinct previousNestedDobj.id) as col2
with cs, collect({field:at.name, originalValue:previousValue.value, newValue:changedValue.value, test1:col1, test2:col2}) as changes
return *

//match (cs)-[:CHANGED]->(av:AttributeValue)
//optional match (av)-[:OF_VALUE]->(nestedDo:NestedDataObject)
//return *
                 */
                var query = client.Cypher
                    .Match("(dobj:DataObject)")
                    .UsingIndex("dobj:DataObject(id)")
                    .Where((NeoDataObject dobj) => dobj.Id == id)
                    .Match("(dobj)-[:HAS_CHANGESET|CONTAINS*]->(cs)")
                    .Where("(cs:ChangeSet or cs:SyncedChangeSet)")
                    .Match("(cs)-[:CHANGED]->(changedValue:AttributeValue)")
                    .Match("(changedValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                    //.OptionalMatch("(changedValue)-[:UPDATED_FROM]->(previousValue:AttributeValue)")
                    .Match("(changedValue)-[:UPDATED_FROM]->(previousValue:AttributeValue)") // initial value is not shown
                    .OptionalMatch("(changedValue)-[:OF_VALUE]->(newNestedDobj:NestedDataObject)")
                    .OptionalMatch("(previousValue)-[:OF_VALUE]->(previousNestedDobj:NestedDataObject)")
                    .With("cs, at, previousValue, changedValue, collect(distinct newNestedDobj.id) as newDataObjectIds, collect(distinct previousNestedDobj.id) as previousDataObjectIds")
                    .With("cs, collect({field:at.name, originalValue:previousValue.value, newValue:changedValue.value, originalDataObjectIds: previousDataObjectIds, newDataObjectIds: newDataObjectIds}) as changes")
                    .OrderByDescending("cs.date")
                    .Return((cs, changes) => new RawChangeSet { ChangeSet = cs.As<NeoChangeSet>(), Changes = changes.As<List<NeoChangeSetEntry>>() });
                //.Return((cs, changes) => new ChangeSet { Date = cs.As<NeoChangeSet>().Date, Changes = changes.As<List<ChangeSetEntry>>() });

                var rawChangeSets = (await query.ResultsAsync).ToList();
                // we have the raw changesets. Now we have to actually hydrate the dataObjects in the case of arrays
                var dataObjectGuidList = rawChangeSets.SelectMany(rcs => rcs.Changes.SelectMany(c => c.OriginalDataObjectIds.Concat(c.NewDataObjectIds))).Distinct();

                var dataObjects = await FindDataObjectsByIdListAsync(dataObjectGuidList, true);

                var returnValue = new List<ChangeSet>();

                foreach (var rawChangeSet in rawChangeSets)
                {
                    var changeSet = new ChangeSet();
                    changeSet.Date = rawChangeSet.ChangeSet.Date;
                    var changesList = new List<ChangeSetEntry>();
                    foreach (var rawChange in rawChangeSet.Changes)
                    {
                        var change = new ChangeSetEntry();
                        change.Field = rawChange.Field;
                        if (rawChange.OriginalValue != null || rawChange.NewValue != null)
                        {
                            // we're talking about a single value here
                            change.OriginalValue = rawChange.OriginalValue;
                            change.NewValue = rawChange.NewValue;
                        }
                        else
                        {
                            // we're talking about a collection of DataObjects here
                            var relevantOriginalDataObjectValues = dataObjects.Where(x => rawChange.OriginalDataObjectIds.Contains(x.Id)).Select(x => x.Values.OrderBy(d => d.Key));
                            var relevantNewDataObjectValues = dataObjects.Where(x => rawChange.NewDataObjectIds.Contains(x.Id)).Select(x => x.Values.OrderBy(d => d.Key));
                            change.OriginalValue = relevantOriginalDataObjectValues.ToJson();
                            change.NewValue = relevantNewDataObjectValues.ToJson();
                        }
                        changesList.Add(change);
                    }
                    changeSet.Changes = changesList;
                    returnValue.Add(changeSet);
                }

                return returnValue;
            }
        }

        private ICypherFluentQuery GetFindByWhereClauseQuery(IGraphClient client, Guid dataTypeId, string where)
        {
            return client.Cypher
                    .Match("(dobj:DataObject)")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .Where((NeoDataType dt, NeoDataObject dobj) => dt.Id == dataTypeId && (dobj.IsDeleted == null || dobj.IsDeleted == false)) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .AndWhere("not (has (dt.isDeleted))")
                    .Match("(dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)")
                    .Match("(av)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)")
                    .Where("not (has (at.isDeleted))")
                    .With("dobj.id as dobjId, collect({type:at.name, value:av.value}) as typeValuePairList")
                    .Where(where);
        }

        private ICypherFluentQuery GetFindBySimpleWhereClauseQuery(IGraphClient client, Guid dataTypeId, string fieldname, string where)
        {
            return client.Cypher
                    .Match("(dobj:DataObject)")
                    .Match("(dobj)-[:OF_DATATYPE]->(dt:DataType)")
                    .UsingIndex("dt:DataType(id)")
                    .Where((NeoDataType dt, NeoDataObject dobj) => dt.Id == dataTypeId && (dobj.IsDeleted == null || dobj.IsDeleted == false)) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                    .AndWhere("not (has (dt.isDeleted))")
                    .Match("(dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)")
                    .Where("not (has(at.isDeleted)) and not(has(at.dataTypeId))")
                    .AndWhere((NeoAttributeType at) => at.Name == fieldname)
                    .Match("(av:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at)")
                    .Where(where)
                    .Match("(dobj)-[:HAS_ATTRIBUTE]->(av)")
                    .With("dobj.id as dobjId");
        }

        public async Task<long> CountDataObjectsForDataTypeIdByWhereClauseAsync(Guid dataTypeId, string where)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = GetFindByWhereClauseQuery(client, dataTypeId, where)
                    .Return((dobjId) => dobjId.Count());

                return (await query.ResultsAsync).First();
            }
        }

        public async Task<long> CountDataObjectsForDataTypeIdBySimpleComparisonAsync(Guid dataTypeId, string fieldname, string where)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = GetFindBySimpleWhereClauseQuery(client, dataTypeId, fieldname, where)
                    .Return((dobjId) => dobjId.Count());

                return (await query.ResultsAsync).First();
            }
        }

        public async Task<List<DataObject>> FindDataObjectsForDataTypeIdBySimpleComparisonAsync(Guid dataTypeId, string fieldname, string where, int page, int pageSize)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = GetFindBySimpleWhereClauseQuery(client, dataTypeId, fieldname, where)
                    .With("dobjId")
                    .Skip((page - 1) * pageSize)
                    .Limit(pageSize)
                    .Return<Guid>("dobjId");

                var guidList = await query.ResultsAsync;
                return await FindDataObjectsByIdListAsync(guidList);
            }
        }

        public async Task<List<DataObject>> FindDataObjectsForDataTypeIdByWhereClauseAsync(Guid dataTypeId, string where, int page, int pageSize)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var query = GetFindByWhereClauseQuery(client, dataTypeId, where)
                    .With("dobjId")
                    .Skip((page - 1) * pageSize)
                    .Limit(pageSize)
                    .Return<Guid>("dobjId");

                var guidList = await query.ResultsAsync;

                /*
                 * MUST HAVE TODO:
                 * following query is ~3 times as fast for large datasets. so this is REQUIRED to be implemented.
                 * Though this method has to be refactored so that the where clause is passed as an ICondition rather than a string
                 * so we can get all fields in the condition.
                 * 
                 * begin query:
MATCH (dobj:DataObject)
MATCH (dobj)-[:OF_DATATYPE]->(dt:DataType)
WHERE ((dt.id = "7baf8c15-67b5-4d1f-9e97-5b8341500ead") AND ((not(has(dobj.isDeleted))) OR (dobj.isDeleted = false)))
AND not (has (dt.isDeleted))
MATCH (dt)-[:HAS_ATTRIBUTE]->(at:AttributeType)
WHERE not (has (at.isDeleted)) and not (has (at.dataTypeId))
and at.name IN ["bedrag"]
MATCH (dobj)-[:HAS_ATTRIBUTE]->(av:AttributeValue)-[:OF_ATTRIBUTETYPE]->(at:AttributeType)
WITH dobj.id as dobjId, collect({type:at.name, value:av.value}) as typeValuePairList
WHERE (SIZE(FILTER(x in typeValuePairList WHERE (x.type = "bedrag" AND x.value < 10))) = 1 AND 1 = 1)
RETURN collect(dobjId) AS Guids
                 * */

                return await FindDataObjectsByIdListAsync(guidList);
            }
        }

        // BIG TODO: rename dobj-HAS_ATTRIBUTE->value to HAS_VALUE
        public async Task<DataObject> UpdateDataObjectAsync(DataObject dataObject, IEnumerable<Change> changes, bool isSyncedDataObject = false)
        {
            using (var pooledClient = await _pooledGraphClientProvider.GetAsync())
            {
                var client = pooledClient.Value;
                if (!client.IsConnected)
                    client.Connect();

                var currentDate = DateTime.Now;

                var query = client.Cypher
                    .Match("(today:Day {value: " + currentDate.Day + "})<-[:CONTAINS]-(:Month {value: " + currentDate.Month + "})<-[:CONTAINS]-(:Year {value: " + currentDate.Year + "})");

                if (isSyncedDataObject)
                {
                    query = query.OptionalMatch("(dobj:DataObject)")
                        .Where((NeoDataObject dobj) => dobj.Id == dataObject.Id)
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) as d1")
                        .OptionalMatch("(dobj:NestedDataObject)")
                        .Where((NeoDataObject dobj) => dobj.Id == dataObject.Id)
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .With("collect(distinct dobj) +d1 as d2")
                        .Unwind("d2", "dobj")
                        .With("dobj")
                        .OptionalMatch("(dobj)-[oldHCs:HAS_CHANGESET]->(oldCs:SyncedChangeSet)") // a synced DO doesnt necessarily have a Changeset yet. We also use a seperate ChangeSet here, because changes to this object arent actual changes on the main DO
                        .Delete("oldHCs") // delete the old link to the CS
                        .Create($"(dobj)-[:HAS_CHANGESET]->(newCs:SyncedChangeSet {{date: \"{DateTime.Now.ToString("u")}\" }})-[:CHANGED_AT]->(today)") // create the new CS and link the dobj to it
                        .ForEach("(ignoreMe IN CASE WHEN oldCs is not null THEN [1] ELSE [] END | CREATE (newCs)-[:CONTAINS]->(oldCs))") // this was due to a bug. When inserting a synced DataObject, it got a normal ChangeSet
                        .With("dobj, newCs");
                } else
                {
                    query = query
                        .Match("(dobj:DataObject)")
                        .Where((NeoDataObject dobj) => dobj.Id == dataObject.Id)
                        .AndWhere((NeoDataObject dobj) => dobj.IsDeleted == null || dobj.IsDeleted == false) // unary operators are not supported because ambigiousness between c# and neo4j on handling null values
                        .Match("(dobj)-[oldHCs:HAS_CHANGESET]->(oldCs:ChangeSet)")
                        .Delete("oldHCs") // delete the old link to the CS
                        .Create($"(dobj)-[:HAS_CHANGESET]->(newCs:ChangeSet {{date: \"{DateTime.Now.ToString("u")}\" }})-[:CHANGED_AT]->(today)") // create the new CS and link the dobj to it
                        .Create("(newCs)-[:CONTAINS]->(oldCs)")
                        .With("dobj, newCs");
                }
                
                foreach (var change in changes)
                {
                    query = _dataObjectQueryProvider.UpdateDataObjectWithChange(dataObject, query, change);
                }

                var result = await query.Return<NeoDataObject>($"dobj").ResultsAsync;

                if (result.Count() == 0)
                {
                    // TODO : make into custom exception
                    throw new Exception("updating of the DataObject failed");
                }
                return dataObject;
            }
        }
    }
}
