﻿using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public class NeoChangeSetEntry
    {
        public string Field { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public IEnumerable<Guid> OriginalDataObjectIds { get; set; }
        public IEnumerable<Guid> NewDataObjectIds { get; set; }
    }
}
