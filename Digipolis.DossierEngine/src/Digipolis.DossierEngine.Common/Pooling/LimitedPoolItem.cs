﻿using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using System;
using System.Diagnostics;

namespace Digipolis.DossierEngine.Common.Pooling
{
    public class LimitedPoolItem<T> : ILimitedPoolItem<T>
    {
        private readonly Action<LimitedPoolItem<T>> _disposeAction;

        private readonly TimeSpan _lifetime;
        private bool _expired;

        public T Value { get; }

        internal bool Expired
        {
            get
            {
                if (_expired)
                    return true;
                _expired = _stopwatch.Elapsed > _lifetime;
                return _expired;
            }
        }
        private readonly Stopwatch _stopwatch;

        internal LimitedPoolItem(T value, Action<LimitedPoolItem<T>> disposeAction, TimeSpan lifetime)
        {
            _disposeAction = disposeAction;
            _lifetime = lifetime;
            Value = value;
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void Dispose(bool disposing)
        {
            if (disposing)
            {
                _disposeAction(this);
            }
        }
    }
}
