﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class AddUniqueConstraintPluginName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Plugin",
                nullable: false);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Plugin_Name",
                table: "Plugin",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Plugin_Name",
                table: "Plugin");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Plugin",
                nullable: true);
        }
    }
}
