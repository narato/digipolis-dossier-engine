﻿using Digipolis.DossierEngine.DataProvider.Models;
using Neo4jClient;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.RawModels
{
    public class RawDataType
    {
        public NeoDataType DataType { get; set; }
        public IEnumerable<Node<string>> Attributes { get; set; }
    }
}
