﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.AspNetCore.Mvc;

using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    [Route("api/plugins")]
    public class PluginController : Controller
    {
        private IPluginManager _pluginManager;
        private IResponseFactory _responseFactory;

        public PluginController(IPluginManager pluginManager, IResponseFactory responseFactory)
        {
            _responseFactory = responseFactory;
            _pluginManager = pluginManager;
        }

        /// <summary>
        /// Load all plugins
        /// </summary>
        /// <returns>Message indicating whether the action was successful</returns>
        // POST api/plugins
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [HttpPost]
        public IActionResult LoadPlugins()
        {
            return _responseFactory.CreateGetResponse(() => _pluginManager.LoadPlugins(), this.GetRequestUri());
        }

        /// <summary>
        /// This methods returns all registered plugins
        /// </summary>
        /// <returns>A response containing a list of all plugins</returns>
        // GET api/plugins
        [ProducesResponseType(typeof(Response<List<Plugin>>), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response<List<Plugin>>), (int)System.Net.HttpStatusCode.BadRequest)]
        [HttpGet]
        public IActionResult GetPlugins()
        {
            return _responseFactory.CreateGetResponse(() => _pluginManager.GetPlugins(),
                this.GetRequestUri());
        }

        /// <summary>
        /// This method will be called from the eventhandler when a relevant event was processed
        /// </summary>
        /// <param name="pluginName">The name of the plugin</param>
        /// <param name="payload">The payload coming from the eventhandler</param>
        /// <returns>Statuscode of the request</returns>
        // POST api/plugins/{pluginName}
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.Created)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [HttpPost]
        [Route("{pluginName}")]
        public IActionResult EventReceiver(string pluginName, [FromBody] object payload)
        {
            return _responseFactory.CreateGetResponse(() => _pluginManager.ProcessEvent(pluginName, payload), this.GetRequestUri());
        }
    }
}
