#!/bin/bash

dotnet build src/Digipolis.DossierEngine.DataType.API/
dotnet publish src/Digipolis.DossierEngine.DataType.API/ -o build/data-type
mkdir build/data-type/Plugins
