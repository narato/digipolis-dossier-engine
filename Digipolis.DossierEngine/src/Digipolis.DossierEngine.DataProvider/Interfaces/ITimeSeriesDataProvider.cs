﻿using Digipolis.DossierEngine.Common.Models;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface ITimeSeriesDataProvider
    {
        void InsertTimeSeriesForYear(Config tenantConfig, int year);
    }
}
