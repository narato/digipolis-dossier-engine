﻿using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Admin.API.Controllers
{
    public class TimeSeriesController : Controller
    {
        private ITimeSeriesManager _timeSeriesManager;
        private IResponseFactory _responseFactory;

        public TimeSeriesController(ITimeSeriesManager timeSeriesManager, IResponseFactory responseFactory)
        {
            _responseFactory = responseFactory;
            _timeSeriesManager = timeSeriesManager;
        }

        /// <summary>
        /// This method adds a year to the timeseries in the neo database for a given tenantKey
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <param name="yearToInsert">The year to insert in the timeseries</param>
        /// <returns>A standard Response object with statuscode 200</returns>
        /// <response code="400">The year already exists</response>
        // POST api/tenants/{tenantKey}/timeseries/years
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.OK)]
        [ProducesResponseType(typeof(Response), (int)System.Net.HttpStatusCode.BadRequest)]
        [HttpPost("api/tenants/{tenantKey}/timeseries/years")]
        public async Task<IActionResult> InsertTimeSeriesForTenantForYearAsync([Required]string tenantKey, [FromBody] TimeSeriesInsertDTO yearToInsert)
        {
            return await _responseFactory.CreateGetResponseAsync(async () => await _timeSeriesManager.InsertTimeSeriesForTenantForYearAsync(tenantKey, yearToInsert.YearToInsert), this.GetRequestUri());
        }
    }
}
