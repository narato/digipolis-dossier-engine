﻿using System;
using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Narato.Common.Models;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataObjectManager
    {
        Task<DataObject> GetDataObjectByIdAsync(Guid id);
        Task<DataObject> DeleteDataObjectByIdAsync(Guid id);
        Task<List<DataObject>> GetDataObjectsByIdListAsync(List<Guid> ids);
        Task<PagedCollectionResponse<IEnumerable<DataObject>>> GetDataObjectsForDataTypeIdAsync(Guid dataTypeId, int page, int pageSize, string orderbyField = null, bool descending = false);
        Task<DataObject> InsertDataObjectAsync(DataObject dataObject);
        Task<DataObject> UpdateDataObjectForIdAsync(Guid id, DataObject dataObject, bool isSyncedDataObject = false);

        Task<List<ChangeSet>> GetHistoryGetHistoryOfDataObjectByIdAsync(Guid id);
        Task<List<Guid>> GetDataObjectIdsForDataTypeIdAsync(Guid dataTypeId);
    }
}