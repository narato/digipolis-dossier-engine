﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public class StringValue : IValue<string>
    {
        public string Type { get; } = "string";
        public string Value { get; }

        public StringValue(string value)
        {
            Value = value;
        }
    }
}
