﻿using Digipolis.DossierEngine.DataProvider.Models;
using Neo4jClient;
using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.RawModels
{
    public class RawDataObject
    {
        public NeoDataType DataType { get; set; }
        public NeoDataObject DataObject { get; set; }
        public IEnumerable<string> AttributeNames { get; set; }
        public IEnumerable<Node<string>> AttributeValues { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
