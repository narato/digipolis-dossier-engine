﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public interface IValue<T> : Comparable
    {
        T Value { get; }
    }
}
