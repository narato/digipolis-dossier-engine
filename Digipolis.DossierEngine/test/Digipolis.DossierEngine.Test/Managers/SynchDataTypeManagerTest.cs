﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class SynchDataTypeManagerTest
    {
        [Fact]
        public void GetAllSynchDataTypesReturnsAListOfSycnDataTypes()
        {
            var pluginDataProviderMock = new Mock<IPluginDataProvider>();
            pluginDataProviderMock.Setup(mock => mock.GetAllSynchDataTypes()).Returns(new List<List<Model.Models.DataType>>());

            var synchDataTypeManager = new SynchDataTypeManager(pluginDataProviderMock.Object);
            var result = synchDataTypeManager.GetAllSynchDataTypes();

            Assert.IsType(typeof(List<List<Model.Models.DataType>>), result);
        }
    }
}
