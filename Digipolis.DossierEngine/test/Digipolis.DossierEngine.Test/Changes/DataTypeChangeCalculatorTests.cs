﻿using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;

namespace Digipolis.DossierEngine.Test.Validators
{
    public class DataTypeChangeCalculatorTests
    {
        [Fact]
        public void CalculateDataTypesChanges_ReturnsChangeOfTypeNew()
        {
            var dtOriginal = InitDataTypeModel();

            var dtUpdated = InitDataTypeModel();
            dtUpdated.Attributes.Add(new StringAttributeType { Name = "TestStringType" });

            var changeCalculator = new DataTypeChangeCalculator();

            var result = changeCalculator.CalculateChangeSet(dtOriginal, dtUpdated).ToList();

            Assert.IsType<List<Change>>(result);
            Assert.Equal(1, result.Count);
            Assert.Equal(ChangeType.NEW, result.First().Type);
        }

        [Fact]
        public void CalculateDataTypesChanges_ReturnsChangeOfTypeDeleted()
        {
            var dtOriginal = InitDataTypeModel();
            dtOriginal.Attributes.Add(new StringAttributeType { Name = "TestStringType" });

            var dtUpdated = InitDataTypeModel();

            var changeCalculator = new DataTypeChangeCalculator();

            var result = changeCalculator.CalculateChangeSet(dtOriginal, dtUpdated).ToList();

            Assert.IsType<List<Change>>(result);
            Assert.Equal(1, result.Count);
            Assert.Equal(ChangeType.DELETED, result.First().Type);
        }

        //[Fact]
        //public void CalculateDataTypesChanges_ReturnsChangeOfTypeSkipped()
        //{
        //    var dtOriginal = InitDataTypeModel();
        //    dtOriginal.Attributes.Add(new StringAttributeType { Name = "TestStringType" });

        //    var dtUpdated = InitDataTypeModel();
        //    dtUpdated.Attributes.Add(new StringAttributeType { Name = "TestStringType" });

        //    var changeCalculator = new DataTypeChangeCalculator();

        //    var result = changeCalculator.CalculateChangeSet(dtOriginal, dtUpdated).ToList();

        //    Assert.IsType<List<Change>>(result);
        //    Assert.Equal(1, result.Count);
        //    Assert.Equal(ChangeType.SKIP, result.First().Type);
        //}

        [Fact]
        public void CalculateDataTypesChanges_IsRequiredChanged_ReturnsChangeOfTypeUpdated()
        {
            var dtOriginal = InitDataTypeModel();
            dtOriginal.Attributes.Add(new StringAttributeType { Name = "TestStringType", IsRequired = true });

            var dtUpdated = InitDataTypeModel();
            dtUpdated.Attributes.Add(new StringAttributeType { Name = "TestStringType", IsRequired = false });

            var changeCalculator = new DataTypeChangeCalculator();

            var result = changeCalculator.CalculateChangeSet(dtOriginal, dtUpdated).ToList();

            Assert.IsType<List<Change>>(result);
            Assert.Equal(1, result.Count);
            Assert.Equal(ChangeType.UPDATED, result.First().Type);
        }

        [Fact]
        public void CalculateDataTypesChanges_DefaultValueChanged_ReturnsChangeOfTypeUpdated()
        {
            var dtOriginal = InitDataTypeModel();
            dtOriginal.Attributes.Add(new StringAttributeType { Name = "TestStringType", DefaultValue = "1" });

            var dtUpdated = InitDataTypeModel();
            dtUpdated.Attributes.Add(new StringAttributeType { Name = "TestStringType", DefaultValue = "2" });

            var changeCalculator = new DataTypeChangeCalculator();

            var result = changeCalculator.CalculateChangeSet(dtOriginal, dtUpdated).ToList();

            Assert.IsType<List<Change>>(result);
            Assert.Equal(1, result.Count);
            Assert.Equal(ChangeType.UPDATED, result.First().Type);
        }

        private Model.Models.DataType InitDataTypeModel()
        {
            return new Model.Models.DataType
            {
                Id = Guid.NewGuid(),
                Name = "DataType1",
                Attributes = new List<IAttributeType>()
            };
        }
    }
}