﻿namespace Digipolis.DossierEngine.Domain.Dto
{
    public class TimeSeriesInsertDTO
    {
        public int YearToInsert { get; set; }
    }
}
