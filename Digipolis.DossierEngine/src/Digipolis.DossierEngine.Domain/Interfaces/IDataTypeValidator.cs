﻿using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataTypeValidator
    {
        Task<List<FeedbackItem>> ValidateDataTypeAttributesAsync(DataType dataType);
        List<FeedbackItem> DifferentiateDataTypeAttributes(DataType originalDataType, DataType dataType);
        List<FeedbackItem> ValidateDataTypeDefaultValue(DataType dataType);
    }
}