﻿using Digipolis.DossierEngine.Model.Models;
using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IPluginDataProvider
    {
        List<Plugin> GetPlugins();
        Plugin GetPluginById(Guid id);
        void InsertPlugins(List<Plugin> plugins);
        TenantPluginLink InsertTenantPluginLink(TenantPluginLink tenantPluginLink);
        List<List<DataType>> GetAllSynchDataTypes();
        bool DeleteTenantPluginLink(Guid linkId);
        List<TenantPluginLink> GetPluginLinksForTenant(string tenantKey);
        List<TenantPluginLink> GetPluginLinksForPluginForActiveTenants(string pluginName);
        TenantPluginLink GetPluginLinksForTenantAndDataTypeId(string tenantKey, Guid dataTypeId);
    }
}