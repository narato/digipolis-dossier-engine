﻿using Digipolis.DossierEngine.Domain.Interfaces;
using System.Collections.Generic;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.SimpleCachers
{
    // WARNING! this is a very simple cacher. This should NOT be instantiated as a singleton. 
    // At most this is suitable on a per request basis. And even then, it's not safe for race conditions.
    // this cache CANNOT be invalidated. This is ONLY to be used for extremely simple use cases, such as the datatype validator!
    public class SimpleDataTypeCacher : ISimpleDataTypeCacher
    {
        private readonly IDataTypeDataProvider _dataTypeDataProvider;

        private IEnumerable<DataType> _cachedDataTypeList;

        public SimpleDataTypeCacher(IDataTypeDataProvider dataTypeDataProvider)
        {
            _dataTypeDataProvider = dataTypeDataProvider;
        }

        public async Task<IEnumerable<DataType>> GetAllDataTypesAsync()
        {
            if (_cachedDataTypeList == null)
            {
                _cachedDataTypeList = await _dataTypeDataProvider.GetAllDataTypesAsync(false);
            }
            return _cachedDataTypeList;
        }
    }
}
