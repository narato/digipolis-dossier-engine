﻿using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Changes
{
    // used for arrays of DataObjects.
    public class NestedChangeList
    {
        public IEnumerable<Change> NestedObjects { get; set; }

        public NestedChangeList(IEnumerable<Change> nestedObjects)
        {
            NestedObjects = nestedObjects;
        }
    }
}
