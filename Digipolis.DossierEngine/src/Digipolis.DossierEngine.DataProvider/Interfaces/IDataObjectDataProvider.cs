﻿using System;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.Common.Status.Models;
using System.Collections.Generic;
using Digipolis.DossierEngine.DataProvider.Changes;
using Digipolis.DossierEngine.DataProvider.RawModels;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IDataObjectDataProvider
    {
        Task<DataObject> GetDataObjectByIdAsync(Guid id, bool includeNested = false);
        Task<bool> DeleteDataObjectByIdAsync(Guid id);
        Task<List<DataObject>> FindDataObjectsByIdListAsync(IEnumerable<Guid> ids, bool includeNested = false);
        Task<long> CountDataObjectsForDataTypeIdAsync(Guid dataTypeId);
        Task<List<DataObject>> FindDataObjectsForDataTypeIdAsync(Guid dataTypeId, int page, int pagesize, string orderbyField = null, bool descending = false);
        Task<List<Guid>> FindDataObjectIdsForDataTypeIdAsync(Guid dataTypeId);
        Task<List<DataObject>> FindDataObjectsForDataTypeIdBySimpleComparisonAsync(Guid dataTypeId, string fieldname, string where, int page, int pageSize);
        Task<long> CountDataObjectsForDataTypeIdBySimpleComparisonAsync(Guid dataTypeId, string fieldname, string where);
        Task<List<DataObject>> FindDataObjectsForDataTypeIdByWhereClauseAsync(Guid dataTypeId, string where, int page, int pageSize);
        Task<long> CountDataObjectsForDataTypeIdByWhereClauseAsync(Guid dataTypeId, string where);
        Task<List<RawDataObject>> GetUnsyncedSyncDataObjectsForDataObjectAsync(DataObject dataObject);
        Task<DataObject> InsertDataObjectForDataTypeAsync(DataObject dataObject, DataType dataType);
        Task<DataObject> UpdateDataObjectAsync(DataObject dataObject, IEnumerable<Change> changes, bool isSyncedDataObject = false);

        Task<List<ChangeSet>> GetHistoryOfDataObjectByIdAsync(Guid id);

        Task<DbConnectionCheckResult> CheckDBConnectionAsync();
    }
}