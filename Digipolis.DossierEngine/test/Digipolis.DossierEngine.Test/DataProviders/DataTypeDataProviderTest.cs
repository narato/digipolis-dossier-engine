﻿using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Moq;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Xunit;

namespace Digipolis.DossierEngine.Test.DataProviders
{
    public class DataTypeDataProviderTest
    {
        [Fact]
        public void GetDataTypeByIdReturnsDataType()
        {
            var guid = new Guid();

            var returnRawDataType = new List<RawDataType>();
            returnRawDataType.Add(new RawDataType());

            var cypherMock = new Mock<ICypherFluentQuery<RawDataType>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.UsingIndex(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<Expression<Func<NeoDataType, bool>>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.AndWhere(It.IsAny<string>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return(It.IsAny<Expression<Func<ICypherResultItem, ICypherResultItem, RawDataType >>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(returnRawDataType);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataTypeMapperMock = new Mock<IDataTypeMapper>();
            dataTypeMapperMock.Setup(domm => domm.Map(It.IsAny<RawDataType>())).Returns(new Model.Models.DataType());

            var dataProvider = new DataTypeDataProvider(pooledGraphClientProviderMock.Object, dataTypeMapperMock.Object);

            var returnDataType = dataProvider.GetDataTypeByIdAsync(guid).Result;

            Assert.NotNull(returnDataType);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            dataTypeMapperMock.Verify(x => x.Map(It.IsAny<RawDataType>()), Times.Once, "RawDataType should be mapped... Once");
        }

        [Fact]
        public void GetDataTypeByNameReturnsDataType()
        {
            var name = "";

            var returnRawDataType = new List<RawDataType>();
            returnRawDataType.Add(new RawDataType());

            var cypherMock = new Mock<ICypherFluentQuery<RawDataType>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.UsingIndex(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<Expression<Func<NeoDataType, bool>>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.AndWhere(It.IsAny<string>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return(It.IsAny<Expression<Func<ICypherResultItem, ICypherResultItem, RawDataType >>>())).Returns(cypherMock.Object);
            cypherMock.SetupGet(cm => cm.Results).Returns(returnRawDataType);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var tenantGraphClientFactoryMock = new Mock<ITenantGraphClientFactory>();
            tenantGraphClientFactoryMock.Setup(tgcfm => tgcfm.CreateAsync()).ReturnsAsync(graphClientMock.Object);

            var dataTypeMapperMock = new Mock<IDataTypeMapper>();
            dataTypeMapperMock.Setup(domm => domm.Map(It.IsAny<RawDataType>())).Returns(new Model.Models.DataType());

            var dataProvider = new DataTypeDataProvider(pooledGraphClientProviderMock.Object, dataTypeMapperMock.Object);

            var returnDataType = dataProvider.GetDataTypeByNameAsync(name).Result;

            Assert.NotNull(returnDataType);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            dataTypeMapperMock.Verify(x => x.Map(It.IsAny<RawDataType>()), Times.Once, "RawDataType should be mapped... Once");
        }

        [Fact]
        public void GetAllDataTypesReturnsListOfDataTypes()
        {
            var returnDataTypes = new List<Model.Models.DataType>();

            var cypherMock = new Mock<ICypherFluentQuery<Model.Models.DataType>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return(It.IsAny<Expression<Func<ICypherResultItem, Model.Models.DataType>>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(returnDataTypes);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataTypeMapperMock = new Mock<IDataTypeMapper>();

            var dataProvider = new DataTypeDataProvider(pooledGraphClientProviderMock.Object, dataTypeMapperMock.Object);

            var value = dataProvider.GetAllDataTypesAsync(false).Result;

            Assert.NotNull(value);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            dataTypeMapperMock.Verify(x => x.Map(It.IsAny<RawDataType>()), Times.Never, "Mapping isn't needed. We can return DataTypes right away");
        }

        [Fact]
        public void GetDataTypesByIdListReturnsListOfDataTypes()
        {
            var idList = new List<Guid>();
            idList.Add(Guid.NewGuid());
            var rawDataTypes = new List<RawDataType>();

            var cypherMock = new Mock<ICypherFluentQuery<RawDataType>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.AndWhere(It.IsAny<string>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return(It.IsAny<Expression<Func<ICypherResultItem, ICypherResultItem, RawDataType>>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(rawDataTypes);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataTypeMapperMock = new Mock<IDataTypeMapper>();
            dataTypeMapperMock.Setup(domm => domm.Map(It.IsAny<IEnumerable<RawDataType>>())).Returns(new List<Model.Models.DataType>());

            var dataProvider = new DataTypeDataProvider(pooledGraphClientProviderMock.Object, dataTypeMapperMock.Object);

            var value = dataProvider.GetDataTypeByIdListAsync(idList).Result;

            Assert.NotNull(value);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            dataTypeMapperMock.Verify(x => x.Map(It.IsAny<IEnumerable<RawDataType>>()), Times.Once, "Mapping of the list should be called once and only once");
        }

        [Fact]
        public void InsertDataTypeTest()
        {

            var dataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };

            dataType.Attributes.Add(new IntAttributeType() { Name = "test" });

            var cypherMock = new Mock<ICypherFluentQuery>();
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Create(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataTypeMapperMock = new Mock<IDataTypeMapper>();

            var dataProvider = new DataTypeDataProvider(pooledGraphClientProviderMock.Object, dataTypeMapperMock.Object);

            var returnDataType = dataProvider.InsertDataTypeAsync(dataType).Result;

            Assert.NotNull(returnDataType);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            cypherMock.Verify(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<IntAttributeType>()), Times.Once, "One Attribute should have been created");
            cypherMock.Verify(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<NeoDataType>()), Times.Once, "One DataType should have been created");
        }
    }
}
