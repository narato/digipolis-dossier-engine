﻿using System;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public class NeoDataType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool? Synced { get; set; }
    }
}