﻿using System;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public class NeoDataObject
    {
        public Guid Id { get; set; }

        public string ExternalId { get; set; } // used by plugins (can be int/guid/string)

        public string Name { get; set; }

        public bool? IsDeleted { get; set; } // used for softDelete
    }
}