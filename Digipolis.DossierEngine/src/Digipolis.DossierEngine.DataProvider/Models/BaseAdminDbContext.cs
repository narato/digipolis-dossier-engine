﻿using Digipolis.DossierEngine.Model.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    // Not an interface because we nee to ensure that (for example) saveChanges method exists
    public abstract class BaseAdminDbContext : DbContext
    {
        public DbSet<Digipolis.Common.Tenancy.Models.Tenant> Tenant { get; set; }
        public DbSet<Plugin> Plugin { get; set; }
        public DbSet<TenantPluginLink> TenantPluginLink { get; set; }
        
        protected DbContextOptions _options;

        public BaseAdminDbContext()
        { }

        public BaseAdminDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Plugin>()
                .HasAlternateKey(p => p.Name);
            modelBuilder.Entity<TenantPluginLink>()
                .HasAlternateKey(tpl => new { tpl.TenantId, tpl.PluginId });
            modelBuilder.HasPostgresExtension("uuid-ossp");
        }
    }
}