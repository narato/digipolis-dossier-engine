﻿using Digipolis.Common.Status.Models;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Microsoft.Extensions.Options;
using Moq;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using System;
using System.Linq;
using Xunit;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class SystemStatusDataTypeManagerTest
    {
        [Fact]
        public void TestWhenEverythingIsOK()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantKey()).Returns("meep");

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(dodpm => dodpm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(true));

            var optionsMock = new Mock<IOptions<SystemConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new SystemConfiguration { Name = "Dossier Engine DataType API" });

            var manager = new SystemStatusDataTypeManager(optionsMock.Object, currentTenantProviderMock.Object, dataTypeDataProviderMock.Object);

            var systemStatus = manager.GetStatusAsync().Result;

            Assert.True(systemStatus.Up);
            Assert.Equal(0, systemStatus.Components.Count);
            Assert.Equal("Dossier Engine DataType API", systemStatus.Name);
        }

        [Fact]
        public void TestDbConnectionIsNotOK()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantKey()).Returns("meep");

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(dodpm => dodpm.CheckDBConnectionAsync()).ReturnsAsync(new DbConnectionCheckResult(false));

            var optionsMock = new Mock<IOptions<SystemConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new SystemConfiguration());

            var manager = new SystemStatusDataTypeManager(optionsMock.Object, currentTenantProviderMock.Object, dataTypeDataProviderMock.Object);

            var systemStatus = manager.GetStatusAsync().Result;

            Assert.False(systemStatus.Up);
            Assert.Equal(1, systemStatus.Components.Count);
            var component = systemStatus.Components.First();
            Assert.Equal("Dossier Engine DataType tenant database", component.Name);
            Assert.Equal("The database used for tenant 'meep'", component.Description);
            Assert.False(component.Up);
            Assert.Equal("The DataType database is down!", component.MoreInfo);
        }

        [Fact]
        public void TestThrowsAggregateException()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantKey()).Returns("meep");

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(dodpm => dodpm.CheckDBConnectionAsync()).Throws(new AggregateException());

            var optionsMock = new Mock<IOptions<SystemConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new SystemConfiguration());

            var manager = new SystemStatusDataTypeManager(optionsMock.Object, currentTenantProviderMock.Object, dataTypeDataProviderMock.Object);

            var systemStatus = manager.GetStatusAsync().Result;

            Assert.False(systemStatus.Up);
            Assert.Equal(1, systemStatus.Components.Count);
            var component = systemStatus.Components.First();
            Assert.Equal("Dossier Engine Admin api", component.Name);
            Assert.Equal("The api used to retrieve the tenant config for tenantkey 'meep'", component.Description);
            Assert.False(component.Up);
            Assert.Equal("The admin API is down!", component.MoreInfo);
        }

        [Fact]
        public void TestThrowsExceptionWithFeedback()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantKey()).Returns("meep");

            var feedback = new FeedbackItem()
            {
                Type = FeedbackType.Error,
                Description = "test"
            };
            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(dodpm => dodpm.CheckDBConnectionAsync()).Throws(new ExceptionWithFeedback(feedback));

            var optionsMock = new Mock<IOptions<SystemConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new SystemConfiguration());

            var manager = new SystemStatusDataTypeManager(optionsMock.Object, currentTenantProviderMock.Object, dataTypeDataProviderMock.Object);

            var systemStatus = manager.GetStatusAsync().Result;

            Assert.False(systemStatus.Up);
            Assert.Equal(1, systemStatus.Components.Count);
            var component = systemStatus.Components.First();
            Assert.Equal("Dossier Engine Admin api", component.Name);
            Assert.Equal("The api used to retrieve the tenant config for tenantkey 'meep'", component.Description);
            Assert.False(component.Up);
            Assert.Equal("test", component.MoreInfo);
        }

        [Fact]
        public void TestThrowsUnauthorizedAccessException()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantKey()).Returns("meep");

            var dataTypeDataProviderMock = new Mock<IDataTypeDataProvider>();
            dataTypeDataProviderMock.Setup(dodpm => dodpm.CheckDBConnectionAsync()).Throws(new UnauthorizedAccessException());

            var optionsMock = new Mock<IOptions<SystemConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new SystemConfiguration());

            var manager = new SystemStatusDataTypeManager(optionsMock.Object, currentTenantProviderMock.Object, dataTypeDataProviderMock.Object);

            var systemStatus = manager.GetStatusAsync().Result;

            Assert.False(systemStatus.Up);
            Assert.Equal(1, systemStatus.Components.Count);
            var component = systemStatus.Components.First();
            Assert.Equal("Dossier Engine Admin api", component.Name);
            Assert.Equal("The api used to retrieve the tenant config for tenantkey 'meep'", component.Description);
            Assert.False(component.Up);
            Assert.Equal("Retrieveing the tenant for tenantkey 'meep' failed with an unauthorized acces exception. Did you use the correct tenantkey?", component.MoreInfo);
        }
    }
}
