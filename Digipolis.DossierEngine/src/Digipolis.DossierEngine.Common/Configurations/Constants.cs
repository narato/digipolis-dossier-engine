﻿
namespace Digipolis.DossierEngine.Common.Configurations
{
    public static class Constants
    {
        /**
         * Event Handler Engine Constants
         */
         // Admin constants
        public const string TENANT_CREATED_TOPIC = "Tenant.Created";
        public const string TENANT_UPDATED_TOPIC = "Tenant.Updated";
        public const string TENANT_DELETED_TOPIC = "Tenant.Deleted";

        // Tenant specific constants
        public const string DATATYPE_CREATED_TOPIC = "DataType.Created";
        public const string DATATYPE_UPDATED_TOPIC = "DataType.Updated";
        public const string DATATYPE_DELETED_TOPIC = "DataType.Deleted";
        public const string DATAOBJECT_CREATED_TOPIC = "DataObject.Created";
        public const string DATAOBJECT_UPDATED_TOPIC = "DataObject.Updated";
        public const string DATAOBJECT_DELETED_TOPIC = "DataObject.Deleted";

        public static string DataTypeCreatedTopic(string tenantKey) => tenantKey + "." + DATATYPE_CREATED_TOPIC;
        public static string DataTypeUpdatedTopic(string tenantKey) => tenantKey + "." + DATATYPE_UPDATED_TOPIC;
        public static string DataTypeDeletedTopic(string tenantKey) => tenantKey + "." + DATATYPE_DELETED_TOPIC;
        public static string DataObjectCreatedTopic(string tenantKey) => tenantKey + "." + DATAOBJECT_CREATED_TOPIC;
        public static string DataObjectUpdatedTopic(string tenantKey) => tenantKey + "." + DATAOBJECT_UPDATED_TOPIC;
        public static string DataObjectDeletedTopic(string tenantKey) => tenantKey + "." + DATAOBJECT_DELETED_TOPIC;
        /**
         * End Event Handler Engine Constants
         */
    }
}
