﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using System;
using Xunit;

namespace Digipolis.DossierEngine.Test.Providers
{
    public class CurrentTenantProviderTest
    {
        [Fact]
        public void BasicGetCurrentTenantTest()
        {
            var actionContext = new ActionContext();
            var contextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var headerDictionary = new HeaderDictionary();

            var returnTenant = new Tenant<Config>();

            headerDictionary.Add("Tenant-Key", "meep");
            requestMock.Setup(r => r.Headers).Returns(headerDictionary);
            contextMock.Setup(x => x.Request).Returns(requestMock.Object);
            actionContext.HttpContext = contextMock.Object;

            var actionContextAccessorMock = new Mock<IActionContextAccessor>();
            actionContextAccessorMock.SetupGet(acam => acam.ActionContext).Returns(actionContext);

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("meep")).ReturnsAsync(returnTenant);

            var currentTenantProvider = new CurrentTenantProvider(memoryCache, actionContextAccessorMock.Object, tenantClientMock.Object);

            var tenant = currentTenantProvider.GetCurrentTenant();

            Assert.NotNull(tenant);
            Assert.IsType(typeof(Tenant<Config>), tenant);

            tenantClientMock.Verify(tcm => tcm.GetTenantAsync("meep"), Times.Once);
        }

        [Fact]
        public void CallingGetCurrentTenantTwiceReturnsCachedTenantSecondTime()
        {
            var actionContext = new ActionContext();
            var contextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var headerDictionary = new HeaderDictionary();

            headerDictionary.Add("Tenant-Key", "meep");
            requestMock.Setup(r => r.Headers).Returns(headerDictionary);
            contextMock.Setup(x => x.Request).Returns(requestMock.Object);
            actionContext.HttpContext = contextMock.Object;

            var actionContextAccessorMock = new Mock<IActionContextAccessor>();
            actionContextAccessorMock.SetupGet(acam => acam.ActionContext).Returns(actionContext);

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("meep")).ReturnsAsync(new Tenant<Config>());

            var currentTenantProvider = new CurrentTenantProvider(memoryCache, actionContextAccessorMock.Object, tenantClientMock.Object);

            var firstTenant = currentTenantProvider.GetCurrentTenant();
            var secondTenant = currentTenantProvider.GetCurrentTenant();

            Assert.NotNull(firstTenant);
            Assert.IsType(typeof(Tenant<Config>), firstTenant);
            Assert.Equal(firstTenant, secondTenant);

            tenantClientMock.Verify(tcm => tcm.GetTenantAsync("meep"), Times.Once);
        }

        [Fact]
        public void CallingGetCurrentTenantTwiceWithForceCallsTenantClientEveryTime()
        {
            var actionContext = new ActionContext();
            var contextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var headerDictionary = new HeaderDictionary();

            headerDictionary.Add("Tenant-Key", "meep");
            requestMock.Setup(r => r.Headers).Returns(headerDictionary);
            contextMock.Setup(x => x.Request).Returns(requestMock.Object);
            actionContext.HttpContext = contextMock.Object;

            var actionContextAccessorMock = new Mock<IActionContextAccessor>();
            actionContextAccessorMock.SetupGet(acam => acam.ActionContext).Returns(actionContext);

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("meep")).ReturnsAsync(new Tenant<Config>());

            var currentTenantProvider = new CurrentTenantProvider(memoryCache, actionContextAccessorMock.Object, tenantClientMock.Object);

            var firstTenant = currentTenantProvider.GetCurrentTenant(true);
            var secondTenant = currentTenantProvider.GetCurrentTenant(true);

            Assert.NotNull(firstTenant);
            Assert.IsType(typeof(Tenant<Config>), firstTenant);
            Assert.Equal(firstTenant, secondTenant);

            tenantClientMock.Verify(tcm => tcm.GetTenantAsync("meep"), Times.Exactly(2));
        }

        [Fact]
        public void ExceptionShouldBeThrownWhenNoTenantKeyInRouteData()
        {
            var actionContext = new ActionContext();
            var contextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var headerDictionary = new HeaderDictionary();

            requestMock.Setup(r => r.Headers).Returns(headerDictionary);
            contextMock.Setup(x => x.Request).Returns(requestMock.Object);
            actionContext.HttpContext = contextMock.Object;

            var actionContextAccessorMock = new Mock<IActionContextAccessor>();
            actionContextAccessorMock.SetupGet(acam => acam.ActionContext).Returns(actionContext);

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var tenantClientMock = new Mock<ITenantClient>();
            tenantClientMock.Setup(tcm => tcm.GetTenantAsync("meep")).ReturnsAsync(new Tenant<Config>());

            var currentTenantProvider = new CurrentTenantProvider(memoryCache, actionContextAccessorMock.Object, tenantClientMock.Object);

            var ex = Assert.ThrowsAsync<Exception>(() => currentTenantProvider.GetCurrentTenantAsync()).Result;
            Assert.Equal("No tenantKey found in header.", ex.Message);
        }

        [Fact]
        public void BasicGetCurrentTenantKeyTest()
        {
            var actionContext = new ActionContext();
            var contextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var headerDictionary = new HeaderDictionary();

            headerDictionary.Add("Tenant-Key", "meep");
            requestMock.Setup(r => r.Headers).Returns(headerDictionary);
            contextMock.Setup(x => x.Request).Returns(requestMock.Object);
            actionContext.HttpContext = contextMock.Object;

            var actionContextAccessorMock = new Mock<IActionContextAccessor>();
            actionContextAccessorMock.SetupGet(acam => acam.ActionContext).Returns(actionContext);

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var tenantClientMock = new Mock<ITenantClient>();

            var currentTenantProvider = new CurrentTenantProvider(memoryCache, actionContextAccessorMock.Object, tenantClientMock.Object);

            var tenantKey = currentTenantProvider.GetCurrentTenantKey();

            Assert.NotNull(tenantKey);
            Assert.Equal("meep", tenantKey);
        }
    }
}
