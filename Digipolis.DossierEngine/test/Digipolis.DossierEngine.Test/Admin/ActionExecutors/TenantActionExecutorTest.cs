﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Admin.API.ActionExecutors;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Ssh.Interfaces;
using Digipolis.Tenant.Library.Managers;
using Microsoft.Extensions.Options;
using Moq;
using Renci.SshNet;
using Xunit;
using Digipolis.Common.Events;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Test.Admin.ActionExecutors
{
    public class TenantActionExecutorTest
    {
        [Fact]
        public async Task BeforeTenantCreateTestWithExistingTenant()
        {
            var tenantManagerMock = new Mock<ITenantManager<Config>>();
            tenantManagerMock.Setup(tm => tm.FindLastInsertedTenantAsync()).ReturnsAsync(new Tenant<Config>() { Config = new Config() { BoltPort = 7476 } });

            var sshClientProviderMock = new Mock<ISshClientProvider>();

            var ineoCommandRunnerMock = new Mock<IIneoCommandRunner>();

            var neoMigrationManagerMock = new Mock<INeoMigrationManager>();

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration());

            var eventPublisherMock = new Mock<IEventPublisher>();
            var eventHandlerAdministratorMock = new Mock<IEventHandlerAdministrator>();

            var tenantActionExecutor = new TenantActionExecutor(tenantManagerMock.Object, sshClientProviderMock.Object, ineoCommandRunnerMock.Object, configOptionsMock.Object, neoMigrationManagerMock.Object, eventHandlerAdministratorMock.Object, eventPublisherMock.Object);

            var tenant = new Tenant<Config>() { Config = new Config() };
            await tenantActionExecutor.BeforeTenantCreateAsync(tenant);

            Assert.Equal(7477, tenant.Config.HttpPort);
            Assert.Equal(7478, tenant.Config.HttpsPort);
            Assert.Equal(7479, tenant.Config.BoltPort);
        }

        [Fact]
        public async Task BeforeTenantCreateTestWithoutExistingTenant()
        {
            var dossierConfig = new DossierConfiguration()
            {
                DefaultHttpPort = 7474,
                DefaultHttpsPort = 7475,
                DefaultBoltPort = 7476
            };

            var tenantManagerMock = new Mock<ITenantManager<Config>>();
            tenantManagerMock.Setup(tm => tm.FindLastInsertedTenantAsync()).ReturnsAsync((Tenant<Config>)null);

            var sshClientProviderMock = new Mock<ISshClientProvider>();

            var ineoCommandRunnerMock = new Mock<IIneoCommandRunner>();

            var neoMigrationManagerMock = new Mock<INeoMigrationManager>();

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(dossierConfig);

            var eventPublisherMock = new Mock<IEventPublisher>();
            var eventHandlerAdministratorMock = new Mock<IEventHandlerAdministrator>();

            var tenantActionExecutor = new TenantActionExecutor(tenantManagerMock.Object, sshClientProviderMock.Object, ineoCommandRunnerMock.Object, configOptionsMock.Object, neoMigrationManagerMock.Object, eventHandlerAdministratorMock.Object, eventPublisherMock.Object);

            var tenant = new Tenant<Config>() { Config = new Config() };
            await tenantActionExecutor.BeforeTenantCreateAsync(tenant);

            Assert.Equal(7474, tenant.Config.HttpPort);
            Assert.Equal(7475, tenant.Config.HttpsPort);
            Assert.Equal(7476, tenant.Config.BoltPort);
        }

        [Fact]
        public async Task OnTenantCreatedBehaviourTest()
        {
            // ITenantManager<Config> tenantManager, ISshClientProvider sshClientProvider, IIneoCommandRunner ineoCommandRunner, IOptions<DossierConfiguration> adminConfiguration
            var config = new DossierConfiguration()
            {
                IneoHome = "~",
                Neo4jPassword = "notsafe"
            };

            var tenant = new Tenant<Config>()
            {
                Key = "MeepMoop",
                Config = new Config()
                {
                    Neo4JServer = "server",
                    SshPort = 22,
                    SshUsername = "username1",
                    SshPassword = "pass1",
                    HttpPort = 7474
                }
            };

            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(sc => sc.Connect());
            sshClientMock.Setup(sc => sc.Disconnect());

            var sshClientProviderMock = new Mock<ISshClientProvider>();
            sshClientProviderMock.Setup(scp => scp.GetSshClient(tenant.Config.Neo4JServer, tenant.Config.SshPort, tenant.Config.SshUsername, tenant.Config.SshPassword)).Returns(sshClientMock.Object);

            var neoMigrationManagerMock = new Mock<INeoMigrationManager>();

            var ineoCommandRunnerMock = new Mock<IIneoCommandRunner>();

            ineoCommandRunnerMock.Setup(icr => icr.CreateNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Config.HttpPort, tenant.Key)).Returns((SshCommand)null);
            ineoCommandRunnerMock.Setup(icr => icr.StartNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Key)).Returns((SshCommand)null);
            ineoCommandRunnerMock.Setup(icr => icr.SetPasswordForNeo4jInstance(sshClientMock.Object, tenant.Config.HttpPort, config.Neo4jPassword)).Returns((SshCommand)null);

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(config);

            var eventPublisherMock = new Mock<IEventPublisher>();
            var eventHandlerAdministratorMock = new Mock<IEventHandlerAdministrator>();

            var tenantActionExecutor = new TenantActionExecutor(tenantManagerMock.Object, sshClientProviderMock.Object, ineoCommandRunnerMock.Object, configOptionsMock.Object, neoMigrationManagerMock.Object, eventHandlerAdministratorMock.Object, eventPublisherMock.Object);

            await tenantActionExecutor.OnTenantCreatedAsync(tenant);

            sshClientProviderMock.Verify(x => x.GetSshClient(tenant.Config.Neo4JServer, tenant.Config.SshPort, tenant.Config.SshUsername, tenant.Config.SshPassword), Times.Once);
            ineoCommandRunnerMock.Verify(x => x.CreateNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Config.HttpPort, tenant.Key), Times.Once);
            ineoCommandRunnerMock.Verify(x => x.StartNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Key), Times.Once);
            ineoCommandRunnerMock.Verify(x => x.SetPasswordForNeo4jInstance(sshClientMock.Object, tenant.Config.HttpPort, config.Neo4jPassword), Times.Once);
            neoMigrationManagerMock.Verify(x => x.SetupDatabase(tenant), Times.Once);

            sshClientMock.Verify(x => x.Connect(), Times.Once);
            sshClientMock.Verify(x => x.Disconnect(), Times.Once);
        }

        [Fact]
        public async Task OnTenantDeletedBehaviourTest()
        {
            var config = new DossierConfiguration()
            {
                IneoHome = "~",
                Neo4jPassword = "notsafe"
            };

            var tenant = new Tenant<Config>()
            {
                Key = "MeepMoop",
                Config = new Config()
                {
                    Neo4JServer = "server",
                    SshPort = 22,
                    SshUsername = "username1",
                    SshPassword = "pass1",
                    HttpPort = 7474
                }
            };

            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var sshClientMock = new Mock<ISshClient>();
            sshClientMock.Setup(sc => sc.Connect());
            sshClientMock.Setup(sc => sc.Disconnect());

            var sshClientProviderMock = new Mock<ISshClientProvider>();
            sshClientProviderMock.Setup(scp => scp.GetSshClient(tenant.Config.Neo4JServer, tenant.Config.SshPort, tenant.Config.SshUsername, tenant.Config.SshPassword)).Returns(sshClientMock.Object);

            var neoMigrationManagerMock = new Mock<INeoMigrationManager>();

            var ineoCommandRunnerMock = new Mock<IIneoCommandRunner>();
            ineoCommandRunnerMock.Setup(icr => icr.StopNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Key)).Returns((SshCommand)null);

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(config);

            var eventPublisherMock = new Mock<IEventPublisher>();
            var eventHandlerAdministratorMock = new Mock<IEventHandlerAdministrator>();

            var tenantActionExecutor = new TenantActionExecutor(tenantManagerMock.Object, sshClientProviderMock.Object, ineoCommandRunnerMock.Object, configOptionsMock.Object, neoMigrationManagerMock.Object, eventHandlerAdministratorMock.Object, eventPublisherMock.Object);

            await tenantActionExecutor.OnTenantDeletedAsync(tenant);

            sshClientProviderMock.Verify(x => x.GetSshClient(tenant.Config.Neo4JServer, tenant.Config.SshPort, tenant.Config.SshUsername, tenant.Config.SshPassword), Times.Once);
            ineoCommandRunnerMock.Verify(x => x.StopNeo4jInstance(sshClientMock.Object, config.IneoHome, tenant.Key), Times.Once);

            sshClientMock.Verify(x => x.Connect(), Times.Once);
            sshClientMock.Verify(x => x.Disconnect(), Times.Once);
        }
    }
}
