﻿namespace Digipolis.DossierEngine.Model.Interfaces
{
    public interface IAttributeType
    {
        string Type { get; }

        string Name { get; set; }

        bool? IsRequired { get; set; }

        object DefaultValue { get; set; }

        bool? IsDeleted { get; set; }

        bool? IsCollection { get; set; }
    }
}
