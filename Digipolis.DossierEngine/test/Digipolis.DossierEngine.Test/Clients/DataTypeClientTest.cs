﻿using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Pooling;
using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.DossierEngine.Domain.Clients;
using Digipolis.DossierEngine.Test.Stubs;
using Microsoft.Extensions.Options;
using Moq;
using Narato.Common.Exceptions;
using Narato.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace Digipolis.DossierEngine.Test.Clients
{
    // TODO: once TenantClient is moved to Digipolis.Common, this can be deleted
    public class DataTypeClientTest
    {
        [Fact]
        public void GetDataTypeReturnsADataType()
        {
            var dataType = new Model.Models.DataType();
            var id = Guid.NewGuid();
            var key = "TenantKey";
            dataType.Id = id;
            dataType.Name = "myDataType";

            var msgHandler = new Mock<HttpMessageHandlerStub>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<HttpClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(httpClient);
            var clientPoolMock = new Mock<IGroupedLimitedPool<string, HttpClient>>();
            clientPoolMock.Setup(cpm => cpm.Get("http://test/api/")).Returns(poolItemMock.Object);

            var optionsMock = new Mock<IOptions<DossierConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new DossierConfiguration { DataTypeApiEndpoint = "http://test/api/" });

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<Model.Models.DataType>() { Data = dataType }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/datatypes/{id}")))
                    .Returns(httpResponseMessage);

            var dataTypeClient = new DataTypeClient(clientPoolMock.Object, optionsMock.Object);
            var dataTypeOut = dataTypeClient.GetDataTypeById(id, key);

            Assert.NotNull(dataTypeOut);
            Assert.Equal(dataType.Name, "myDataType");
        }

        [Fact]
        public void DataTypeClientThrowsExceptionWithFeedbackWhenDataTypeAPIReturnsBadRequest()
        {
            var dataType = new Model.Models.DataType();
            var id = Guid.NewGuid();
            var key = "TenantKey";
            dataType.Id = id;
            dataType.Name = "myDataType";

            var msgHandler = new Mock<HttpMessageHandlerStub>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<HttpClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(httpClient);
            var clientPoolMock = new Mock<IGroupedLimitedPool<string, HttpClient>>();
            clientPoolMock.Setup(cpm => cpm.Get("http://test/api/")).Returns(poolItemMock.Object);

            var optionsMock = new Mock<IOptions<DossierConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new DossierConfiguration { DataTypeApiEndpoint = "http://test/api/" });

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest);
            httpResponseMessage.Content = new StringContent("");
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(new Response<Model.Models.DataType>() { Feedback = new List<FeedbackItem> { new FeedbackItem() { Description = "feedback" } } }));
            httpClient.BaseAddress = new Uri("http://test/api/");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/datatypes/{id}")))
                    .Returns(httpResponseMessage);

            var dataTypeClient = new DataTypeClient(clientPoolMock.Object, optionsMock.Object);

            ExceptionWithFeedback ex = Assert.ThrowsAsync<ExceptionWithFeedback>(() => dataTypeClient.GetDataTypeById(id, key)).Result;
        }

        [Fact]
        public void DataTypeClientThrowsEntityNotFoundExceptionWhenDataTypeAPIReturnsNotFound()
        {
            var dataType = new Model.Models.DataType();
            var id = Guid.NewGuid();
            var key = "TenantKey";
            dataType.Id = id;
            dataType.Name = "myDataType";

            var msgHandler = new Mock<HttpMessageHandlerStub>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/api/");

            var poolItemMock = new Mock<ILimitedPoolItem<HttpClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(httpClient);
            var clientPoolMock = new Mock<IGroupedLimitedPool<string, HttpClient>>();
            clientPoolMock.Setup(cpm => cpm.Get("http://test/api/")).Returns(poolItemMock.Object);

            var optionsMock = new Mock<IOptions<DossierConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new DossierConfiguration { DataTypeApiEndpoint = "http://test/api/" });

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            httpResponseMessage.Content = new StringContent("");

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg =>
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/datatypes/{id}")))
                    .Returns(httpResponseMessage);

            var dataTypeClient = new DataTypeClient(clientPoolMock.Object, optionsMock.Object);

            EntityNotFoundException ex = Assert.ThrowsAsync<EntityNotFoundException>(() => dataTypeClient.GetDataTypeById(id, key)).Result;
        }

        [Fact]
        public void DataTypeClientThrowsExceptionWithFeedbackWhenDataTypeAPIReturnsOtherStatusCode()
        {
            var dataType = new Model.Models.DataType();
            var id = Guid.NewGuid();
            var key = "TenantKey";
            dataType.Id = id;
            dataType.Name = "myDataType";

            var msgHandler = new Mock<HttpMessageHandlerStub>() { CallBase = true };
            var httpClient = new HttpClient(msgHandler.Object);
            httpClient.BaseAddress = new Uri("http://test/api/");

            var poolItemMock = new Mock<ILimitedPoolItem<HttpClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(httpClient);
            var clientPoolMock = new Mock<IGroupedLimitedPool<string, HttpClient>>();
            clientPoolMock.Setup(cpm => cpm.Get("http://test/api/")).Returns(poolItemMock.Object);

            var optionsMock = new Mock<IOptions<DossierConfiguration>>();
            optionsMock.SetupGet(om => om.Value).Returns(new DossierConfiguration { DataTypeApiEndpoint = "http://test/api/" });

            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.NoContent);
            var content = new Response(FeedbackItem.CreateInfoFeedbackItem("meep"), "moop", 418);
            httpResponseMessage.Content = new StringContent(JsonConvert.SerializeObject(content));

            msgHandler.Setup(t => t.Send(It.Is<HttpRequestMessage>(
            msg => 
                    msg.Method == HttpMethod.Get &&
                    msg.RequestUri.ToString() == $"http://test/api/datatypes/{id}")))
                    .Returns(httpResponseMessage);

            var dataTypeClient = new DataTypeClient(clientPoolMock.Object, optionsMock.Object);

            ExceptionWithFeedback ex = Assert.ThrowsAsync<ExceptionWithFeedback>(() => dataTypeClient.GetDataTypeById(id, key)).Result;

            Assert.Equal(ex.Feedback[0].Description, "meep");
        }
    }
}
