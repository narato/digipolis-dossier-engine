﻿using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.DossierEngine.Common.Util;
using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Helpers;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Digipolis.DossierEngine.DataProvider.RawModels;
using Digipolis.DossierEngine.Model.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Moq;
using Narato.Common.Exceptions;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.DossierEngine.Test.DataProviders
{
    public class DataObjectDataProviderTest
    {
        [Fact]
        public void GetDataObjectByIdReturnsDataObject()
        {
            var guid = new Guid();

            var cypherMock = new Mock<ICypherFluentQuery<RawDataObject>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.OptionalMatch(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Union()).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.UsingIndex(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.AndWhere(It.IsAny<Expression<Func<NeoDataObject, bool>>>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.AndWhere(It.IsAny<string>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return(It.IsAny<Expression<Func<ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, RawDataObject>>>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(new List<RawDataObject>());

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataObjectList = new List<Model.Models.DataObject>();
            dataObjectList.Add(new Model.Models.DataObject());
            var dataObjectMapperMock = new Mock<IDataObjectMapper>();
            dataObjectMapperMock.Setup(domm => domm.Map(It.IsAny<IEnumerable<RawDataObject>>())).Returns(dataObjectList);

            var queryProviderMock = new Mock<IDataObjectQueryProvider>();

            var dataProvider = new DataObjectDataProvider(pooledGraphClientProviderMock.Object, dataObjectMapperMock.Object, new StringHelper(), new NestedDataObjectHelper(), queryProviderMock.Object);

            var returnDataObject = dataProvider.GetDataObjectByIdAsync(guid).Result;

            Assert.NotNull(returnDataObject);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            cypherMock.Verify(x => x.Union(), Times.Once, "Fetching a DataObject should be composed out of 2 queries that are unioned");

            dataObjectMapperMock.Verify(x => x.Map(It.IsAny<IEnumerable<RawDataObject>>()), Times.Once, "RawDataObject list should be mapped... Once");
        }

        [Fact]
        public void InsertSilentFailShouldThrowException()
        {
            var dataObject = new Model.Models.DataObject()
            {
                Id = Guid.NewGuid(),
                Values = new Dictionary<string, object>()
            };
            dataObject.Values.Add("test", 5);

            var dataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };

            dataType.Attributes.Add(new IntAttributeType() { Name = "test" });

            var cypherMock = new Mock<ICypherFluentQuery<NeoDataObject>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Create(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);
            cypherMock.SetupGet(cm => cm.Query).Returns(new CypherQuery("test", null, CypherResultMode.Projection));

            cypherMock.Setup(cm => cm.Return<NeoDataObject>(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(new List<NeoDataObject>());

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataObjectMapperMock = new Mock<IDataObjectMapper>();

            var stringHelperMock = new Mock<StringHelper>();
            stringHelperMock.Setup(shm => shm.RemoveSpecialCharacters(It.IsAny<string>())).Returns<string>(x => x);
            stringHelperMock.Setup(shm => shm.ConcatenateWithDelimiter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<char>())).Returns("meep");

            var queryProviderMock = new Mock<IDataObjectQueryProvider>();
            var dataProvider = new DataObjectDataProvider(pooledGraphClientProviderMock.Object, dataObjectMapperMock.Object, stringHelperMock.Object, new NestedDataObjectHelper(), queryProviderMock.Object);

            var ex = Assert.ThrowsAsync<ExceptionWithFeedback>(() => dataProvider.InsertDataObjectForDataTypeAsync(dataObject, dataType)).Result;

            Assert.Equal("Insertion of the DataObject failed. Probably the timeseries is not complete enough. Contact support.", ex.Feedback.First().Description);
        }

        [Fact]
        public void InsertDataObjectForSimpleDataTypeWithDateTimeAttribute()
        {
            var dataObject = new Model.Models.DataObject()
            {
                Id = Guid.NewGuid(),
                Values = new Dictionary<string, object>()
            };
            dataObject.Values.Add("test", 5);
            dataObject.Values.Add("date", "2014-04-21T10:30:50Z");

            var dataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };

            var queryReturnValue = new List<NeoDataObject>();
            queryReturnValue.Add(new NeoDataObject());

            dataType.Attributes.Add(new IntAttributeType() { Name = "test" });
            dataType.Attributes.Add(new DateTimeAttributeType() { Name = "date" });

            var cypherMock = new Mock<ICypherFluentQuery<NeoDataObject>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Create(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return<NeoDataObject>(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(queryReturnValue);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataObjectMapperMock = new Mock<IDataObjectMapper>();

            var stringHelperMock = new Mock<StringHelper>();
            stringHelperMock.Setup(shm => shm.RemoveSpecialCharacters(It.IsAny<string>())).Returns<string>(x => x);
            stringHelperMock.Setup(shm => shm.ConcatenateWithDelimiter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<char>())).Returns("meep");

            var queryProviderMock = new Mock<IDataObjectQueryProvider>();
            var dataProvider = new DataObjectDataProvider(pooledGraphClientProviderMock.Object, dataObjectMapperMock.Object, stringHelperMock.Object, new NestedDataObjectHelper(), queryProviderMock.Object);

            var returnDataObject = dataProvider.InsertDataObjectForDataTypeAsync(dataObject, dataType).Result;

            Assert.NotNull(returnDataObject);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            cypherMock.Verify(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<NeoDataObject>()), Times.Once, "One and only one DataObject should be inserted for a simple, non nested dataObject");
        }

        [Fact]
        public void InsertDataObjectShouldThrowExceptionWhenDataTypeTreeIsNotHydrated()
        {
            var dataObject = new Model.Models.DataObject()
            {
                Id = Guid.NewGuid(),
                Values = new Dictionary<string, object>()
            };
            dataObject.Values.Add("test", "meep");

            var dataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };

            var queryReturnValue = new List<NeoDataObject>();
            queryReturnValue.Add(new NeoDataObject());

            dataType.Attributes.Add(new LinkDataTypeAttributeType() { Name = "test" });

            var cypherMock = new Mock<ICypherFluentQuery<NeoDataObject>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Create(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return<NeoDataObject>(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(queryReturnValue);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataObjectMapperMock = new Mock<IDataObjectMapper>();

            var stringHelperMock = new Mock<StringHelper>();
            stringHelperMock.Setup(shm => shm.RemoveSpecialCharacters(It.IsAny<string>())).Returns<string>(x => x);
            stringHelperMock.Setup(shm => shm.ConcatenateWithDelimiter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<char>())).Returns("meep");

            var queryProviderMock = new Mock<IDataObjectQueryProvider>();
            var dataProvider = new DataObjectDataProvider(pooledGraphClientProviderMock.Object, dataObjectMapperMock.Object, stringHelperMock.Object, new NestedDataObjectHelper(), queryProviderMock.Object);

            var ex = Assert.ThrowsAsync<ArgumentException>(() => dataProvider.InsertDataObjectForDataTypeAsync(dataObject, dataType));

            Assert.Equal("DataType has to be fetched with the tree of nested datatypes.", ex.Result.Message);
        }

        [Fact]
        public void InsertDataObjectWithNestingShouldWorkProperly()
        {
            var dataObject = new Model.Models.DataObject()
            {
                Id = Guid.NewGuid(),
                Values = new Dictionary<string, object>()
            };

            dataObject.Values.Add("nesting", new { });

            var dataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };
            var nestedDataType = new Model.Models.DataType()
            {
                Id = Guid.NewGuid(),
                Attributes = new List<IAttributeType>()
            };

            var queryReturnValue = new List<NeoDataObject>();
            queryReturnValue.Add(new NeoDataObject());

            dataType.Attributes.Add(new LinkDataTypeAttributeType() { Name = "nesting", DataType = nestedDataType });

            var cypherMock = new Mock<ICypherFluentQuery<NeoDataObject>>(MockBehavior.Strict);
            cypherMock.Setup(cm => cm.Planner(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Match(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.With(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Create(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.Where(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<object>())).Returns(cypherMock.Object);

            cypherMock.Setup(cm => cm.Return<NeoDataObject>(It.IsAny<string>())).Returns(cypherMock.Object);
            cypherMock.Setup(cm => cm.ResultsAsync).ReturnsAsync(queryReturnValue);

            var graphClientMock = new Mock<IGraphClient>();
            graphClientMock.Setup(gcm => gcm.Connect(null));
            graphClientMock.SetupGet(gcm => gcm.Cypher).Returns(cypherMock.Object);

            var poolItemMock = new Mock<ILimitedPoolItem<IGraphClient>>();
            poolItemMock.SetupGet(pim => pim.Value).Returns(graphClientMock.Object);
            var pooledGraphClientProviderMock = new Mock<IPooledGraphClientProvider>();
            pooledGraphClientProviderMock.Setup(tgcfm => tgcfm.GetAsync()).ReturnsAsync(poolItemMock.Object);

            var dataObjectMapperMock = new Mock<IDataObjectMapper>();

            var stringHelperMock = new Mock<StringHelper>();
            stringHelperMock.Setup(shm => shm.RemoveSpecialCharacters(It.IsAny<string>())).Returns<string>(x => x);
            stringHelperMock.Setup(shm => shm.ConcatenateWithDelimiter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<char>())).Returns("meep");

            var queryProviderMock = new Mock<IDataObjectQueryProvider>();
            var dataProvider = new DataObjectDataProvider(pooledGraphClientProviderMock.Object, dataObjectMapperMock.Object, stringHelperMock.Object, new NestedDataObjectHelper(), queryProviderMock.Object);

            var returnDataObject = dataProvider.InsertDataObjectForDataTypeAsync(dataObject, dataType).Result;

            Assert.NotNull(returnDataObject);

            pooledGraphClientProviderMock.Verify(x => x.GetAsync(), Times.Once, "graphClient should only be created once");
            graphClientMock.Verify(x => x.Connect(null), Times.Once, "graphClient should call Connect");
            graphClientMock.VerifyGet(x => x.Cypher, Times.Once, "Only one Cypher query should be run");

            cypherMock.Verify(cm => cm.WithParam(It.IsAny<string>(), It.IsAny<NeoDataObject>()), Times.Exactly(2), "One dataObject should be created for the Parent, and one for the Child");
        }
    }
}
