﻿using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface ISyncedDataObjectDataProvider
    {
        Task<List<DataObject>> FindSyncedDataObjectsByExternalIdAsync(string externalId);
    }
}
