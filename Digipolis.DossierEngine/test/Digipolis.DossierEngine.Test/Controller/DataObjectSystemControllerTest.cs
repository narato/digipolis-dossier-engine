﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Digipolis.DossierEngine.DataObject.API.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Digipolis.DossierEngine.Test.Controller
{
    public class DataObjectSystemControllerTest
    {
        [Fact]
        public void GetReturnsAnActionResult()
        {
            var systemStatusMock = new Mock<ISystemStatusManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateGetResponseAsync(It.IsAny<Func<Task<SystemStatus>>>(), It.IsAny<string>())).ReturnsAsync(new ObjectResult(null));

            var controller = new SystemController(systemStatusMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.Get("tentanShortName").Result;

            Assert.NotNull(actionResult);
        }

        [Fact]
        public void GetWithMissingTenantIdReturnsBadRequest()
        {
            var systemStatusMock = new Mock<ISystemStatusManager>();
            var factoryMock = new Mock<IResponseFactory>();

            factoryMock.Setup(fm => fm.CreateMissingParam(It.IsAny<MissingParam>())).Returns(new BadRequestObjectResult(new object()));

            var controller = new SystemController(systemStatusMock.Object, factoryMock.Object);

            IActionResult actionResult = controller.Get(string.Empty).Result;

            Assert.NotNull(actionResult);
            Assert.IsType(typeof(BadRequestObjectResult), actionResult);
        }
    }
}