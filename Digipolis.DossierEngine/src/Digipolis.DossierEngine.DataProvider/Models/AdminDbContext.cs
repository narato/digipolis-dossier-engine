﻿using Digipolis.Tenant.Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public partial class AdminDbContext : BaseAdminDbContext
    {
        private string DatabaseName = "Admin";
        private TenantConfiguration _tenantConfiguration;

        public AdminDbContext()
        {
            //Only used to migrate database in development
            _tenantConfiguration = new TenantConfiguration() { ConnectionString = "Server = 192.168.99.100; Port = 5432; Database = Admin; User Id = postgres; Password = narato;" };
        }

        public AdminDbContext(TenantConfiguration tenantConfiguration)
        { 
            _tenantConfiguration = tenantConfiguration;
        }

        public AdminDbContext(IOptions<TenantConfiguration> tenanConfiguration)
        {
            _tenantConfiguration = tenanConfiguration.Value;
        }

        public AdminDbContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_options == null)
            {
                optionsBuilder.UseNpgsql(_tenantConfiguration.ConnectionString);
            }
        }
    }
}