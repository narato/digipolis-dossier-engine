﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Digipolis.DossierEngine.Model.Dto
{
    public class DataObjectDTO
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public Guid DataTypeId { get; set; }

        public Dictionary<string, object> Values { get; set; }
    }
}
