cd $env:USERPROFILE\Source\Repos\digipolis-dossier-engine\Digipolis.DossierEngine\test\Digipolis.DossierEngine.Test
&"$env:USERPROFILE\.nuget\packages\OpenCover\4.6.519\tools\OpenCover.Console.exe" "-target:${env:programfiles}\dotnet\dotnet.exe" "-targetargs: test" "-register:User" "-filter:+[Digipolis.DossierEngine*]* -[*.Test]* -[Digipolis.DossierEngine*]*.Program -[Digipolis.DossierEngine.Admin.API]*TenantController -[Digipolis.DossierEngine.Admin.API]*AdminSystemController -[Digipolis.DossierEngine.DataProvider]Digipolis.DossierEngine.DataProvider.Migrations.* -[Digipolis.DossierEngine*]*Startup* -[Digipolis.DossierEngine.DataProvider]*.QueryProviders.DataObjectQueryProvider" "-output:Unit_Test_Coverage_Report.xml" -oldStyle


&"$env:USERPROFILE\.nuget\packages\ReportGenerator\2.4.5\tools\ReportGenerator.exe" -reports:"$env:USERPROFILE\Source\Repos\digipolis-dossier-engine\Digipolis.DossierEngine\test\Digipolis.DossierEngine.Test\Unit_Test_Coverage_Report.xml" -targetdir:"$env:USERPROFILE\Source\Repos\digipolis-dossier-engine\Digipolis.DossierEngine\UnitTestCoverage_Report"

cd $env:USERPROFILE\Source\Repos\digipolis-dossier-engine\Digipolis.DossierEngine\