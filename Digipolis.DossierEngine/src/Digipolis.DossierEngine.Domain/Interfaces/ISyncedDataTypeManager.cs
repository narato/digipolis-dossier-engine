﻿using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ISyncedDataTypeManager
    {
        List<List<DataType>> GetAllSynchDataTypes();
    }
}
