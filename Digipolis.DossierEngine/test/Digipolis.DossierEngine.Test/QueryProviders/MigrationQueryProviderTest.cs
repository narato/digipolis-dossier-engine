﻿using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Xunit;

namespace Digipolis.DossierEngine.Test.QueryProviders
{
    public class MigrationQueryProviderTest
    {
        [Fact]
        public void GetCreateDbQueryReturnsACreateDatabaseStatement()
        {
            var dbName = "testName";
            IMigrationQueryProvider queryProvider = new MigrationQueryProvider();
            var statement = queryProvider.GetCreateDbQuery(dbName);
            Assert.Equal($"CREATE DATABASE \"{dbName}\" ENCODING = 'UTF8'", statement);
        }

        [Fact]
        public void GetDbExistsQueryReturnsASelectDbStatement()
        {
            var dbName = "testName";
            IMigrationQueryProvider queryProvider = new MigrationQueryProvider();
            var statement = queryProvider.GetDbExistsQuery(dbName);
            Assert.Equal($"SELECT 1 FROM pg_database WHERE datname='{dbName}'", statement);
        }
    }
}
