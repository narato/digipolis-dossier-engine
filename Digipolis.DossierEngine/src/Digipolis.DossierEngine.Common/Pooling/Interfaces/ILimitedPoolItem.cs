﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Common.Pooling.Interfaces
{
    public interface ILimitedPoolItem<T> : IDisposable
    {
        T Value { get; }
    }
}
