﻿using Digipolis.DossierEngine.Domain.Ssh.Interfaces;
using Renci.SshNet;

namespace Digipolis.DossierEngine.Domain.Ssh
{
    // We had to do this so that the SshClient was mockable.... quite important dontchathink? :-)
    public class IneoSshClient : SshClient, ISshClient
    {
        public IneoSshClient(string host, int port, string username, string password) : base(host, port, username, password) { }
    }
}
