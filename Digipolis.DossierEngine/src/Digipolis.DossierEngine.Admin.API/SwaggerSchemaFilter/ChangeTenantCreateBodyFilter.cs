using System.Linq;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Admin.API.SwaggerSchemaFilter
{
    public class ChangeTenantDocumentationFilter : IDocumentFilter
    {
        void IDocumentFilter.Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            // Replace generic config object with the Config object of Dossier Engine
            swaggerDoc.Definitions["Tenant[Object]"].Properties["config"] = swaggerDoc.Definitions["Tenant[Config]"].Properties["config"];

            // remove generic Tenant[Config]
            swaggerDoc.Definitions.Remove("Tenant[Config]");

            // Update request body model of POST /api/tenants
            BodyParameter param = (BodyParameter) swaggerDoc.Paths["/api/tenants"].Post.Parameters.Where(x => x.In == "body" && x.Name == "tenant").First();
            param.Required = true;
            Schema schema = param.Schema;
            schema.Properties = swaggerDoc.Definitions["Tenant[Object]"].Properties.Where(x => x.Key == "name" || x.Key == "description").ToDictionary(x => x.Key, x => x.Value);
            schema.Ref = null;
            schema.Type = "object";
        }
    }
}