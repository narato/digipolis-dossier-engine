﻿using System;
using System.Collections.Generic;
using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Search.Conditions.Operators;
using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using System.Globalization;

namespace Digipolis.DossierEngine.Domain.Search.Cypher
{
    public class CypherQueryBuilder : ICypherQueryBuilder
    {
        //entry point to be used in other business logic. The specific methods below this are to be used internally
        public string Visit(ICondition condition)
        {
            return condition.visit(this);
        }

        public string WalkComparison(IComparison condition, bool partOfComposite)
        {
            var leftHandSide = EvaluateComparable(condition.LeftHandSide);
            var rightHandSide = EvaluateComparable(condition.RightHandSide);

            var prefix = partOfComposite ? $"x.type = {leftHandSide} AND " : "";
            var variableName = partOfComposite ? "x" : "av"; // in a compositeCondition we use "x in typeValuePairList". in a simple comparison, we work right on the atributeValue

            switch (condition.Operator.Value)
            {
                // return $"(at.name = {leftHandSide} AND av.value = {rightHandSide})";
                case Operator.EQUAL:
                    return $"({prefix}{variableName}.value = {rightHandSide})";
                case Operator.NOT_EQUAL:
                    return $"({prefix}{variableName}.value <> {rightHandSide})";
                case Operator.LESS_THAN:
                    return $"({prefix}{variableName}.value < {rightHandSide})";
                case Operator.LESS_EQUAL:
                    return $"({prefix}{variableName}.value <= {rightHandSide})";
                case Operator.GREATER_THAN:
                    return $"({prefix}{variableName}.value > {rightHandSide})";
                case Operator.GREATER_EQUAL:
                    return $"({prefix}{variableName}.value >= {rightHandSide})";
                case Operator.CONTAINS:
                    rightHandSide = rightHandSide.Insert(1, ".*").Insert(rightHandSide.Length + 1, ".*"); // +1 because it's lenght - 1 + 2 of the characters we just inserted
                    return $"({prefix}{variableName}.value =~ {rightHandSide})";

                default:
                    throw new Exception($"Unknown operator: {condition.Operator.Value}");
            }
        }

        public string WalkCompositeCondition(CompositeCondition condition)
        {
            switch (condition.Operator)
            {
                case CompositeCondition.TYPE_AND:
                    return WalkAndCompositeCondition(condition);
                case CompositeCondition.TYPE_OR:
                    return WalkOrCompositeCondition(condition);
                default:
                    throw new Exception($"Invalid composite operator: {condition.Operator}");
            }
        }

        private string WalkAndCompositeCondition(CompositeCondition condition)
        {
            var childComparisons = new List<string>();
            var nestedConditions = new List<string>();
            nestedConditions.Add("1 = 1"); // for fluent purposes 
            foreach (var child in condition.Conditions)
            {
                if (child is CompositeCondition)
                {
                    nestedConditions.Add(child.visit(this));
                } else if (child is Comparison)
                {
                    childComparisons.Add(((Comparison)child).visit(this, true));
                } else
                {
                    throw new Exception("unknown type of condition: " + child.GetType().Name);
                }
                
            }
            return "(SIZE(FILTER(x in typeValuePairList WHERE " + string.Join(" OR ", childComparisons) + ")) = " + childComparisons.Count + " AND " + string.Join(" AND ", nestedConditions) + ")";
        }

        private string WalkOrCompositeCondition(CompositeCondition condition)
        {
            var childComparisons = new List<string>();
            var nestedConditions = new List<string>();
            nestedConditions.Add("1 = 0"); // for fluent purposes 
            foreach (var child in condition.Conditions)
            {
                if (child is CompositeCondition)
                {
                    nestedConditions.Add(child.visit(this));
                }
                else if (child is Comparison)
                {
                    childComparisons.Add(((Comparison)child).visit(this, true));
                }
                else
                {
                    throw new Exception("unknown type of condition: " + child.GetType().Name);
                }

            }
            return "(SIZE(FILTER(x in typeValuePairList WHERE " + string.Join(" OR ", childComparisons) + ")) > 0 OR " + string.Join(" OR ", nestedConditions) + ")";
        }

        private string EvaluateComparable(Comparable comparable)
        {
            if (comparable is Field)
            {
                // extract to seperate class?
                return "\"" + ((Field)comparable).Name + "\"";
            }
            if (comparable is IValue<int>)
            {
                return ((IValue<int>)comparable).Value.ToString();
            }
            if (comparable is IValue<double>)
            {
                return ((IValue<double>)comparable).Value.ToString(CultureInfo.InvariantCulture);
            }
            if (comparable is IValue<bool>)
            {
                return ((IValue<bool>)comparable).Value.ToString();
            }
            if (comparable is IValue<string>)
            {
                return "\"" + ((IValue<string>)comparable).Value + "\"";
            }

            throw new Exception("Unknown comparable " + comparable.GetType().Name);
        }
    }
}
