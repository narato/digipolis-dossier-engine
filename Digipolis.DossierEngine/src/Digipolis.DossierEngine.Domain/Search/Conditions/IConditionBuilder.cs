﻿
namespace Digipolis.DossierEngine.Domain.Search.Conditions
{
    public interface IConditionBuilder
    {
        ICondition BuildConditionFromJsonObject(dynamic jsonObject);
    }
}
