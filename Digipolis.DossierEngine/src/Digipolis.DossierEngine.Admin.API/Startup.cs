﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Narato.Common.ActionFilters;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.Common.Events.Config;
using Digipolis.Common.Tenancy.Mappers;
using Digipolis.DossierEngine.Admin.API.ActionExecutors;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Domain.Mappers;
using Digipolis.Tenant.Library.ActionExecutors;
using Digipolis.Tenant.Library.DataProviders;
using Digipolis.Tenant.Library.Managers;
using Digipolis.Tenant.Library.Models;
using Narato.Common.Factory;
using Narato.Common.Exceptions;
using Narato.Common.Interfaces;
using Digipolis.Common.Status.Interfaces;
using Digipolis.Tenant.Library.Tenancy;
using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Digipolis.DossierEngine.Domain.Ssh;
using Digipolis.DossierEngine.Domain.Providers;
using Digipolis.DossierEngine.Domain.Factories;
using Digipolis.Common.Events;
using System;
using System.Net.Http;
using Digipolis.DossierEngine.DataProvider.Models;
using System.Runtime.Loader;
using Microsoft.Extensions.DependencyModel;
using System.Linq;
using Digipolis.DossierEngine.Synchronization;
using Digipolis.DossierEngine.Domain.Clients;
using Microsoft.Extensions.DependencyInjection;
using Digipolis.DossierEngine.Common.OperationFilters;
using Microsoft.Extensions.PlatformAbstractions;
using System.Collections.Generic;
using Digipolis.DossierEngine.Domain.Validators;
using Digipolis.Common.Tenancy.Providers;
using Narato.Common.Checksum;
using Swashbuckle.Swagger.Model;
using Digipolis.DossierEngine.Common.Events;
using NLog.Extensions.Logging;
using Digipolis.Common.Status.Models;
using Narato.Common.Mappers;
using Digipolis.DossierEngine.Admin.API.SwaggerSchemaFilter;
using Digipolis.DossierEngine.Domain.ReplaceableTags;
using Digipolis.DossierEngine.Common.Pooling;
using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Digipolis.DossierEngine.PluginCreation;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.BedrijfsEigendommen.Sync.SAP.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.CRS.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.BeaMaster.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.CRSITMiddelen.Hardware.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.CRSITMiddelen.Software.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.Planon.FuelCard.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.EFacturatie.Plugin.Plugins;
using Digipolis.BedrijfsEigendommen.Sync.Planon.CompanyCar.Plugin.Plugins;

namespace Digipolis.DossierEngine.Admin.API
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public class AssemblyLoader : AssemblyLoadContext
        {
            protected override Assembly Load(AssemblyName assemblyName)
            {
                var deps = DependencyContext.Default;
                var res = deps.CompileLibraries.Where(d => d.Name.Contains(assemblyName.Name)).ToList();
                var assembly = Assembly.Load(new AssemblyName(res.First().Name));
                return assembly;
            }
        }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile("config.json")
               .AddJsonFile("config.json.local", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<SystemConfiguration>(Configuration.GetSection("SystemConfiguration"));
            services.Configure<DossierConfiguration>(Configuration.GetSection("DossierConfiguration"));
            services.Configure<EventHandlerConfiguration>(Configuration.GetSection("EventHandlerConfiguration"));
            services.Configure<TenantConfiguration>(Configuration.GetSection("TenantConfiguration"));
            services.Configure<PluginConfiguration>(Configuration.GetSection("PluginConfiguration"));

            services.AddMvc(
            //Add this filter globally so every request runs this filter to recored execution time
            config =>
            {
                config.Filters.Add(new ExecutionTimingFilter());
                config.Filters.Add(new ModelValidationFilter());
            })
                //Add formatter for JSON output to client and to format received objects         
                .AddJsonOptions(x =>
                {
                    x.SerializerSettings.ContractResolver =
                     new CamelCasePropertyNamesContractResolver();
                });


            var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);


            //Swagger configuation + swagger UI
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Contact = new Contact { Name = "Narato NV" },
                    Description = "Admin API",
                    Version = "v1",
                    Title = "Dossier Engine"
                });
                options.OperationFilter<ProducesConsumesFilter>();
                options.DocumentFilter<ChangeTenantDocumentationFilter>();

                var xmlPaths = GetXmlCommentsPaths();
                foreach (var entry in xmlPaths)
                {
                    try
                    {
                        options.IncludeXmlComments(entry);
                    }
                    catch (Exception e)
                    {
                        //TODO: Automate the generation and copying of the tenant library xmldocs for swagger
                    }
                }
            });

            var eventHandlerConfigSection = Configuration.GetSection("EventHandlerConfiguration");
            var eventHandlerEndpoint = eventHandlerConfigSection.GetValue<string>("EventHandlerEndpoint");
            var dataTypeApiEndpoint = Configuration.GetSection("DossierConfiguration").GetValue<string>("DataTypeApiEndpoint");
            var dataObjectApiEndpoint = Configuration.GetSection("DossierConfiguration").GetValue<string>("DataObjectApiEndpoint");


            //Confgure Autofac
            var builder = new ContainerBuilder();

            //TODO: Make base interface so this configuration can be simplified

            services.AddSingleton<IGroupedLimitedPool<string, HttpClient>, GroupedLimitedPool<string, HttpClient>>(
                c =>
                    new GroupedLimitedPool<string, HttpClient>(
                        (string group) => new HttpClient() { BaseAddress = new Uri(group) },
                        (client) => client.Dispose()
                    )
            );

            builder.RegisterType<ExceptionHandler>().As<IExceptionHandler>();
            builder.RegisterType<ExceptionToActionResultMapper>().As<IExceptionToActionResultMapper>();
            builder.RegisterType<TenantManager<Config>>().As<ITenantManager<Config>>();
            builder.RegisterType<TenantCreator<Config>>().As<ITenantCreator<Config>>();
            builder.RegisterType<TenantValidator>().As<ITenantValidator<Config>>();
            builder.RegisterType<ChecksumCalculator>().AsSelf();
            builder.RegisterType<TenantDataProvider>().As<ITenantDataProvider>();
            builder.RegisterType<AdminDbContext>().As<BaseAdminDbContext>();
            builder.RegisterType<PluginDataProvider>().As<IPluginDataProvider>();
            builder.RegisterType<TenantDbContext>().As<BaseTenantDbContext>();
            builder.RegisterType<TenantActionExecutor>().As<IActionExecutor<Config>>();
            builder.RegisterType<SshClientProvider>().As<ISshClientProvider>();
            builder.RegisterType<IneoCommandRunner>().As<IIneoCommandRunner>();

            builder.RegisterType<TagReplacer>().As<ITagReplacer>();

            builder.RegisterType<ConfigMapper<Config>>().As<IConfigMapper<Config>>();
            builder.RegisterType<DataProvider.Managers.MigrationManager>().As<DataProvider.Managers.IMigrationManager>();
            builder.RegisterType<PluginManager>().As<IPluginManager>();
            builder.RegisterType<ResponseFactory>().As<IResponseFactory>();
            builder.RegisterType<DataProvider.QueryProviders.MigrationQueryProvider>().As<DataProvider.QueryProviders.IMigrationQueryProvider>();
            builder.RegisterType<NeoMigrationDataProvider>().As<INeoMigrationDataProvider>();
            builder.RegisterType<TimeSeriesDataProvider>().As<ITimeSeriesDataProvider>();
            builder.RegisterType<TimeSeriesManager>().As<ITimeSeriesManager>();
            builder.RegisterType<DataProvider.QueryProviders.QueryExecutor>().As<DataProvider.QueryProviders.IQueryExecutor>();
            builder.RegisterType<SystemStatusAdminManager>().As<ISystemStatusManager>();
            builder.RegisterType<NeoMigrationManager>().As<INeoMigrationManager>();
            builder.RegisterType<NoOpCurrentTenantProvider>().As<ICurrentTenantProvider<Config>>();
            builder.RegisterType<TenantGraphClientFactory>().As<ITenantGraphClientFactory>();
            builder.RegisterType<SynchDataTypeManager>().As<ISyncedDataTypeManager>();
            builder.RegisterType<DataTypeClient>().As<IDataTypeClient>();
            builder.RegisterType<DataTypeChangeCalculator>().As<IChangeCalculator<Model.Models.DataType>>();
            builder.RegisterType<DataObjectClient>().As<IDataObjectClient>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(dataObjectApiEndpoint) });

            if (string.IsNullOrEmpty(eventHandlerEndpoint))
            {
                builder.RegisterType<NoOpEventHandler>().As<IEventHandlerAdministrator>();
                builder.Register(c => (NoOpEventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventPublisher>(); // basically an alias
                builder.Register(c => (NoOpEventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventSubscriber>(); // basically an alias
            } else
            {
                builder.RegisterType<Digipolis.Common.Events.EventHandler>().As<IEventHandlerAdministrator>().WithParameter("httpClient", new HttpClient() { BaseAddress = new Uri(eventHandlerEndpoint) });
                builder.Register(c => (Digipolis.Common.Events.EventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventPublisher>(); // basically an alias
                builder.Register(c => (Digipolis.Common.Events.EventHandler)c.Resolve<IEventHandlerAdministrator>()).As<IEventSubscriber>(); // basically an alias
            }

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != null && Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").ToLower().Equals("development"))
            {
                builder.RegisterType<TestPlugin>().As<IPlugin>();
                builder.RegisterType<TestPlugin2>().As<IPlugin>();
                builder.RegisterType<NestedPlugin>().As<IPlugin>();
                builder.RegisterType<FaultyPlugin>().As<IPlugin>();
            }
            
            // REGISTER CUSTOM PLUGINS BELOW THIS LINE!
            builder.RegisterType<SAPPlugin>().As<IPlugin>();
            builder.RegisterType<CRSPlugin>().As<IPlugin>();
            services.AddBeaMasterPlugin();
            builder.RegisterType<CRSITMiddelenHardwarePlugin>().As<IPlugin>();
            builder.RegisterType<CRSITMiddelenSoftwarePlugin>().As<IPlugin>();
            builder.RegisterType<EFacturatiePlugin>().As<IPlugin>();
            builder.RegisterType<PlanonFuelCardPlugin>().As<IPlugin>();
            builder.RegisterType<PlanonCompanyCarPlugin>().As<IPlugin>();

            // commented out because I suspect Digipolis might rethink their decision on this
            //var pluginFiles = new DirectoryInfo(Directory.GetCurrentDirectory() + @"/Plugins/");
            //var pluginInterface = typeof(IPlugin);

            //foreach (var file in pluginFiles.GetFiles())
            //{
            //    if (file.Name.Contains(".notempty")) {
            //        continue;
            //    }
            //    var asl = new AssemblyLoader();
            //    var asm = asl.LoadFromAssemblyPath(file.FullName);
            //    var types = asm.ExportedTypes.Where(t => pluginInterface.IsAssignableFrom(t));

            //    foreach (var type in types)
            //    {
            //        IPlugin obj = (IPlugin)Activator.CreateInstance(type);
            //        builder.RegisterInstance(obj).As<IPlugin>();
            //    }
            //}

            builder.Populate(services);
            var container = builder.Build();

            // Return the IServiceProvider resolved from the container.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddNLog();

            env.ConfigureNLog("nlog.config");

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            app.UseMvc();
        }

        private List<string> GetXmlCommentsPaths()
        {
            var app = PlatformServices.Default.Application;
            var files = new List<string>()
                        {
                            "Digipolis.DossierEngine.Admin.API.xml"
                        };

            List<string> paths = new List<string>();
            foreach (var file in files)
            {
                paths.Add(Path.Combine(app.ApplicationBasePath, file));
            }

            return paths;
        }
    }
}
