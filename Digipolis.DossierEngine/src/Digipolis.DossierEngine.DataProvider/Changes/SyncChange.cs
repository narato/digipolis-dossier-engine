﻿namespace Digipolis.DossierEngine.DataProvider.Changes
{
    public class SyncChange
    {
        public string ExternalId { get; set; }

        public SyncChange(string externalId)
        {
            ExternalId = externalId;
        }
    }
}
