﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.Domain.Factories;
using Microsoft.Extensions.Options;
using Moq;
using Neo4jClient;
using Xunit;

namespace Digipolis.DossierEngine.Test.Factories
{
    public class TenantGraphClientFactoryTest
    {
        [Fact]
        public void CreateTest()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenant(false)).Returns(new Tenant<Config>() { Config = new Config() { HttpPort = 1, Neo4JServer = "meh" } });

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { Neo4jUsername = "user", Neo4jPassword = "pass" });

            var tenantGraphClientFactory = new TenantGraphClientFactory(currentTenantProviderMock.Object, configOptionsMock.Object);

            var graphClient = tenantGraphClientFactory.Create();

            Assert.NotNull(graphClient);
            Assert.IsType(typeof(GraphClient), graphClient);

            Assert.Equal("user", graphClient.ExecutionConfiguration.Username);
            Assert.Equal("pass", graphClient.ExecutionConfiguration.Password);
        }

        [Fact]
        public void CreateAsyncTest()
        {
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctpm => ctpm.GetCurrentTenantAsync(false)).ReturnsAsync(new Tenant<Config>() { Config = new Config() { HttpPort = 1, Neo4JServer = "meh" } });

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { Neo4jUsername = "user", Neo4jPassword = "pass" });

            var tenantGraphClientFactory = new TenantGraphClientFactory(currentTenantProviderMock.Object, configOptionsMock.Object);

            var graphClient = tenantGraphClientFactory.CreateAsync().Result;

            Assert.NotNull(graphClient);
            Assert.IsType(typeof(GraphClient), graphClient);

            Assert.Equal("user", graphClient.ExecutionConfiguration.Username);
            Assert.Equal("pass", graphClient.ExecutionConfiguration.Password);
        }
    }
}
