﻿using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ISearchManager
    {
        Task<PagedCollectionResponse<IEnumerable<DataObject>>> FindDataObjectsForDataTypeIdBySearchQueryObjectAsync(Guid dataTypeId, dynamic queryObject, int page, int pageSize);
    }
}
