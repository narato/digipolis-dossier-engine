﻿using Digipolis.DossierEngine.Synchronization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.PluginCreation
{
    public class FaultyPlugin : AbstractPlugin
    {
        public FaultyPlugin()
        {
            Name = "FaultyPlugin";
            Version = 1;
            EventNamespace = "LocalDevDossierEngine";
            EventHandlerTopic = "Tenant.Created";
            DataTypes = new List<string>() {
                @"{""name"":""NestedPluginNested"",""attributes"":[{""type"":""String"",""name"":""nestedValue"",""regexValidation"":""^[a-zA-Z]*$"",""isRequired"":""true""}]}",
            };
        }

        public override string GetObjectById(object id)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<object> GetAllObjects()
        {
            throw new NotImplementedException();
        }
    }
}
