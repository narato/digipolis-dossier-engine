﻿using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Search.Cypher;
using Moq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using Xunit;
using System.Linq;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class SearchManagerTest
    {

        [Fact]
        public void GetDataTypeByIdReturnsDataType()
        {
            var guid = Guid.NewGuid();
            var dataObjectList = new List<Model.Models.DataObject>();
            var dynamicObject = new ExpandoObject();

            var whereClause = "blabla";

            var dataObjectDataProviderMock = new Mock<IDataObjectDataProvider>();
            dataObjectDataProviderMock.Setup(dodpm => dodpm.FindDataObjectsForDataTypeIdByWhereClauseAsync(guid, whereClause, 1, 10)).ReturnsAsync(dataObjectList);

            var conditionMock = new Mock<ICondition>();

            var conditionBuilderMock = new Mock<IConditionBuilder>();
            conditionBuilderMock.Setup(cbm => cbm.BuildConditionFromJsonObject(dynamicObject)).Returns(conditionMock.Object);

            var cypherQueryBuilderMock = new Mock<ICypherQueryBuilder>();
            cypherQueryBuilderMock.Setup(cqbm => cqbm.Visit(conditionMock.Object)).Returns(whereClause);

            var manager = new SearchManager(dataObjectDataProviderMock.Object, conditionBuilderMock.Object, cypherQueryBuilderMock.Object);

            var returnedDataObjects = manager.FindDataObjectsForDataTypeIdBySearchQueryObjectAsync(guid, dynamicObject, 1, 10).Result;

            Assert.NotNull(returnedDataObjects);
            Assert.Equal(dataObjectList.Count, returnedDataObjects.Data.ToList().Count);

            conditionBuilderMock.Verify(cbm => cbm.BuildConditionFromJsonObject(dynamicObject), Times.Once);
            cypherQueryBuilderMock.Verify(cqbm => cqbm.Visit(conditionMock.Object), Times.Once);
            dataObjectDataProviderMock.Verify(dodpm => dodpm.FindDataObjectsForDataTypeIdByWhereClauseAsync(guid, whereClause, 1, 10), Times.Once);
        }
    }
}
