﻿using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Common.Pooling
{
    public class GroupedLimitedPool<K, T> : IGroupedLimitedPool<K, T> where T : class
    {
        private readonly Func<K, T> _valueFactory;
        private readonly Func<K, Task<T>> _valueFactoryAsync;
        private readonly Action<T> _valueDisposeAction;
        private readonly TimeSpan _valueLifetime;
        private readonly ConcurrentDictionary<K, ConcurrentStack<LimitedPoolItem<T>>> _pool;
        private bool _disposed;

        public GroupedLimitedPool(Func<K, T> valueFactory, Action<T> valueDisposeAction, TimeSpan? valueLifetime = null)
        {
            _valueFactory = valueFactory;
            _valueDisposeAction = valueDisposeAction;
            _valueLifetime = valueLifetime ?? TimeSpan.FromHours(1);
            _pool = new ConcurrentDictionary<K, ConcurrentStack<LimitedPoolItem<T>>>();
        }

        public GroupedLimitedPool(Func<K, Task<T>> valueFactoryAsync, Action<T> valueDisposeAction, TimeSpan? valueLifetime = null)
        {
            _valueFactoryAsync = valueFactoryAsync;
            _valueDisposeAction = valueDisposeAction;
            _valueLifetime = valueLifetime ?? TimeSpan.FromHours(1);
            _pool = new ConcurrentDictionary<K, ConcurrentStack<LimitedPoolItem<T>>>();
        }

        public int IdleCount(K group) {
            return _pool.GetOrAdd(group, new ConcurrentStack<LimitedPoolItem<T>>()).Count;
        }

        public ILimitedPoolItem<T> Get(K group)
        {
            if (_valueFactory == null)
                throw new InvalidOperationException("This pool was set up with an async valueFactory. Use GetAsync instead.");
            var innerPool = _pool.GetOrAdd(group, new ConcurrentStack<LimitedPoolItem<T>>());
            LimitedPoolItem<T> item;
            // try to get live cached item
            while (!_disposed && innerPool.TryPop(out item))
            {
                if (!item.Expired)
                    return item;
                // dispose expired item
                item.Dispose();
            }
            // since no cached items available we create a new one
            return new LimitedPoolItem<T>(_valueFactory(group), disposedItem =>
            {
                if (disposedItem.Expired)
                {
                    // item has been expired, dispose it forever
                    _valueDisposeAction(disposedItem.Value);
                }
                else
                {
                    // item is still fresh enough, return it to the pool
                    if (!_disposed)
                        innerPool.Push(disposedItem);
                }
            }, _valueLifetime);
        }

        public async Task<ILimitedPoolItem<T>> GetAsync(K group)
        {
            if (_valueFactoryAsync == null)
                throw new InvalidOperationException("This pool was set up without an async valueFactory. Use Get instead.");
            var innerPool = _pool.GetOrAdd(group, new ConcurrentStack<LimitedPoolItem<T>>());
            LimitedPoolItem<T> item;
            // try to get live cached item
            while (!_disposed && innerPool.TryPop(out item))
            {
                if (!item.Expired)
                    return item;
                // dispose expired item
                item.Dispose();
            }
            // since no cached items available we create a new one
            return new LimitedPoolItem<T>(await _valueFactoryAsync(group), disposedItem =>
            {
                if (disposedItem.Expired)
                {
                    // item has been expired, dispose it forever
                    _valueDisposeAction(disposedItem.Value);
                }
                else
                {
                    // item is still fresh enough, return it to the pool
                    if (!_disposed)
                        innerPool.Push(disposedItem);
                }
            }, _valueLifetime);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _disposed = true;
                var groups = _pool.ToArray();
                foreach (var group in groups)
                {
                    var items = group.Value.ToArray();
                    foreach (var item in items)
                        _valueDisposeAction(item.Value);
                }
            }
        }
    }
}
