﻿namespace Digipolis.DossierEngine.DataProvider.Managers
{
    public interface IMigrationManager
    {
        string ExecutePendingMigrations();
    }
}
