﻿using Digipolis.Common.Events;
using Digipolis.Common.Events.Models;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Domain.Dto;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Digipolis.DossierEngine.Model.Models;
using Digipolis.DossierEngine.Synchronization;
using Digipolis.Tenant.Library.DataProviders;
using Digipolis.Tenant.Library.Managers;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using TenantModel = Digipolis.Common.Tenancy.Models;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class PluginManagerTest
    {
        [Fact]
        public void LoadPluginsWithNewPluginCallsInsertPlugins()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1" },
                new Plugin { Name = "myPlugin2" }
            };

            var dossierConfig = new DossierConfiguration()
            {
                PluginEndpoint = "myEndPoint"
            };

            var pluginDataProviderMock = new Mock<IPluginDataProvider>();
            pluginDataProviderMock.Setup(p => p.GetPlugins()).Returns(plugins);

            var pluginList = new List<IPlugin>();
            var pluginMock = new Mock<IPlugin>();
            pluginList.Add(pluginMock.Object);

            var tagReplacerMock = new Mock<ITagReplacer>();

            var tenantDataProviderMock = new Mock<ITenantDataProvider>();
            var eventSubscriberMock = new Mock<IEventSubscriber>();
            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var dataObjectClientMock = new Mock<IDataObjectClient>();
            configOptionsMock.SetupGet(x => x.Value).Returns(dossierConfig);

            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var changeCalculator = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var pluginManager = new PluginManager(changeCalculator.Object, tenantManagerMock.Object, pluginDataProviderMock.Object, pluginList, tenantDataProviderMock.Object, eventSubscriberMock.Object,dataTypeClientMock.Object, dataObjectClientMock.Object, tagReplacerMock.Object, configOptionsMock.Object);

            var result = pluginManager.LoadPlugins();

            Assert.IsType(typeof(string), result);
            eventSubscriberMock.Verify(tc => tc.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Subscription>(), It.IsAny<string>()), Times.Once);
            pluginDataProviderMock.Verify(pd => pd.InsertPlugins(It.IsAny<List<Plugin>>()), Times.Once);
        }

        [Fact]
        public void LoadPluginsWithNoNewPluginDoesNotCallInsertPlugins()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1" },
                new Plugin { Name = "myPlugin2" }
            };

            var pluginDataProviderMock = new Mock<IPluginDataProvider>();
            pluginDataProviderMock.Setup(p => p.GetPlugins()).Returns(plugins);

            var tagReplacerMock = new Mock<ITagReplacer>();

            var pluginList = new List<IPlugin>();
            var tenantDataProviderMock = new Mock<ITenantDataProvider>();
            var eventSubscriberMock = new Mock<IEventSubscriber>();
            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var dataObjectClientMock = new Mock<IDataObjectClient>();
            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var changeCalculator = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var pluginManager = new PluginManager(changeCalculator.Object, tenantManagerMock.Object, pluginDataProviderMock.Object, pluginList, tenantDataProviderMock.Object, eventSubscriberMock.Object, dataTypeClientMock.Object, dataObjectClientMock.Object, tagReplacerMock.Object, configOptionsMock.Object);

            var result = pluginManager.LoadPlugins();

            Assert.IsType(typeof(string), result);
            eventSubscriberMock.Verify(tc => tc.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Subscription>(), It.IsAny<string>()), Times.Never);
            pluginDataProviderMock.Verify(pd => pd.InsertPlugins(It.IsAny<List<Plugin>>()), Times.Never);
        }

        [Fact]
        public void GetPluginsReturnsPluginList()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1" },
                new Plugin { Name = "myPlugin2" }
            };

            var pluginDataProviderMock = new Mock<IPluginDataProvider>();
            pluginDataProviderMock.Setup(p => p.GetPlugins()).Returns(plugins);

            var tagReplacerMock = new Mock<ITagReplacer>();

            var pluginList = new List<IPlugin>();
            var tenantDataProviderMock = new Mock<ITenantDataProvider>();
            var eventSubscriberMock = new Mock<IEventSubscriber>();
            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var dataObjectClientMock = new Mock<IDataObjectClient>();
            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var changeCalculator = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var pluginManager = new PluginManager(changeCalculator.Object, tenantManagerMock.Object, pluginDataProviderMock.Object, pluginList, tenantDataProviderMock.Object, eventSubscriberMock.Object, dataTypeClientMock.Object, dataObjectClientMock.Object, tagReplacerMock.Object, configOptionsMock.Object);

            var result = pluginManager.GetPlugins();

            Assert.IsType(typeof(List<Plugin>), result);
            Assert.Equal(plugins.Count, result.Count);
        }

        [Fact]
        public void InsertTenantPluginLink_TenantPluginLinkDTONull_ReturnsArgumentNullException()
        {
            var pluginDataProviderMock = new Mock<IPluginDataProvider>();
            pluginDataProviderMock.Setup(pd => pd.InsertTenantPluginLink(null)).Returns<TenantPluginLinkDTO>(null);

            var tagReplacerMock = new Mock<ITagReplacer>();

            var pluginList = new List<IPlugin>();
            var tenantDataProviderMock = new Mock<ITenantDataProvider>();
            var eventSubscriberMock = new Mock<IEventSubscriber>();
            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var dataObjectClientMock = new Mock<IDataObjectClient>();
            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var changeCalculator = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var pluginManager = new PluginManager(changeCalculator.Object, tenantManagerMock.Object, pluginDataProviderMock.Object, pluginList, tenantDataProviderMock.Object, eventSubscriberMock.Object, dataTypeClientMock.Object, dataObjectClientMock.Object, tagReplacerMock.Object, configOptionsMock.Object);

            Exception ex = Assert.ThrowsAsync<Exception>(() => pluginManager.InsertTenantPluginLinkAsync(null)).Result;

            Assert.Contains("Parameter name: tenantPluginLinkDto", ex.Message);
        }

        [Fact]
        public void InsertTenantPluginLinkReturnsTenantPluginLinkCreatedDTO()
        {
            var pluginId = Guid.NewGuid();
            var tenantId = Guid.NewGuid();
            var dto = new TenantPluginLinkDTO { PluginId = pluginId, TenantId = tenantId };
            var link = new TenantPluginLink() { PluginId = pluginId, TenantId = tenantId };

            var pluginDataProviderMock = new Mock<IPluginDataProvider>(MockBehavior.Strict);
            pluginDataProviderMock.Setup(pd => pd.InsertTenantPluginLink(It.IsAny<TenantPluginLink>())).Returns(link);
            pluginDataProviderMock.Setup(pd => pd.GetPluginById(dto.PluginId)).Returns(new Plugin { Id = Guid.NewGuid(), DataType = "[1]" });

            var tagReplacerMock = new Mock<ITagReplacer>();
            tagReplacerMock.Setup(trm => trm.replace(It.IsAny<string>(), It.IsAny<IDictionary<string, object>>())).Returns("test");

            var pluginList = new List<IPlugin>();
            var tenantDataProviderMock = new Mock<ITenantDataProvider>();
            tenantDataProviderMock.Setup(pd => pd.GetTenantByIdAsync(dto.TenantId)).ReturnsAsync(new TenantModel.Tenant { Id = Guid.NewGuid() });

            var eventSubscriberMock = new Mock<IEventSubscriber>();
            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            var dataTypeClientMock = new Mock<IDataTypeClient>();

            dataTypeClientMock.Setup(dtcm => dtcm.InsertSyncedDataTypeAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new Model.Models.DataType { Id = Guid.NewGuid(), Name = "bla" });
            var dataObjectClientMock = new Mock<IDataObjectClient>();
            var tenantManagerMock = new Mock<ITenantManager<Config>>();

            var changeCalculator = new Mock<IChangeCalculator<Model.Models.DataType>>();

            var pluginManager = new PluginManager(changeCalculator.Object, tenantManagerMock.Object, pluginDataProviderMock.Object, pluginList, tenantDataProviderMock.Object, eventSubscriberMock.Object, dataTypeClientMock.Object, dataObjectClientMock.Object, tagReplacerMock.Object, configOptionsMock.Object);

            var result = pluginManager.InsertTenantPluginLinkAsync(dto).Result;

            Assert.IsType(typeof(TenantPluginLink), result);
            tenantDataProviderMock.Verify(td => td.GetTenantByIdAsync(It.IsAny<Guid>()), Times.Once);
            pluginDataProviderMock.Verify(pd => pd.GetPluginById(It.IsAny<Guid>()), Times.Once);
            pluginDataProviderMock.Verify(pd => pd.InsertTenantPluginLink(It.IsAny<TenantPluginLink>()), Times.Once);
        }
    }
}
