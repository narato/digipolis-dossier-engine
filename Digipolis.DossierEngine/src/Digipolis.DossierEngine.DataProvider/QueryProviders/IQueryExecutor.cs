﻿using Digipolis.Tenant.Library.Models;

namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public interface IQueryExecutor
    {
        object FindDatabase(string connectionString, string databaseName);
        int CreateDatabase(string connectionString, string databaseName);
        string MigrateTenantDb(TenantConfiguration config);
    }
}