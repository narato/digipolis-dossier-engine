﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Narato.Common.Logging;

namespace Digipolis.DossierEngine.DataObject.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/synceddataobjects/")]
    public class SyncedDataObjectController : Controller
    {
        private ISyncedDataObjectManager _syncedDataObjectManager;
        private IResponseFactory _responseFactory;

        public SyncedDataObjectController(ISyncedDataObjectManager dataObjectManager, IResponseFactory responseFactory)
        {
            _syncedDataObjectManager = dataObjectManager;
            _responseFactory = responseFactory;
        }

        [HttpPut("{externalId}")]
        public async Task<IActionResult> UpdateSyncedDataObjectAsync([Required]string externalId, [FromBody] [Required] Model.Models.DataObject dataObject, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataObject == null)
                missingParams.Add(new MissingParam("dataobject", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            return await _responseFactory.CreatePostResponseAsync(async () => await _syncedDataObjectManager.UpdateSyncedDataObjectAsync(dataObject).LogOnException(), this.GetRequestUri());
        }

        [HttpPost]
        public async Task<IActionResult> CreateSyncedDataObjectAsync([FromBody] [Required] Model.Models.DataObject dataObject, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataObject == null)
                missingParams.Add(new MissingParam("dataobject", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            Func<Model.Models.DataObject, object> pathMapper = (data) => { 
                return new { controller = "DataObject", id = data.Id, tenantKey = tenantKey }; 
            };

            return await _responseFactory.CreatePostResponseAsync(async () => await _syncedDataObjectManager.CreateSyncedDataObjectAsync(dataObject), this.GetRequestUri(),
                        "GetDataObjectById", pathMapper);
        }
    }
}
