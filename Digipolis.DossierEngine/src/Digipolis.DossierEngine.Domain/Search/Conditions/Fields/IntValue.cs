﻿namespace Digipolis.DossierEngine.Domain.Search.Conditions.Fields
{
    public class IntValue : IValue<int>
    {
        public string Type { get; } = "int";
        public int Value { get; }

        public IntValue(int value)
        {
            Value = value;
        }
    }
}
