﻿using Digipolis.DossierEngine.Model.Models;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IDataObjectClient
    {
        DataObject InsertDataObject(string dataObjectJson, string tenantKey);
        DataObject UpdateSyncedDataObject(string dataObjectJson, string externalId, string tenantKey);
        DataObject CreateSyncedDataObject(string dataObjectJson, string tenantKey);
    }
}