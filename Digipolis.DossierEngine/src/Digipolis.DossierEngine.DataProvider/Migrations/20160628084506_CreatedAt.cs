﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Digipolis.DossierEngine.DataProvider.Migrations
{
    public partial class CreatedAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Tenant",
                nullable: false,
                defaultValueSql: "NOW() at time zone 'utc'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Tenant");
        }
    }
}
