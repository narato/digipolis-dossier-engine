﻿namespace Digipolis.DossierEngine.Model.Models
{
    public class BooleanAttributeType:BaseAttributeType
    {
        public const string TypeName = "Boolean";

        public BooleanAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
    }
}