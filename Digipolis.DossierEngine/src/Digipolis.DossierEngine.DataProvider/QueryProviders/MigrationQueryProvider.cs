﻿namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public class MigrationQueryProvider : IMigrationQueryProvider
    {
        public MigrationQueryProvider() {}

        public string GetCreateDbQuery(string dbName)
        {
            return $"CREATE DATABASE \"{dbName}\" ENCODING = 'UTF8'";
        }

        public string GetDbExistsQuery(string dbName)
        {
            return $"SELECT 1 FROM pg_database WHERE datname='{dbName}'";
        }
    }
}
