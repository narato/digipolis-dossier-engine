namespace Digipolis.DossierEngine.Common.Configurations
{
    public class PluginConfiguration
    {
        public string BeaMasterTenantKey { get; set; }
        public string BeaMasterDataObjectApiBaseUrl { get; set; }
        public string BeaMasterDataTypeApiBaseUrl { get; set; }
        public string BeaMasterEventNamespace { get; set; }
        public string BeaMasterEventHandlerTopic { get; set; }
    }
}
