﻿using Digipolis.DossierEngine.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    // WARNING! this is a very simple cacher. This should NOT be instantiated as a singleton. 
    // At most this is suitable on a per request basis. And even then, it's not safe for race conditions.
    // this cache CANNOT be invalidated. This is ONLY to be used for extremely simple use cases, such as the datatype validator!
    public interface ISimpleDataTypeCacher
    {
        Task<IEnumerable<DataType>> GetAllDataTypesAsync();
    }
}
