﻿using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using Digipolis.DossierEngine.Domain.Search.Conditions.Operators;
using Digipolis.DossierEngine.Domain.Search.Conditions.Visitor;

namespace Digipolis.DossierEngine.Domain.Search.Conditions
{
    // for mocking purposes
    public interface IComparison : ICondition
    {
        Comparable LeftHandSide { get; }
        IOperator Operator { get; }
        Comparable RightHandSide { get; }

        string visit(IConditionVisitor visitor, bool partOfComposite);
    }
}
