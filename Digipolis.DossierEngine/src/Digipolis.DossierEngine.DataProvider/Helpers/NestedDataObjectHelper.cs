﻿using Digipolis.DossierEngine.Model.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.Helpers
{
    public class NestedDataObjectHelper
    {
        public DataObject CreateNestedDataObjectForLinkDataAttributeType(LinkDataTypeAttributeType attribute, DataObject dataObject)
        {
            var nestedDataObject = new DataObject()
            {
                Id = Guid.NewGuid(),
                DataTypeId = attribute.DataTypeId,
                Name = $"{dataObject.Name}.{attribute.Name}",
                IsNested = true
            };

            if (attribute.DataType.Synced == true)
            {
                nestedDataObject.ExternalId = dataObject.Values[attribute.Name].ToString();
                nestedDataObject.Values = new Dictionary<string, object>();
            } else
            {
                nestedDataObject.Values = JsonConvert.DeserializeObject<Dictionary<string, object>>(dataObject.Values[attribute.Name].ToString());
            }
            return nestedDataObject;
        }

        public List<DataObject> CreateNestedDataObjectListForLinkDataAttributeType(LinkDataTypeAttributeType attribute, DataObject dataObject)
        {
            if (attribute.IsCollection != true)
            {
                throw new Exception("Trying to get a list of DataObjects, but the attributeType isn't a collection");
            }
            var rawList = dataObject.Values[attribute.Name] as JArray;
            var list = new List<DataObject>();

            var counter = 0;
            foreach (var rawListItem in rawList)
            {
                var nestedDataObject = new DataObject()
                {
                    Id = Guid.NewGuid(),
                    DataTypeId = attribute.DataTypeId,
                    Name = $"{dataObject.Name}.{attribute.Name}[{counter++}]",
                    IsNested = true
                };
                if (attribute.DataType.Synced == true) {
                    nestedDataObject.ExternalId = rawListItem.ToString();
                    nestedDataObject.Values = new Dictionary<string, object>();
                } else
                {
                    nestedDataObject.Values = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawListItem.ToString());
                }

                list.Add(nestedDataObject);
            }

            return list;
        }
    }
}
