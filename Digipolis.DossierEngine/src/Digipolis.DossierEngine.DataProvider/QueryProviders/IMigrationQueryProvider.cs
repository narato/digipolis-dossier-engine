﻿namespace Digipolis.DossierEngine.DataProvider.QueryProviders
{
    public interface IMigrationQueryProvider
    {
        string GetDbExistsQuery(string dbName);
        string GetCreateDbQuery(string dbName);
    }
}