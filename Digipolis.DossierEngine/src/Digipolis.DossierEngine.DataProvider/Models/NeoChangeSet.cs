﻿using System;

namespace Digipolis.DossierEngine.DataProvider.Models
{
    public class NeoChangeSet
    {
        public DateTime Date { get; set; }
    }
}
