﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataObject.API.Controllers
{
    [Route("api/system")]
    public class SystemController : Controller
    {
        private ISystemStatusManager _systemStatusManager;
        private IResponseFactory _responseFactory;

        public SystemController(ISystemStatusManager systemStatusManager, IResponseFactory responseFactory)
        {
            _systemStatusManager = systemStatusManager;
            _responseFactory = responseFactory;
        }
        /// <summary>
        /// This method returns the status of the system for the given tenantkey
        /// </summary>
        /// <param name="tenantKey">The key of the tenant</param>
        /// <returns>A system status for the given tenantid</returns>
        // GET api/system
        [ProducesResponseType(typeof(Response<SystemStatus>), (int)System.Net.HttpStatusCode.OK)]
        [HttpGet("status")]
        public async Task<IActionResult> Get([Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            if (string.IsNullOrEmpty(tenantKey))
                return _responseFactory.CreateMissingParam(new MissingParam("tenantKey", MissingParamType.QuerystringParam));

            return await _responseFactory.CreateGetResponseAsync(async () => await _systemStatusManager.GetStatusAsync(), this.GetRequestUri());
        }
    }
}