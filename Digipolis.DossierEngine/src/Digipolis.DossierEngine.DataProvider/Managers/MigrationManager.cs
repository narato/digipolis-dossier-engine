﻿using Digipolis.DossierEngine.DataProvider.QueryProviders;
using Digipolis.Tenant.Library.Models;
using Microsoft.Extensions.Options;
using Npgsql;
using System;

namespace Digipolis.DossierEngine.DataProvider.Managers
{
    // TODO: who put this here?.... managers should be in the Domain project :)
    // so when bored or too much time: move it
    public class MigrationManager : IMigrationManager
    {
        private TenantConfiguration _tenantConfiguration;
        
        private IQueryExecutor _queryExecutor;
        public MigrationManager(IOptions<TenantConfiguration> tenantConfiguration, IQueryExecutor queryExecutor)
        {
            _tenantConfiguration = tenantConfiguration.Value;
            _queryExecutor = queryExecutor;
        }

        public string ExecutePendingMigrations()
        {
            try
            {
                string returnMessage = "Nothing was updated";
                var builder = new NpgsqlConnectionStringBuilder(_tenantConfiguration.ConnectionString);
                var databaseName = builder.Database; // The target database
                builder.Database = "postgres"; // The postgres db should always be present
               
                bool dbExists;
                dbExists = _queryExecutor.FindDatabase(builder.ConnectionString, databaseName) != null;

                if (dbExists == false)
                {
                    _queryExecutor.CreateDatabase(builder.ConnectionString, databaseName);
                }

                builder.Database = databaseName;
                
                returnMessage = _queryExecutor.MigrateTenantDb(_tenantConfiguration);

                return returnMessage;
            }
            catch (Exception e)
            {
                //TODO: logging?
                throw e;
            }
        }
    }
}
