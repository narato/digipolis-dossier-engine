﻿using Digipolis.DossierEngine.DataProvider.Helpers;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Model.Models;
using Narato.Common.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Digipolis.DossierEngine.Domain.Validators
{
    // TODO: maybe refactor to a strategy pattern?
    public class DataObjectValidator : IDataObjectValidator
    {
        private readonly NestedDataObjectHelper _nestedDataObjectHelper;

        public DataObjectValidator(NestedDataObjectHelper nestedDataObjectHelper)
        {
            _nestedDataObjectHelper = nestedDataObjectHelper;
        }

        private FeedbackItem CreateFeedBackItem(string description)
        {
            return new FeedbackItem { Type = FeedbackType.ValidationError, Description = description };
        }

        public List<FeedbackItem> ValidateStringObject(StringAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (! (value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of String type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var stringValue in (JArray)value)
                {
                    feedbackItems.AddRange(ValidateSingleString(att, stringValue.ToString()));
                }
                return feedbackItems;
            }
            return ValidateSingleString(att, value?.ToString() ?? "");
        }

        private List<FeedbackItem> ValidateSingleString(StringAttributeType att, string valueAsString)
        {
            var feedbackItems = new List<FeedbackItem>();
            Regex rgx = new Regex(att.RegexValidation);

            if (att.RegexValidation != "" && !rgx.IsMatch(valueAsString))
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for String type ({0}) does not match the required RegexValidation : {1}", att.Name, att.RegexValidation) });

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateIntegerObject(IntAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (! (value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of Int type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var intValue in (JArray) value)
                {
                    feedbackItems.AddRange(ValidateSingleInteger(att, intValue.ToString()));
                }
                return feedbackItems;
            }
            return ValidateSingleInteger(att, value.ToString());
        }

        private List<FeedbackItem> ValidateSingleInteger(IntAttributeType att, string valueAsString)
        {
            var feedbackItems = new List<FeedbackItem>();
            int parseResult;
            if (! int.TryParse(valueAsString, out parseResult))
            {
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Int type ({0}) is not a valid Integer : {1}", att.Name, valueAsString) });
                return feedbackItems;
            }

            if (att.MinRangeValidation != null && parseResult < att.MinRangeValidation)
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Int type ({0}) cannot be lower than MinRangeValidation : {1}", att.Name, att.MinRangeValidation) });

            if (att.MaxRangeValidation != null && parseResult > att.MaxRangeValidation)
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Int type ({0}) cannot be higher than MaxRangeValidation : {1}", att.Name, att.MaxRangeValidation) });

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateDoubleObject(DoubleAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (! (value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of Double type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var doubleValue in (JArray)value)
                {
                    feedbackItems.AddRange(ValidateSingleDouble(att, doubleValue.ToString()));
                }
                return feedbackItems;
            }
            return ValidateSingleDouble(att, value.ToString());
        }

        private List<FeedbackItem> ValidateSingleDouble(DoubleAttributeType att, string valueAsString)
        {
            var feedbackItems = new List<FeedbackItem>();
            double parseResult;
            if (! double.TryParse(valueAsString, out parseResult))
            {
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Double type ({0}) is not a valid Integer : {1}", att.Name, valueAsString) });
                return feedbackItems;
            }

            if (att.MinRangeValidation != null && parseResult < att.MinRangeValidation)
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Double type ({0}) cannot be lower than MinRangeValidation : {1}", att.Name, att.MinRangeValidation) });

            if (att.MaxRangeValidation != null && parseResult > att.MaxRangeValidation)
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Double type ({0}) cannot be higher than MaxRangeValidation : {1}", att.Name, att.MaxRangeValidation) });

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateBooleanObject(BooleanAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (! (value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of Boolean type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var boolValue in (JArray)value)
                {
                    feedbackItems.AddRange(ValidateSingleBoolean(att, boolValue.ToString()));
                }
                return feedbackItems;
            }
            return ValidateSingleBoolean(att, value.ToString());
        }

        private List<FeedbackItem> ValidateSingleBoolean(BooleanAttributeType att, string valueAsString)
        {
            var feedbackItems = new List<FeedbackItem>();
            bool parseResult;
            if (! bool.TryParse(valueAsString, out parseResult))
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Boolean type ({0}) is not a valid Boolean : {1}", att.Name, valueAsString) });

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateLinkObject(LinkAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (!(value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of Link type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var linkValue in (JArray)value)
                {
                    feedbackItems.AddRange(ValidateSingleLink(att, linkValue.ToString()));
                }
                return feedbackItems;
            }
            return ValidateSingleLink(att, value.ToString());
        }

        private List<FeedbackItem> ValidateSingleLink(LinkAttributeType att, string valueAsString)
        {
            var feedbackItems = new List<FeedbackItem>();
            Regex rgx = new Regex(att.RegexValidation);

            if (att.RegexValidation != "" && !rgx.IsMatch(valueAsString))
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for Link type ({0}) is not a valid URI : {1}", att.Name, valueAsString) });

            return feedbackItems;
        }

        public List<FeedbackItem> ValidateDateTimeObject(DateTimeAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (!(value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of DateTime type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                foreach (var dateValue in (JArray)value)
                {
                    feedbackItems.AddRange(ValidateSingleDateTime(att, ((JValue)dateValue).Value));
                }
                return feedbackItems;
            }
            return ValidateSingleDateTime(att, value);
        }

        private List<FeedbackItem> ValidateSingleDateTime(DateTimeAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (value is DateTime)
            {
                if (((DateTime)value).Kind != DateTimeKind.Utc)
                    feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for DateTime type ({0}) is not a valid UTC date format : '{1}' (eg: 2014-04-21T10:30:50Z)", att.Name, value) });
            }
            else
            {
                feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("Given value for DateTime type ({0}) is not a valid UTC date format : '{1}' (eg: 2014-04-21T10:30:50Z)", att.Name, value) });
            }

            return feedbackItems;
        }

        private List<FeedbackItem> ValidateLinkDataTypeObject(LinkDataTypeAttributeType att, object value, DataObject dataObject)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (att.IsCollection == true)
            {
                if (! (value is JArray))
                {
                    feedbackItems.Add(CreateFeedBackItem(string.Format("Given value for Collection of Nested Objects type ({0}) is not a collection : {1}", att.Name, value)));
                    return feedbackItems;
                }
                if (att.DataType.Synced == true)
                {
                    foreach (var externalId in (JArray)value)
                    {
                        feedbackItems.AddRange(ValidateSingleSyncedLinkDataTypeObject(att, externalId));
                    }
                    return feedbackItems;
                }
                var nestedDataObjects = _nestedDataObjectHelper.CreateNestedDataObjectListForLinkDataAttributeType(att, dataObject);
                foreach (var nestedDataObject in nestedDataObjects)
                {
                    feedbackItems.AddRange(ValidateDataObjectAttributes(att.DataType, nestedDataObject));
                }
                return feedbackItems;
            }
            
            // if we're talking about a synced datatype, we only need to supply an (external) id
            if (att.DataType.Synced == true)
            {
                return ValidateSingleSyncedLinkDataTypeObject(att, value);
            }
            return ValidateDataObjectAttributes(att.DataType, _nestedDataObjectHelper.CreateNestedDataObjectForLinkDataAttributeType(att, dataObject));
        }

        private List<FeedbackItem> ValidateSingleSyncedLinkDataTypeObject(LinkDataTypeAttributeType att, object value)
        {
            var feedbackItems = new List<FeedbackItem>();
            if (value is JArray || value is JObject)
            {
                feedbackItems.Add(CreateFeedBackItem(string.Format("Given value of the external ID for a synced DataObject is not valid. ({0}) is not a guid/int/string : {1}", att.Name, value)));
            }
            if (string.IsNullOrEmpty(value.ToString()))
            {
                feedbackItems.Add(CreateFeedBackItem(string.Format("Given value of the external ID for a synced DataObject is not valid. ({0}) is null or empty : {1}", att.Name, value)));
            }
            return feedbackItems;
        }

        public List<FeedbackItem> ValidateDataObjectAttributes(DataType dataType, DataObject dataObject)
        {
            var feedbackItems = new List<FeedbackItem>();
            List<string> dataTypeNames = new List<string>();

            foreach (BaseAttributeType att in dataType.Attributes)
            {
                //Check if all required DataTypes contain a value in the given DataObject
                if (att.IsRequired == true && !dataObject.Values.ContainsKey(att.Name))
                {
                    //When no value found apply the default value if available else provide feedback
                    if (att.DefaultValue != null)
                    {
                        dataObject.Values.Add(att.Name, att.DefaultValue);
                    }
                    else
                    {
                        feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The required field ({0}) was not found in the collection and no default value was found", att.Name) });
                    }
                }

                // if value doesn't exist, don't validate it
                if (!dataObject.Values.ContainsKey(att.Name))
                    continue;

                if (att is StringAttributeType)
                {
                    var stringType = (StringAttributeType)att;

                    feedbackItems.AddRange(ValidateStringObject(stringType, dataObject.Values[stringType.Name]));
                }
                else if (att is IntAttributeType)
                {
                    var intType = (IntAttributeType)att;

                    feedbackItems.AddRange(ValidateIntegerObject(intType, dataObject.Values[intType.Name]));
                }
                else if (att is DoubleAttributeType)
                {
                    var doubleType = (DoubleAttributeType)att;

                    feedbackItems.AddRange(ValidateDoubleObject(doubleType, dataObject.Values[doubleType.Name]));
                }
                else if (att is BooleanAttributeType)
                {
                    var boolType = (BooleanAttributeType)att;

                    feedbackItems.AddRange(ValidateBooleanObject(boolType, dataObject.Values[boolType.Name]));
                }
                else if (att is LinkAttributeType)
                {
                    var linkType = (LinkAttributeType)att;

                    feedbackItems.AddRange(ValidateLinkObject(linkType, dataObject.Values[linkType.Name]));
                }
                else if (att is LinkDataTypeAttributeType)
                {
                    var linkDataType = (LinkDataTypeAttributeType)att;

                    feedbackItems.AddRange(ValidateLinkDataTypeObject(linkDataType, dataObject.Values[linkDataType.Name], dataObject));
                }
                else if (att is DateTimeAttributeType)
                {
                    var dateType = (DateTimeAttributeType)att;

                    feedbackItems.AddRange(ValidateDateTimeObject(dateType, dataObject.Values[dateType.Name]));
                }

                dataTypeNames.Add(att.Name);
            }
            
            var diff = dataObject.Values.Keys.ToList().Except(dataTypeNames).ToList();
            diff.ForEach(n => feedbackItems.Add(new FeedbackItem { Type = FeedbackType.ValidationError, Description = string.Format("The field ({0}) was found in the collection that does not exist in the DataType", n) }));

            return feedbackItems;
        }
    }
}