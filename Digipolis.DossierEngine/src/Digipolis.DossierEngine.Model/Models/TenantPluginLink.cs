﻿using Newtonsoft.Json;
using System;
using TenantModel = Digipolis.Common.Tenancy.Models;

namespace Digipolis.DossierEngine.Model.Models
{
    public class TenantPluginLink
    {
        public Guid Id { get; set; }
        public Guid PluginId { get; set; }
        public Guid TenantId { get; set; }
        public bool Prepopulate { get; set; }
        public Guid DataTypeId { get; set; }
        [JsonIgnore]
        public Plugin Plugin { get; set; }
        [JsonIgnore]
        public TenantModel.Tenant Tenant { get; set; }
    }
}