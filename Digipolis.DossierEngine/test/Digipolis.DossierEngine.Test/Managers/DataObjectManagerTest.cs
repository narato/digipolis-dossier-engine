﻿using Digipolis.Common.Events;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Changes;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Microsoft.Extensions.Options;
using Moq;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class DataObjectManagerTest
    {
        [Fact]
        public void GetDataObjectByIdReturnsDataObject()
        {
            var guid = new Guid();
            var dataObject = new Model.Models.DataObject();

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            var dataObjectDataProviderMock = new Mock<IDataObjectDataProvider>();
            dataObjectDataProviderMock.Setup(dodpm => dodpm.GetDataObjectByIdAsync(guid, false)).ReturnsAsync(dataObject);

            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataObject>>();

            var pluginClientMock = new Mock<IPluginClient>();

            var manager = new DataObjectManager(dataObjectDataProviderMock.Object, dataTypeClientMock.Object, pluginClientMock.Object, dataObjectValidatorMock.Object, currentTenantProviderMock.Object, eventPublisherMock.Object, changeCalculatorMock.Object, configOptionsMock.Object);

            var returnDataObject = manager.GetDataObjectByIdAsync(guid).Result;

            Assert.NotNull(returnDataObject);
            Assert.Equal(dataObject, returnDataObject);
        }

        [Fact]
        public void GetDataObjectsForDataTypeReturnsListOfDataObjects()
        {
            var guid = new Guid();

            var dataObjectList = new List<Model.Models.DataObject>();
            dataObjectList.Add(new Model.Models.DataObject());
            dataObjectList.Add(new Model.Models.DataObject());

            var dataObjectDataProviderMock = new Mock<IDataObjectDataProvider>();
            dataObjectDataProviderMock.Setup(top => top.FindDataObjectsForDataTypeIdAsync(guid, 1, 10, null, false)).ReturnsAsync(dataObjectList);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();

            var dataTypeClientMock = new Mock<IDataTypeClient>();
            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataObject>>();

            var pluginClientMock = new Mock<IPluginClient>();

            var manager = new DataObjectManager(dataObjectDataProviderMock.Object, dataTypeClientMock.Object, pluginClientMock.Object, dataObjectValidatorMock.Object, currentTenantProviderMock.Object, eventPublisherMock.Object, changeCalculatorMock.Object, configOptionsMock.Object);

            var dataObjects = manager.GetDataObjectsForDataTypeIdAsync(guid, 1, 10).Result;

            Assert.NotNull(dataObjects);
            Assert.Equal(dataObjects.Data.ToList().Count, dataObjectList.Count);
        }


        [Fact]
        public void InsertDataTypeReturnsDataTypeWithGuid()
        {
            var dataObject = new Model.Models.DataObject();
            Assert.Equal(Guid.Empty, dataObject.Id);
            var dataType = new Model.Models.DataType();
            dataType.Id = Guid.NewGuid();
            dataObject.DataTypeId = dataType.Id;

            var dataObjectDataProviderMock = new Mock<IDataObjectDataProvider>();
            dataObjectDataProviderMock.Setup(tdp => tdp.InsertDataObjectForDataTypeAsync(dataObject, dataType)).ReturnsAsync(dataObject);
            dataObjectDataProviderMock.Setup(tdp => tdp.GetUnsyncedSyncDataObjectsForDataObjectAsync(It.IsAny<Model.Models.DataObject>())).ReturnsAsync(new List<DataProvider.RawModels.RawDataObject>());
            dataObjectDataProviderMock.Setup(tdp => tdp.GetDataObjectByIdAsync(It.IsAny<Guid>(), false)).ReturnsAsync(dataObject);

            var dataObjectValidatorMock = new Mock<IDataObjectValidator>();
            dataObjectValidatorMock.Setup(val => val.ValidateDataObjectAttributes(dataType, dataObject)).Returns(new List<FeedbackItem>());

            var dataTypeClientMock = new Mock<IDataTypeClient>();
            dataTypeClientMock.Setup(dtc => dtc.GetDataTypeTreeById(It.IsAny<Guid>(), "CurrentTennantey")).ReturnsAsync(dataType);

            var currentTenantProviderMock = new Mock<ICurrentTenantProvider<Config>>();
            currentTenantProviderMock.Setup(ctp => ctp.GetCurrentTenantKey()).Returns("CurrentTennantey");

            var configOptionsMock = new Mock<IOptions<DossierConfiguration>>();
            configOptionsMock.SetupGet(x => x.Value).Returns(new DossierConfiguration() { EventHandlerNamespace = "ns", EventHandlerOwnerKey = "me" });

            var eventPublisherMock = new Mock<IEventPublisher>();

            var changeCalculatorMock = new Mock<IChangeCalculator<Model.Models.DataObject>>();

            var pluginClientMock = new Mock<IPluginClient>();

            var manager = new DataObjectManager(dataObjectDataProviderMock.Object, dataTypeClientMock.Object, pluginClientMock.Object, dataObjectValidatorMock.Object, currentTenantProviderMock.Object, eventPublisherMock.Object, changeCalculatorMock.Object, configOptionsMock.Object);

            var returnedDataObject = manager.InsertDataObjectAsync(dataObject).Result;

            Assert.NotEqual(Guid.Empty, dataObject.Id);
            Assert.Equal(dataObject, returnedDataObject);
        }
    }
}
