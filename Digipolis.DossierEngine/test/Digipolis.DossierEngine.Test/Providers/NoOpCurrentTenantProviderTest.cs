﻿using Digipolis.DossierEngine.Domain.Providers;
using Xunit;

namespace Digipolis.DossierEngine.Test.Providers
{
    public class NoOpCurrentTenantProviderTest
    {
        [Fact]
        public void GetCurrentTenantReturnsNull()
        {
            var provider = new NoOpCurrentTenantProvider();
            Assert.Null(provider.GetCurrentTenant());
        }

        [Fact]
        public void GetCurrentTenantKeyReturnsNull()
        {
            var provider = new NoOpCurrentTenantProvider();
            Assert.Null(provider.GetCurrentTenantKey());
        }
    }
}
