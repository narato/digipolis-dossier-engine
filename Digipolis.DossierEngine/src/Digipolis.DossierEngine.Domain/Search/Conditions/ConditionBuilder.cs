﻿using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using Digipolis.DossierEngine.Domain.Search.Conditions.Operators;
using System;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Search.Conditions
{
    public class ConditionBuilder : IConditionBuilder 
    {
        public ICondition BuildConditionFromJsonObject(dynamic jsonObject)
        {
            // BIG TODO : implement some anti-injection measures like remove DELETE/MERGE/CREATE

            // check whether it's a simple comparison or a composite condition
            if (IsJsonForComparison(jsonObject))
            {
                var comparison = CreateComparison(jsonObject);
                return comparison;
            }
            if (IsJsonForCompositeCondition(jsonObject))
            {
                return CreateCompositeCondition(jsonObject);
            }
            throw new Exception("Invalid query");
        }

        private bool IsJsonForCompositeCondition(dynamic jsonObject)
        {
            return (jsonObject["operator"] != null && jsonObject["conditions"] != null);
        }

        private bool IsJsonForComparison(dynamic jsonObject)
        {
            return (jsonObject["operator"] != null && jsonObject["field"] != null && jsonObject["value"] != null);
        }

        private Comparison CreateComparison(dynamic jsonObject)
        {
            // TODO : validate whether operator is valid for value type?
            // note: string </<=/>/>= string IS valid :-)
            return new Comparison(
               new Field((string)jsonObject["field"]),
               new Operator((string)jsonObject["operator"]),
               CreateValueForJsonObject(jsonObject)
           );
        }

        private CompositeCondition CreateCompositeCondition(dynamic jsonObject)
        {
            var conditions = new List<ICondition>();

            foreach(dynamic condition in jsonObject["conditions"])
            {
                conditions.Add(BuildConditionFromJsonObject(condition));
            }
            return new CompositeCondition((string)jsonObject["operator"], conditions);
        }

        private Comparable CreateValueForJsonObject(dynamic jsonObject)
        {
            // TODO : support DateTime 
            switch ((string)jsonObject.value.Type.ToString())
            {
                case "Integer":
                    return new IntValue((int)jsonObject.value);
                case "String":
                    return new StringValue((string)jsonObject.value);
                case "Float":
                    return new DoubleValue((double)jsonObject.value);
                case "Boolean":
                    return new BooleanValue((bool)jsonObject.value);
                default:
                    throw new Exception($"Unknown JObject field type: {(string)jsonObject.value.Type}");
            }
        }

    }
}
