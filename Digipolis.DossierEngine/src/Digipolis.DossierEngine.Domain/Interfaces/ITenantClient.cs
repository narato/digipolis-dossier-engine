﻿using System.Threading.Tasks;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ITenantClient
    {
        Task<Tenant<Config>> GetTenantAsync(string key);
    }
}
