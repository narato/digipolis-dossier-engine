﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Digipolis.DossierEngine.Synchronization
{
    public abstract class AbstractPlugin : IPlugin
    {
        public virtual int Version { get; set; }
        public virtual string Name { get; set; }
        public virtual IEnumerable<string> DataTypes { get; set; }
        public virtual string EventHandlerTopic { get; set; }
        public virtual string EventNamespace { get; set; }

        public abstract string GetObjectById(object id);
        protected abstract IEnumerable<object> GetAllObjects();

        // sensible default
        public virtual string ProcessMessage(Guid dataTypeId, object payload)
        {
            return MergeDataTypeIdWithJson(GetObjectById(payload), dataTypeId);
        }

        // callback will actually insert the DataObject
        public virtual void Prepopulate(Guid dataTypeId, Action<string> callback)
        {
            foreach (var obj in GetAllObjects())
            {
                var dataObjectJson = ProcessMessage(dataTypeId, obj);
                callback(dataObjectJson);
            }
        }

        protected string MergeDataTypeIdWithJson(string dataObjectJson, Guid dataTypeId)
        {
            var json = JObject.Parse(dataObjectJson);
            var attributeToMerge = JObject.Parse("{\"dataTypeId\": \"" + dataTypeId.ToString() + "\"}");

            json.Merge(attributeToMerge);
            return json.ToString();
        }
    }
}
