﻿using Digipolis.DossierEngine.DataProvider.DataProviders;
using Digipolis.DossierEngine.DataProvider.Models;
using Digipolis.DossierEngine.Model.Models;
using Microsoft.EntityFrameworkCore;
using Narato.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Digipolis.DossierEngine.Test.DataProviders
{
    public class PluginDataProviderTest : IDisposable
    {
        private AdminDbContext _adminDbContext;

        public PluginDataProviderTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AdminDbContext>();
            optionsBuilder.UseInMemoryDatabase();

            _adminDbContext = new AdminDbContext(optionsBuilder.Options);
        }

        public void Dispose()
        {
            _adminDbContext.Database.EnsureDeleted();

            _adminDbContext.Dispose();
        }

        [Fact]
        public void GetPluginsReturnsPluginList()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1" },
                new Plugin { Name = "myPlugin2" }
            };

            _adminDbContext.Plugin.AddRange(plugins);
            _adminDbContext.SaveChanges();

            var provider = new PluginDataProvider(_adminDbContext);

            var result = provider.GetPlugins();

            Assert.IsType(typeof(List<Plugin>), result);
            Assert.Equal(2, result.Count);
        }

        [Fact]
        public void GetPluginByIdReturnsPlugin()
        {
            var guid = Guid.NewGuid();
            var plugin = new Plugin { Id = guid, Name = "myPlugin1" };

            _adminDbContext.Plugin.Add(plugin);
            _adminDbContext.SaveChanges();

            var provider = new PluginDataProvider(_adminDbContext);

            var result = provider.GetPluginById(guid);

            Assert.IsType(typeof(Plugin), result);
            Assert.Equal(guid, result.Id);
        }

        [Fact]
        public void InsertPluginsReturnsAddsToPlugins()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1" },
                new Plugin { Name = "myPlugin2" }
            };

            var provider = new PluginDataProvider(_adminDbContext);

            var count = _adminDbContext.Plugin.ToList().Count;
            provider.InsertPlugins(plugins);

            Assert.True(_adminDbContext.Plugin.ToList().Count > count);
            Assert.Equal(plugins.First(f => f.Name == "myPlugin1").Name, _adminDbContext.Plugin.First(t => t.Name == "myPlugin1").Name);
        }

        [Fact]
        public void InsertTenantPluginLinkReturnsAddsToTenantPluginLinks()
        {
            var pluginId = Guid.NewGuid();
            var tenantId = Guid.NewGuid();
            var tenantPluginLink = new TenantPluginLink { PluginId = pluginId, TenantId = tenantId };

            var provider = new PluginDataProvider(_adminDbContext);

            var count = _adminDbContext.TenantPluginLink.ToList().Count;
            provider.InsertTenantPluginLink(tenantPluginLink);

            Assert.True(_adminDbContext.TenantPluginLink.ToList().Count > count);
            Assert.Equal(tenantPluginLink.PluginId, _adminDbContext.TenantPluginLink.First(t => t.PluginId == pluginId).PluginId);
        }

        [Fact]
        public void GetAllSynchDataTypesReturnAListOfDataTypes()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Name = "myPlugin1", DataType = @"[""{\""name\"": \""VerySimpleType\"",\""attributes\"": [{\""type\"": \""String\"",\""name\"": \""locatie\"",\""regexValidation\"": \""^[a-zA-Z]*$\"",\""isRequired\"": \""true\""}]}""]" },
                new Plugin { Name = "myPlugin2", DataType = @"[""{\""name\"": \""SlightlyMoreComplex\"",\""attributes\"": [{\""type\"": \""String\"",\""name\"": \""locatie\"",\""regexValidation\"": \""^[a-zA-Z]*$\"",\""isRequired\"": \""true\""}, {\""type\"": \""Int\"",\""name\"": \""aantal\"",\""minRangeValidation\"": 0,\""maxRangeValidation\"": 1000,\""isRequired\"": true}]}""]" }
                
            };

            _adminDbContext.Plugin.AddRange(plugins);
            _adminDbContext.SaveChanges();

            var provider = new PluginDataProvider(_adminDbContext);

            var dataTypes = provider.GetAllSynchDataTypes();

            Assert.Equal(dataTypes.First().First().GetType().Name, "DataType");
            Assert.Equal(dataTypes.Count, plugins.Count);
        }

        [Fact]
        public void DeleteTenantPluginLinkRemovesTenantPluginLink()
        {
            var plugin = new Plugin { Name = "myPlugin1", DataType = @"{""name"": ""VerySimpleType"",""attributes"": [{""type"": ""String"",""name"": ""locatie"",""regexValidation"": ""^[a-zA-Z]*$"",""isRequired"": ""true""}]}" };
            var tenantId = Guid.NewGuid();

            var tenantPluginLink = new TenantPluginLink { PluginId = plugin.Id, TenantId = tenantId };

            _adminDbContext.Plugin.Add(plugin);
            _adminDbContext.TenantPluginLink.Add(tenantPluginLink);
            _adminDbContext.SaveChanges();

            //Check to verify that the link was properly created
            var tenantPluginLinks = _adminDbContext.TenantPluginLink.ToList();
            Assert.Equal(tenantPluginLinks.Count, 1);

            var provider = new PluginDataProvider(_adminDbContext);

            provider.DeleteTenantPluginLink(tenantPluginLink.Id);
            tenantPluginLinks = _adminDbContext.TenantPluginLink.ToList();

            Assert.Equal(tenantPluginLinks.Count, 0);
        }

        [Fact]
        public void DeleteTenantPluginLinkThrowsWhenLinkCannotBeFoundById()
        {
            var nonExistentTenantPluginLinkId = Guid.NewGuid();
            var provider = new PluginDataProvider(_adminDbContext);

            EntityNotFoundException ex = Assert.Throws<EntityNotFoundException>(() => provider.DeleteTenantPluginLink(nonExistentTenantPluginLinkId));
            Assert.Equal($"Link with id {nonExistentTenantPluginLinkId} could not be found", ex.Message);
        }

        [Fact]
        public void GetPluginsForTenantReturnsAllPluginsForAGivenTenantKey()
        {
            var plugins = new List<Plugin>
            {
                new Plugin { Id = Guid.NewGuid(), Name = "myPlugin1", DataType = @"{""name"": ""VerySimpleType"",""attributes"": [{""type"": ""String"",""name"": ""locatie"",""regexValidation"": ""^[a-zA-Z]*$"",""isRequired"": ""true""}]}" },
                new Plugin { Id = Guid.NewGuid(), Name = "myPlugin2", DataType = @"{""name"": ""SlightlyMoreComplex"",""attributes"": [{""type"": ""String"",""name"": ""locatie"",""regexValidation"": ""^[a-zA-Z]*$"",""isRequired"": ""true""}, {""type"": ""Int"",""name"": ""aantal"",""minRangeValidation"": 0,""maxRangeValidation"": 1000,""isRequired"": true}]}" }
            };

            var tenant1 = new Digipolis.Common.Tenancy.Models.Tenant() { Id 
                = Guid.NewGuid(), Key = "TenantKey1" };
            var tenant2 = new Digipolis.Common.Tenancy.Models.Tenant() { Id = Guid.NewGuid(), Key = "TenantKey2" };
            var tenantList = new List<Digipolis.Common.Tenancy.Models.Tenant>() { tenant1, tenant2 };

            var link = new TenantPluginLink() { PluginId = plugins.First().Id, TenantId = tenant2.Id };
            var link2 = new TenantPluginLink() { PluginId = plugins.Last().Id, TenantId = tenant1.Id };

            _adminDbContext.Plugin.AddRange(plugins);
            _adminDbContext.Tenant.AddRange(tenantList);
            _adminDbContext.TenantPluginLink.Add(link);
            _adminDbContext.TenantPluginLink.Add(link2);
            _adminDbContext.SaveChanges();

            var provider = new PluginDataProvider(_adminDbContext);

            var returnedLinks = provider.GetPluginLinksForTenant(tenant1.Key).ToList();
            Assert.Equal(returnedLinks.First().Id, link2.Id);

            returnedLinks = provider.GetPluginLinksForTenant(tenant2.Key).ToList();
            Assert.Equal(returnedLinks.Last().Id, link.Id);
        }
    }
}
