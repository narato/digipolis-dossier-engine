﻿using Digipolis.DossierEngine.Common.Util;
using Xunit;

namespace Digipolis.DossierEngine.Test.Util
{
    public class StringHelperTest
    {
        [Fact]
        public void RemoveSpecialCharactersTest()
        {
            var stringHelper = new StringHelper();
            Assert.Equal("blibla", stringHelper.RemoveSpecialCharacters("bl.-)i..b.  la"));
        }

        [Fact]
        public void ConcatenateWithDelimiterTest()
        {
            var stringHelper = new StringHelper();
            Assert.Equal("blibla, blabli", stringHelper.ConcatenateWithDelimiter("blibla", "blabli"));
        }

        [Fact]
        public void ConcatenateWithDelimiterToConcatenateEmptyTest()
        {
            var stringHelper = new StringHelper();
            Assert.Equal("blibla", stringHelper.ConcatenateWithDelimiter("blibla", ""));
        }



    }
}
