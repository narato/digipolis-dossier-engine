﻿namespace Digipolis.DossierEngine.Common.Configurations
{
    public class DataTypeConfiguration
    {
        public string Neo4jUsername { get; set; }
        public string Neo4jPassword { get; set; }
    }
}