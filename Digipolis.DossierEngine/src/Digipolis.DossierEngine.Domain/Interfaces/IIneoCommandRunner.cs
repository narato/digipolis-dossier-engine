﻿using Digipolis.DossierEngine.Domain.Ssh.Interfaces;
using Renci.SshNet;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface IIneoCommandRunner
    {
        SshCommand CreateNeo4jInstance(ISshClient client, string ineoHome, int instanceHttpPort, string instanceName);

        SshCommand StartNeo4jInstance(ISshClient client, string ineoHome, string instanceName);

        SshCommand StopNeo4jInstance(ISshClient client, string ineoHome, string instanceName);

        SshCommand SetPasswordForNeo4jInstance(ISshClient client, int instanceHttpPort, string password);
    }
}
