﻿using System.Collections.Generic;

namespace Digipolis.DossierEngine.Common.Dictionaries
{
    public class DictionaryComparisonHelper<TKey, TValue>
    {
        private readonly ComparisonHelper _comparisonHelper;
        private readonly IEqualityComparer<IDictionary<TKey, TValue>> _dictionaryComparer;

        public DictionaryComparisonHelper(ComparisonHelper comparisonHelper, IEqualityComparer<IDictionary<TKey, TValue>> dictionaryComparer)
        {
            _comparisonHelper = comparisonHelper;
            _dictionaryComparer = dictionaryComparer;
        }

        public bool Equals(IEnumerable<IDictionary<TKey, TValue>> list1, IEnumerable<IDictionary<TKey, TValue>> list2)
        {
            return _comparisonHelper.Equals(list1, list2, _dictionaryComparer);
        }
    }
}
