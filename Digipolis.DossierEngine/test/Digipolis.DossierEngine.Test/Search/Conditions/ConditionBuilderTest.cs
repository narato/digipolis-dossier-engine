﻿using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using Newtonsoft.Json;
using System;
using System.Linq;
using Xunit;

namespace Digipolis.DossierEngine.Test.Search.Conditions
{
    public class ConditionBuilderTest
    {
        [Fact]
        public void BuildSimpleComparisonTest()
        {

            var conditionBuilder = new ConditionBuilder();

            var json = @"{
	            ""field"": ""Leeftijd"",
	            ""operator"": ""="",
	            ""value"": 50
            }";

            dynamic argument = JsonConvert.DeserializeObject(json);

            var condition = conditionBuilder.BuildConditionFromJsonObject(argument);

            Assert.IsType(typeof(Comparison), condition);

            var comparison = condition as Comparison;

            Assert.IsType(typeof(Field), comparison.LeftHandSide);
            Assert.IsType(typeof(IntValue), comparison.RightHandSide);

            Assert.Equal("Leeftijd", ((Field)comparison.LeftHandSide).Name);
            Assert.Equal(50, ((IntValue)comparison.RightHandSide).Value);
        }

        [Fact]
        public void BuildSimpleRootComparisonTest()
        {

            var conditionBuilder = new ConditionBuilder();

            var json = @"{
	            ""field"": ""IsLelijk"",
	            ""operator"": ""="",
	            ""value"": true
            }";

            dynamic argument = JsonConvert.DeserializeObject(json);

            var condition = conditionBuilder.BuildConditionFromJsonObject(argument);

            Assert.IsType(typeof(Comparison), condition); // this used to be a CompositeCondition, but we splitted search queries in "simple" queries and "advanced queries"

            var comparison = condition as Comparison;

            Assert.IsType(typeof(Field), comparison.LeftHandSide);
            Assert.IsType(typeof(BooleanValue), comparison.RightHandSide);

            Assert.Equal("IsLelijk", ((Field)comparison.LeftHandSide).Name);
            Assert.Equal(true, ((BooleanValue)comparison.RightHandSide).Value);
        }

        [Fact]
        public void BuildSimpleCompositeConditionTest()
        {

            var conditionBuilder = new ConditionBuilder();

            var json = @"{
	            ""operator"": ""AND"",
	            ""conditions"": [
		            {
			            ""field"": ""locatie"",
			            ""operator"": ""="",
			            ""value"": ""Antwerpen""
		            },
		            {
			            ""field"": ""bedrag"",
			            ""operator"": ""="",
			            ""value"": 15.5
		            },
                    {
			            ""field"": ""betaald"",
			            ""operator"": ""="",
			            ""value"": true
		            }
	            ]
            }";

            dynamic argument = JsonConvert.DeserializeObject(json);

            var condition = conditionBuilder.BuildConditionFromJsonObject(argument);

            Assert.IsType(typeof(CompositeCondition), condition);

            var compositeCondtion = condition as CompositeCondition;

            Assert.Equal(CompositeCondition.TYPE_AND, compositeCondtion.Operator);
            Assert.Equal(3, compositeCondtion.Conditions.Count);

            Assert.IsType(typeof(Comparison), compositeCondtion.Conditions.First());

            var comparison = compositeCondtion.Conditions.First() as Comparison;

            Assert.IsType(typeof(Field), comparison.LeftHandSide);
            Assert.IsType(typeof(StringValue), comparison.RightHandSide);

            Assert.Equal("locatie", ((Field)comparison.LeftHandSide).Name);
            Assert.Equal("Antwerpen", ((StringValue)comparison.RightHandSide).Value);
        }

        [Fact]
        public void TestInvalidQueryShouldThrowException()
        {

            var conditionBuilder = new ConditionBuilder();

            var json = @"{
	            ""idontexist"": ""Leeftijd"",
	            ""neitherdoi"": ""="",
	            ""whassup guys"": 50
            }";

            dynamic argument = JsonConvert.DeserializeObject(json);

            var ex = Assert.Throws<Exception>(() => conditionBuilder.BuildConditionFromJsonObject(argument));

            Assert.Equal("Invalid query", ex.Message);
        }
    }
}
