﻿using Digipolis.DossierEngine.DataProvider.Models;
using System.Collections.Generic;

namespace Digipolis.DossierEngine.DataProvider.RawModels
{
    public class RawChangeSet
    {
        public NeoChangeSet ChangeSet { get; set; }
        public IEnumerable<NeoChangeSetEntry> Changes { get; set; }
    }
}
