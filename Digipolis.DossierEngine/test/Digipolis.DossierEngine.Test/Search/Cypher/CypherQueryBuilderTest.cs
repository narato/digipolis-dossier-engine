﻿using Digipolis.DossierEngine.Domain.Search.Conditions;
using Digipolis.DossierEngine.Domain.Search.Conditions.Fields;
using Digipolis.DossierEngine.Domain.Search.Conditions.Operators;
using Digipolis.DossierEngine.Domain.Search.Cypher;
using System.Collections.Generic;
using Moq;
using Xunit;
using System;

namespace Digipolis.DossierEngine.Test.Search.Cypher
{
    public class CypherQueryBuilderTest
    {
        private Comparison CreateComparison(string postfix, string type = "string", string op = "=")
        {
            Comparable value;
            switch (type) {
                case "int":
                    value = new IntValue(1);
                    break;
                case "double":
                    value = new DoubleValue(1.5);
                    break;
                case "bool":
                    value = new BooleanValue(true);
                    break;
                default:
                    value = new StringValue("value" + postfix);
                    break;
            }
            return new Comparison(
               new Field("veld" + postfix),
               new Operator(op),
               value
           );
        }

        private CompositeCondition CreateCompositeCondition(string op = CompositeCondition.TYPE_AND)
        {
            var conditions = new List<ICondition>();
            conditions.Add(CreateComparison("1"));
            conditions.Add(CreateComparison("2"));
            return new CompositeCondition(op, conditions);
        }

        [Fact]
        public void TestWalkSimpleEqualComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "string"));

            Assert.Equal(cypherQueryBuilder.WalkComparison(CreateComparison("1", "string"), false), query); // sort of behaviour test
            Assert.Equal("(av.value = \"value1\")", query);
        }

        [Fact]
        public void TestWalkSimpleNonEqualComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "string", "<>"));

            Assert.Equal("(av.value <> \"value1\")", query);
        }

        [Fact]
        public void TestWalkSimpleLessThanComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "int", "<"));

            Assert.Equal("(av.value < 1)", query);
        }

        [Fact]
        public void TestWalkSimpleLessThanOrEqualComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "int", "<="));

            Assert.Equal("(av.value <= 1)", query);
        }

        [Fact]
        public void TestWalkSimpleGreaterThanComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "int", ">"));

            Assert.Equal("(av.value > 1)", query);
        }

        [Fact]
        public void TestWalkSimpleGreaterThanOrEqualComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "int", ">="));

            Assert.Equal("(av.value >= 1)", query);
        }

        [Fact]
        public void TestWalkSimpleContainsComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "string", "CONTAINS"));

            Assert.Equal("(av.value =~ \".*value1.*\")", query);
        }

        [Fact]
        public void TestWalkSimpleIntegerComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "int"));

            Assert.Equal("(av.value = 1)", query);
        }

        [Fact]
        public void TestWalkSimpleDoubleComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "double"));

            Assert.Equal("(av.value = 1.5)", query);
        }

        [Fact]
        public void TestWalkSimpleBooleanComparison()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateComparison("1", "bool"));

            Assert.Equal("(av.value = True)", query);
        }

        [Fact]
        public void TestWalkNonNestedAndCompositeCondition()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateCompositeCondition());

            Assert.Equal(cypherQueryBuilder.WalkCompositeCondition(CreateCompositeCondition()), query); // sort of behaviour test
            Assert.Equal("(SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) = 2 AND 1 = 1)", query);
        }

        [Fact]
        public void TestWalkNonNestedOrCompositeCondition()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var query = cypherQueryBuilder.Visit(CreateCompositeCondition(CompositeCondition.TYPE_OR));

            Assert.Equal(cypherQueryBuilder.WalkCompositeCondition(CreateCompositeCondition(CompositeCondition.TYPE_OR)), query); // sort of behaviour test
            Assert.Equal("(SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) > 0 OR 1 = 0)", query);
        }

        [Fact]
        public void TestWalkNestedAndCompositeCondition()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var condition = CreateCompositeCondition();
            condition.Conditions.Add(CreateCompositeCondition());

            var query = cypherQueryBuilder.Visit(condition);

            Assert.Equal(cypherQueryBuilder.WalkCompositeCondition(condition), query); // sort of behaviour test
            Assert.Equal("(SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) = 2 AND 1 = 1 AND (SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) = 2 AND 1 = 1))", query);
        }

        [Fact]
        public void TestWalkNestedOrCompositeCondition()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var condition = CreateCompositeCondition(CompositeCondition.TYPE_OR);
            condition.Conditions.Add(CreateCompositeCondition(CompositeCondition.TYPE_OR));

            var query = cypherQueryBuilder.Visit(condition);

            Assert.Equal(cypherQueryBuilder.WalkCompositeCondition(condition), query); // sort of behaviour test
            Assert.Equal("(SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) > 0 OR 1 = 0 OR (SIZE(FILTER(x in typeValuePairList WHERE (x.type = \"veld1\" AND x.value = \"value1\") OR (x.type = \"veld2\" AND x.value = \"value2\"))) > 0 OR 1 = 0))", query);
        }

        [Fact]
        public void TestWrongOperator()
        {
            var cypherQueryBuilder = new CypherQueryBuilder();

            var operatorMock = new Mock<IOperator>();
            operatorMock.SetupGet(om => om.Value).Returns("Idontexist");

            var comparison = CreateComparison("1");
            var comparisonMock = new Mock<IComparison>();
            comparisonMock.SetupGet(cm => cm.LeftHandSide).Returns(comparison.LeftHandSide);
            comparisonMock.SetupGet(cm => cm.RightHandSide).Returns(comparison.RightHandSide);
            comparisonMock.SetupGet(cm => cm.Operator).Returns(operatorMock.Object);

            var ex = Assert.Throws<Exception>(() => cypherQueryBuilder.WalkComparison(comparisonMock.Object, true));


            Assert.Equal("Unknown operator: Idontexist", ex.Message);
        }
    }
}
