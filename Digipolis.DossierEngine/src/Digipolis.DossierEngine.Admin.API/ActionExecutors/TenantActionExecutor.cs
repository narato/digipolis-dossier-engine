﻿using System;
using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.Tenant.Library.ActionExecutors;
using Digipolis.Tenant.Library.Managers;
using Microsoft.Extensions.Options;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Domain.Interfaces;
using Digipolis.Common.Events;
using NLog;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Admin.API.ActionExecutors
{
    public class TenantActionExecutor : IActionExecutor<Config>
    {
        private readonly ITenantManager<Config> _tenantManager;
        private readonly ISshClientProvider _sshClientProvider;
        private readonly IIneoCommandRunner _ineoCommandRunner;
        private readonly DossierConfiguration _dossierConfiguration;
        private readonly INeoMigrationManager _neoMigrationManager;
        private readonly IEventHandlerAdministrator _eventHandlerAdministrator;
        private readonly IEventPublisher _eventHandler;

        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public TenantActionExecutor(ITenantManager<Config> tenantManager, ISshClientProvider sshClientProvider, IIneoCommandRunner ineoCommandRunner, IOptions<DossierConfiguration> adminConfiguration, INeoMigrationManager neoMigrationManager, IEventHandlerAdministrator eventHandlerAdministrator, IEventPublisher eventHandler)
        {
            _tenantManager = tenantManager;
            _neoMigrationManager = neoMigrationManager;
            _sshClientProvider = sshClientProvider;
            _ineoCommandRunner = ineoCommandRunner;
            _dossierConfiguration = adminConfiguration.Value;
            _eventHandlerAdministrator = eventHandlerAdministrator;
            _eventHandler = eventHandler;
        }

        public async Task BeforeTenantCreateAsync(Tenant<Config> tenant)
        {
            tenant.Config = new Config();

            //Ports for a tenant are unique across all servers. This way we won't have port conflicts when moving a tenant to another server
            var lastInsertedTenant = await _tenantManager.FindLastInsertedTenantAsync();
            if (lastInsertedTenant == null)
            {
                tenant.Config.HttpPort = _dossierConfiguration.DefaultHttpPort;
                tenant.Config.HttpsPort = _dossierConfiguration.DefaultHttpsPort;
                tenant.Config.BoltPort = _dossierConfiguration.DefaultBoltPort;
            }
            else
            {
                var lastPort = lastInsertedTenant.Config.BoltPort;
                tenant.Config.HttpPort = lastPort + 1;
                tenant.Config.HttpsPort = lastPort + 2;
                tenant.Config.BoltPort = lastPort + 3;
            }
            tenant.Config.Neo4JServer = _dossierConfiguration.ActiveNeo4JServer;
            tenant.Config.SshPort = _dossierConfiguration.SshPort;
            tenant.Config.SshUsername = _dossierConfiguration.SshUsername;
            tenant.Config.SshPassword = _dossierConfiguration.SshPassword;
        }

        public Task OnTenantCreatedAsync(Tenant<Config> tenant)
        {
            var tenantKey = tenant.Key;
            var portHttp = tenant.Config.HttpPort;

            using (var client = _sshClientProvider.GetSshClient(tenant.Config.Neo4JServer,
                                              tenant.Config.SshPort,
                                              tenant.Config.SshUsername,
                                              tenant.Config.SshPassword))
            {
                client.Connect();

                var output = _ineoCommandRunner.CreateNeo4jInstance(client, _dossierConfiguration.IneoHome, portHttp, tenantKey);
                if (!string.IsNullOrEmpty(output?.Error))
                {
                    throw new Exception(output.Error);
                }

                var output2 = _ineoCommandRunner.StartNeo4jInstance(client, _dossierConfiguration.IneoHome, tenantKey);
                if (!string.IsNullOrEmpty(output2?.Error))
                    throw new Exception(output2.Error);

                var output3 = _ineoCommandRunner.SetPasswordForNeo4jInstance(client, portHttp, _dossierConfiguration.Neo4jPassword);
                if (!string.IsNullOrEmpty(output3?.Error))
                    throw new Exception(output3.Error);

                client.Disconnect();
            }

            _neoMigrationManager.SetupDatabase(tenant);
            EnsureEventHandlerTopicsExist(tenant);

            _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.TENANT_CREATED_TOPIC, new { Id = tenant.Id, Name = tenant.Name });

            return Task.CompletedTask;
        }

        private void EnsureEventHandlerTopicsExist(Tenant<Config> tenant)
        {
            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataObjectCreatedTopic(tenant.Key));
            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataObjectUpdatedTopic(tenant.Key));
            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataObjectDeletedTopic(tenant.Key));

            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataTypeCreatedTopic(tenant.Key));
            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataTypeUpdatedTopic(tenant.Key));
            _eventHandlerAdministrator.EnsureTopicExists(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.DataTypeDeletedTopic(tenant.Key));
        }

        public Task OnTenantDeletedAsync(Tenant<Config> tenant)
        {
            var tenantKey = tenant.Key;
            var portHttp = tenant.Config.HttpPort;

            using (var client = _sshClientProvider.GetSshClient(tenant.Config.Neo4JServer,
                                              tenant.Config.SshPort,
                                              tenant.Config.SshUsername,
                                              tenant.Config.SshPassword))
            {
                client.Connect();

                var output2 = _ineoCommandRunner.StopNeo4jInstance(client, _dossierConfiguration.IneoHome, tenantKey);
                if (!string.IsNullOrEmpty(output2?.Error))
                    throw new Exception(output2.Error);

                client.Disconnect();
            }

            _eventHandler.Publish(_dossierConfiguration.EventHandlerOwnerKey, _dossierConfiguration.EventHandlerNamespace, Constants.TENANT_DELETED_TOPIC, new { Id = tenant.Id, Name = tenant.Name });
            
            return Task.CompletedTask;
        }
    }
}
