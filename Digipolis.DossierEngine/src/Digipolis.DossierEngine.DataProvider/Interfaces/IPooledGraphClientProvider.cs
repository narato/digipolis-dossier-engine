﻿using Digipolis.DossierEngine.Common.Pooling.Interfaces;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataProvider.Interfaces
{
    public interface IPooledGraphClientProvider
    {
        Task<ILimitedPoolItem<IGraphClient>> GetAsync();
    }
}
