﻿using Digipolis.Common.Status.Interfaces;
using Digipolis.Common.Status.Models;
using Narato.Common.Exceptions;
using System;
using System.Linq;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.Common.Tenancy.Providers;
using Digipolis.DossierEngine.Common.Models;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Managers
{
    public class SystemStatusDataObjectManager : ISystemStatusManager
    {
        private readonly IDataObjectDataProvider _dataObjectDataProvider;
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;
        private readonly SystemConfiguration _systemConfig;

        public SystemStatusDataObjectManager(IOptions<SystemConfiguration> systemConfig, ICurrentTenantProvider<Config> currentTenantProvider, IDataObjectDataProvider dataObjectDataProvider)
        {
            _dataObjectDataProvider = dataObjectDataProvider;
            _currentTenantProvider = currentTenantProvider;
            _systemConfig = systemConfig.Value;
        }

        public async Task<SystemStatus> GetStatusAsync()
        {
            var systemStatus = new SystemStatus(Guid.NewGuid(), _systemConfig.Name, _systemConfig.Description, _systemConfig.Version, _systemConfig.Environment, _systemConfig.BuiltOn) { Up = true };
            var tenantKey = _currentTenantProvider.GetCurrentTenantKey();

            try
            {
                
                var dbResult = await _dataObjectDataProvider.CheckDBConnectionAsync();
                if (!dbResult.ConnectionOk)
                {
                    systemStatus.Up = false;
                    systemStatus.Components.Add(
                            new Component()
                            {
                                Name = "Dossier Engine DataObject tenant database",
                                Description = $"The database used for tenant '{tenantKey}'",
                                Up = false,
                                MoreInfo = $"The DataObject database is down!"
                            });
                }
            }
            catch (AggregateException ex)
            {
                systemStatus.Up = false;
                systemStatus.Components.Add(
                    new Component()
                    {
                        Name = "Dossier Engine Admin api",
                        Description = $"The api used to retrieve the tenant config for tenantkey '{tenantKey}'",
                        Up = false,
                        MoreInfo = $"The admin API is down!"
                    });
            }
            catch (ExceptionWithFeedback ex)
            {
                systemStatus.Up = false;
                systemStatus.Components.Add(
                    new Component()
                    {
                        Name = "Dossier Engine Admin api",
                        Description = $"The api used to retrieve the tenant config for tenantkey '{tenantKey}'",
                        Up = false,
                        MoreInfo = $"{string.Join(Environment.NewLine, ex.Feedback.Select(x => x.Description))}"
                    });
            }
            catch (UnauthorizedAccessException ex)
            {
                systemStatus.Up = false;
                systemStatus.Components.Add(
                    new Component()
                    {
                        Name = "Dossier Engine Admin api",
                        Description = $"The api used to retrieve the tenant config for tenantkey '{tenantKey}'",
                        Up = false,
                        MoreInfo = $"Retrieveing the tenant for tenantkey '{tenantKey}' failed with an unauthorized acces exception. Did you use the correct tenantkey?"
                    });
            }

            return systemStatus;
        }

        public SystemStatus GetStatus()
        {
            throw new NotImplementedException();
        }
    }
}
