﻿using Digipolis.Common.Tenancy.MappedModels;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Digipolis.DossierEngine.Domain.Managers;
using Moq;
using Xunit;

namespace Digipolis.DossierEngine.Test.Managers
{
    public class NeoMigrationManagerTest
    {
        [Fact]
        public void SetupDatabaseOnTenantCreated()
        {
            var tenant = new Tenant<Config>()
            {
                Key = "key",
                Config = new Config()
                {
                    Neo4JServer = "server",
                    SshPort = 22,
                    SshUsername = "user",
                    SshPassword = "pass",
                    HttpPort = 7474
                }
            };

            var neoMigrationDataProviderMock = new Mock<INeoMigrationDataProvider>();
            neoMigrationDataProviderMock.Setup(x => x.SetupDatabase(tenant.Config));

            var neoMigrationManager = new NeoMigrationManager(neoMigrationDataProviderMock.Object);

            neoMigrationManager.SetupDatabase(tenant);

            neoMigrationDataProviderMock.Verify(x => x.SetupDatabase(tenant.Config), Times.Once);
        }
    }
}