﻿using Digipolis.DossierEngine.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Narato.Common;
using Narato.Common.Factory;
using Narato.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.DataType.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/synceddatatypes/")]
    public class SyncedDataTypeController : Controller
    {
        private IDataTypeManager _dataTypeManager;
        private IResponseFactory _responseFactory;

        public SyncedDataTypeController(IDataTypeManager dataTypeManager, IResponseFactory responseFactory)
        {
            _dataTypeManager = dataTypeManager;
            _responseFactory = responseFactory;
        }

        [HttpPost]
        public async Task<IActionResult> InsertDataTypeAsync([FromBody] [Required] Model.Models.DataType dataType, [Required][FromHeader(Name = "Tenant-Key")]string tenantKey)
        {
            var missingParams = new List<MissingParam>();
            if (string.IsNullOrEmpty(tenantKey))
                missingParams.Add(new MissingParam("tenantKey", MissingParamType.QuerystringParam));
            if (dataType == null)
                missingParams.Add(new MissingParam("datatype", MissingParamType.Body));

            if (missingParams.Any())
                return _responseFactory.CreateMissingParam(missingParams);

            Func<Model.Models.DataType, object> pathMapper = (data) => { return new { controller = "DataType", id = dataType.Id, tenantKey = tenantKey }; };

            return await _responseFactory.CreatePostResponseAsync(async () => await _dataTypeManager.InsertDataTypeAsync(dataType), this.GetRequestUri(),
                        "GetDataTypeById", pathMapper);
        }
    }
}
