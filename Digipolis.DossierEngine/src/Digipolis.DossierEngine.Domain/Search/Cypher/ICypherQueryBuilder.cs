﻿using Digipolis.DossierEngine.Domain.Search.Conditions.Visitor;

namespace Digipolis.DossierEngine.Domain.Search.Cypher
{
    public interface ICypherQueryBuilder : IConditionVisitor
    {
    }
}
