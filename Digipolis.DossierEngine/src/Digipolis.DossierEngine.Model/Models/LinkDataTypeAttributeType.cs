﻿using System;

namespace Digipolis.DossierEngine.Model.Models
{
    public class LinkDataTypeAttributeType : BaseAttributeType
    {
        public const string TypeName = "LinkDataType";

        public LinkDataTypeAttributeType()
        {
            Type = TypeName;
        }

        public override string Type { get; }
        public Guid DataTypeId { get; set; }
        public DataType DataType { get; set; } // used for building DataType Tree
    }
}