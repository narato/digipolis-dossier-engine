﻿using Digipolis.DossierEngine.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Digipolis.DossierEngine.Domain.ReplaceableTags
{
    public class TagReplacer : ITagReplacer
    {
        private const string BEGIN_TAG = "{{";
        private const string END_TAG = "}}";

        public string replace(string subject, IDictionary<string, object> context)
        {
            var matches = Regex.Matches(subject, BEGIN_TAG + @"[A-z\.]*" + END_TAG);
            foreach (var match in matches)
            {
                var innerText = match.ToString().Substring(BEGIN_TAG.Length, match.ToString().Length - (BEGIN_TAG.Length + END_TAG.Length));
                var replacementText = "";
                if (innerText.Contains('.')) // replace with property of object
                {
                    var segments = innerText.Split('.');
                    var obj = context[segments[0]];
                    if (obj == null)
                    {
                        throw new Exception("trying to replace following tag: " + match.ToString() + ", but " + segments[0] + " doesn't exist in the context");
                    }
                    
                    foreach(var segment in segments.Skip(1)) // skip first, because we already handled that one above
                    {
                        try
                        {
                            obj = obj.GetType().GetProperty(segment).GetValue(obj, null);
                        } catch (Exception ex)
                        {
                            throw new Exception("trying to replace following tag: " + match.ToString() + ", but property" + segment + " doesn't exist", ex);
                        }
                    }
                    replacementText = obj.ToString();

                } else // replace with object itself
                {
                    var obj = context[innerText];
                    if (obj == null)
                    {
                        throw new Exception("trying to replace following tag: " + match.ToString() + ", but " + innerText + " doesn't exist in the context");
                    }
                    replacementText = obj.ToString();
                }
                subject = subject.Replace(match.ToString(), replacementText);
            }
            return subject;
        }
    }
}
