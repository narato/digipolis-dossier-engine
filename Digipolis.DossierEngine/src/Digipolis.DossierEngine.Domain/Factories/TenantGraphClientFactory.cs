﻿using Neo4jClient;
using System;
using Neo4jClient.Execution;
using Microsoft.Extensions.Options;
using Digipolis.DossierEngine.Common.Configurations;
using Digipolis.DossierEngine.Common.Models;
using Digipolis.DossierEngine.DataProvider.Interfaces;
using Newtonsoft.Json.Serialization;
using Digipolis.Common.Tenancy.Providers;
using System.Threading.Tasks;

namespace Digipolis.DossierEngine.Domain.Factories
{
    public class TenantGraphClientFactory : ITenantGraphClientFactory
    {
        private readonly ICurrentTenantProvider<Config> _currentTenantProvider;
        private DossierConfiguration _dossierConfiguration;

        public TenantGraphClientFactory(ICurrentTenantProvider<Config> currentTenantProvider, IOptions<DossierConfiguration> dossierConfiguration)
        {
            _currentTenantProvider = currentTenantProvider;
            _dossierConfiguration = dossierConfiguration.Value;
        }

        public async Task<IGraphClient> CreateAsync()
        {
            var currentTenant = await _currentTenantProvider.GetCurrentTenantAsync();
            return CreateForConfig(currentTenant.Config);
        }

        public IGraphClient Create()
        {
            var currentTenant = _currentTenantProvider.GetCurrentTenant();
            return CreateForConfig(currentTenant.Config);
        }

        public IGraphClient CreateForConfig(Config tenantConfig)
        {
            var client = new GraphClient(new Uri($"http://{tenantConfig.Neo4JServer}:{tenantConfig.HttpPort}/db/data"),
                                                    _dossierConfiguration.Neo4jUsername,
                                                    _dossierConfiguration.Neo4jPassword)
            {
                JsonContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return client;
        }

        public IGraphClient Create(IHttpClient client)
        {
            // implement when we need it? :)
            throw new NotImplementedException();
        }
    }
}
