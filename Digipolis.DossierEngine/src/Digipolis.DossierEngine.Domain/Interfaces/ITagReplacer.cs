﻿using System.Collections.Generic;

namespace Digipolis.DossierEngine.Domain.Interfaces
{
    public interface ITagReplacer
    {
        string replace(string subject, IDictionary<string, object> context);
    }
}
